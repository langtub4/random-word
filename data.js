var dataE1 = [
    {
        "english": "opinion",
        "type": "",
        "vietnamese": "ý kiến"
    },
    {
        "english": "positive",
        "type": "adj",
        "vietnamese": "tích cực (the positive way theo hướng tích cực)"
    },
    {
        "english": "straight",
        "type": "adv",
        "vietnamese": "thẳng "
    },
    {
        "english": "pharmacy",
        "type": "n",
        "vietnamese": "hiệu thuốc"
    },
    {
        "english": "complain",
        "type": "",
        "vietnamese": "phàn nàn"
    },
    {
        "english": "straw",
        "type": "",
        "vietnamese": "ống hút"
    },
    {
        "english": "argument",
        "type": "",
        "vietnamese": "lí lẽ, lý do, sự cãi vã"
    },
    {
        "english": "helmet",
        "type": "",
        "vietnamese": "mũ bảo hiểm"
    },
    {
        "english": "intersection",
        "type": "",
        "vietnamese": "ngã giao nhau"
    },
    {
        "english": "roundabout",
        "type": "",
        "vietnamese": "vòng xuyến"
    },
    {
        "english": "pavement",
        "type": "",
        "vietnamese": "vỉa hè"
    },
    {
        "english": "street vending",
        "type": "",
        "vietnamese": "hàng rong"
    },
    {
        "english": "essential",
        "type": "",
        "vietnamese": "thiết yếu"
    },
    {
        "english": "pedestrian",
        "type": "",
        "vietnamese": "người đi bộ"
    },
    {
        "english": "stadium",
        "type": "",
        "vietnamese": "sân vận động"
    },
    {
        "english": "heels",
        "type": "",
        "vietnamese": "đôi giày cao gót"
    },
    {
        "english": "career",
        "type": "",
        "vietnamese": "sự nghiệp"
    },
    {
        "english": "ashamed",
        "type": "",
        "vietnamese": "hổ thẹn, xấu hổ"
    },
    {
        "english": "challenge",
        "type": "",
        "vietnamese": "thách thức"
    },
    {
        "english": "integral",
        "type": "adj",
        "vietnamese": "không thể thiếu"
    },
    {
        "english": "quality",
        "type": "n",
        "vietnamese": "chất lượng"
    },
    {
        "english": "inspire",
        "type": "",
        "vietnamese": "chuyển cảm hứng"
    },
    {
        "english": "especially",
        "type": "adv",
        "vietnamese": "một cách đặc biệt"
    },
    {
        "english": "true self",
        "type": "",
        "vietnamese": "con người thật"
    },
    {
        "english": "generally",
        "type": "",
        "vietnamese": "một cách chung chung, nói chung chung"
    },
    {
        "english": "suburb",
        "type": "",
        "vietnamese": "ngoại ô"
    },
    {
        "english": "grocery",
        "type": "n",
        "vietnamese": "cửa hàng tiện lợi - (a grocer's store or business) - grocery stores."
    },
  {
    "english": "annoy",
    "type": "v",
    "pronounce": "ə'nɔi",
    "vietnamese": "chọc tức, làm bực mình, làm phiền, quẫy nhiễu"
  },
    
  {
    "english": "disturbing",
    "type": "adj",
    "pronounce": "dis ́tə:biη",
    "vietnamese": "xáo trộn"
  },
    {
        "english": "pursue",
        "type": "",
        "vietnamese": "theo đuổi"
    },
    {
        "english": "original",
        "type": "adj",
        "vietnamese": "nguyên bản, ban đầu"
    },
    {
        "english": "basically",
        "type": "",
        "vietnamese": "về cơ bản"
    },
    {
        "english": "mature",
        "type": "",
        "vietnamese": "trưởng thành - (mature person)"
    },
    {
        "english": "responsibility",
        "type": "",
        "vietnamese": "trách nhiệm (responsibility to the society)"
    },
    {
        "english": "instruction",
        "type": "n",
        "vietnamese": "chỉ dẫn"
    },
    {
        "english": "spacious",
        "type": "",
        "vietnamese": "rộng rãi"
    },
    {
        "english": "compared to",
        "type": "",
        "vietnamese": "so với"
    },
    {
        "english": "probably",
        "type": "",
        "vietnamese": "có lẽ"
    },
    {
        "english": "addictive",
        "type": "",
        "vietnamese": "gây nghiện"
    },
    {
        "english": "discuss",
        "type": "v",
        "vietnamese": "bàn luận"
    },
    {
        "english": "personally",
        "type": "adv",
        "vietnamese": "cá nhân"
    },
    {
        "english": "corner",
        "type": "",
        "vietnamese": "ngóc ngách"
    },
    {
        "english": "absorb",
        "type": "",
        "vietnamese": "hấp thụ"
    },
    {
        "english": "suitable",
        "type": "",
        "vietnamese": "phù hợp"
    },
    {
        "english": "endearing",
        "type": "",
        "vietnamese": "dễ thương"
    },
    {
        "english": "honor",
        "type": "",
        "vietnamese": "vinh dự"
    },
    {
        "english": "combine",
        "type": "",
        "vietnamese": "kết hợp"
    },
    {
        "english": "process",
        "type": "",
        "vietnamese": "quá trình"
    },
    {
        "english": "handy",
        "type": "adj",
        "vietnamese": "tiện dụng"
    },
    {
        "english": "grocery",
        "type": "",
        "vietnamese": "cửa hàng tiện lợi"
    },
    {
        "english": "figure out",
        "type": "",
        "vietnamese": "tìm ra"
    },
    {
        "english": "public places",
        "type": "",
        "vietnamese": "công cộng"
    },
    {
        "english": "complexity",
        "type": "",
        "vietnamese": "phức tạp"
    },
    {
        "english": "elementary",
        "type": "",
        "vietnamese": "tiểu học"
    },
    {
        "english": "tranquil",
        "type": "",
        "vietnamese": "bình yên"
    },
    {
        "english": "tiring day",
        "type": "",
        "vietnamese": "mệt mỏi"
    },
    {
        "english": "aware",
        "type": "",
        "vietnamese": "nhận thức"
    },
    {
        "english": "shopping center = apartment store",
        "type": "",
        "vietnamese": ""
    },
    {
        "english": "recall",
        "type": "",
        "vietnamese": "gọi lại"
    },
    {
        "english": "relate",
        "type": "v",
        "vietnamese": "liên quan"
    },
    {
        "english": "audience",
        "type": "v",
        "vietnamese": "khán giả"
    },
    {
        "english": "absolutely",
        "type": "adv",
        "vietnamese": "hoàn toàn, chắc chắn"
    },
    {
        "english": "whole",
        "type": "",
        "vietnamese": "toàn bộ (whole + something + ...)"
    },
    {
        "english": "especially",
        "type": "",
        "vietnamese": "đặc biệt"
    },
    {
        "english": "conversation",
        "type": "",
        "vietnamese": "cuộc trò chuyện"
    },
    {
        "english": "blood",
        "type": "",
        "vietnamese": "máu"
    },
    {
        "english": "affection/affectionate",
        "type": "n/adj",
        "vietnamese": "dễ mến"
    },
    {
        "english": "uptight",
        "type": "adj",
        "vietnamese": "căng thẳng "
    },
    {
        "english": "hug",
        "type": "v",
        "vietnamese": "ôm"
    },
    {
        "english": "illegal",
        "type": "adj",
        "vietnamese": "bất hợp pháp"
    },
    {
        "english": "domestic",
        "type": "n/adj",
        "vietnamese": "nội địa/ gia đình, gia tộc, quốc nội"
    },
    {
        "english": "officially",
        "type": "adv",
        "vietnamese": "một cách chính thức"
    },
    {
        "english": "announce",
        "type": "v",
        "vietnamese": "thông báo"
    },
    {
        "english": "decoration",
        "type": "n",
        "vietnamese": "trang trí"
    },
    {
        "english": "similar",
        "type": "adj",
        "vietnamese": "tương tự"
    },
    {
        "english": "tradition",
        "type": "n",
        "vietnamese": "truyền thống"
    },
    {
        "english": "rest",
        "type": "v",
        "vietnamese": "nghỉ ngơi"
    },
    {
        "english": "concept",
        "type": "n",
        "vietnamese": "ý tưởng, khái niệm"
    },
    {
        "english": "definitely",
        "type": "adv",
        "vietnamese": "chắc chắn"
    },
    {
        "english": "assumption",
        "type": "n",
        "vietnamese": "giả thiết"
    },
    {
        "english": "achieve",
        "type": "v",
        "vietnamese": "đạt được"
    },
    {
        "english": "invincible",
        "type": "adj",
        "vietnamese": "bất khả chiến bại"
    },
    {
        "english": "fit in",
        "type": "v",
        "vietnamese": "hòa nhập với, thích hợp với"
    },
    {
        "english": "competitive/competition/competing",
        "type": "adj/n/v",
        "vietnamese": "thi đấu/cuộc thi/ thi đấu"
    },
    {
        "english": "intelligent",
        "type": "adj",
        "vietnamese": "thông minh"
    },
    {
        "english": "invented",
        "type": "v",
        "vietnamese": "phát minh"
    },
    {
        "english": "motivation",
        "type": "n",
        "vietnamese": "động lực"
    },
    {
        "english": "imagine",
        "type": "v",
        "vietnamese": "tưởng tượng"
    },
    {
        "english": "survey",
        "type": "v",
        "vietnamese": "khảo sát"
    },
    {
        "english": "aspirations",
        "type": "n",
        "vietnamese": "nguyện vọng"
    },
    {
        "english": "aspirations",
        "type": "n",
        "vietnamese": "hoài bão"
    },
    {
        "english": "patient/patience",
        "type": "v/n",
        "vietnamese": "kiên nhẫn/tính kiên nhẫn"
    },
    {
        "english": "academy/academic",
        "type": "n/adj",
        "vietnamese": "học viện/ học thuật"
    },
    {
        "english": "overuse",
        "type": "adj",
        "vietnamese": "lạm dụng(lạm dụng 1 cái gì đó)"
    },
    {
        "english": "lack of",
        "type": "pre",
        "vietnamese": "thiếu (lack of time, lack of support, lack of energy)"
    },
    {
        "english": "balance",
        "type": "n/v",
        "vietnamese": "cân bằng"
    },
    {
        "english": "unique",
        "type": "adj",
        "vietnamese": "độc nhất"
    },
    {
        "english": "reasonable",
        "type": "adj",
        "vietnamese": "hợp lý"
    },
    {
        "english": "inspire",
        "type": "v",
        "vietnamese": "truyền cảm hứng"
    },
    {
        "english": "eager",
        "type": "adj",
        "vietnamese": "háo hức"
    },
    {
        "english": "curious",
        "type": "adj",
        "vietnamese": "tò mò"
    },
    {
        "english": "reveal",
        "type": "v",
        "vietnamese": "tiết lộ"
    },
    {
        "english": "internalize",
        "type": "v",
        "vietnamese": "tiếp thu"
    },
    {
        "english": "involved in",
        "type": "v",
        "vietnamese": "tham gia vào"
    },
    {
        "english": "Involved in",
        "type": "v",
        "vietnamese": "tham gia vào, có liên quan tới"
    },
    {
        "english": "selfish/unselfish",
        "type": "adj",
        "vietnamese": "ích kỷ/ không ích kỷ"
    },
    {
        "english": "contribute",
        "type": "v",
        "vietnamese": "đóng góp"
    },
    {
        "english": "honored",
        "type": "adj",
        "vietnamese": "vinh dự"
    },
    {
        "english": "tunity",
        "type": "v",
        "vietnamese": "cơ hội"
    },
    {
        "english": "obvious",
        "type": "adj",
        "vietnamese": "rõ ràng"
    },
    {
        "english": "further",
        "type": "adv",
        "vietnamese": "hơn nữa, thêm nữa, vả lại.."
    },
 
    {
        "english": "dedication",
        "type": "v/adj/n",
        "vietnamese": "cống hiến/ tận tụy/sự cống hiến, sự tận tụy"
    },
    {
        "english": "expert",
        "type": "n",
        "vietnamese": "chuyên gia"
    },
    {
        "english": "counsel/counselling",
        "type": "n/v",
        "vietnamese": "lời khuyên/ tư vấn"
    },
    {
        "english": "career",
        "type": "n",
        "vietnamese": "sự nghiệp"
    },
    {
        "english": "direction",
        "type": "n",
        "vietnamese": "phương hướng"
    },
    {
        "english": "confused",
        "type": "adj",
        "vietnamese": "bối rối"
    },
    {
        "english": "honest",
        "type": "adj",
        "vietnamese": "trung thực"
    },
    {
        "english": "concentrate",
        "type": "v",
        "vietnamese": "tập trung"
    },
    {
        "english": "effective",
        "type": "adj",
        "vietnamese": "hiệu quả"
    },
    {
        "english": "journey",
        "type": "n",
        "vietnamese": "hành trình"
    },
    {
        "english": "passion",
        "type": "n",
        "vietnamese": "niềm đam mê"
    },
    {
        "english": "appreciate",
        "type": "v",
        "vietnamese": "đánh giá"
    },
    {
        "english": "colleague",
        "type": "n",
        "vietnamese": "đồng nghiệp"
    },
    {
        "english": "considerable",
        "type": "adj",
        "vietnamese": "to, đáng kể"
    },
    {
        "english": "investigation",
        "type": "n",
        "vietnamese": "cuộc điều tra"
    },
    {
        "english": "aspire",
        "type": "v",
        "vietnamese": "khao khát"
    },
    {
        "english": "corner",
        "type": "n",
        "vietnamese": "ngóc ngách"
    },
    {
        "english": "pursue",
        "type": "v",
        "vietnamese": "theo đuổi"
    },
    {
        "english": "deserve",
        "type": "v",
        "vietnamese": "xứng đáng"
    },
    {
        "english": "mess",
        "type": "n/v",
        "vietnamese": "rối tung"
    },
    {
        "english": "considered",
        "type": "v",
        "vietnamese": "xem xét"
    },
    {
        "english": "pain",
        "type": "n",
        "vietnamese": "lỗi đau"
    },
    {
        "english": "mentality",
        "type": "n",
        "vietnamese": "tâm lý"
    },
    {
        "english": "discipline",
        "type": "n",
        "vietnamese": "kỷ luật"
    },
    {
        "english": "regret",
        "type": "n",
        "vietnamese": "hối tiếc"
    },
    {
        "english": "kindness/kind",
        "type": "n/adj",
        "vietnamese": "Tốt bụng/ tốt bụng"
    },
    {
        "english": "wisdom/wise",
        "type": "n/adj",
        "vietnamese": "Khôn ngoan/ Khôn ngoan"
    },
    {
        "english": "gratitude/grateful",
        "type": "n/adj",
        "vietnamese": "Sự biết ơn/ lòng biết ơn"
    },
    {
        "english": "compassion/compassionate",
        "type": "n/adj",
        "vietnamese": "sự trắc ẩn/ lòng chắc ẩn"
    },
    {
        "english": "diligent",
        "type": "adj",
        "vietnamese": "chăm chỉ"
    },
    {
        "english": "integrity",
        "type": "n",
        "vietnamese": "Lòng chính trực"
    },
	  {
		"english": "courage",
		"type": "n",
		"pronounce": "kʌridʤ",
		"vietnamese": "sự can đảm, sự dũng cảm, dũng khí"
	  },
    {
        "english": "ambition/ambitious",
        "type": "n/adj",
        "vietnamese": "sự tham vọng/ tham vọng"
    },
    {
        "english": "optimistic",
        "type": "adj",
        "vietnamese": "lạc quan"
    },
    {
        "english": "course",
        "type": "n",
        "vietnamese": "khóa học"
    },
    {
        "english": "conference",
        "type": "n",
        "vietnamese": "hội nghị"
    },
    {
        "english": "matter",
        "type": "n",
        "vietnamese": "vấn  đề"
    },
    {
        "english": "weird",
        "type": "adj",
        "vietnamese": "kỳ quặc, kỳ dị"
    },
    {
        "english": "strange",
        "type": "adj",
        "vietnamese": "lạ lùng, không quen"
    },
    {
        "english": "negative",
        "type": "adj",
        "vietnamese": "tiêu cực"
    },
    {
        "english": "personality",
        "type": "n",
        "vietnamese": "tính cách, nhân cách"
    },
    {
        "english": "introverted",
        "type": "v",
        "vietnamese": "sống nội tâm"
    },
    {
        "english": "period",
        "type": "n",
        "vietnamese": "giai đoạn"
    },
    {
        "english": "situation",
        "type": "n",
        "vietnamese": "tình huống"
    },
    {
        "english": "distance",
        "type": "n",
        "vietnamese": "khoảng cách"
    },
    {
        "english": "fear",
        "type": "n",
        "vietnamese": "lỗi sợ"
    },
    {
        "english": "phrased/phrases",
        "type": "v/n",
        "vietnamese": "diễn đạt,cụm từ"
    },
    {
        "english": "expressions",
        "type": "n",
        "vietnamese": "cách diễn tả"
    },
    {
        "english": "singular",
        "type": "n",
        "vietnamese": "đặc biệt, hiếm có"
    },
    {
        "english": "coast",
        "type": "n",
        "vietnamese": "bờ biển"
    },
    {
        "english": "atmosphere",
        "type": "n",
        "vietnamese": "bầu không khí"
    },
    {
        "english": "statement",
        "type": "v",
        "vietnamese": "tuyên bố"
    },
    {
        "english": "possibility",
        "type": "n",
        "vietnamese": "khả năng, việc có thể được"
    },
    {
        "english": "general",
        "type": "adj",
        "vietnamese": "tổng quan, chung chung"
    },
    {
        "english": "routine",
        "type": "n",
        "vietnamese": "lịch trình, thói quen, công việc thường ngày"
    },
    {
        "english": "argue",
        "type": "v",
        "vietnamese": "tranh cãi, tranh luận"
    },
    {
        "english": "cheer",
        "type": "v",
        "vietnamese": "reo hò"
    },
    {
        "english": "solve",
        "type": "v",
        "vietnamese": "giải quyết"
    },
    {
        "english": "contrast",
        "type": "n",
        "vietnamese": "sự tương phản, đối lập"
    },
    {
        "english": "correct",
        "type": "adj",
        "vietnamese": "chính xác"
    },
    {
        "english": "graduate",
        "type": "v",
        "vietnamese": "tốt nghiệp"
    },
    {
        "english": "crowded",
        "type": "adj",
        "vietnamese": "đông đúc"
    },
    {
        "english": "regularly",
        "type": "adv",
        "vietnamese": "thường xuyên"
    },
    {
        "english": "literature",
        "type": "n",
        "vietnamese": "văn học"
    },
    {
        "english": "semester",
        "type": "n",
        "vietnamese": "học kỳ"
    },
    {
        "english": "recognize",
        "type": "v",
        "vietnamese": "nhận ra"
    },
    {
        "english": "effortless",
        "type": "adj",
        "vietnamese": "dễ dàng"
    },
    {
        "english": "range",
        "type": "n",
        "vietnamese": "phạm vi"
    },
    {
        "english": "impact",
        "type": "v",
        "vietnamese": "va chạm, tác động"
    },
    {
        "english": "rhythm",
        "type": "n",
        "vietnamese": "âm tiết, nhịp"
    },
    {
        "english": "approach",
        "type": "n",
        "vietnamese": "cách tiếp cận"
    },
    {
        "english": "slightly",
        "type": "adj",
        "vietnamese": "nhẹ nhàng"
    },
    {
        "english": "elaborate/elaborately/elaborated",
        "type": "adj/adv/adj",
        "vietnamese": "phức tạp/ một cách tỉ mỉ/ công phu"
    },
    {
        "english": "rant",
        "type": "n",
        "vietnamese": "lời nói hênh hoang"
    },
    {
        "english": "tricky",
        "type": "adj",
        "vietnamese": "khôn lanh, khó vận dụng, quỉ quyệt, xảo quyệt"
    },
    {
        "english": "struggled",
        "type": "v",
        "vietnamese": "đấu tranh"
    },
    {
        "english": "polite",
        "type": "adj",
        "vietnamese": "lịch sự"
    },
    {
        "english": "remind",
        "type": "v",
        "vietnamese": "nhắc nhở"
    },
    {
        "english": "complicated",
        "type": "adj",
        "vietnamese": "phức tạp, rắc rối"
    },
    {
        "english": "deserve",
        "type": "v",
        "vietnamese": "xứng đáng"
    },
    {
        "english": "relate",
        "type": "v",
        "vietnamese": "kể lại, thuật lại"
    },
    {
        "english": "express",
        "type": "v",
        "vietnamese": "biểu lộ, diễn đạt, bày tỏ"
    },
    {
        "english": "harshly",
        "type": "adv",
        "vietnamese": "chỉ trích, nói gay gắt"
    },
    {
        "english": "doubt",
        "type": "v",
        "vietnamese": "nghi ngờ, ngờ vực"
    },
    {
        "english": "genius",
        "type": "n",
        "vietnamese": "thiên tài"
    },
    {
        "english": "rude",
        "type": "adj",
        "vietnamese": "khiếm nhã, hỗn láo"
    },
    {
        "english": "mind",
        "type": "v",
        "vietnamese": "chú ý, lưu ý"
    },
    {
        "english": "fixated",
        "type": "adj",
        "vietnamese": "cố định, gắn bó, lưu luyến"
    },
    {
        "english": "sponsor",
        "type": "v",
        "vietnamese": "đỡ đầu, bảo trợ"
    },
    {
        "english": "hire",
        "type": "n/v",
        "vietnamese": "sự thuê, cho thuê"
    },
    {
        "english": "emotional",
        "type": "adj",
        "vietnamese": "cảm động, xúc động, dễ cảm động"
    },
    {
        "english": "mature",
        "type": "adj",
        "vietnamese": "chín chắn, thuần thục, trưởng thành"
    },
    {
        "english": "passionate",
        "type": "v",
        "vietnamese": "say đắm"
    },
    {
        "english": "mention",
        "type": "n/v",
        "vietnamese": "đề cập đến"
    },
    {
        "english": "scenery",
        "type": "n",
        "vietnamese": "phong cảnh"
    },
    {
        "english": "inconvenience",
        "type": "adj",
        "vietnamese": "bất tiện"
    },
    {
        "english": "treat",
        "type": "v",
        "vietnamese": "đối xử"
    },
    {
        "english": "inspiring",
        "type": "adj",
        "vietnamese": "cảm hứng"
    },
    {
        "english": "several",
        "type": "adj",
        "vietnamese": "một số"
    },
    {
        "english": "sense",
        "type": "n",
        "vietnamese": "giác quan, cảm giác"
    },
    {
        "english": "satisfaction",
        "type": "n",
        "vietnamese": "sự hài lòng"
    },
    {
        "english": "morals",
        "type": "n",
        "vietnamese": "đạo đức"
    },
    {
        "english": "principles",
        "type": "n",
        "vietnamese": "nguyên tắc, bản chất"
    },
    {
        "english": "extent",
        "type": "n",
        "vietnamese": "phạm vi"
    },
    {
        "english": "frustrate",
        "type": "adj",
        "vietnamese": "chống lại, làm hỏng, sự thất bại, thất bại"
    },
    {
        "english": "expertise",
        "type": "n",
        "vietnamese": "chuyên môn"
    },
    {
        "english": "current",
        "type": "adj",
        "vietnamese": "hiện tại, hiện hành"
    },
    {
        "english": "socienty",
        "type": "n",
        "vietnamese": "xã hội, sự giao thiệp, sự xã giao"
    },
    {
        "english": "norm",
        "type": "n",
        "vietnamese": "chuẩn mực"
    },
    {
        "english": "lecturer",
        "type": "n",
        "vietnamese": "giảng viên"
    },
    {
        "english": "debt",
        "type": "n",
        "vietnamese": "món nợ"
    },
    {
        "english": "decent",
        "type": "adj",
        "vietnamese": "tử tế, đoan chính"
    },
    {
        "english": "borrow",
        "type": "v",
        "vietnamese": "vay mượn"
    },
    {
        "english": "tuition",
        "type": "n",
        "vietnamese": "học phí"
    },
    {
        "english": "candidate",
        "type": "n",
        "vietnamese": "ứng viên"
    },
    {
        "english": "contest",
        "type": "n",
        "vietnamese": "cuộc thi"
    },
    {
        "english": "stable",
        "type": "n",
        "vietnamese": "ổn định"
    },
    {
        "english": "invitation",
        "type": "n",
        "vietnamese": "lời mời"
    },
    {
        "english": "persuade",
        "type": "v",
        "vietnamese": "thuyết phục"
    },
    {
        "english": "hesitate",
        "type": "v",
        "vietnamese": "do dự"
    },
    {
        "english": "blame",
        "type": "v",
        "vietnamese": "đổ tội"
    },
    {
        "english": "appear",
        "type": "n",
        "vietnamese": "chứng tỏ rằng, giống như, hiện ra"
    },
  {
    "english": "regarding",
    "type": "prep",
    "pronounce": "ri ́ga:diη",
    "vietnamese": "về, về việc, đối với (vấn đề...)"
  },
    {
        "english": "deny",
        "type": "v",
        "vietnamese": "từ chối"
    },
    {
        "english": "punish",
        "type": "v",
        "vietnamese": "trừng phạt"
    },
    {
        "english": "judged",
        "type": "v",
        "vietnamese": "phán xét"
    },
    {
        "english": "discover",
        "type": "v",
        "vietnamese": "phát hiện"
    },
    {
        "english": "participant",
        "type": "n",
        "vietnamese": "người tham gia"
    },
    {
        "english": "dreaded",
        "type": "adj",
        "vietnamese": "sợ hãi"
    },
    {
        "english": "screwing up",
        "type": "v",
        "vietnamese": "làm hỏng việc"
    },
    {
        "english": "measure",
        "type": "v",
        "vietnamese": "đo lường"
    },
    {
        "english": "lousy",
        "type": "adj",
        "vietnamese": "tồi tệ, tệ hại"
    },
    {
        "english": "suddenly",
        "type": "adv",
        "vietnamese": "một cách đột nhiên"
    },
    {
        "english": "counter",
        "type": "n",
        "vietnamese": "quầy tính tiền"
    },
    {
        "english": "inefficient",
        "type": "adj",
        "vietnamese": "không hiệu quả"
    },
    {
        "english": "distinct",
        "type": "adj",
        "vietnamese": "riêng biệt"
    },
    {
        "english": "planet",
        "type": "n",
        "vietnamese": "hành tinh"
    },
    {
        "english": "contain",
        "type": "v",
        "vietnamese": "chứa"
    },
    {
        "english": "integrated",
        "type": "adj",
        "vietnamese": "tích hợp"
    },
    {
        "english": "clarify",
        "type": "v",
        "vietnamese": "làm rõ"
    },
    {
        "english": "paragraph",
        "type": "n",
        "vietnamese": "đoạn văn"
    },
    {
        "english": "adult",
        "type": "n",
        "vietnamese": "người lớn"
    },
    {
        "english": "nodding",
        "type": "v",
        "vietnamese": "gật đầu"
    },
    {
        "english": "disappear",
        "type": "v",
        "vietnamese": "biến mất"
    },
    {
        "english": "ability",
        "type": "n",
        "vietnamese": "khả năng"
    },
    {
        "english": "priority",
        "type": "n",
        "vietnamese": "sự ưu tiên"
    },
    {
        "english": "immediately",
        "type": "adv",
        "vietnamese": "ngay lập tức"
    },
    {
        "english": "tremendous",
        "type": "adj",
        "vietnamese": "to lớn-mô tả về ấn tượng -  We received a .... amount of support for our project"
    },
    {
        "english": "author",
        "type": "n",
        "vietnamese": "tác giả"
    },
    {
        "english": "infant",
        "type": "n",
        "vietnamese": "trẻ sơ sinh"
    },
    {
        "english": "deprivation",
        "type": "n",
        "vietnamese": "sự thiếu thốn, sự ít ỏi"
    },
    {
        "english": "across",
        "type": "v",
        "vietnamese": "đi qua, đi sang"
    },
    {
        "english": "acquire",
        "type": "v",
        "vietnamese": "dành được"
    },
    {
        "english": "narrow",
        "type": "adj",
        "vietnamese": "chật hẹp, giới hạn"
    },
    {
        "english": "incompetent",
        "type": "adj",
        "vietnamese": "bất tài"
    },
    {
        "english": "certain",
        "type": "n",
        "vietnamese": "nhất định"
    },
    {
        "english": "accumulate",
        "type": "v",
        "vietnamese": "tích chữ"
    },
    {
        "english": "improvement",
        "type": "n",
        "vietnamese": "sự cải tiến"
    },
    {
        "english": "massive",
        "type": "adj",
        "vietnamese": "to lớn(mô tả vật lý - The earthquake caused a ... shift in the landscape.)"
    },
    {
        "english": "invest",
        "type": "v",
        "vietnamese": "đầu tư"
    },
    {
        "english": "distractions",
        "type": "n",
        "vietnamese": "phiền nhiễu"
    },
    {
        "english": "initial",
        "type": "n",
        "vietnamese": "ban đầu"
    },
    {
        "english": "instrument",
        "type": "n",
        "vietnamese": "công cụ"
    },
    {
        "english": "assistant",
        "type": "n",
        "vietnamese": "trợ lý"
    },
    {
        "english": "accompany",
        "type": "v",
        "vietnamese": "đồng hành"
    },
    {
        "english": "intellectual",
        "type": "adj",
        "vietnamese": "trí tuệ, trí năng"
    },
    {
        "english": "emotional",
        "type": "adj",
        "vietnamese": "xúc động "
    },
    {
        "english": "journalist",
        "type": "n",
        "vietnamese": "nhà báo"
    },
    {
        "english": "brave",
        "type": "adj",
        "vietnamese": "can đảm"
    },
    {
        "english": "consume",
        "type": "v",
        "vietnamese": "tiêu thụ"
    },
    {
        "english": "determined",
        "type": "v",
        "vietnamese": "xác định"
    },
    {
        "english": "guarded",
        "type": "v",
        "vietnamese": "canh gác, cảnh giác, đề phòng"
    },
    {
        "english": "navigate",
        "type": "v",
        "vietnamese": "điều hướng"
    },
    {
        "english": "refugee",
        "type": "n",
        "vietnamese": "người tị nạn"
    },
    {
        "english": "immigrant",
        "type": "v",
        "vietnamese": "di chú, di cư"
    },
    {
        "english": "deport",
        "type": "v",
        "vietnamese": "trục xuất"
    },
    {
        "english": "threaten",
        "type": "v",
        "vietnamese": "hăm dọa"
    },
    {
        "english": "defend",
        "type": "v",
        "vietnamese": "phòng thủ, bảo vệ"
    },
    {
        "english": "covered",
        "type": "v",
        "vietnamese": "đề cập"
    },
    {
        "english": "ingrain",
        "type": "v",
        "vietnamese": "ăn sâu vào, in sâu vào"
    },
    {
        "english": "traumatic",
        "type": "n",
        "vietnamese": "chấn thương"
    },
    {
        "english": "tough",
        "type": "adj",
        "vietnamese": "khó khăn"
    },
    {
        "english": "society",
        "type": "n",
        "vietnamese": "xã hội"
    },
    {
        "english": "encourage",
        "type": "v",
        "vietnamese": "khuyến khích"
    },
    {
        "english": "imperfection",
        "type": "n",
        "vietnamese": "sự không hoàn hảo"
    },
    {
        "english": "claim",
        "type": "v",
        "vietnamese": "khẳng định(khẳng định bản thân)"
    },
    {
        "english": "criticize",
        "type": "v",
        "vietnamese": "chỉ trích, phê bình"
    },
    {
        "english": "despise",
        "type": "v",
        "vietnamese": "coi thường"
    },
    {
        "english": "articulate",
        "type": "adj",
        "vietnamese": "rõ ràng"
    },
    {
        "english": "frank",
        "type": "adj",
        "vietnamese": "thẳng thắn"
    },
    {
        "english": "structure",
        "type": "n",
        "vietnamese": "cấu trúc"
    },
    {
        "english": "incessant",
        "type": "adj",
        "vietnamese": "không ngừng, liên tục"
    },
    {
        "english": "opportunity",
        "type": "n",
        "vietnamese": "cơ hội"
    },
    {
        "english": "ignore",
        "type": "v",
        "vietnamese": "làm ngơ"
    },
    {
        "english": "brief",
        "type": "adj",
        "vietnamese": "ngắn ngọn"
    },
    {
        "english": "behavior",
        "type": "n",
        "vietnamese": "hành vi"
    },
    {
        "english": "beyond",
        "type": "",
        "vietnamese": "vượt ra"
    },
    {
        "english": "particular",
        "type": "adj",
        "vietnamese": "cụ thể"
    },
    {
        "english": "context",
        "type": "n",
        "vietnamese": "bối cảnh"
    },
    {
        "english": "barely",
        "type": "adv",
        "vietnamese": "một cách vừa đủ"
    },
    {
        "english": "collaboration",
        "type": "n",
        "vietnamese": "sự hợp tác"
    },
    {
        "english": "fascination",
        "type": "n",
        "vietnamese": "mê hoặc"
    },
    {
        "english": "entrepreneur",
        "type": "n",
        "vietnamese": "doanh nhân"
    },
    {
        "english": "remain",
        "type": "v",
        "vietnamese": "còn lại, ở lại, duy trì"
    },
    {
        "english": "civilization",
        "type": "n",
        "vietnamese": "nền văn minh"
    },
    {
        "english": "relative",
        "type": "adj",
        "vietnamese": "liên quan đến"
    },
    {
        "english": "spiritual",
        "type": "n",
        "vietnamese": "tinh thần"
    },
    {
        "english": "unprovable",
        "type": "adj",
        "vietnamese": "không thể chứng minh được"
    },
    {
        "english": "explode",
        "type": "v",
        "vietnamese": "làm nổ, đập tan tiêu tan"
    },
    {
        "english": "reality",
        "type": "n",
        "vietnamese": "thực tế"
    },
    {
        "english": "reframe",
        "type": "v",
        "vietnamese": "điều chỉnh lại"
    },
    {
        "english": "empathy/empathetic",
        "type": "n/adj",
        "vietnamese": "Sự thông cảm/ Lòng thông cảm"
    },
    {
        "english": "multiple",
        "type": "adj",
        "vietnamese": "nhiều"
    },
    {
        "english": "pernicious",
        "type": "adj",
        "vietnamese": "nguy hiểm"
    },
    {
        "english": "innovation",
        "type": "n",
        "vietnamese": "sự đổi mới"
    },
    {
        "english": "calm",
        "type": "adj",
        "vietnamese": "điềm tĩnh"
    }, 
    {
        "english": "lecture",
        "type": "n",
        "vietnamese": "bài học"
    },
    {
        "english": "sector",
        "type": "n",
        "vietnamese": "lĩnh vực"
    },
    {
        "english": "borrow",
        "type": "n",
        "vietnamese": "vay mượn"
    },
    {
        "english": "obviously",
        "type": "adv",
        "vietnamese": "rõ ràng, chắc chắn, xác nhận"
    },
    {
        "english": "reassured",
        "type": "adj",
        "vietnamese": "yên tâm "
    },
    {
        "english": "discourage",
        "type": "v",
        "vietnamese": "chán nản, thất vọng"
    },
    {
        "english": "passion",
        "type": "n",
        "vietnamese": "niềm đam mê"
    },
    {
        "english": "determine",
        "type": "v",
        "vietnamese": "quyết tâm"
    },
    {
        "english": "adversity",
        "type": "",
        "vietnamese": "nghịch cảnh, khó khăn, vận đen -(overcome adversity)"
    },
    {
        "english": "sight",
        "type": "n",
        "vietnamese": "thị giác"
    },
    {
        "english": "punish",
        "type": "v",
        "vietnamese": "trừng phạt"
    },
    {
        "english": "covered up",
        "type": "",
        "vietnamese": "che đậy"
    },
    {
        "english": "perception",
        "type": "n",
        "vietnamese": "sự nhận thức"
    },
    {
        "english": "norm",
        "type": "n",
        "vietnamese": "chuẩn mực"
    },
    {
        "english": "individual",
        "type": "adj",
        "vietnamese": "cá nhân"
    },
    {
        "english": "diversity",
        "type": "n",
        "vietnamese": "đa dạng"
    },
    {
        "english": "precious",
        "type": "adj",
        "vietnamese": "quý giá, cầu kỳ, đắt giá"
    },
    {
        "english": "pressure",
        "type": "n",
        "vietnamese": "áp lực, sức ép, sự đè nén"
    },
    {
        "english": "humble",
        "type": "adj",
        "vietnamese": "khiêm tốn"
    },
    {
        "english": "commercials",
        "type": "n",
        "vietnamese": "quảng cáo"
    },
    {
        "english": "charity",
        "type": "n",
        "vietnamese": "tổ chức từ thiện"
    },
    {
        "english": "stare",
        "type": "v",
        "vietnamese": "nhìn chằm chằm"
    },
    {
        "english": "humanitarian",
        "type": "n",
        "vietnamese": "nhân đạo, thuộc về nhân đạo"
    },
  {
    "english": "declare",
    "type": "v",
    "pronounce": "di'kleə",
    "vietnamese": "tuyên bố, công bố"
  },
    {
        "english": "aimed",
        "type": "v",
        "vietnamese": "nhắm vào"
    },
    {
        "english": "society",
        "type": "n",
        "vietnamese": "thuộc về xã hội"
    },
    {
        "english": "including",
        "type": "",
        "vietnamese": "bao gồm"
    },
    {
        "english": "effort",
        "type": "n",
        "vietnamese": "sự cố gắng"
    },
    {
        "english": "scholarship",
        "type": "n",
        "vietnamese": "học bổng"
    },
    {
        "english": "condition",
        "type": "n",
        "vietnamese": "tình trạng"
    },
    {
        "english": "reach",
        "type": "v",
        "vietnamese": "với tới, đạt được"
    },
    {
        "english": "incorporate",
        "type": "v",
        "vietnamese": "kết hợp"
    },
    {
        "english": "desperate",
        "type": "adj",
        "vietnamese": "tuyệt vọng"
    },
    {
        "english": "cozy",
        "type": "adj",
        "vietnamese": "ấm áp"
    },
    {
        "english": "attractive",
        "type": "adj",
        "vietnamese": "hấp dẫn, cuốn hút"
    },
    {
        "english": "rejoice",
        "type": "v",
        "vietnamese": "hân hoan, chúc mừng"
    },
    {
        "english": "miserable",
        "type": "adj",
        "vietnamese": "khốn khổ, nghèo khó"
    },
    {
        "english": "incapable",
        "type": "adj",
        "vietnamese": "không có khả năng"
    },
    {
        "english": "permanent",
        "type": "adj",
        "vietnamese": "vĩnh viễn"
    },
    {
        "english": "faithful",
        "type": "adj",
        "vietnamese": "trung thành"
    },
    {
        "english": "retrospect",
        "type": "n",
        "vietnamese": "sự nhìn lại, xem xét lại"
    },
    {
        "english": "lottery winners",
        "type": "n",
        "vietnamese": "người chúng xổ số"
    },
    {
        "english": "fortunes",
        "type": "n",
        "vietnamese": "vận may"
    },
    {
        "english": "equip",
        "type": "v",
        "vietnamese": "trang bị"
    },
    {
        "english": "countryside",
        "type": "n",
        "vietnamese": "đồng quê, nông thôn"
    },
    {
        "english": "superficial",
        "type": "adj",
        "vietnamese": "hời hợt, nông cạn"
    },
    {
        "english": "perseverance",
        "type": "n",
        "vietnamese": "sự kiên trì"
    },
    {
        "english": "inaccurate",
        "type": "adj",
        "vietnamese": "không chính xác"
    },
    {
        "english": "prisoner",
        "type": "n",
        "vietnamese": "tù nhân"
    },
    {
        "english": "trapped",
        "type": "v",
        "vietnamese": "mắc kẹt"
    },
    {
        "english": "haunt",
        "type": "v",
        "vietnamese": "ám ảnh"
    },
  {
    "english": "unpleasant",
    "type": "adj",
    "pronounce": "ʌn'plezənt",
    "vietnamese": "không dễ chịu, khó chịu, khó ưa"
  },
    {
        "english": "belittle",
        "type": "v",
        "vietnamese": "chê bai"
    },
    {
        "english": "nurture",
        "type": "v",
        "vietnamese": "nuôi dưỡng, dưỡng dục"
    },
    {
        "english": "desire",
        "type": "n",
        "vietnamese": "sự mong muốn"
    },
    {
        "english": "potential",
        "type": "adj",
        "vietnamese": "tiềm năng"
    },
    {
        "english": "afterwards",
        "type": "adv",
        "vietnamese": "sau đó"
    },
    {
        "english": "influence",
        "type": "v",
        "vietnamese": "ảnh hưởng"
    },
    {
        "english": "vibe",
        "type": "adj",
        "vietnamese": "rung cảm"
    },
    {
        "english": "reinforce",
        "type": "v",
        "vietnamese": "củng cố"
    },
    {
        "english": "rarely",
        "type": "adv",
        "vietnamese": "hiếm khi"
    },
    {
        "english": "resentment",
        "type": "adj",
        "vietnamese": "phẫn nộ"
    },
    {
        "english": "bitterness",
        "type": "v",
        "vietnamese": "sự cay đắng"
    },
    {
        "english": "jealous",
        "type": "adj",
        "vietnamese": "gen tị"
    },
    {
        "english": "disrupt",
        "type": "v",
        "vietnamese": "làm gián đoạn"
    },
    {
        "english": "explicitly",
        "type": "adj",
        "vietnamese": "rõ ràng"
    },
    {
        "english": "pure",
        "type": "adj",
        "vietnamese": "nguyên chất"
    },
    {
        "english": "seek",
        "type": "v",
        "vietnamese": "tìm kiếm"
    },
    {
        "english": "betray",
        "type": "v",
        "vietnamese": "phản bội"
    },
    {
        "english": "devoted",
        "type": "adj",
        "vietnamese": "tận tâm"
    },
    {
        "english": "enrich",
        "type": "v",
        "vietnamese": "làm giàu"
    },
    {
        "english": "willful",
        "type": "v",
        "vietnamese": "cố ý"
    },
  {
    "english": "pleasure",
    "type": "n",
    "pronounce": "ˈplɛʒuə(r)",
    "vietnamese": "niềm vui thích, điều thích thú, điều thú vị; ý muốn, ý thích"
  },
    {
        "english": "harm",
        "type": "v",
        "vietnamese": "làm hại"
    },
    {
        "english": "interfere",
        "type": "v",
        "vietnamese": "can thiệp vào"
    },
    {
        "english": "endeavor",
        "type": "v",
        "vietnamese": "nỗ lực"
    },
    {
        "english": "delusional",
        "type": "adj",
        "vietnamese": "ảo tưởng"
    },
  {
    "english": "tendency",
    "type": "n",
    "pronounce": "ˈtɛndənsi",
    "vietnamese": "xu hướng, chiều hướng, khuynh hướng"
  },
    {
        "english": "ego",
        "type": "n",
        "vietnamese": "cái tôi"
    },
    {
        "english": "exhausted",
        "type": "adj",
        "vietnamese": "kiệt sức"
    },
    {
        "english": "anxious",
        "type": "adj",
        "vietnamese": "lo lắng"
    },
    {
        "english": "rectify",
        "type": "v",
        "vietnamese": "sửa chữa"
    },
    {
        "english": "tribulations",
        "type": "v",
        "vietnamese": "hoạn nạn"
    },
    {
        "english": "surrender",
        "type": "v",
        "vietnamese": "đầu hàng"
    },
    {
        "english": "guide",
        "type": "n",
        "vietnamese": "hướng dẫn"
    },
    {
        "english": "attentive",
        "type": "adj",
        "vietnamese": "chú ý"
    },
    {
        "english": "intuition",
        "type": "n",
        "vietnamese": "trực giác"
    },
    {
        "english": "delicate",
        "type": "adj",
        "vietnamese": "tinh xảo, thanh tú"
    },
    {
        "english": "variety",
        "type": "n",
        "vietnamese": "sự đa dạng"
    },
    {
        "english": "surrounded",
        "type": "v",
        "vietnamese": "được bao quanh"
    },
    {
        "english": "captivate",
        "type": "v",
        "vietnamese": "mê hoặc"
    },
    {
        "english": "distraction",
        "type": "n",
        "vietnamese": "xao lãng"
    },
    {
        "english": "passing out",
        "type": "",
        "vietnamese": "bất tỉnh"
    },
    {
        "english": "confront",
        "type": "v",
        "vietnamese": "đối đầu, đối chất, so sánh"
    },
    {
        "english": "frantically",
        "type": "adv",
        "vietnamese": "một cách điên cuồng"
    },
    {
        "english": "contradict",
        "type": "v",
        "vietnamese": "mâu thuẫn"
    },
    {
        "english": "sympathize",
        "type": "v",
        "vietnamese": "thông cảm"
    },
    {
        "english": "regain",
        "type": "v",
        "vietnamese": "lấy lại, chiếm lại, chuộc lại"
    },
    {
        "english": "praise",
        "type": "v",
        "vietnamese": "khen"
    },
    {
        "english": "mental",
        "type": "adj",
        "vietnamese": "tâm thần"
    },
    {
        "english": "productive",
        "type": "adj",
        "vietnamese": "năng suất"
    },
    {
        "english": "stigmatize",
        "type": "adj",
        "vietnamese": "kì thị"
    },
    {
        "english": "guilty",
        "type": "adj",
        "vietnamese": "tội lỗi"
    },
    {
        "english": "urgent",
        "type": "adj",
        "vietnamese": "cấp bách"
    },
    {
        "english": "sluggish",
        "type": "adj",
        "vietnamese": "trì trệ, chậm chạp"
    },
    {
        "english": "strict",
        "type": "adj",
        "vietnamese": "nghiêm khắc"
    },
    {
        "english": "against",
        "type": "v",
        "vietnamese": "chống lại"
    },{
        "english": "typical",
        "type": "adj",
        "pronounce": "́tipikəl",
        "vietnamese": "tiêu biểu, điển hình, đặc trưng"
    },
    {
        "english": "revise",
        "type": "v",
        "pronounce": "ri'vaiz",
        "vietnamese": "đọc lại, xem lại, sửa lại, ôn lại"
    },
    {
        "english": "pay attention to",
        "type": "",
        "pronounce": "",
        "vietnamese": "chú ý tới"
    },
    {
        "english": "gradually",
        "type": "adv",
        "pronounce": "grædzuəli",
        "vietnamese": "dần dần, tư tư"
    },
    {
        "english": "permission",
        "type": "n",
        "pronounce": "pə'miʃn",
        "vietnamese": "sự cho phép, giấy phép"
    },
    {
        "english": "capable",
        "type": "of, adj",
        "pronounce": "keipəb(ə)l",
        "vietnamese": "có tài, có năng lực; có khả năng, cả gan"
    },
    {
        "english": "possibly",
        "type": "adv",
        "pronounce": "́pɔsibli",
        "vietnamese": "có lẽ, có thể, có thể chấp nhận được"
    },
    {
        "english": "suitable",
        "type": "adj",
        "pronounce": "́su:təbl",
        "vietnamese": "hợp, phù hợp, thích hợp với"
    },
    {
        "english": "recording",
        "type": "n",
        "pronounce": "ri ́kɔ:diη",
        "vietnamese": "sự ghi, sự thu âm"
    },
    {
        "english": "deliver",
        "type": "v",
        "pronounce": "di'livə",
        "vietnamese": "cứu khỏi, thoát khỏi, bày tỏ, giãi bày"
    },
    {
        "english": "material",
        "type": "n, adj",
        "pronounce": "mə ́tiəriəl",
        "vietnamese": "nguyên vật liệu; vật chất, hữu hình"
    },
    {
        "english": "tribulations",
        "type": "v",
        "vietnamese": "hoạn nạn"
    },
    {
        "english": "sensitive",
        "type": "adj",
        "pronounce": "sensitiv",
        "vietnamese": "dễ bị thương, dễ bị hỏng; dễ bị xúc phạm"
    },
    {
        "english": "entire",
        "type": "adj",
        "pronounce": "in'taiə",
        "vietnamese": "toàn thể, toàn bộ"
    },
    {
        "english": "wonder",
        "type": "v",
        "pronounce": "wʌndə",
        "vietnamese": "ngạc nhiên, lấy làm lạ, kinh ngạc"
    },
    {
        "english": "struggled",
        "type": "v",
        "vietnamese": "đấu tranh"
    },
    {
        "english": "worth",
        "type": "adj",
        "pronounce": "wɜrθ",
        "vietnamese": "đáng giá, có giá trị"
    },
    {
        "english": "criticize",
        "type": "v",
        "pronounce": "ˈkrɪtəˌsaɪz",
        "vietnamese": "phê bình, phê phán, chỉ trích"
    },{
    "english": "confined",
    "type": "adj",
    "pronounce": "kən'faind",
    "vietnamese": "hạn chế, giới hạn"
  },{
    "english": "decision",
    "type": "n",
    "pronounce": "diˈsiʒn",
    "vietnamese": "sự quyết định, sự giải quyết, sự phân xử"
  },{
    "english": "instance",
    "type": "n",
    "pronounce": "instəns",
    "vietnamese": "thí dị, ví dụ; trường hợp cá biệt. for instance ví dụ chẳng hạn"
  },{
    "english": "political",
    "type": "adj",
    "pronounce": "pə'litikl",
    "vietnamese": "về chính trị, về chính phủ, có tính chính trị"
  },{
    "english": "consider",
    "type": "v",
    "pronounce": "kən ́sidə",
    "vietnamese": "cân nhắc, xem xét; để ý, quan tâm, lưu ý đến"
  },{
    "english": "preference",
    "type": "n",
    "pronounce": "prefərəns",
    "vietnamese": "sự thích hơn, sự ưa hơn; cái được ưa thích hơn"
  },{
        "english": "precious",
        "type": "adj",
        "vietnamese": "quý giá, cầu kỳ, đắt giá"
    },{
    "english": "discover",
    "type": "v",
    "pronounce": "dis'kʌvə",
    "vietnamese": "khám phá, phát hiện ra, nhận ra"
  },{
    "english": "observe",
    "type": "v",
    "pronounce": "əbˈzə:v",
    "vietnamese": "quan sát, theo dõi"
  },{
    "english": "shame",
    "type": "n",
    "pronounce": "ʃeɪm",
    "vietnamese": "sự xấu hổ, thẹn thùng, sự ngượng"
  },{
    "english": "relevant",
    "type": "adj",
    "pronounce": "́reləvənt",
    "vietnamese": "thích hợp, có liên quan"
  },{
    "english": "consider",
    "type": "v",
    "pronounce": "kən ́sidə",
    "vietnamese": "cân nhắc, xem xét; để ý, quan tâm, lưu ý đến"
  },{
    "english": "anticipate",
    "type": "v",
    "pronounce": "æn'tisipeit",
    "vietnamese": "thấy trước, chặn trước, lường trước"
  },{
    "english": "predict",
    "type": "v",
    "pronounce": "pri'dikt",
    "vietnamese": "báo trước, tiên đoán, dự báo"
  },{
    "english": "belief",
    "type": "n",
    "pronounce": "bi'li:f",
    "vietnamese": "lòng tin, đức tin, sự tin tưởng"
  },{
    "english": "consider",
    "type": "v",
    "pronounce": "kən ́sidə",
    "vietnamese": "cân nhắc, xem xét; để ý, quan tâm, lưu ý đến"
  },{
        "english": "frustrate",
        "type": "adj",
        "vietnamese": "chống lại, làm hỏng, sự thất bại, thất bại"
    },{
        "english": "resentment",
        "type": "adj",
        "vietnamese": "phẫn nộ"
    },{
    "english": "predict",
    "type": "v",
    "pronounce": "pri'dikt",
    "vietnamese": "báo trước, tiên đoán, dự báo"
  },{
    "english": "majority",
    "type": "n",
    "pronounce": "mə'dʒɔriti",
    "vietnamese": "phần lớn, đa số, ưu thế"
  },{
    "english": "partly",
    "type": "adv",
    "pronounce": "́pa:tli",
    "vietnamese": "đến chừng mực nào đó, phần nào đó"
  },{
        "english": "reframe",
        "type": "v",
        "vietnamese": "điều chỉnh lại"
    },{
    "english": "artificial",
    "type": "adj",
    "pronounce": ",ɑ:ti'fiʃəl",
    "vietnamese": "nhân tạo"
  },{
    "english": "distance",
    "type": "n",
    "pronounce": "distəns",
    "vietnamese": "khoảng cách, tầm xa"
  },{
    "english": "observe",
    "type": "v",
    "pronounce": "əbˈzə:v",
    "vietnamese": "quan sát, theo dõi"
  },{
        "english": "frustrate",
        "type": "adj",
        "vietnamese": "chống lại, làm hỏng, sự thất bại, thất bại"
    },{
    "english": "exercise",
    "type": "n, v",
    "pronounce": "eksəsaiz",
    "vietnamese": "bài tập, sự thi hành, sự thực hiện; làm, thi hành, thực hiện"
  },{
    "english": "reduce",
    "type": "v",
    "pronounce": "ri'dju:s",
    "vietnamese": "giảm, giảm bớt"
  },{
    "english": "approach",
    "type": "v, n",
    "pronounce": "ə'proutʃ",
    "vietnamese": "đến gần, lại gần; sự đến gần, sự lại gần"
  },{
    "english": "regarding",
    "type": "prep",
    "pronounce": "ri ́ga:diη",
    "vietnamese": "về, về việc, đối với (vấn đề...)"
  },{
    "english": "analyse, analyze",
    "type": "v",
    "pronounce": "ænəlaiz",
    "vietnamese": "phân tích"
  },{
    "english": "previous",
    "type": "adj",
    "pronounce": "ˈpriviəs",
    "vietnamese": "vội vàng, hấp tấp; trước (vd. ngày hôm trước), ưu tiên"
  },{
    "english": "behind",
    "type": "prep, adv",
    "pronounce": "bi'haind",
    "vietnamese": "sau, ở đằng sau"
  },{
    "english": "entire",
    "type": "adj",
    "pronounce": "in'taiə",
    "vietnamese": "toàn thể, toàn bộ"
  }
]
;
let dataE2 = [
  {
    "english": "abandon",
    "type": "v",
    "pronounce": "ə'bændən",
    "vietnamese": "bỏ, từ bỏ"
  },
  {
    "english": "abandoned",
    "type": "adj",
    "pronounce": "ə'bændənd",
    "vietnamese": "bị bỏ rơi, bị ruồng bỏ"
  },
  {
    "english": "ability",
    "type": "n",
    "pronounce": "ə'biliti",
    "vietnamese": "khả năng, năng lực"
  },
  {
    "english": "able",
    "type": "adj",
    "pronounce": "eibl",
    "vietnamese": "có năng lực, có tài"
  },
  {
    "english": "about",
    "type": "adv, prep",
    "pronounce": "ə'baut",
    "vietnamese": "khoảng, về"
  },
  {
    "english": "above",
    "type": "prep, adv",
    "pronounce": "ə'bʌv",
    "vietnamese": "ở trên, lên trên"
  },
  {
    "english": "abroad",
    "type": "adv",
    "pronounce": "ə'brɔ:d",
    "vietnamese": "ở, ra nước ngoài, ngoài trời"
  },
  {
    "english": "absence",
    "type": "n",
    "pronounce": "æbsəns",
    "vietnamese": "sự vắng mặt"
  },
  {
    "english": "absent",
    "type": "adj",
    "pronounce": "æbsənt",
    "vietnamese": "vắng mặt, nghỉ"
  },
  {
    "english": "absolute",
    "type": "adj",
    "pronounce": "æbsəlu:t",
    "vietnamese": "tuyệt đối, hoàn toàn"
  },
  {
    "english": "absolutely",
    "type": "adv",
    "pronounce": "æbsəlu:tli",
    "vietnamese": "tuyệt đối, hoàn toàn"
  },
  {
    "english": "absorb",
    "type": "v",
    "pronounce": "əb'sɔ:b",
    "vietnamese": "thu hút, hấp thu, lôi cuốn"
  },
  {
    "english": "abuse",
    "type": "n, v",
    "pronounce": "ə'bju:s",
    "vietnamese": "lộng hành, lạm dụng"
  },
  {
    "english": "academic",
    "type": "adj",
    "pronounce": ",ækə'demik",
    "vietnamese": "thuộc học viện, ĐH, viện hàn lâm"
  },
  {
    "english": "accent",
    "type": "n",
    "pronounce": "æksənt",
    "vietnamese": "trọng âm, dấu trọng âm"
  },
  {
    "english": "accept",
    "type": "v",
    "pronounce": "ək'sept",
    "vietnamese": "chấp nhận, chấp thuận"
  },
  {
    "english": "acceptable",
    "type": "adj",
    "pronounce": "ək'septəbl",
    "vietnamese": "có thể chấp nhận, chấp thuận"
  },
  {
    "english": "access",
    "type": "n",
    "pronounce": "ækses",
    "vietnamese": "lối, cửa, đường vào"
  },
  {
    "english": "accident",
    "type": "n",
    "pronounce": "æksidənt",
    "vietnamese": "tai nạn, rủi ro. by accident: tình cờ"
  },
  {
    "english": "accidental",
    "type": "adj",
    "pronounce": ",æksi'dentl",
    "vietnamese": "tình cờ, bất ngờ"
  },
  {
    "english": "accidentally",
    "type": "adv",
    "pronounce": ",æksi'dentəli",
    "vietnamese": "tình cờ, ngẫu nhiên"
  },
  {
    "english": "accommodation",
    "type": "n",
    "pronounce": "ə,kɔmə'deiʃn",
    "vietnamese": "sự thích nghi, sự điều tiết, sự làm cho phù hợp"
  },
  {
    "english": "accompany",
    "type": "v",
    "pronounce": "ə'kʌmpəni",
    "vietnamese": "đi theo, đi cùng, kèm theo."
  },
  {
    "english": "according to",
    "type": "prep",
    "pronounce": "ə'kɔ:diɳ",
    "vietnamese": "theo, y theo"
  },
  {
    "english": "account",
    "type": "n, v",
    "pronounce": "ə'kaunt",
    "vietnamese": "tài khoản, kế toán; tính toán, tính đến"
  },
  {
    "english": "accurate",
    "type": "adj",
    "pronounce": "ækjurit",
    "vietnamese": "đúng đắn, chính xác, xác đáng"
  },
  {
    "english": "accurately",
    "type": "adv",
    "pronounce": "ækjuritli",
    "vietnamese": "đúng đắn, chính xác"
  },
  {
    "english": "accuse",
    "type": "v",
    "pronounce": "ə'kju:z",
    "vietnamese": "tố cáo, buộc tội, kết tội"
  },
  {
    "english": "achieve",
    "type": "v",
    "pronounce": "ə'tʃi:v",
    "vietnamese": "đạt được, dành được"
  },
  {
    "english": "achievement",
    "type": "n",
    "pronounce": "ə'tʃi:vmənt",
    "vietnamese": "thành tích, thành tựu "
  },
  {
    "english": "acid",
    "type": "n",
    "pronounce": "æsid",
    "vietnamese": "axit"
  },
  {
    "english": "acknowledge",
    "type": "v",
    "pronounce": "ək'nɔlidʤ",
    "vietnamese": "công nhận, thừa nhận"
  },
  {
    "english": "acquire",
    "type": "v",
    "pronounce": "ə'kwaiə",
    "vietnamese": "dành được, đạt được, kiếm được"
  },
  {
    "english": "across",
    "type": "adv, prep",
    "pronounce": "ə'krɔs",
    "vietnamese": "qua, ngang qua"
  },
  {
    "english": "act",
    "type": "n, v",
    "pronounce": "ækt",
    "vietnamese": "hành động, hành vi, cử chỉ, đối xử"
  },
  {
    "english": "action",
    "type": "n",
    "pronounce": "ækʃn",
    "vietnamese": "hành động, hành vi, tác động. Take action: hành động"
  },
  {
    "english": "active",
    "type": "adj",
    "pronounce": "æktiv",
    "vietnamese": "tích cực hoạt động, nhanh nhẹn"
  },
  {
    "english": "actively",
    "type": "adv",
    "pronounce": "æktivli",
    "vietnamese": "tích cực hoạt động; nhanh nhẹn, linh lợi; có hiệu lực"
  },
  {
    "english": "activity",
    "type": "n",
    "pronounce": "æk'tiviti",
    "vietnamese": "sự tích cực, sự hoạt động, sự nhanh nhẹn, sự linh lợi"
  },
  {
    "english": "actor",
    "type": "n",
    "pronounce": "æktə",
    "vietnamese": "diễn viên nam"
  },
  {
    "english": "actress",
    "type": "n",
    "pronounce": "æktris",
    "vietnamese": "diễn viên nữ"
  },
  {
    "english": "actual",
    "type": "adj",
    "pronounce": "æktjuəl",
    "vietnamese": "thực tế, có thật"
  },
  {
    "english": "actually",
    "type": "adv",
    "pronounce": "æktjuəli",
    "vietnamese": "hiện nay, hiện tại"
  },
  {
    "english": "adapt",
    "type": "v",
    "pronounce": "ə'dæpt",
    "vietnamese": "tra, lắp vào"
  },
  {
    "english": "add",
    "type": "v",
    "pronounce": "æd",
    "vietnamese": "cộng, thêm vào"
  },
  {
    "english": "addition",
    "type": "n",
    "pronounce": "ə'diʃn",
    "vietnamese": "tính cộng, phép cộng"
  },
  {
    "english": "additional",
    "type": "adj",
    "pronounce": "ə'diʃənl",
    "vietnamese": "thêm vào, tăng thêm"
  },
  {
    "english": "address",
    "type": "n, v",
    "pronounce": "ə'dres",
    "vietnamese": "địa chỉ, đề địa chỉ"
  },
  {
    "english": "adequate",
    "type": "adj",
    "pronounce": "ædikwit",
    "vietnamese": "đầy, đầy đủ"
  },
  {
    "english": "adequately",
    "type": "adv",
    "pronounce": "ædikwitli",
    "vietnamese": "tương xứng, thỏa đáng"
  },
  {
    "english": "adjust",
    "type": "v",
    "pronounce": "ə'dʤʌst",
    "vietnamese": "sửa lại cho đúng, điều chỉnh"
  },
  {
    "english": "admiration",
    "type": "n",
    "pronounce": ",ædmə'reiʃn",
    "vietnamese": "sự khâm phục, thán phục"
  },
  {
    "english": "admire",
    "type": "v",
    "pronounce": "əd'maiə",
    "vietnamese": "khâm phục, thán phục"
  },
  {
    "english": "admit",
    "type": "v",
    "pronounce": "əd'mit",
    "vietnamese": "nhận vào, cho vào, kết hợp"
  },
  {
    "english": "adopt",
    "type": "v",
    "pronounce": "ə'dɔpt",
    "vietnamese": "nhận làm con nuôi, bố mẹ nuôi"
  },
  {
    "english": "adult",
    "type": "n, adj",
    "pronounce": "ædʌlt",
    "vietnamese": "người lớn, người trưởng thành, trưởng thành"
  },
  {
    "english": "advance",
    "type": "n, v",
    "pronounce": "əd'vɑ:ns",
    "vietnamese": "sự tiến bộ, tiến lên; đưa lên, đề xuat"
  },
  {
    "english": "advanced",
    "type": "adj",
    "pronounce": "əd'vɑ:nst",
    "vietnamese": "tiên tiến, tiến bộ, cap cao. in advance trước, sớm"
  },
  {
    "english": "advantage",
    "type": "n",
    "pronounce": "əb'vɑ:ntidʤ",
    "vietnamese": "sự thuận lợi, lợi ích, lợi thế. take advantage of lợi dụng "
  },
  {
    "english": "adventure",
    "type": "n",
    "pronounce": "əd'ventʃə",
    "vietnamese": "sự phiêu lưu, mạo hiểm"
  },
  {
    "english": "advertise",
    "type": "v",
    "pronounce": "ædvətaiz",
    "vietnamese": "báo cho biết, báo cho biết trước"
  },
  {
    "english": "advertisement",
    "type": "n",
    "pronounce": "əd'və:tismənt",
    "vietnamese": "quảng cáo"
  },
  {
    "english": "advertising",
    "type": "n",
    "pronounce": "",
    "vietnamese": "sự quảng cáo, nghề quảng cáo"
  },
  {
    "english": "advice",
    "type": "n",
    "pronounce": "əd'vais",
    "vietnamese": "lời khuyên, lời chỉ bảo"
  },
  {
    "english": "advise",
    "type": "v",
    "pronounce": "əd'vaiz",
    "vietnamese": "khuyên, khuyên bảo, răn bảo"
  },
  {
    "english": "affair",
    "type": "n",
    "pronounce": "ə'feə",
    "vietnamese": "việc"
  },
  {
    "english": "affect",
    "type": "v",
    "pronounce": "ə'fekt",
    "vietnamese": "làm ảnh hưởng, tác động đến"
  },
  {
    "english": "affection",
    "type": "n",
    "pronounce": "ə'fekʃn",
    "vietnamese": "tình cảm, sự yêu mến"
  },
  {
    "english": "afford",
    "type": "v",
    "pronounce": "ə'fɔ:d",
    "vietnamese": "có thể, có đủ khả năng, điều kiện(làm gì)"
  },
  {
    "english": "afraid",
    "type": "adj",
    "pronounce": "ə'freid",
    "vietnamese": "sợ, sợ hãi, hoảng sợ"
  },
  {
    "english": "after",
    "type": "prep, conj, adv",
    "pronounce": "ɑ:ftə",
    "vietnamese": "sau, đằng sau, sau khi"
  },
  {
    "english": "afternoon",
    "type": "n",
    "pronounce": "ɑ:ftə'nu:n",
    "vietnamese": "buổi chiều"
  },
  {
    "english": "afterwards",
    "type": "adv",
    "pronounce": "ɑ:ftəwəd",
    "vietnamese": "sau này, về sau, rồi thì, sau đây"
  },
  {
    "english": "again",
    "type": "adv",
    "pronounce": "ə'gen",
    "vietnamese": "lại, nữa, lần nữa"
  },
  {
    "english": "against",
    "type": "prep",
    "pronounce": "ə'geinst",
    "vietnamese": "chống lại, phản đối"
  },
  {
    "english": "age",
    "type": "n",
    "pronounce": "eidʤ",
    "vietnamese": "tuổi"
  },
  {
    "english": "aged",
    "type": "adj",
    "pronounce": "eidʤid",
    "vietnamese": "già đi"
  },
  {
    "english": "agency",
    "type": "n",
    "pronounce": "eidʤənsi",
    "vietnamese": "tác dụng, lực; môi giới, trung gian"
  },
  {
    "english": "agent",
    "type": "n",
    "pronounce": "eidʤənt",
    "vietnamese": "đại lý, tác nhân"
  },
  {
    "english": "aggressive",
    "type": "adj",
    "pronounce": "ə'gresiv",
    "vietnamese": "xâm lược, hung hăng (US: xông xáo)"
  },
  {
    "english": "ago",
    "type": "adv",
    "pronounce": "ə'gou",
    "vietnamese": "trước đây"
  },
  {
    "english": "agree",
    "type": "v",
    "pronounce": "ə'gri:",
    "vietnamese": "đồng ý, tán"
  },
  {
    "english": "agreement",
    "type": "n",
    "pronounce": "ə'gri:mənt",
    "vietnamese": "sự đồng ý, tán thành; hiệp định, hợp đồng"
  },
  {
    "english": "ahead",
    "type": "adv",
    "pronounce": "ə'hed",
    "vietnamese": "trước, về phía trước"
  },
  {
    "english": "aid",
    "type": "n, v",
    "pronounce": "eid",
    "vietnamese": "sự giúp đỡ; thêm vào, phụ vào"
  },
  {
    "english": "aim",
    "type": "n, v",
    "pronounce": "eim",
    "vietnamese": "sự nhắm (bắn), mục tiêu, ý định; nhắm, tập trung, hướng vào"
  },
  {
    "english": "air",
    "type": "n",
    "pronounce": "eə",
    "vietnamese": "không khí, bầu không khí, không gian"
  },
  {
    "english": "aircraft",
    "type": "n",
    "pronounce": "eəkrɑ:ft",
    "vietnamese": "máy bay, khí cầu"
  },
  {
    "english": "airport",
    "type": "n",
    "pronounce": "",
    "vietnamese": "sân bay, phi trường "
  },
  {
    "english": "alarm",
    "type": "n, v",
    "pronounce": "ə'lɑ:m",
    "vietnamese": "báo động, báo nguy"
  },
  {
    "english": "alarmed",
    "type": "adj",
    "pronounce": "ə'lɑ:m",
    "vietnamese": "báo động"
  },
  {
    "english": "alarming",
    "type": "adj",
    "pronounce": "ə'lɑ:miɳ",
    "vietnamese": "làm lo sợ, làm hốt hoảng, làm sợ hãi"
  },
  {
    "english": "alcohol",
    "type": "n",
    "pronounce": "ælkəhɔl",
    "vietnamese": "rượu cồn"
  },
  {
    "english": "alcoholic",
    "type": "adj, n",
    "pronounce": ",ælkə'hɔlik",
    "vietnamese": "rượu; người nghiện rượu"
  },
  {
    "english": "alive",
    "type": "adj",
    "pronounce": "ə'laiv",
    "vietnamese": "sống, vẫn còn sống, còn tồn tại"
  },
  {
    "english": "all",
    "type": "pron, adv",
    "pronounce": "ɔ:l",
    "vietnamese": "tất cả"
  },
  {
    "english": "all right",
    "type": "adj, adv",
    "pronounce": "ɔ:l'rait",
    "vietnamese": "tốt, ổn, khỏe mạnh; được"
  },
  {
    "english": "allied",
    "type": "adj",
    "pronounce": "ə'laid",
    "vietnamese": "liên minh, đồng minh, thông gia"
  },
  {
    "english": "allow",
    "type": "v",
    "pronounce": "ə'lau",
    "vietnamese": "cho phép, để cho"
  },
  {
    "english": "ally",
    "type": "n, v",
    "pronounce": "æli",
    "vietnamese": "nước đồng minh, liên minh; liên kết, kết thông gia"
  },
  {
    "english": "almost",
    "type": "adv",
    "pronounce": "ɔ:lmoust",
    "vietnamese": "hầu như, gần như"
  },
  {
    "english": "alone",
    "type": "adj, adv",
    "pronounce": "ə'loun",
    "vietnamese": "cô đơn, một mình"
  },
  {
    "english": "along",
    "type": "prep, adv",
    "pronounce": "ə'lɔɳ",
    "vietnamese": "dọc theo, theo; theo chiều dài,suốt theo"
  },
  {
    "english": "alongside",
    "type": "prep, adv",
    "pronounce": "ə'lɔɳ'said",
    "vietnamese": "sát cạnh, kế bên, dọc theo"
  },
  {
    "english": "aloud",
    "type": "adv",
    "pronounce": "ə'laud",
    "vietnamese": "lớn tiếng, to tiếng"
  },
  {
    "english": "alphabet",
    "type": "n",
    "pronounce": "ælfəbit",
    "vietnamese": "bảng chữ cái, bước đầu, điều cơ bản"
  },
  {
    "english": "alphabetical",
    "type": "adj",
    "pronounce": ",æflə'betikl",
    "vietnamese": "thuộc bảng chứ cái"
  },
  {
    "english": "alphabetically",
    "type": "adv",
    "pronounce": ",ælfə'betikəli",
    "vietnamese": "theo thứ tự abc"
  },
  {
    "english": "already",
    "type": "adv",
    "pronounce": "ɔ:l'redi",
    "vietnamese": "đã, rồi, đã... rồi"
  },
  {
    "english": "also",
    "type": "adv",
    "pronounce": "ɔ:lsou",
    "vietnamese": "cũng, cũng vậy, cũng thế"
  },
  {
    "english": "alter",
    "type": "v",
    "pronounce": "ɔ:ltə",
    "vietnamese": "thay đổi, biến đổi, sửa đổi"
  },
  {
    "english": "alternative",
    "type": "n, adj",
    "pronounce": "ɔ:l'tə:nətiv",
    "vietnamese": "sự lựa chọn; lựa chọn"
  },
  {
    "english": "alternatively",
    "type": "adv",
    "pronounce": "",
    "vietnamese": "như một sự lựa chọn"
  },
  {
    "english": "although",
    "type": "conj",
    "pronounce": "ɔ:l'ðou",
    "vietnamese": "mặc dù, dẫu cho"
  },
  {
    "english": "altogether",
    "type": "adv",
    "pronounce": ",ɔ:ltə'geðə",
    "vietnamese": "hoàn toàn, hầu như; nói chung"
  },
  {
    "english": "always",
    "type": "adv",
    "pronounce": "ɔ:lwəz",
    "vietnamese": "luôn luôn"
  },
  {
    "english": "amaze",
    "type": "v",
    "pronounce": "ə'meiz",
    "vietnamese": "làm ngạc nhiên, làm sửng sốt"
  },
  {
    "english": "amazed",
    "type": "adj",
    "pronounce": "ə'meiz",
    "vietnamese": "kinh ngạc, sửng sốt"
  },
  {
    "english": "amazing",
    "type": "adj",
    "pronounce": "ə'meiziɳ",
    "vietnamese": "kinh ngạc, sửng sốt "
  },
  {
    "english": "ambition",
    "type": "n",
    "pronounce": "æm'biʃn",
    "vietnamese": "hoài bão, khát vọng"
  },
  {
    "english": "ambulance",
    "type": "n",
    "pronounce": "æmbjuləns",
    "vietnamese": "xe cứu thương, xe cấp cứu"
  },
  {
    "english": "among, amongst",
    "type": "prep",
    "pronounce": "ə'mʌɳ",
    "vietnamese": "giữa, ở giữa"
  },
  {
    "english": "amount",
    "type": "n, v",
    "pronounce": "ə'maunt",
    "vietnamese": "số lượng, số nhiều; lên tới(money)"
  },
  {
    "english": "amuse",
    "type": "v",
    "pronounce": "ə'mju:z",
    "vietnamese": "làm cho vui, thích, làm buồn cười"
  },
  {
    "english": "amused",
    "type": "adj",
    "pronounce": "ə'mju:zd",
    "vietnamese": "vui thích"
  },
  {
    "english": "amusing",
    "type": "adj",
    "pronounce": "ə'mju:ziɳ",
    "vietnamese": "vui thích"
  },
  {
    "english": "analyse, analyze",
    "type": "v",
    "pronounce": "ænəlaiz",
    "vietnamese": "phân tích"
  },
  {
    "english": "analysis",
    "type": "n",
    "pronounce": "ə'næləsis",
    "vietnamese": "sự phân tích"
  },
  {
    "english": "ancient",
    "type": "adj",
    "pronounce": "einʃənt",
    "vietnamese": "xưa, cổ"
  },
  {
    "english": "and",
    "type": "conj",
    "pronounce": "ænd, ənd, ən",
    "vietnamese": "và"
  },
  {
    "english": "anger",
    "type": "n",
    "pronounce": "æɳgə",
    "vietnamese": "sự tức giận, sự giận dữ"
  },
  {
    "english": "angle",
    "type": "n",
    "pronounce": "æɳgl",
    "vietnamese": "góc"
  },
  {
    "english": "angrily",
    "type": "adv",
    "pronounce": "æɳgrili",
    "vietnamese": "tức giận, giận dữ"
  },
  {
    "english": "angry",
    "type": "adj",
    "pronounce": "æɳgri",
    "vietnamese": "giận, tức giận"
  },
  {
    "english": "animal",
    "type": "n",
    "pronounce": "æniməl",
    "vietnamese": "động vật, thú vật"
  },
  {
    "english": "ankle",
    "type": "n",
    "pronounce": "æɳkl",
    "vietnamese": "mắt cá chân"
  },
  {
    "english": "anniversary",
    "type": "n",
    "pronounce": ",æni'və:səri",
    "vietnamese": "ngày, lễ kỉ niệm"
  },
  {
    "english": "announce",
    "type": "v",
    "pronounce": "ə'nauns",
    "vietnamese": "báo, thông báo"
  },
  {
    "english": "annoy",
    "type": "v",
    "pronounce": "ə'nɔi",
    "vietnamese": "chọc tức, làm bực mình; làm phiền, quẫy nhiễu"
  },
  {
    "english": "annoyed",
    "type": "adj",
    "pronounce": "ə'nɔid",
    "vietnamese": "bị khó chịu, bực mình, bị quấy rầy"
  },
  {
    "english": "annoying",
    "type": "adj",
    "pronounce": "ə'nɔiiɳ",
    "vietnamese": "chọc tức, làm bực mình; làm phiền, quấy nhiễu"
  },
  {
    "english": "annual",
    "type": "adj",
    "pronounce": "ænjuəl",
    "vietnamese": "hàng năm, từng năm"
  },
  {
    "english": "annually",
    "type": "adv",
    "pronounce": "ænjuəli",
    "vietnamese": "hàng năm, từng năm"
  },
  {
    "english": "another",
    "type": "det, pron",
    "pronounce": "ə'nʌðə",
    "vietnamese": "khác"
  },
  {
    "english": "answer",
    "type": "n, v",
    "pronounce": "ɑ:nsə",
    "vietnamese": "sự trả lời; trả lời"
  },
  {
    "english": "anti",
    "type": "prefix",
    "pronounce": "",
    "vietnamese": "chống lại"
  },
  {
    "english": "anticipate",
    "type": "v",
    "pronounce": "æn'tisipeit",
    "vietnamese": "thấy trước, chặn trước, lường trước"
  },
  {
    "english": "anxiety",
    "type": "n",
    "pronounce": "æɳ'zaiəti",
    "vietnamese": "mối lo âu, sự lo lắng"
  },
  {
    "english": "anxious",
    "type": "adj",
    "pronounce": "æɳkʃəs",
    "vietnamese": "lo âu, lo lắng, băn khoăn"
  },
  {
    "english": "anxiously",
    "type": "adv",
    "pronounce": "æɳkʃəsli",
    "vietnamese": "lo âu, lo lắng, băn khoăn "
  },
  {
    "english": "any",
    "type": "detpron, adv",
    "pronounce": "",
    "vietnamese": "một người, vật nào đó; bất cứ; một chút nào, tí nào"
  },
  {
    "english": "anyone (anybod)",
    "type": "pron",
    "pronounce": "eniwʌn",
    "vietnamese": "người nào, bất cứ ai"
  },
  {
    "english": "anything",
    "type": "pron",
    "pronounce": "eniθiɳ",
    "vietnamese": "việc gì, vật gì; bất cứ việc gì, vật gì"
  },
  {
    "english": "anyway",
    "type": "adv",
    "pronounce": "eniwei",
    "vietnamese": "thế nào cũng được, dù sấo chăng nữa"
  },
  {
    "english": "anywhere",
    "type": "adv",
    "pronounce": "eniweə",
    "vietnamese": "bất cứ chỗ nào, bất cứ nơi đâu"
  },
  {
    "english": "apart",
    "type": "adv",
    "pronounce": "ə'pɑ:t",
    "vietnamese": "về một bên, qua một bên"
  },
  {
    "english": "apart from",
    "type": "prep",
    "pronounce": "ə'pɑ:t",
    "vietnamese": "ngoài... ra"
  },
  {
    "english": "apart from, aside from",
    "type": "prep",
    "pronounce": "",
    "vietnamese": "ngoài ra"
  },
  {
    "english": "apartment",
    "type": "n",
    "pronounce": "ə'pɑ:tmənt",
    "vietnamese": "căn phòng, căn buồng"
  },
  {
    "english": "apologize",
    "type": "v",
    "pronounce": "ə'pɔlədʤaiz",
    "vietnamese": "xin lỗi, tạ lỗi"
  },
  {
    "english": "apparent",
    "type": "adj",
    "pronounce": "ə'pærənt",
    "vietnamese": "rõ ràng, rành mạch; bề ngoài, có vẻ"
  },
  {
    "english": "apparently",
    "type": "adv",
    "pronounce": "",
    "vietnamese": "nhìn bên ngoài, hình như"
  },
  {
    "english": "appeal",
    "type": "n, v",
    "pronounce": "ə'pi:l",
    "vietnamese": "sự kêu gọi, lời kêu gọi; kêu gọi, cầu khẩn"
  },
  {
    "english": "appear",
    "type": "v",
    "pronounce": "ə'piə",
    "vietnamese": "xuất hiện, hiện ra, trình diện"
  },
  {
    "english": "appearance",
    "type": "n",
    "pronounce": "ə'piərəns",
    "vietnamese": "sự xuất hiện, sự trình diện"
  },
  {
    "english": "apple",
    "type": "n",
    "pronounce": "æpl",
    "vietnamese": "quả táo"
  },
  {
    "english": "application",
    "type": "n",
    "pronounce": ",æpli'keiʃn",
    "vietnamese": "sự gắn vào, vật gắn vào; sự chuyên cần, chuyên tâm"
  },
  {
    "english": "apply",
    "type": "v",
    "pronounce": "ə'plai",
    "vietnamese": "gắn vào, ghép vào, áp dụng vào"
  },
  {
    "english": "appoint",
    "type": "v",
    "pronounce": "ə'pɔint",
    "vietnamese": "bổ nhiệm, chỉ định, chọn"
  },
  {
    "english": "appointment",
    "type": "n",
    "pronounce": "ə'pɔintmənt",
    "vietnamese": "sự bổ nhiệm, người được bổ nhiệm"
  },
  {
    "english": "appreciate",
    "type": "v",
    "pronounce": "ə'pri:ʃieit",
    "vietnamese": "thấy rõ; nhận thức"
  },
  {
    "english": "approach",
    "type": "v, n",
    "pronounce": "ə'proutʃ",
    "vietnamese": "đến gần, lại gần; sự đến gần, sự lại gần"
  },
  {
    "english": "appropriate (to, for)",
    "type": "adj",
    "pronounce": "ə'proupriit",
    "vietnamese": "thích hợp, thích đáng"
  },
  {
    "english": "approval",
    "type": "n",
    "pronounce": "ə'pru:vəl",
    "vietnamese": "sự tán thành, đồng ý, sự chấp thuận"
  },
  {
    "english": "approve",
    "type": "of, v",
    "pronounce": "ə'pru:v",
    "vietnamese": "tán thành, đồng ý, chấp thuận"
  },
  {
    "english": "approving",
    "type": "adj",
    "pronounce": "ə'pru:viɳ",
    "vietnamese": "tán thành, đồng ý, chấp thuận"
  },
  {
    "english": "approximate",
    "type": "adj, to",
    "pronounce": "ə'prɔksimit",
    "vietnamese": "giống với, giống hệt với"
  },
  {
    "english": "approximately",
    "type": "adv",
    "pronounce": "ə'prɔksimitli",
    "vietnamese": "khoảng chừng, độ chừng "
  },
  {
    "english": "April (abbr Apr)",
    "type": "n",
    "pronounce": "eiprəl",
    "vietnamese": "tháng Tư"
  },
  {
    "english": "area",
    "type": "n",
    "pronounce": "eəriə",
    "vietnamese": "diện tích, bề mặt"
  },
  {
    "english": "argue",
    "type": "v",
    "pronounce": "ɑ:gju:",
    "vietnamese": "chứng tỏ, chỉ rõ"
  },
  {
    "english": "argument",
    "type": "n",
    "pronounce": "ɑ:gjumənt",
    "vietnamese": "lý lẽ"
  },
  {
    "english": "arise",
    "type": "v",
    "pronounce": "ə'raiz",
    "vietnamese": "xuất hiện, nảy ra, nảy sinh ra"
  },
  {
    "english": "arm",
    "type": "n, v",
    "pronounce": "ɑ:m",
    "vietnamese": "cánh tay; vũ trang, trang bị (vũ khí)"
  },
  {
    "english": "armed",
    "type": "adj",
    "pronounce": "ɑ:md",
    "vietnamese": "vũ trang"
  },
  {
    "english": "arms",
    "type": "n",
    "pronounce": "",
    "vietnamese": "vũ khí, binh giới, binh khí"
  },
  {
    "english": "army",
    "type": "n",
    "pronounce": "ɑ:mi",
    "vietnamese": "quân đội"
  },
  {
    "english": "around",
    "type": "adv, prep",
    "pronounce": "ə'raund",
    "vietnamese": "xung quanh, vòng quanh"
  },
  {
    "english": "arrange",
    "type": "v",
    "pronounce": "ə'reindʤ",
    "vietnamese": "sắp xếp, sắp đặt, sửa soạn"
  },
  {
    "english": "arrangement",
    "type": "n",
    "pronounce": "ə'reindʤmənt",
    "vietnamese": "sự sắp xếp, sắp đặt, sự sửa soạn"
  },
  {
    "english": "arrest",
    "type": "v, n",
    "pronounce": "ə'rest",
    "vietnamese": "bắt giữ, sự bắt giữ"
  },
  {
    "english": "arrival",
    "type": "n",
    "pronounce": "ə'raivəl",
    "vietnamese": "sự đến, sự tới nơi"
  },
  {
    "english": "arrive (at, in)",
    "type": "v",
    "pronounce": "ə'raiv",
    "vietnamese": "đến, tới nơi"
  },
  {
    "english": "arrow",
    "type": "n",
    "pronounce": "ærou",
    "vietnamese": "tên, mũi tên"
  },
  {
    "english": "art",
    "type": "n",
    "pronounce": "ɑ:t",
    "vietnamese": "nghệ thuật, mỹ thuật"
  },
  {
    "english": "article",
    "type": "n",
    "pronounce": "ɑ:tikl",
    "vietnamese": "bài báo, đề mục"
  },
  {
    "english": "artificial",
    "type": "adj",
    "pronounce": ",ɑ:ti'fiʃəl",
    "vietnamese": "nhân tạo"
  },
  {
    "english": "artificially",
    "type": "adv",
    "pronounce": ",ɑ:ti'fiʃəli",
    "vietnamese": "nhân tạo"
  },
  {
    "english": "artist",
    "type": "n",
    "pronounce": "ɑ:tist",
    "vietnamese": "nghệ sĩ"
  },
  {
    "english": "artistic",
    "type": "adj",
    "pronounce": "ɑ:'tistik",
    "vietnamese": "thuộc nghệ thuật, thuộc mỹ thuật"
  },
  {
    "english": "as",
    "type": "adv, conj, prep",
    "pronounce": "æz, əz",
    "vietnamese": "như (as you know...)"
  },
  {
    "english": "as well",
    "type": "",
    "pronounce": "",
    "vietnamese": "cũng, cũng như"
  },
  {
    "english": "ashamed",
    "type": "adj",
    "pronounce": "ə'ʃeimd",
    "vietnamese": "ngượng, xấu hổ"
  },
  {
    "english": "aside",
    "type": "adv",
    "pronounce": "ə'said",
    "vietnamese": "về một bên, sang một bên. aside from: ngoài ra, trư ra"
  },
  {
    "english": "ask",
    "type": "v",
    "pronounce": "ɑ:sk",
    "vietnamese": "hỏi"
  },
  {
    "english": "asleep",
    "type": "adj",
    "pronounce": "ə'sli:p",
    "vietnamese": "ngủ, đang ngủ. fall asleep ngủ thiếp đi"
  },
  {
    "english": "aspect",
    "type": "n",
    "pronounce": "æspekt",
    "vietnamese": "vẻ bề ngoài, diện mạo"
  },
  {
    "english": "assist",
    "type": "v",
    "pronounce": "ə'sist",
    "vietnamese": "giúp, giúp đỡ; tham dự, có mặt"
  },
  {
    "english": "assistance",
    "type": "n",
    "pronounce": "ə'sistəns",
    "vietnamese": "sự giúp đỡ "
  },
  {
    "english": "assistant",
    "type": "n, adj",
    "pronounce": "ə'sistənt",
    "vietnamese": "người giúp đỡ, người phụ tá; giúp đỡ"
  },
  {
    "english": "associate",
    "type": "v",
    "pronounce": "ə'souʃiit",
    "vietnamese": "kết giao, liên kết, kết hợp, cho cộng tác. associated with liên kết với"
  },
  {
    "english": "association",
    "type": "n",
    "pronounce": "ə,sousi'eiʃn",
    "vietnamese": "sự kết hợp, sự liên kết"
  },
  {
    "english": "assume",
    "type": "v",
    "pronounce": "ə'sju:m",
    "vietnamese": "mang, khoác, có, lấy (cái vẻ, tính chất...)"
  },
  {
    "english": "assure",
    "type": "v",
    "pronounce": "ə'ʃuə",
    "vietnamese": "đảm bảo, cấm đoán"
  },
  {
    "english": "atmosphere",
    "type": "n",
    "pronounce": "ætməsfiə",
    "vietnamese": "khí quyển"
  },
  {
    "english": "atom",
    "type": "n",
    "pronounce": "ætəm",
    "vietnamese": "nguyên tử"
  },
  {
    "english": "attach",
    "type": "v",
    "pronounce": "ə'tætʃ",
    "vietnamese": "gắn, dán, trói, buộc"
  },
  {
    "english": "attached",
    "type": "adj",
    "pronounce": "",
    "vietnamese": "gắn bó"
  },
  {
    "english": "attack",
    "type": "n, v",
    "pronounce": "ə'tæk",
    "vietnamese": "sự tấn công, sự công kích; tấn công, công kích"
  },
  {
    "english": "attempt",
    "type": "n, v",
    "pronounce": "ə'tempt",
    "vietnamese": "sự cố gắng, sự thử; cố gắng, thử"
  },
  {
    "english": "attempted",
    "type": "adj",
    "pronounce": "ə'temptid",
    "vietnamese": "cố gắng, thử"
  },
  {
    "english": "attend",
    "type": "v",
    "pronounce": "ə'tend",
    "vietnamese": "dự, có mặt"
  },
  {
    "english": "attention",
    "type": "n",
    "pronounce": "ə'tenʃn",
    "vietnamese": "sự chú ý"
  },
  {
    "english": "attitude",
    "type": "n",
    "pronounce": "ætitju:d",
    "vietnamese": "thái độ, quan điểm"
  },
  {
    "english": "attorney",
    "type": "n",
    "pronounce": "ə'tə:ni",
    "vietnamese": "người được ủy quyền"
  },
  {
    "english": "attract",
    "type": "v",
    "pronounce": "ə'trækt",
    "vietnamese": "hút; thu hút, hấp dẫn"
  },
  {
    "english": "attraction",
    "type": "n",
    "pronounce": "ə'trækʃn",
    "vietnamese": "sự hút, sức hút"
  },
  {
    "english": "attractive",
    "type": "adj",
    "pronounce": "ə'træktiv",
    "vietnamese": "hút, thu hút, có duyên, lôi cuốn"
  },
  {
    "english": "audience",
    "type": "n",
    "pronounce": "ɔ:djəns",
    "vietnamese": "thính, khan giả"
  },
  {
    "english": "August",
    "type": "(abbr Aug)n",
    "pronounce": "ɔ:gəst - ɔ:'gʌst",
    "vietnamese": "tháng Tám"
  },
  {
    "english": "aunt",
    "type": "n",
    "pronounce": "ɑ:nt",
    "vietnamese": "cô, dì"
  },
  {
    "english": "author",
    "type": "n",
    "pronounce": "ɔ:θə",
    "vietnamese": "tác giả"
  },
  {
    "english": "authority",
    "type": "n",
    "pronounce": "ɔ:'θɔriti",
    "vietnamese": "uy quyền, quyền lực"
  },
  {
    "english": "automatic",
    "type": "adj",
    "pronounce": ",ɔ:tə'mætik",
    "vietnamese": "tự động"
  },
  {
    "english": "automatically",
    "type": "adv",
    "pronounce": "",
    "vietnamese": "một cách tự động"
  },
  {
    "english": "autumn",
    "type": "n",
    "pronounce": "ɔ:təm",
    "vietnamese": "mùa thu (US: mùa thu là fall)"
  },
  {
    "english": "available",
    "type": "adj",
    "pronounce": "ə'veiləbl",
    "vietnamese": "có thể dùng được, có giá trị, hiệu lực"
  },
  {
    "english": "average",
    "type": "adj, n",
    "pronounce": "ævəridʤ",
    "vietnamese": "trung bình, số trung bình, mức trung bình "
  },
  {
    "english": "avoid",
    "type": "v",
    "pronounce": "ə'vɔid",
    "vietnamese": "tránh, tránh xa"
  },
  {
    "english": "awake",
    "type": "adj",
    "pronounce": "ə'weik",
    "vietnamese": "đánh thức, làm thức dậy"
  },
  {
    "english": "award",
    "type": "n, v",
    "pronounce": "ə'wɔ:d",
    "vietnamese": "phần thưởng; tặng, thưởng"
  },
  {
    "english": "aware",
    "type": "adj",
    "pronounce": "ə'weə",
    "vietnamese": "biết, nhận thức, nhận thức thấy"
  },
  {
    "english": "away",
    "type": "adv",
    "pronounce": "ə'wei",
    "vietnamese": "xa, xa cách, rời xa, đi xa"
  },
  {
    "english": "awful",
    "type": "adj",
    "pronounce": "ɔ:ful",
    "vietnamese": "oai nghiêm, dễ sợ"
  },
  {
    "english": "awfully",
    "type": "adv",
    "pronounce": "",
    "vietnamese": "tàn khốc, khủng khiếp"
  },
  {
    "english": "awkward",
    "type": "adj",
    "pronounce": "ɔ:kwəd",
    "vietnamese": "vụng về, lung túng"
  },
  {
    "english": "awkwardly",
    "type": "adv",
    "pronounce": "",
    "vietnamese": "vụng về, lung túng"
  },
  {
    "english": "back",
    "type": "n, adj, adv, v",
    "pronounce": "bæk",
    "vietnamese": "lưng, sau, về phía sau, trở lại"
  },
  {
    "english": "background",
    "type": "n",
    "pronounce": "bækgraund",
    "vietnamese": "phía sau; nền"
  },
  {
    "english": "backward",
    "type": "adj",
    "pronounce": "bækwəd",
    "vietnamese": "về phía sau, lùi lại"
  },
  {
    "english": "backwards",
    "type": "adv",
    "pronounce": "bækwədz",
    "vietnamese": "ngược"
  },
  {
    "english": "bacteria",
    "type": "n",
    "pronounce": "bæk'tiəriəm",
    "vietnamese": "vi khuẩn"
  },
  {
    "english": "bad",
    "type": "adj",
    "pronounce": "bæd",
    "vietnamese": "xấu, tồi. go bad bẩn thỉu, thối, hỏng"
  },
  {
    "english": "badly",
    "type": "adv",
    "pronounce": "bædli",
    "vietnamese": "xấu, tồi"
  },
  {
    "english": "bad-tempered",
    "type": "adj",
    "pronounce": "bæd'tempəd",
    "vietnamese": "xấu tính, dễ nổi cáu"
  },
  {
    "english": "bag",
    "type": "n",
    "pronounce": "bæg",
    "vietnamese": "bao, túi, cặp xách"
  },
  {
    "english": "baggage",
    "type": "n",
    "pronounce": "bædidʤ",
    "vietnamese": "hành lý"
  },
  {
    "english": "bake",
    "type": "v",
    "pronounce": "beik",
    "vietnamese": "nung, nướng bằng lò"
  },
  {
    "english": "balance",
    "type": "n, v",
    "pronounce": "bæləns",
    "vietnamese": "cái cân; làm cho cân bằng, tương xứng"
  },
  {
    "english": "ball",
    "type": "n",
    "pronounce": "bɔ:l",
    "vietnamese": "quả bóng"
  },
  {
    "english": "ban",
    "type": "v, n",
    "pronounce": "bæn",
    "vietnamese": "cấm, cấm chỉ; sự cấm"
  },
  {
    "english": "band",
    "type": "n",
    "pronounce": "bænd",
    "vietnamese": "băng, đai, nẹp"
  },
  {
    "english": "bandage",
    "type": "n, v",
    "pronounce": "bændidʤ",
    "vietnamese": "dải băng; băng bó"
  },
  {
    "english": "bank",
    "type": "n",
    "pronounce": "bæɳk",
    "vietnamese": "bờ (sông...) , đê"
  },
  {
    "english": "bar",
    "type": "n",
    "pronounce": "bɑ:",
    "vietnamese": "quán bán rượu"
  },
  {
    "english": "bargain",
    "type": "n",
    "pronounce": "bɑ:gin",
    "vietnamese": "sự mặc cả, sự giao kèo mua bán"
  },
  {
    "english": "barrier",
    "type": "n",
    "pronounce": "bæriə",
    "vietnamese": "đặt chướng ngại vật"
  },
  {
    "english": "base",
    "type": "n, v",
    "pronounce": "beis",
    "vietnamese": "cơ sở, cơ bản, nền móng; đặt tên, đặt cơ sở trên cái gì. based on dựa trên"
  },
  {
    "english": "basic",
    "type": "adj",
    "pronounce": "beisik",
    "vietnamese": "cơ bản, cơ sở"
  },
  {
    "english": "basis",
    "type": "n",
    "pronounce": "beisis",
    "vietnamese": "nền tảng, cơ sở"
  },
  {
    "english": "bath",
    "type": "n",
    "pronounce": "bɑ:θ",
    "vietnamese": "sự tắm"
  },
  {
    "english": "bathroom",
    "type": "n",
    "pronounce": "",
    "vietnamese": "buồng tắm, nhà vệ sinh"
  },
  {
    "english": "battery",
    "type": "n",
    "pronounce": "bætəri",
    "vietnamese": "pin, ắc quy"
  },
  {
    "english": "battle",
    "type": "n",
    "pronounce": "bætl",
    "vietnamese": "trận đánh, chiến thuật"
  },
  {
    "english": "bay",
    "type": "n",
    "pronounce": "bei",
    "vietnamese": "gian (nhà), nhịp (cầu), chuồng (ngựa); bays: vòng nguyệt quế, vịnh"
  },
  {
    "english": "be sick",
    "type": "",
    "pronounce": "",
    "vietnamese": "bị ốm"
  },
  {
    "english": "beach",
    "type": "n",
    "pronounce": "bi:tʃ",
    "vietnamese": "bãi biển"
  },
  {
    "english": "beak",
    "type": "n",
    "pronounce": "bi:k",
    "vietnamese": "mỏ chim"
  },
  {
    "english": "bear",
    "type": "v",
    "pronounce": "beə",
    "vietnamese": "mang, cầm, vác, đeo, ôm"
  },
  {
    "english": "beard",
    "type": "n",
    "pronounce": "biəd",
    "vietnamese": "râu"
  },
  {
    "english": "beat",
    "type": "n, v",
    "pronounce": "bi:t",
    "vietnamese": "tiếng đập, sự đập; đánh đập, đấm"
  },
  {
    "english": "beautiful",
    "type": "adj",
    "pronounce": "bju:təful",
    "vietnamese": "đẹp"
  },
  {
    "english": "beautifully",
    "type": "adv",
    "pronounce": "bju:təfuli",
    "vietnamese": "tốt đẹp, đáng hài lòng"
  },
  {
    "english": "beauty",
    "type": "n",
    "pronounce": "bju:ti",
    "vietnamese": "vẻ đẹp, cái đẹp; người đẹp"
  },
  {
    "english": "because",
    "type": "conj",
    "pronounce": "bi'kɔz",
    "vietnamese": "bởi vì, vì. because of prep. vì, do bởi"
  },
  {
    "english": "become",
    "type": "v",
    "pronounce": "bi'kʌm",
    "vietnamese": "trở thành, trở nên"
  },
  {
    "english": "bed",
    "type": "n",
    "pronounce": "bed",
    "vietnamese": "cái giường"
  },
  {
    "english": "bedroom",
    "type": "n",
    "pronounce": "bedrum",
    "vietnamese": "phòng ngủ"
  },
  {
    "english": "beef",
    "type": "n",
    "pronounce": "bi:f",
    "vietnamese": "thịt bò"
  },
  {
    "english": "beer",
    "type": "n",
    "pronounce": "bi:ə",
    "vietnamese": "rượu bia"
  },
  {
    "english": "before",
    "type": "prep, conj, adv",
    "pronounce": "bi'fɔ:",
    "vietnamese": "trước, đằng trước"
  },
  {
    "english": "begin",
    "type": "v",
    "pronounce": "bi'gin",
    "vietnamese": "bắt đầu, khởi đầu"
  },
  {
    "english": "beginning",
    "type": "n",
    "pronounce": "bi'giniɳ",
    "vietnamese": "phần đầu, lúc bắt đầu, lúc khởi đầu"
  },
  {
    "english": "behalf",
    "type": "n",
    "pronounce": "bi:hɑ:f",
    "vietnamese": "sự thay mặt. on behalf of sb thay mặt cho ai, nhân danh ai"
  },
  {
    "english": "behalf, on sb’s behalf",
    "type": "",
    "pronounce": "",
    "vietnamese": "nhân danh cá nhân ai"
  },
  {
    "english": "behave",
    "type": "v",
    "pronounce": "bi'heiv",
    "vietnamese": "đối xử, ăn ở, cư xử"
  },
  {
    "english": "behaviour, behavior",
    "type": "n",
    "pronounce": "",
    "vietnamese": "thái độ, cách đối xử; cách cư xử, >cách ăn ở; tư cách đạo đức"
  },
  {
    "english": "behind",
    "type": "prep, adv",
    "pronounce": "bi'haind",
    "vietnamese": "sau, ở đằng sau"
  },
  {
    "english": "belief",
    "type": "n",
    "pronounce": "bi'li:f",
    "vietnamese": "lòng tin, đức tin, sự tin tưởng"
  },
  {
    "english": "believe",
    "type": "v",
    "pronounce": "bi'li:v",
    "vietnamese": "tin, tin tưởng"
  },
  {
    "english": "bell",
    "type": "n",
    "pronounce": "bel",
    "vietnamese": "cái chuông, tiếng chuông"
  },
  {
    "english": "belong",
    "type": "v",
    "pronounce": "bi'lɔɳ",
    "vietnamese": "thuộc về, của, thuộc quyền sở hữu"
  },
  {
    "english": "below",
    "type": "prep, adv",
    "pronounce": "bi'lou",
    "vietnamese": "ở dưới, dưới thấp, phía dưới"
  },
  {
    "english": "belt",
    "type": "n",
    "pronounce": "belt",
    "vietnamese": "dây lưng, thắt lưng"
  },
  {
    "english": "bend",
    "type": "v, n",
    "pronounce": "bentʃ",
    "vietnamese": "chỗ rẽ, chỗ uốn; khuỷu tay; cúi >xuống, uốn cong"
  },
  {
    "english": "beneath",
    "type": "prep, adv",
    "pronounce": "bi'ni:θ",
    "vietnamese": "ở dưới, dưới thấp"
  },
  {
    "english": "benefit",
    "type": "n, v",
    "pronounce": "benifit",
    "vietnamese": "lợi, lợi ích; giúp ích, làm lợi cho"
  },
  {
    "english": "bent",
    "type": "adj",
    "pronounce": "bent",
    "vietnamese": "khiếu, sở thích, khuynh hướng"
  },
  {
    "english": "beside",
    "type": "prep",
    "pronounce": "bi'said",
    "vietnamese": "bên cạnh, so với"
  },
  {
    "english": "bet",
    "type": "v, n",
    "pronounce": "bet",
    "vietnamese": "đánh cuộc, cá cược; sự đánh >cuộc"
  },
  {
    "english": "better, best",
    "type": "adj",
    "pronounce": "betə, best",
    "vietnamese": "tốt hơn, tốt nhất"
  },
  {
    "english": "betting",
    "type": "n",
    "pronounce": "beting",
    "vietnamese": "sự đánh cuộc"
  },
  {
    "english": "between",
    "type": "prep, adv",
    "pronounce": "bi'twi:n",
    "vietnamese": "giữa, ở giữa"
  },
  {
    "english": "beyond",
    "type": "prep, adv",
    "pronounce": "bi'jɔnd",
    "vietnamese": "ở >xa, phía bên kia"
  },
  {
    "english": "bicycle (bike)",
    "type": "n",
    "pronounce": "baisikl",
    "vietnamese": "xe đạp"
  },
  {
    "english": "bid",
    "type": "v, n",
    "pronounce": "bid",
    "vietnamese": "đặt giá, trả giá; sự đặt giá, sự trả >giá"
  },
  {
    "english": "big",
    "type": "adj",
    "pronounce": "big",
    "vietnamese": "to, lớn"
  },
  {
    "english": "bill",
    "type": "n",
    "pronounce": "bil",
    "vietnamese": "hóa đơn, giấy bạc"
  },
  {
    "english": "bin",
    "type": "n",
    "pronounce": "bin",
    "vietnamese": "thùng, thùng đựng rượu"
  },
  {
    "english": "biology",
    "type": "n",
    "pronounce": "bai'ɔlədʤi",
    "vietnamese": "sinh vật học"
  },
  {
    "english": "bird",
    "type": "n",
    "pronounce": "bə:d",
    "vietnamese": "chim"
  },
  {
    "english": "birth",
    "type": "n",
    "pronounce": "bə:θ",
    "vietnamese": "sự ra đời, sự sinh đẻ"
  },
  {
    "english": "birthday",
    "type": "n",
    "pronounce": "bə:θdei",
    "vietnamese": "ngày sinh, sinh nhật"
  },
  {
    "english": "biscuit",
    "type": "n",
    "pronounce": "biskit",
    "vietnamese": "bánh quy"
  },
  {
    "english": "bit",
    "type": "n",
    "pronounce": "bit",
    "vietnamese": "miếng, mảnh. a bit một chút, một t"
  },
  {
    "english": "bite",
    "type": "v, n",
    "pronounce": "bait",
    "vietnamese": "cắn, ngoạm; sự cắn, sự ngoạm"
  },
  {
    "english": "bitter",
    "type": "adj",
    "pronounce": "bitə",
    "vietnamese": "đắng; đắng cay, chua xót"
  },
  {
    "english": "bitterly",
    "type": "adv",
    "pronounce": "bitəli",
    "vietnamese": "đắng, đắng cay, chua xót"
  },
  {
    "english": "black",
    "type": "adj, n",
    "pronounce": "blæk",
    "vietnamese": "đen; màu đen"
  },
  {
    "english": "bowl",
    "type": "n",
    "pronounce": "boul",
    "vietnamese": "cái bát"
  },
  {
    "english": "box",
    "type": "n",
    "pronounce": "bɔks",
    "vietnamese": "hộp, thùng"
  },
  {
    "english": "boy",
    "type": "n",
    "pronounce": "bɔi",
    "vietnamese": "con trai, thiếu niên"
  },
  {
    "english": "boyfriend",
    "type": "n",
    "pronounce": "",
    "vietnamese": "bạn trai"
  },
  {
    "english": "brain",
    "type": "n",
    "pronounce": "brein",
    "vietnamese": "óc não; đầu óc, trí não"
  },
  {
    "english": "branch",
    "type": "n",
    "pronounce": "brɑ:ntʃ",
    "vietnamese": "ngành; nhành cây, nhánh song, >ngả đường"
  },
  {
    "english": "brand",
    "type": "n",
    "pronounce": "brænd",
    "vietnamese": "nhãn (hàng hóa)"
  },
  {
    "english": "brave",
    "type": "adj",
    "pronounce": "breiv",
    "vietnamese": "gan dạ, can đảm"
  },
  {
    "english": "bread",
    "type": "n",
    "pronounce": "bred",
    "vietnamese": "bánh mỳ"
  },
  {
    "english": "break",
    "type": "v, n",
    "pronounce": "breik",
    "vietnamese": "bẻ gẫy, đập vỡ; sự >gãy, sự vỡ"
  },
  {
    "english": "breakfast",
    "type": "n",
    "pronounce": "brekfəst",
    "vietnamese": "bữa điểm tâm, bữa sáng"
  },
  {
    "english": "breast",
    "type": "n",
    "pronounce": "brest",
    "vietnamese": "ngực, vú"
  },
  {
    "english": "breath",
    "type": "n",
    "pronounce": "breθ",
    "vietnamese": "hơi thở, hơi"
  },
  {
    "english": "breathe",
    "type": "v",
    "pronounce": "bri:ð",
    "vietnamese": "hít, thở"
  },
  {
    "english": "breathing",
    "type": "n",
    "pronounce": "bri:ðiɳ",
    "vietnamese": "sự hô hấp, sự thở"
  },
  {
    "english": "breed",
    "type": "v, n",
    "pronounce": "bri:d",
    "vietnamese": "nuôi dưỡng, chăm sóc, giáo dục; >sinh đẻ; nòi giống"
  },
  {
    "english": "brick",
    "type": "n",
    "pronounce": "brik",
    "vietnamese": "gạch"
  },
  {
    "english": "bridge",
    "type": "n",
    "pronounce": "bridʤ",
    "vietnamese": "cái cầu"
  },
  {
    "english": "brief",
    "type": "adj",
    "pronounce": "bri:f",
    "vietnamese": "ngắn, gọn, vắn tắt"
  },
  {
    "english": "briefly",
    "type": "adv",
    "pronounce": "bri:fli",
    "vietnamese": "ngắn, gọn, vắn tắt, tóm tắt"
  },
  {
    "english": "bright",
    "type": "adj",
    "pronounce": "brait",
    "vietnamese": "sáng, sáng chói"
  },
  {
    "english": "brightly",
    "type": "adv",
    "pronounce": "braitli",
    "vietnamese": "sáng chói, tươi"
  },
  {
    "english": "brilliant",
    "type": "adj",
    "pronounce": "briljənt",
    "vietnamese": "tỏa sáng, rực rỡ, chói lọi"
  },
  {
    "english": "bring",
    "type": "v",
    "pronounce": "briɳ",
    "vietnamese": "mang, cầm , xách lại"
  },
  {
    "english": "broad",
    "type": "adj",
    "pronounce": "broutʃ",
    "vietnamese": "rộng"
  },
  {
    "english": "broadcast",
    "type": "v, n",
    "pronounce": "brɔ:dkɑ:st",
    "vietnamese": "tung ra khắp nơi,truyền rộng rãi; >phát thanh, quảng bá"
  },
  {
    "english": "broadly",
    "type": "adv",
    "pronounce": "brɔ:dli",
    "vietnamese": "rộng, rộng rãi"
  },
  {
    "english": "broken",
    "type": "adj",
    "pronounce": "broukən",
    "vietnamese": "bị >gãy, bị vỡ"
  },
  {
    "english": "brother",
    "type": "n",
    "pronounce": "brΔðз",
    "vietnamese": "anh, em trai"
  },
  {
    "english": "brown",
    "type": "adj, n",
    "pronounce": "braun",
    "vietnamese": "nâu, màu nâu"
  },
  {
    "english": "brush",
    "type": "n, v",
    "pronounce": "brΔ∫",
    "vietnamese": "bàn chải; chải, quét"
  },
  {
    "english": "bubble",
    "type": "n",
    "pronounce": "bΔbl",
    "vietnamese": "bong bóng, bọt, tăm"
  },
  {
    "english": "budget",
    "type": "n",
    "pronounce": "bʌdʒɪt",
    "vietnamese": "ngân sách"
  },
  {
    "english": "build",
    "type": "v",
    "pronounce": "bild",
    "vietnamese": "xây dựng"
  },
  {
    "english": "building",
    "type": "n",
    "pronounce": "bildiŋ",
    "vietnamese": "sự xây >dựng, công trình xây dựng >tòa nhà"
  },
  {
    "english": "bullet",
    "type": "n",
    "pronounce": "bulit",
    "vietnamese": "đạn (súng trường, súng lục)"
  },
  {
    "english": "bunch",
    "type": "n",
    "pronounce": "bΛnt∫",
    "vietnamese": "búi, chùm, bó, cụm, buồng; bầy, >đàn"
  },
  {
    "english": "burn",
    "type": "v",
    "pronounce": "bə:n",
    "vietnamese": "đốt, đốt cháy, thắp, nung, thiêu"
  },
  {
    "english": "burnt",
    "type": "adj",
    "pronounce": "bə:nt",
    "vietnamese": "bị đốt, bị cháy, khê; rám nắng, >sạm (da)"
  },
  {
    "english": "burst",
    "type": "v",
    "pronounce": "bə:st",
    "vietnamese": "nổ, nổ tung (bom, đạn); nổ, vỡ >(bong bóng); háo hức"
  },
  {
    "english": "bury",
    "type": "v",
    "pronounce": "beri",
    "vietnamese": "chôn cất, mai táng"
  },
  {
    "english": "bus",
    "type": "n",
    "pronounce": "bʌs",
    "vietnamese": "xe buýt"
  },
  {
    "english": "bush",
    "type": "n",
    "pronounce": "bu∫",
    "vietnamese": "bụi cây, bụi rậm"
  },
  {
    "english": "business",
    "type": "n",
    "pronounce": "bizinis",
    "vietnamese": "việc buôn bán, thương mại, kinh >doanh"
  },
  {
    "english": "businessman, >businesswoman",
    "type": "n",
    "pronounce": "",
    "vietnamese": "thương nhân"
  },
  {
    "english": "busy",
    "type": "adj",
    "pronounce": "́bizi",
    "vietnamese": "bận, bận rộn"
  },
  {
    "english": "but",
    "type": "conj",
    "pronounce": "bʌt",
    "vietnamese": "nhưng"
  },
  {
    "english": "butter",
    "type": "n",
    "pronounce": "bʌtə",
    "vietnamese": "bơ"
  },
  {
    "english": "button",
    "type": "n",
    "pronounce": "bʌtn",
    "vietnamese": "cái nút, cái khuy, cúc"
  },
  {
    "english": "buy",
    "type": "v",
    "pronounce": "bai",
    "vietnamese": "mua"
  },
  {
    "english": "buyer",
    "type": "n",
    "pronounce": "́baiə",
    "vietnamese": "người mua"
  },
  {
    "english": "by",
    "type": "prep, adv",
    "pronounce": "bai",
    "vietnamese": "bởi, bằng"
  },
  {
    "english": "bye",
    "type": "exclamation",
    "pronounce": "bai",
    "vietnamese": "tạm biệt"
  },
  {
    "english": "cabinet",
    "type": "n",
    "pronounce": "kæbinit",
    "vietnamese": "tủ có nhiều ngăn đựng đồ"
  },
  {
    "english": "cable",
    "type": "n",
    "pronounce": "keibl",
    "vietnamese": "dây cáp"
  },
  {
    "english": "cake",
    "type": "n",
    "pronounce": "keik",
    "vietnamese": "bánh ngọt"
  },
  {
    "english": "calculate",
    "type": "v",
    "pronounce": "kælkjuleit",
    "vietnamese": "tính toán"
  },
  {
    "english": "calculation",
    "type": "n",
    "pronounce": ",kælkju'lei∫n",
    "vietnamese": "sự tính toán"
  },
  {
    "english": "call",
    "type": "v, n",
    "pronounce": "kɔ:l",
    "vietnamese": "gọi; tiếng kêu, tiếng gọi. be called: >được gọi, bị gọi"
  },
  {
    "english": "calm",
    "type": "adj, v, n",
    "pronounce": "kɑ:m",
    "vietnamese": "yên lặng, làm dịu đi; sự yên lặng, >sự êm ả"
  },
  {
    "english": "calmly",
    "type": "adv",
    "pronounce": "kɑ:mli",
    "vietnamese": "yên lặng, êm ả; bình tĩnh, điềm tĩnh"
  },
  {
    "english": "camera",
    "type": "n",
    "pronounce": "kæmərə",
    "vietnamese": "máy ảnh"
  },
  {
    "english": "camp",
    "type": "n, v",
    "pronounce": "kæmp",
    "vietnamese": "trại, chỗ cắm trại; cắm trại, hạ trại"
  },
  {
    "english": "campaign",
    "type": "n",
    "pronounce": "kæmˈpeɪn",
    "vietnamese": "chiến dịch, cuộc vận động"
  },
  {
    "english": "camping",
    "type": "n",
    "pronounce": "kæmpiη",
    "vietnamese": "sự cắm trại"
  },
  {
    "english": "can",
    "type": "modal, v, n",
    "pronounce": "kæn",
    "vietnamese": "có thể; nhà tù, nhà giam, bình, ca đựng. cannot không thể"
  },
  {
    "english": "cancel",
    "type": "v",
    "pronounce": "́kænsəl",
    "vietnamese": "hủy bỏ, xóa bỏ"
  },
  {
    "english": "cancer",
    "type": "n",
    "pronounce": "kænsə",
    "vietnamese": "bệnh ung thư"
  },
  {
    "english": "candidate",
    "type": "n",
    "pronounce": "kændidit",
    "vietnamese": "người ứng cử, thí sinh, người dự thi"
  },
  {
    "english": "candy",
    "type": "n",
    "pronounce": "́kændi",
    "vietnamese": "kẹo"
  },
  {
    "english": "cap",
    "type": "n",
    "pronounce": "kæp",
    "vietnamese": "mũ lưỡi trai, mũ vải"
  },
  {
    "english": "capable",
    "type": "of, adj",
    "pronounce": "keipəb(ə)l",
    "vietnamese": "có tài, có năng lực; có khả năng, cả gan"
  },
  {
    "english": "capacity",
    "type": "n",
    "pronounce": "kə'pæsiti",
    "vietnamese": "năng lực, khả năng tiếp thu, năng suất"
  },
  {
    "english": "capital",
    "type": "n, adj",
    "pronounce": "ˈkæpɪtl",
    "vietnamese": "thủ đô, tiền vốn; chủ yếu, chính yếu, cơ bản"
  },
  {
    "english": "captain",
    "type": "n",
    "pronounce": "kæptin",
    "vietnamese": "người cầm đầu, người chỉ huy, thủ lĩnh"
  },
  {
    "english": "capture",
    "type": "v, n",
    "pronounce": "kæptʃə",
    "vietnamese": "bắt giữ, bắt; sự bắt giữ, sự bị bắt"
  },
  {
    "english": "car",
    "type": "n",
    "pronounce": "kɑ:",
    "vietnamese": "xe hơi"
  },
  {
    "english": "card",
    "type": "n",
    "pronounce": "kɑ:d",
    "vietnamese": "thẻ, thiếp"
  },
  {
    "english": "cardboard",
    "type": "n",
    "pronounce": "́ka:d ̧bɔ:d",
    "vietnamese": "bìa cứng, các tông"
  },
  {
    "english": "care",
    "type": "n, v",
    "pronounce": "kɛər",
    "vietnamese": "sự chăm sóc, chăm nom; chăm sóc"
  },
  {
    "english": "career",
    "type": "n",
    "pronounce": "kə'riə",
    "vietnamese": "nghề nghiệp, sự nghiệp"
  },
  {
    "english": "careful",
    "type": "adj",
    "pronounce": "keəful",
    "vietnamese": "cẩn thận, cẩn trọng, biết giữ gìn"
  },
  {
    "english": "carefully",
    "type": "adv",
    "pronounce": "́kɛəfuli",
    "vietnamese": "cẩn thận, chu đáo"
  },
  {
    "english": "careless",
    "type": "adj",
    "pronounce": "́kɛəlis",
    "vietnamese": "sơ suất, cầu thả"
  },
  {
    "english": "carelessly",
    "type": "adv",
    "pronounce": "",
    "vietnamese": "cẩu thả, bất cẩn"
  },
  {
    "english": "carpet",
    "type": "n",
    "pronounce": "kɑ:pit",
    "vietnamese": "tấm thảm, thảm (cỏ)"
  },
  {
    "english": "carrot",
    "type": "n",
    "pronounce": "́kærət",
    "vietnamese": "củ cà rốt"
  },
  {
    "english": "carry",
    "type": "v",
    "pronounce": "ˈkæri",
    "vietnamese": "mang, vác, khuân chở"
  },
  {
    "english": "case",
    "type": "n",
    "pronounce": "keis",
    "vietnamese": "vỏ, ngăn, túi,trường hợp, cảnh ngộ, hoàn cảnh, tình thế"
  },
  {
    "english": "cash",
    "type": "n",
    "pronounce": "kæʃ",
    "vietnamese": "tiền, tiền mặt"
  },
  {
    "english": "cast",
    "type": "v, n",
    "pronounce": "kɑ:st",
    "vietnamese": "quăng, ném, thả, đánh gục; sự quăng, sự ném (lưới), sự thả (neo)"
  },
  {
    "english": "castle",
    "type": "n",
    "pronounce": "kɑ:sl",
    "vietnamese": "thành trì, thành quách"
  },
  {
    "english": "cat",
    "type": "n",
    "pronounce": "kæt",
    "vietnamese": "con mèo"
  },
  {
    "english": "catch",
    "type": "v",
    "pronounce": "kætʃ",
    "vietnamese": "bắt lấy, nắm lấy, tóm lấy, chộp lấy"
  },
  {
    "english": "category",
    "type": "n",
    "pronounce": "kætigəri",
    "vietnamese": "hạng, loại; phạm trù"
  },
  {
    "english": "cause",
    "type": "n, v",
    "pronounce": "kɔ:z",
    "vietnamese": "nguyên nhân, nguyên do; gây ra, gây nên"
  },
  {
    "english": "CD",
    "type": "n",
    "pronounce": "",
    "vietnamese": "đĩa CD"
  },
  {
    "english": "cease",
    "type": "v",
    "pronounce": "si:s",
    "vietnamese": "dừng, ngưng, ngớt, thôi, hết, tạnh"
  },
  {
    "english": "ceiling",
    "type": "n",
    "pronounce": "ˈsilɪŋ",
    "vietnamese": "trần nhà"
  },
  {
    "english": "celebrate",
    "type": "v",
    "pronounce": "selibreit",
    "vietnamese": "kỷ niệm, làm lễ kỷ niệm; tán dương, ca tụng"
  },
  {
    "english": "celebration",
    "type": "n",
    "pronounce": ",seli'breiʃn",
    "vietnamese": "sự kỷ niệm, lễ kỷ niệm; sự tán dương, sự ca tụng"
  },
  {
    "english": "cell",
    "type": "n",
    "pronounce": "sel",
    "vietnamese": "ô, ngăn"
  },
  {
    "english": "cellphone, cellular phone",
    "type": "n",
    "pronounce": "",
    "vietnamese": "điện thoại di động"
  },
  {
    "english": "cent",
    "type": "sent",
    "pronounce": "",
    "vietnamese": "đồng xu (bằng 1/100 đô la)"
  },
  {
    "english": "centimetre",
    "type": "n",
    "pronounce": "senti,mi:tз",
    "vietnamese": "xen ti mét"
  },
  {
    "english": "centimetre, centimeter",
    "type": "n",
    "pronounce": "",
    "vietnamese": "xen ti met"
  },
  {
    "english": "central",
    "type": "adj",
    "pronounce": "́sentrəl",
    "vietnamese": "trung tâm, ở giữa, trung ương"
  },
  {
    "english": "centre",
    "type": "n",
    "pronounce": "sentə",
    "vietnamese": "điểm giữa, trung tâm, trung ương"
  },
  {
    "english": "century",
    "type": "n",
    "pronounce": "sentʃuri",
    "vietnamese": "thế kỷ"
  },
  {
    "english": "ceremony",
    "type": "n",
    "pronounce": "́seriməni",
    "vietnamese": "nghi thức, nghi lễ"
  },
  {
    "english": "certain",
    "type": "adj, pron",
    "pronounce": "sə:tn",
    "vietnamese": "chắc chắn"
  },
  {
    "english": "certainly",
    "type": "adv",
    "pronounce": "́sə:tnli",
    "vietnamese": "chắc chắn, nhất định"
  },
  {
    "english": "certificate",
    "type": "n",
    "pronounce": "sə'tifikit",
    "vietnamese": "giấy chứng nhận, bằng, chứng chỉ"
  },
  {
    "english": "chain",
    "type": "n, v",
    "pronounce": "tʃeɪn",
    "vietnamese": "dây, xích; xính lại, trói lại"
  },
  {
    "english": "chair",
    "type": "n",
    "pronounce": "tʃeə",
    "vietnamese": "ghế"
  },
  {
    "english": "chairman, chairwoman",
    "type": "n",
    "pronounce": "tʃeəmən, 'tʃeə,wumən",
    "vietnamese": "chủ tịch, chủ tọa"
  },
  {
    "english": "challenge",
    "type": "n, v",
    "pronounce": "tʃælindʤ",
    "vietnamese": "sự thử thách, sự thách thức; thách thức, thử thách "
  },
  {
    "english": "chamber",
    "type": "n",
    "pronounce": "ˈtʃeɪmbər",
    "vietnamese": "buồng, phòng, buồng ngủ"
  },
  {
    "english": "chance",
    "type": "n",
    "pronounce": "tʃæns , tʃɑ:ns",
    "vietnamese": "sự may rủi, sự tình cờ, ngẫu nhiên"
  },
  {
    "english": "change",
    "type": "v, n",
    "pronounce": "tʃeɪndʒ",
    "vietnamese": "thấy đổi, sự thấy đổi, sự biến đổi"
  },
  {
    "english": "channel",
    "type": "n",
    "pronounce": "tʃænl",
    "vietnamese": "kênh (TV, radio), eo biển"
  },
  {
    "english": "chapter",
    "type": "n",
    "pronounce": "t∫æptə(r)",
    "vietnamese": "chương (sách)"
  },
  {
    "english": "character",
    "type": "n",
    "pronounce": "kæriktə",
    "vietnamese": "tính cách, đặc tính, nhân vật"
  },
  {
    "english": "characteristic",
    "type": "adj, n",
    "pronounce": "̧kærəktə ́ristik",
    "vietnamese": "riêng, riêng biệt, đặc trưng, đặc tính, đặc điểm"
  },
  {
    "english": "charge",
    "type": "n, v",
    "pronounce": "tʃɑ:dʤ",
    "vietnamese": "nhiệm vụ, bổn phận, trách nhiệm; giao nhiệm vụ, giao việc. in charge of phụ trách"
  },
  {
    "english": "charity",
    "type": "n",
    "pronounce": "́tʃæriti",
    "vietnamese": "lòng tư thiện, lòng nhân đức; sự bố thí"
  },
  {
    "english": "chart",
    "type": "n, v",
    "pronounce": "tʃa:t",
    "vietnamese": "đồ thị, biểu đồ; vẽ đồ thị, lập biểu đồ"
  },
  {
    "english": "chase",
    "type": "v, n",
    "pronounce": "tʃeis",
    "vietnamese": "săn bắt; sự săn bắt"
  },
  {
    "english": "chat",
    "type": "v, n",
    "pronounce": "tʃæt",
    "vietnamese": "nói chuyện, tán gẫu; chuyện phiếm, chuyện gẫu"
  },
  {
    "english": "cheap",
    "type": "adj",
    "pronounce": "tʃi:p",
    "vietnamese": "rẻ"
  },
  {
    "english": "cheaply",
    "type": "adv",
    "pronounce": "",
    "vietnamese": "rẻ, rẻ tiền"
  },
  {
    "english": "cheat",
    "type": "v, n",
    "pronounce": "tʃit",
    "vietnamese": "lưa, lưa đảo; trò lưa đảo, trò gian lận"
  },
  {
    "english": "check",
    "type": "v, n",
    "pronounce": "tʃek",
    "vietnamese": "kiểm tra; sự kiểm tra"
  },
  {
    "english": "cheek",
    "type": "n",
    "pronounce": "́tʃi:k",
    "vietnamese": "má"
  },
  {
    "english": "cheerful",
    "type": "adj",
    "pronounce": "́tʃiəful",
    "vietnamese": "vui mưng, phấn khởi, hồ hởi"
  },
  {
    "english": "cheerfully",
    "type": "adv",
    "pronounce": "",
    "vietnamese": "vui vẻ, phấn khởi"
  },
  {
    "english": "cheese",
    "type": "n",
    "pronounce": "tʃi:z",
    "vietnamese": "pho mát"
  },
  {
    "english": "chemical",
    "type": "adj, n",
    "pronounce": "ˈkɛmɪkəl",
    "vietnamese": "thuộc hóa học; chất hóa học, hóa chất"
  },
  {
    "english": "chemist",
    "type": "n",
    "pronounce": "́kemist",
    "vietnamese": "nhà hóa học"
  },
  {
    "english": "chemist’s",
    "type": "n",
    "pronounce": "",
    "vietnamese": "nhà hóa học"
  },
  {
    "english": "chemistry",
    "type": "n",
    "pronounce": "́kemistri",
    "vietnamese": "hóa học, môn hóa học, ngành hóa học"
  },
  {
    "english": "cheque",
    "type": "n",
    "pronounce": "t∫ek",
    "vietnamese": "séc"
  },
  {
    "english": "chest",
    "type": "n",
    "pronounce": "tʃest",
    "vietnamese": "tủ, rương, hòm"
  },
  {
    "english": "chew",
    "type": "v",
    "pronounce": "tʃu:",
    "vietnamese": "nhai, ngẫm nghĩ "
  },
  {
    "english": "chicken",
    "type": "n",
    "pronounce": "ˈtʃɪkin",
    "vietnamese": "gà, gà con, thịt gà"
  },
  {
    "english": "chief",
    "type": "adj, n",
    "pronounce": "tʃi:f",
    "vietnamese": "trọng yếu, chính yếu; thủ lĩnh, lãnh tụ, người đứng đầu, xếp"
  },
  {
    "english": "child",
    "type": "n",
    "pronounce": "tʃaild",
    "vietnamese": "đứa bé, đứa trẻ"
  },
  {
    "english": "chin",
    "type": "n",
    "pronounce": "tʃin",
    "vietnamese": "cằm"
  },
  {
    "english": "chip",
    "type": "n",
    "pronounce": "tʃip",
    "vietnamese": "vỏ bào, mảnh vỡ, chỗ sứt, mẻ"
  },
  {
    "english": "chocolate",
    "type": "n",
    "pronounce": "ˈtʃɒklɪt",
    "vietnamese": "sô cô la"
  },
  {
    "english": "choice",
    "type": "n",
    "pronounce": "tʃɔɪs",
    "vietnamese": "sự lựa chọn"
  },
  {
    "english": "choose",
    "type": "v",
    "pronounce": "t∫u:z",
    "vietnamese": "chọn, lựa chọn"
  },
  {
    "english": "chop",
    "type": "v",
    "pronounce": "tʃɔp",
    "vietnamese": "chặt, đốn, chẻ"
  },
  {
    "english": "church",
    "type": "n",
    "pronounce": "tʃə:tʃ",
    "vietnamese": "nhà thờ"
  },
  {
    "english": "cigarette",
    "type": "n",
    "pronounce": "̧sigə ́ret",
    "vietnamese": "điếu thuốc lá"
  },
  {
    "english": "cinema",
    "type": "n",
    "pronounce": "ˈsɪnəmə",
    "vietnamese": "rạp xi nê, rạp chiếu bóng"
  },
  {
    "english": "circle",
    "type": "n",
    "pronounce": "sə:kl",
    "vietnamese": "đường tròn, hình tròn"
  },
  {
    "english": "circumstance",
    "type": "n",
    "pronounce": "ˈsɜrkəmˌstəns",
    "vietnamese": "hoàn cảnh, trường hợp, tình huống"
  },
  {
    "english": "citizen",
    "type": "n",
    "pronounce": "́sitizən",
    "vietnamese": "người thành thị"
  },
  {
    "english": "city",
    "type": "n",
    "pronounce": "si:ti",
    "vietnamese": "thành phố"
  },
  {
    "english": "civil",
    "type": "adj",
    "pronounce": "sivl",
    "vietnamese": "(thuộc) công dân"
  },
  {
    "english": "claim",
    "type": "v, n",
    "pronounce": "kleim",
    "vietnamese": "đòi hỏi, yêu sách; sự đòi hỏi, sự yêu sách, sự thỉnh cầu"
  },
  {
    "english": "clap",
    "type": "v, n",
    "pronounce": "klæp",
    "vietnamese": "vỗ, vỗ tay; tiếng nổ, tiếng vỗ tay"
  },
  {
    "english": "class",
    "type": "n",
    "pronounce": "klɑ:s",
    "vietnamese": "lớp học"
  },
  {
    "english": "classic",
    "type": "adj, n",
    "pronounce": "klæsik",
    "vietnamese": "cổ điển, kinh điển"
  },
  {
    "english": "classroom",
    "type": "n",
    "pronounce": "klα:si",
    "vietnamese": "lớp học, phòng học"
  },
  {
    "english": "clean",
    "type": "adj, v",
    "pronounce": "kli:n",
    "vietnamese": "sạch, sạch sẽ;"
  },
  {
    "english": "clear",
    "type": "adj, v",
    "pronounce": "",
    "vietnamese": "lau chùi, quét dọn"
  },
  {
    "english": "clearly",
    "type": "adv",
    "pronounce": "́kliəli",
    "vietnamese": "rõ ràng, sáng sủa"
  },
  {
    "english": "clerk",
    "type": "n",
    "pronounce": "kla:k",
    "vietnamese": "thư ký, linh mục, mục sư"
  },
  {
    "english": "clever",
    "type": "adj",
    "pronounce": "klevə",
    "vietnamese": "lanh lợi, thông minh. tài giỏi, khéo léo"
  },
  {
    "english": "click",
    "type": "v, n",
    "pronounce": "klik",
    "vietnamese": "làm thành tiếng lách cách; tiếng lách cách, cú nhắp (chuột)"
  },
  {
    "english": "client",
    "type": "n",
    "pronounce": "́klaiənt",
    "vietnamese": "khách hàng"
  },
  {
    "english": "climate",
    "type": "n",
    "pronounce": "klaimit",
    "vietnamese": "khí hậu, thời tiết "
  },
  {
    "english": "climb",
    "type": "v",
    "pronounce": "klaim",
    "vietnamese": "leo, trèo"
  },
  {
    "english": "climbing",
    "type": "n",
    "pronounce": "́klaimiη",
    "vietnamese": "sự leo trèo"
  },
  {
    "english": "clock",
    "type": "n",
    "pronounce": "klɔk",
    "vietnamese": "đồng hồ"
  },
  {
    "english": "close",
    "type": "adj",
    "pronounce": "klouz",
    "vietnamese": "đóng kín, chật chội, che đậy"
  },
  {
    "english": "closed",
    "type": "adj",
    "pronounce": "klouzd",
    "vietnamese": "bảo thủ, không cởi mở, khép kín"
  },
  {
    "english": "closely",
    "type": "adv",
    "pronounce": "́klousli",
    "vietnamese": "chặt chẽ, kỹ lưỡng, tỉ mỉ"
  },
  {
    "english": "closet",
    "type": "n",
    "pronounce": "klozit",
    "vietnamese": "buồng, phòng để đồ, phòng kho"
  },
  {
    "english": "cloth",
    "type": "n",
    "pronounce": "klɔθ",
    "vietnamese": "vải, khăn trải bàn, áo thầy tu"
  },
  {
    "english": "clothes",
    "type": "n",
    "pronounce": "klouðz",
    "vietnamese": "quần áo"
  },
  {
    "english": "clothing",
    "type": "n",
    "pronounce": "́klouðiη",
    "vietnamese": "quần áo, y phục"
  },
  {
    "english": "cloud",
    "type": "n",
    "pronounce": "klaud",
    "vietnamese": "mây, đám mây"
  },
  {
    "english": "club",
    "type": "n",
    "pronounce": "́klʌb",
    "vietnamese": "câu lạc bộ; gậy, dùi cui"
  },
  {
    "english": "coach",
    "type": "n",
    "pronounce": "koʊtʃ",
    "vietnamese": "huấn luyện viên"
  },
  {
    "english": "coal",
    "type": "n",
    "pronounce": "koul",
    "vietnamese": "than đá"
  },
  {
    "english": "coast",
    "type": "n",
    "pronounce": "koust",
    "vietnamese": "sự lao dốc; bờ biển"
  },
  {
    "english": "coat",
    "type": "n",
    "pronounce": "koʊt",
    "vietnamese": "áo choàng"
  },
  {
    "english": "code",
    "type": "n",
    "pronounce": "koud",
    "vietnamese": "mật mã, luật, điều lệ"
  },
  {
    "english": "coffee",
    "type": "n",
    "pronounce": "kɔfi",
    "vietnamese": "cà phê"
  },
  {
    "english": "coin",
    "type": "n",
    "pronounce": "kɔin",
    "vietnamese": "tiền kim loại"
  },
  {
    "english": "cold",
    "type": "adj, n",
    "pronounce": "kould",
    "vietnamese": "lạnh, sự lạnh lẽo, lạnh nhạt"
  },
  {
    "english": "coldly",
    "type": "adv",
    "pronounce": "kouldli",
    "vietnamese": "lạnh nhạt, hờ hững, vô tâm"
  },
  {
    "english": "collapse",
    "type": "v, n",
    "pronounce": "kз'læps",
    "vietnamese": "đổ, sụp đổ; sự đổ nát, sự sụp đổ"
  },
  {
    "english": "colleague",
    "type": "n",
    "pronounce": "ˈkɒlig",
    "vietnamese": "bạn đồng nghiệp"
  },
  {
    "english": "collect",
    "type": "v",
    "pronounce": "kə ́lekt",
    "vietnamese": "sưu tập, tập trung lại"
  },
  {
    "english": "collection",
    "type": "n",
    "pronounce": "kəˈlɛkʃən",
    "vietnamese": "sự sưu tập, sự tụ họp"
  },
  {
    "english": "college",
    "type": "n",
    "pronounce": "kɔlidʤ",
    "vietnamese": "trường cấo đẳng, trường đại học"
  },
  {
    "english": "color, colour",
    "type": "n, v",
    "pronounce": "kʌlə",
    "vietnamese": "màu sắc; tô màu"
  },
  {
    "english": "coloured",
    "type": "adj",
    "pronounce": "́kʌləd",
    "vietnamese": "mang màu sắc, có màu sắc"
  },
  {
    "english": "column",
    "type": "n",
    "pronounce": "kɔləm",
    "vietnamese": "cột , mục (báo)"
  },
  {
    "english": "combination",
    "type": "n",
    "pronounce": ",kɔmbi'neiʃn",
    "vietnamese": "sự kết hợp, sự phối hợp"
  },
  {
    "english": "combine",
    "type": "v",
    "pronounce": "kɔmbain",
    "vietnamese": "kết hợp, phối hợp"
  },
  {
    "english": "come",
    "type": "v",
    "pronounce": "kʌm",
    "vietnamese": "đến, tới, đi đến, đi tới"
  },
  {
    "english": "comedy",
    "type": "n",
    "pronounce": "́kɔmidi",
    "vietnamese": "hài kịch "
  },
  {
    "english": "comfort",
    "type": "n, v",
    "pronounce": "kΔmfзt",
    "vietnamese": "sự an ủi, khuyên giải, lời động viên, sự an nhàn; dỗ dành, an ủi"
  },
  {
    "english": "comfortable",
    "type": "adj",
    "pronounce": "kΔmfзtзbl",
    "vietnamese": "thoải mái, tiện nghi, đầy đủ"
  },
  {
    "english": "comfortably",
    "type": "adv",
    "pronounce": "́kʌmfətəbli",
    "vietnamese": "dễ chịu, thoải mái, tiện nghi, ấm cúng"
  },
  {
    "english": "command",
    "type": "v, n",
    "pronounce": "kə'mɑ:nd",
    "vietnamese": "ra lệnh, chỉ huy; lệnh, mệnh lệnh, quyền ra lệnh, quyền chỉ huy"
  },
  {
    "english": "comment",
    "type": "n, v",
    "pronounce": "ˈkɒment",
    "vietnamese": "lời bình luận, lời chú giải; bình luận, phê bình, chú thích, dẫn giải"
  },
  {
    "english": "commercial",
    "type": "adj",
    "pronounce": "kə'mə:ʃl",
    "vietnamese": "buôn bán, thương mại"
  },
  {
    "english": "commission",
    "type": "n, v",
    "pronounce": "kəˈmɪʃən",
    "vietnamese": "hội đồng, ủy ban, sự ủy nhiệm, sự ủy thác; ủy nhiệm, ủy thác"
  },
  {
    "english": "commit",
    "type": "v",
    "pronounce": "kə'mit",
    "vietnamese": "giao, gửi, ủy nhiệm, ủy thác; tống giam, bỏ tù"
  },
  {
    "english": "commitment",
    "type": "n",
    "pronounce": "kə'mmənt",
    "vietnamese": "sự phạm tội, sự tận tụy, tận tâm"
  },
  {
    "english": "committee",
    "type": "n",
    "pronounce": "kə'miti",
    "vietnamese": "ủy ban"
  },
  {
    "english": "common",
    "type": "adj",
    "pronounce": "kɔmən",
    "vietnamese": "công, công cộng, thông thường, phổ biến. in common sự chung, của chung"
  },
  {
    "english": "commonly",
    "type": "adv",
    "pronounce": "́kɔmənli",
    "vietnamese": "thông thường, bình thường"
  },
  {
    "english": "communicate",
    "type": "v",
    "pronounce": "kə'mju:nikeit",
    "vietnamese": "truyền, truyền đạt; giao thiệp, liên lạc"
  },
  {
    "english": "communication",
    "type": "n",
    "pronounce": "kə,mju:ni'keiʃn",
    "vietnamese": "sự giao tiếp, liên lạc, sự truyền đạt, truyền tin"
  },
  {
    "english": "community",
    "type": "n",
    "pronounce": "kə'mju:niti",
    "vietnamese": "dân chúng, nhân dân"
  },
  {
    "english": "company",
    "type": "n",
    "pronounce": "́kʌmpəni",
    "vietnamese": "công ty"
  },
  {
    "english": "compare",
    "type": "v",
    "pronounce": "kəm'peə(r)",
    "vietnamese": "so sánh, đối chiếu"
  },
  {
    "english": "comparison",
    "type": "n",
    "pronounce": "kəm'pærisn",
    "vietnamese": "sự so sánh"
  },
  {
    "english": "compete",
    "type": "v",
    "pronounce": "kəm'pi:t",
    "vietnamese": "đua tranh, ganh đua, cạnh tranh"
  },
  {
    "english": "competition",
    "type": "n",
    "pronounce": ",kɔmpi'tiʃn",
    "vietnamese": "sự cạnh tranh, cuộc thi, cuộc thi đau"
  },
  {
    "english": "competitive",
    "type": "adj",
    "pronounce": "kəm ́petitiv",
    "vietnamese": "cạnh tranh, đua tranh"
  },
  {
    "english": "complain",
    "type": "v",
    "pronounce": "kəm ́plein",
    "vietnamese": "phàn nàn, kêu ca"
  },
  {
    "english": "complaint",
    "type": "n",
    "pronounce": "kəmˈpleɪnt",
    "vietnamese": "lời than phiền, than thở; sự khiếu nại, đơn kiện"
  },
  {
    "english": "complete",
    "type": "adj, v",
    "pronounce": "kəm'pli:t",
    "vietnamese": "hoàn thành, xong;"
  },
  {
    "english": "completely",
    "type": "adv",
    "pronounce": "kзm'pli:tli",
    "vietnamese": "hoàn thành, đầy đủ, trọn vẹn"
  },
  {
    "english": "complex",
    "type": "adj",
    "pronounce": "kɔmleks",
    "vietnamese": "phức tạp, rắc rối "
  },
  {
    "english": "complicate",
    "type": "v",
    "pronounce": "komplikeit",
    "vietnamese": "làm phức tạp, rắc rối"
  },
  {
    "english": "complicated",
    "type": "adj",
    "pronounce": "komplikeitid",
    "vietnamese": "phức tạp, rắc rối"
  },
  {
    "english": "computer",
    "type": "n",
    "pronounce": "kəm'pju:tə",
    "vietnamese": "máy tính"
  },
  {
    "english": "concentrate",
    "type": "v",
    "pronounce": "kɔnsentreit",
    "vietnamese": "tập trung"
  },
  {
    "english": "concentration",
    "type": "n",
    "pronounce": ",kɒnsn'trei∫n",
    "vietnamese": "sự tập trung, nơi tập trung"
  },
  {
    "english": "concept",
    "type": "n",
    "pronounce": "ˈkɒnsept",
    "vietnamese": "khái niệm"
  },
  {
    "english": "concern",
    "type": "v, n",
    "pronounce": "kn'sз:n",
    "vietnamese": "liên quan, dính líu tới; sự liên quan, sự dính líu tới"
  },
  {
    "english": "concerned",
    "type": "adj",
    "pronounce": "kən ́sə:nd",
    "vietnamese": "có liên quan, có dính líu"
  },
  {
    "english": "concerning",
    "type": "n",
    "pronounce": "kən ́sə:niη",
    "vietnamese": "có liên quan, dính líu tới"
  },
  {
    "english": "concert",
    "type": "n",
    "pronounce": "kən'sə:t",
    "vietnamese": "buổi hòa nhạc"
  },
  {
    "english": "conclude",
    "type": "v",
    "pronounce": "kənˈklud",
    "vietnamese": "kết luận, kết thúc, chấm dứt (công việc)"
  },
  {
    "english": "conclusion",
    "type": "n",
    "pronounce": "kənˈkluʒən",
    "vietnamese": "sự kết thúc, sự kết luận, phần kết luận"
  },
  {
    "english": "concrete",
    "type": "adj, n",
    "pronounce": "kɔnkri:t",
    "vietnamese": "bằng bê tông; bê tông"
  },
  {
    "english": "condition",
    "type": "n",
    "pronounce": "kən'di∫ən",
    "vietnamese": "điều kiện, tình cảnh, tình thế"
  },
  {
    "english": "conduct",
    "type": "v, n",
    "pronounce": "kən'dʌkt",
    "vietnamese": "điều khiển, chỉ đạo, chỉ huy; sự điều khiển, chỉ huy"
  },
  {
    "english": "conference",
    "type": "n",
    "pronounce": "ˈkɒnfrəns",
    "vietnamese": "hội nghị, sự bàn bạc"
  },
  {
    "english": "confidence",
    "type": "n",
    "pronounce": "konfid(ə)ns",
    "vietnamese": "lòng tin tưởng, sự tin cậy"
  },
  {
    "english": "confident",
    "type": "adj",
    "pronounce": "kɔnfidənt",
    "vietnamese": "tin tưởng, tin cậy, tự tin"
  },
  {
    "english": "confidently",
    "type": "adv",
    "pronounce": "kɔnfidəntli",
    "vietnamese": "tự tin"
  },
  {
    "english": "confine",
    "type": "v",
    "pronounce": "kən'fain",
    "vietnamese": "giam giữ, hạn chế"
  },
  {
    "english": "confined",
    "type": "adj",
    "pronounce": "kən'faind",
    "vietnamese": "hạn chế, giới hạn"
  },
  {
    "english": "confirm",
    "type": "v",
    "pronounce": "kən'fə:m",
    "vietnamese": "xác nhận, chứng thực"
  },
  {
    "english": "conflict",
    "type": "n, v",
    "pronounce": "ˈkɒnflɪkt",
    "vietnamese": "xung đột, va chạm; sự xung đột, sự va chạm"
  },
  {
    "english": "confront",
    "type": "v",
    "pronounce": "kən'frʌnt",
    "vietnamese": "đối mặt, đối diện, đối chiếu"
  },
  {
    "english": "confuse",
    "type": "v",
    "pronounce": "",
    "vietnamese": "làm lộn xộn, xáo trộn"
  },
  {
    "english": "confused",
    "type": "adj",
    "pronounce": "kən'fju:zd",
    "vietnamese": "bối rối, lúng túng, ngượng"
  },
  {
    "english": "confusing",
    "type": "adj",
    "pronounce": "kən'fju:ziη",
    "vietnamese": "khó hiểu, gây bối rối"
  },
  {
    "english": "confusion",
    "type": "n",
    "pronounce": "kən'fju:ʒn",
    "vietnamese": "sự lộn xộn, sự rối loạn"
  },
  {
    "english": "congratulations",
    "type": "n",
    "pronounce": "kən,grætju'lei∫n",
    "vietnamese": "sự chúc mưng, khen ngợi; lời chúc mưng, khen ngợi (s)"
  },
  {
    "english": "congress",
    "type": "n",
    "pronounce": "kɔɳgres",
    "vietnamese": "đại hội, hội nghị, Quốc hội"
  },
  {
    "english": "connect",
    "type": "v",
    "pronounce": "kə'nekt",
    "vietnamese": "kết nối, nối"
  },
  {
    "english": "connection",
    "type": "n",
    "pronounce": "kə ́nekʃən,",
    "vietnamese": "sự kết nối, sự giao kết"
  },
  {
    "english": "conscious",
    "type": "adj",
    "pronounce": "ˈkɒnʃəs",
    "vietnamese": "tỉnh táo, có ý thức, biết rõ"
  },
  {
    "english": "consequence",
    "type": "n",
    "pronounce": "kɔnsikwəns",
    "vietnamese": "kết quả, hậu quả"
  },
  {
    "english": "conservative",
    "type": "adj",
    "pronounce": "kən ́sə:vətiv",
    "vietnamese": "thận trọng, dè dặt, bảo thủ"
  },
  {
    "english": "consider",
    "type": "v",
    "pronounce": "kən ́sidə",
    "vietnamese": "cân nhắc, xem xét; để ý, quan tâm, lưu ý đến"
  },
  {
    "english": "considerable",
    "type": "adj",
    "pronounce": "kən'sidərəbl",
    "vietnamese": "lớn lao, to tát, đáng kể"
  },
  {
    "english": "considerably",
    "type": "adv",
    "pronounce": "kən'sidərəbly",
    "vietnamese": "đáng kể, lớn lao, nhiều"
  },
  {
    "english": "consideration",
    "type": "n",
    "pronounce": "kənsidə'reiʃn",
    "vietnamese": "sự cân nhắc, sự xem xét, sự để ý, sự quan tâm"
  },
  {
    "english": "consist of",
    "type": "v",
    "pronounce": "kən'sist",
    "vietnamese": "gồm có"
  },
  {
    "english": "constant",
    "type": "adj",
    "pronounce": "kɔnstənt",
    "vietnamese": "kiên trì, bền lòng"
  },
  {
    "english": "constantly",
    "type": "adv",
    "pronounce": "kɔnstəntli",
    "vietnamese": "kiên định"
  },
  {
    "english": "construct",
    "type": "v",
    "pronounce": "kən ́strʌkt",
    "vietnamese": "xây dựng"
  },
  {
    "english": "construction",
    "type": "n",
    "pronounce": "kən'strʌkʃn",
    "vietnamese": "sự xây dựng"
  },
  {
    "english": "consult",
    "type": "v",
    "pronounce": "kən'sʌlt",
    "vietnamese": "tra cứu, tham khảo, thăm dò, hỏi ý kiến"
  },
  {
    "english": "consumer",
    "type": "n",
    "pronounce": "kən'sju:mə",
    "vietnamese": "người tiêu dùng"
  },
  {
    "english": "contact",
    "type": "n, v",
    "pronounce": "ˈkɒntækt",
    "vietnamese": "sự liên lạc, sự giao thiệp; tiếp xúc"
  },
  {
    "english": "contain",
    "type": "v",
    "pronounce": "kən'tein",
    "vietnamese": "bao hàm, chứa đựng, bao gồm"
  },
  {
    "english": "container",
    "type": "n",
    "pronounce": "kən'teinə",
    "vietnamese": "cái đựng, chứa; công te nơ"
  },
  {
    "english": "contemporary",
    "type": "adj",
    "pronounce": "kən'tempərəri",
    "vietnamese": "đương thời, đương đại"
  },
  {
    "english": "content",
    "type": "n",
    "pronounce": "kən'tent",
    "vietnamese": "nội dung, sự hài lòng"
  },
  {
    "english": "contest",
    "type": "n",
    "pronounce": "kən ́test",
    "vietnamese": "cuộc thi, trận đấu, cuộc tranh luận cuộc chiến đấu, chiến tranh"
  },
  {
    "english": "context",
    "type": "n",
    "pronounce": "kɔntekst",
    "vietnamese": "văn cảnh, khung cảnh, phạm vi"
  },
  {
    "english": "continent",
    "type": "n",
    "pronounce": "kɔntinənt",
    "vietnamese": "lục địa, đại lục (lục địa Bắc Mỹ)"
  },
  {
    "english": "continue",
    "type": "v",
    "pronounce": "kən ́tinju:",
    "vietnamese": "tiếp tục, làm tiếp"
  },
  {
    "english": "continuous",
    "type": "adj",
    "pronounce": "kən'tinjuəs",
    "vietnamese": "liên tục, liên tiếp"
  },
  {
    "english": "continuously",
    "type": "adv",
    "pronounce": "kən'tinjuəsli",
    "vietnamese": "liên tục, liên tiếp"
  },
  {
    "english": "contract",
    "type": "n, v",
    "pronounce": "kɔntrækt",
    "vietnamese": "hợp đồng, sự ký hợp đồng; ký kết"
  },
  {
    "english": "contrast",
    "type": "n, v",
    "pronounce": "kən'træst",
    "vietnamese": "sự tương phản; làm tương phản, làm trái ngược"
  },
  {
    "english": "contrasting",
    "type": "adj",
    "pronounce": "kən'træsti",
    "vietnamese": "tương phản"
  },
  {
    "english": "contribute",
    "type": "v",
    "pronounce": "kən'tribju:t",
    "vietnamese": "đóng góp, ghóp phần"
  },
  {
    "english": "contribution",
    "type": "n",
    "pronounce": "̧kɔntri ́bju:ʃən",
    "vietnamese": "sự đóng góp, sự góp phần"
  },
  {
    "english": "control",
    "type": "n, v",
    "pronounce": "kən'troul",
    "vietnamese": "sự điều khiển, quyền hành, quyền lực, quyền chỉ huy"
  },
  {
    "english": "controlled",
    "type": "adj",
    "pronounce": "kən'trould",
    "vietnamese": "được điều khiển, được kiểm tra"
  },
  {
    "english": "convenient",
    "type": "adj",
    "pronounce": "kən ́vi:njənt",
    "vietnamese": "tiện lợi, thuận lợi, thích hợp"
  },
  {
    "english": "convention",
    "type": "n",
    "pronounce": "kən'ven∫n",
    "vietnamese": "hội nghị, hiệp định, quy ước"
  },
  {
    "english": "conventional",
    "type": "adj",
    "pronounce": "kən'ven∫ənl",
    "vietnamese": "quy ước"
  },
  {
    "english": "conversation",
    "type": "n",
    "pronounce": ",kɔnvə'seiʃn",
    "vietnamese": "cuộc đàm thoại, cuộc trò chuyện"
  },
  {
    "english": "convert",
    "type": "v",
    "pronounce": "kən'və:t",
    "vietnamese": "đổi, biến đổi"
  },
  {
    "english": "convince",
    "type": "v",
    "pronounce": "kən'vins",
    "vietnamese": "làm cho tin, thuyết phục; làm cho nhận thức thấy"
  },
  {
    "english": "cook",
    "type": "v, n",
    "pronounce": "kʊk",
    "vietnamese": "nấu ăn, người nấu ăn"
  },
  {
    "english": "cooker",
    "type": "n",
    "pronounce": "́kukə",
    "vietnamese": "lò, bếp, nồi nấu"
  },
  {
    "english": "cookie",
    "type": "n",
    "pronounce": "́kuki",
    "vietnamese": "bánh quy"
  },
  {
    "english": "cooking",
    "type": "n",
    "pronounce": "kʊkiɳ",
    "vietnamese": "sự nấu ăn, cách nấu ăn"
  },
  {
    "english": "cool",
    "type": "adj, v",
    "pronounce": "ku:l",
    "vietnamese": "mát mẻ, điềm tĩnh; làm mát,"
  },
  {
    "english": "cope (+ with)",
    "type": "v",
    "pronounce": "koup",
    "vietnamese": "đối phó, đương đầu"
  },
  {
    "english": "copy",
    "type": "n, v",
    "pronounce": "kɔpi",
    "vietnamese": "bản sao, bản chép lại; sự sao chép; sao chép, bắt chước"
  },
  {
    "english": "core",
    "type": "n",
    "pronounce": "kɔ:",
    "vietnamese": "nòng cốt, hạt nhân; đáy lòng"
  },
  {
    "english": "corner",
    "type": "n",
    "pronounce": "́kɔ:nə",
    "vietnamese": "góc (tường, nhà, phố...)"
  },
  {
    "english": "correct",
    "type": "adj, v",
    "pronounce": "kə ́rekt",
    "vietnamese": "đúng, chính xác; sửa, sửa chữa"
  },
  {
    "english": "correctly",
    "type": "adv",
    "pronounce": "kə ́rektli",
    "vietnamese": "đúng, chính xác"
  },
  {
    "english": "cost",
    "type": "n, v",
    "pronounce": "kɔst , kɒst",
    "vietnamese": "giá, chi phí; trả giá, phải trả"
  },
  {
    "english": "cottage",
    "type": "n",
    "pronounce": "kɔtidʤ",
    "vietnamese": "nhà tranh"
  },
  {
    "english": "cotton",
    "type": "n",
    "pronounce": "ˈkɒtn",
    "vietnamese": "bông, chỉ, sợi"
  },
  {
    "english": "cough",
    "type": "v, n",
    "pronounce": "kɔf",
    "vietnamese": "ho, sự ho, tiếng hoa"
  },
  {
    "english": "coughing",
    "type": "n",
    "pronounce": "́kɔfiη",
    "vietnamese": "ho"
  },
  {
    "english": "could",
    "type": "modal, v",
    "pronounce": "kud",
    "vietnamese": "có thể"
  },
  {
    "english": "council",
    "type": "n",
    "pronounce": "kaunsl",
    "vietnamese": "hội đồng"
  },
  {
    "english": "count",
    "type": "v",
    "pronounce": "kaunt",
    "vietnamese": "đếm, tính"
  },
  {
    "english": "counter",
    "type": "n",
    "pronounce": "ˈkaʊntər",
    "vietnamese": "quầy hàng, quầy thu tiền, máy đếm"
  },
  {
    "english": "country",
    "type": "n",
    "pronounce": "ˈkʌntri",
    "vietnamese": "nước, quốc gia, đất nước"
  },
  {
    "english": "countryside",
    "type": "n",
    "pronounce": "kʌntri'said",
    "vietnamese": "miền quê, miền nông thôn"
  },
  {
    "english": "county",
    "type": "n",
    "pronounce": "koun'ti",
    "vietnamese": "hạt, tỉnh"
  },
  {
    "english": "couple",
    "type": "n",
    "pronounce": "kʌpl",
    "vietnamese": "đôi, cặp; đôi vợ chồng, cặp nam nữ. a couple một cặp, một đôi"
  },
  {
    "english": "courage",
    "type": "n",
    "pronounce": "kʌridʤ",
    "vietnamese": "sự can đảm, sự dũng cảm, dũng khí"
  },
  {
    "english": "course",
    "type": "n",
    "pronounce": "kɔ:s",
    "vietnamese": "tiến trình, quá trình diễn tiến; sân chạy đua. of course dĩ nhiên, loạt; khoá; đợt; lớp"
  },
  {
    "english": "court",
    "type": "n",
    "pronounce": "kɔrt , koʊrt",
    "vietnamese": "sân, sân (tennis...), tòa án, quan tòa, phiên tòa"
  },
  {
    "english": "cousin",
    "type": "n",
    "pronounce": "ˈkʌzən",
    "vietnamese": "anh em họ"
  },
  {
    "english": "cover",
    "type": "v, n",
    "pronounce": "kʌvə",
    "vietnamese": "bao bọc, che phủ; vỏ, vỏ bọc"
  },
  {
    "english": "covered",
    "type": "adj",
    "pronounce": "kʌvərd",
    "vietnamese": "có mái che, kín đáo"
  },
  {
    "english": "covering",
    "type": "n",
    "pronounce": "́kʌvəriη",
    "vietnamese": "sự bao bọc, sự che phủ, cái bao, bọc"
  },
  {
    "english": "cow",
    "type": "n",
    "pronounce": "kaʊ",
    "vietnamese": "con bò cái"
  },
  {
    "english": "crack",
    "type": "n, v",
    "pronounce": "kræk",
    "vietnamese": "cừ, xuất sắc; làm nứt, làm vỡ, nứt nẻ, rạn nứt"
  },
  {
    "english": "cracked",
    "type": "adj",
    "pronounce": "krækt",
    "vietnamese": "rạn, nứt"
  },
  {
    "english": "craft",
    "type": "n",
    "pronounce": "kra:ft",
    "vietnamese": "nghề, nghề thủ công"
  },
  {
    "english": "crash",
    "type": "n, v",
    "pronounce": "kræʃ",
    "vietnamese": "vải thô; sự rơi (máy bấy), sự phá sản, sụp đổ; phá tan tành, phá vụn"
  },
  {
    "english": "crazy",
    "type": "adj",
    "pronounce": "kreizi",
    "vietnamese": "điên, mất trí"
  },
  {
    "english": "cream",
    "type": "n",
    "pronounce": "kri:m",
    "vietnamese": "kem"
  },
  {
    "english": "create",
    "type": "v",
    "pronounce": "kri:'eit",
    "vietnamese": "sáng tạo, tạo nên"
  },
  {
    "english": "creature",
    "type": "n",
    "pronounce": "kri:tʃə",
    "vietnamese": "sinh vật, loài vật"
  },
  {
    "english": "credit",
    "type": "n",
    "pronounce": "ˈkrɛdɪt",
    "vietnamese": "sự tin, lòng tin, danh tiếng; tiền gử ngân hàng"
  },
  {
    "english": "credit card",
    "type": "n",
    "pronounce": "",
    "vietnamese": "thẻ tín dụng"
  },
  {
    "english": "crime",
    "type": "n",
    "pronounce": "kraim",
    "vietnamese": "tội, tội ác, tội phạm"
  },
  {
    "english": "criminal",
    "type": "adj, n",
    "pronounce": "ˈkrɪmənl",
    "vietnamese": "có tội, phạm tội; kẻ phạm tội, tội phạm"
  },
  {
    "english": "crisis",
    "type": "n",
    "pronounce": "ˈkraɪsɪs",
    "vietnamese": "sự khủng hoảng, cơn khủng hoảng"
  },
  {
    "english": "crisp",
    "type": "adj",
    "pronounce": "krips",
    "vietnamese": "giòn"
  },
  {
    "english": "criterion",
    "type": "n",
    "pronounce": "kraɪˈtɪəriən",
    "vietnamese": "tiêu chuẩn"
  },
  {
    "english": "critical",
    "type": "adj",
    "pronounce": "ˈkrɪtɪkəl",
    "vietnamese": "phê bình, phê phán; khó tính"
  },
  {
    "english": "criticism",
    "type": "n",
    "pronounce": "́kriti ̧sizəm",
    "vietnamese": "sự phê bình, sự phê phán, lời phê bình, lời phê phán"
  },
  {
    "english": "criticize",
    "type": "v",
    "pronounce": "ˈkrɪtəˌsaɪz",
    "vietnamese": "phê bình, phê phán, chỉ trích"
  },
  {
    "english": "crop",
    "type": "n",
    "pronounce": "krop",
    "vietnamese": "vụ mùa"
  },
  {
    "english": "cross",
    "type": "n, v",
    "pronounce": "krɔs",
    "vietnamese": "cây Thánh Giá, nỗi thống khổ; sự băng qua; băng qua, vượt qua"
  },
  {
    "english": "crowd",
    "type": "n",
    "pronounce": "kraud",
    "vietnamese": "đám đông"
  },
  {
    "english": "crowded",
    "type": "adj",
    "pronounce": "kraudid",
    "vietnamese": "đông đúc"
  },
  {
    "english": "crown",
    "type": "n",
    "pronounce": "kraun",
    "vietnamese": "vương miện, vua, ngai vàng; đỉnh cao nhất"
  },
  {
    "english": "crucial",
    "type": "adj",
    "pronounce": "́kru:ʃəl",
    "vietnamese": "quyết định, cốt yếu, chủ yếu"
  },
  {
    "english": "cruel",
    "type": "adj",
    "pronounce": "kru:ə(l)",
    "vietnamese": "độc ác, dữ tợn, tàn nhẫn"
  },
  {
    "english": "crush",
    "type": "v",
    "pronounce": "krᴧ∫",
    "vietnamese": "ép, vắt, đè nát, đè bẹp"
  },
  {
    "english": "cry",
    "type": "v, n",
    "pronounce": "krai",
    "vietnamese": "khóc, kêu la; sự khóc, tiếng khóc, sự kêu la"
  },
  {
    "english": "cultural",
    "type": "adj",
    "pronounce": "ˈkʌltʃərəl",
    "vietnamese": "(thuộc) văn hóa"
  },
  {
    "english": "culture",
    "type": "n",
    "pronounce": "ˈkʌltʃər",
    "vietnamese": "văn hóa, sự mở mang, sự giáo dục"
  },
  {
    "english": "cup",
    "type": "n",
    "pronounce": "kʌp",
    "vietnamese": "tách, chén"
  },
  {
    "english": "cupboard",
    "type": "n",
    "pronounce": "kʌpbəd",
    "vietnamese": "1 loại tủ có ngăn"
  },
  {
    "english": "curb",
    "type": "v",
    "pronounce": "kə:b",
    "vietnamese": "kiềm chế, nén lại, hạn chế"
  },
  {
    "english": "cure",
    "type": "v, n",
    "pronounce": "kjuə",
    "vietnamese": "chữa trị, điều trị; cách chữa bệnh, cách điều trị; thuốc"
  },
  {
    "english": "curious",
    "type": "adj",
    "pronounce": "kjuəriəs",
    "vietnamese": "ham muốn, tò mò, lạ lùng"
  },
  {
    "english": "curiously",
    "type": "adv",
    "pronounce": "kjuəriəsli",
    "vietnamese": "tò mò, hiếu kỳ, lạ kỳ"
  },
  {
    "english": "curl",
    "type": "v, n",
    "pronounce": "kə:l",
    "vietnamese": "quăn, xoắn, uốn quăn, làm xoắn; sự uốn quăn"
  },
  {
    "english": "curly",
    "type": "adj",
    "pronounce": "́kə:li",
    "vietnamese": "quăn, xoắn"
  },
  {
    "english": "current",
    "type": "adj, n",
    "pronounce": "kʌrənt",
    "vietnamese": "hiện hành, phổ biến, hiện nấy; dòng (nước), luống (gió)"
  },
  {
    "english": "currently",
    "type": "adv",
    "pronounce": "kʌrəntli",
    "vietnamese": "hiện thời, hiện nay"
  },
  {
    "english": "curtain",
    "type": "n",
    "pronounce": "kə:tn",
    "vietnamese": "màn (cửa, rạp hát, khói, sương)"
  },
  {
    "english": "curve",
    "type": "n, v",
    "pronounce": "kə:v",
    "vietnamese": "đường cong, đường vòng; cong, uốn cong, bẻ cong"
  },
  {
    "english": "curved",
    "type": "adj",
    "pronounce": "kə:vd",
    "vietnamese": "cong"
  },
  {
    "english": "custom",
    "type": "n",
    "pronounce": "kʌstəm",
    "vietnamese": "phong tục, tục lệ, thói quen, tập quán"
  },
  {
    "english": "customer",
    "type": "n",
    "pronounce": "́kʌstəmə",
    "vietnamese": "khách hàng"
  },
  {
    "english": "customs",
    "type": "n",
    "pronounce": "́kʌstəmz",
    "vietnamese": "thuế nhập khẩu, hải quan"
  },
  {
    "english": "cut",
    "type": "v, n",
    "pronounce": "kʌt",
    "vietnamese": "cắt, chặt; sự cắt"
  },
  {
    "english": "cycle",
    "type": "n, v",
    "pronounce": "saikl",
    "vietnamese": "chu kỳ, chu trình, vòng; quay vòng theo chu kỳ, đi xe đạp"
  },
  {
    "english": "cycling",
    "type": "n",
    "pronounce": "saikliŋ",
    "vietnamese": "sự đi xe đạp"
  },
  {
    "english": "dad",
    "type": "n",
    "pronounce": "dæd",
    "vietnamese": "bố, cha"
  },
  {
    "english": "daily",
    "type": "adj",
    "pronounce": "deili",
    "vietnamese": "hàng ngày"
  },
  {
    "english": "damage",
    "type": "n, v",
    "pronounce": "dæmidʤ",
    "vietnamese": "mối hạn, điều hại, sự thiệt hại; làm hư hại, làm hỏng, gây thiệt hại"
  },
  {
    "english": "damp",
    "type": "adj",
    "pronounce": "dæmp",
    "vietnamese": "ẩm, ẩm ướt, ẩm thấp"
  },
  {
    "english": "dance",
    "type": "n, v",
    "pronounce": "dɑ:ns",
    "vietnamese": "sự nhảy múa, sự khiêu vũ; nhảy múa, khiêu vũ"
  },
  {
    "english": "dancer",
    "type": "n",
    "pronounce": "dɑ:nsə",
    "vietnamese": "diễn viên múa, người nhảy múa"
  },
  {
    "english": "dancing",
    "type": "n",
    "pronounce": "dɑ:nsiɳ",
    "vietnamese": "sự nhảy múa, sự khiêu vũ"
  },
  {
    "english": "danger",
    "type": "n",
    "pronounce": "deindʤə",
    "vietnamese": "sự nguy hiểm, mối hiểm nghèo; nguy cơ, mối đe dọa"
  },
  {
    "english": "dangerous",
    "type": "adj",
    "pronounce": "́deindʒərəs",
    "vietnamese": "nguy hiểm"
  },
  {
    "english": "dare",
    "type": "v",
    "pronounce": "deər",
    "vietnamese": "dám, dám đương đầu với; thách"
  },
  {
    "english": "dark",
    "type": "adj, n",
    "pronounce": "dɑ:k",
    "vietnamese": "tối, tối tăm; bóng tối, ám muội"
  },
  {
    "english": "data",
    "type": "n",
    "pronounce": "́deitə",
    "vietnamese": "số liệu, dữ liệu"
  },
  {
    "english": "date",
    "type": "n, v",
    "pronounce": "deit",
    "vietnamese": "ngày, kỳ, kỳ hạn, thời kỳ, thời đại; đề ngày tháng, ghi niên hiệu"
  },
  {
    "english": "daughter",
    "type": "n",
    "pronounce": "ˈdɔtər",
    "vietnamese": "con gái"
  },
  {
    "english": "day",
    "type": "n",
    "pronounce": "dei",
    "vietnamese": "ngày, ban ngày"
  },
  {
    "english": "dead",
    "type": "adj",
    "pronounce": "ded",
    "vietnamese": "chết, tắt"
  },
  {
    "english": "deaf",
    "type": "adj",
    "pronounce": "def",
    "vietnamese": "điếc, làm thinh, làm ngơ"
  },
  {
    "english": "deal",
    "type": "v, n",
    "pronounce": "di:l",
    "vietnamese": "phân phát, phân phối; sự giao dịch, thỏa thuận mua bán. deal with giải quyết"
  },
  {
    "english": "dear",
    "type": "adj",
    "pronounce": "diə",
    "vietnamese": "thân, thân yêu, thân mến; kính thưa, thưa"
  },
  {
    "english": "death",
    "type": "n",
    "pronounce": "deθ",
    "vietnamese": "sự chết, cái chết"
  },
  {
    "english": "debate",
    "type": "n, v",
    "pronounce": "dɪˈbeɪt",
    "vietnamese": "cuộc tranh luận, cuộc tranh cãi; tranh luận, bàn cãi"
  },
  {
    "english": "debt",
    "type": "n",
    "pronounce": "det",
    "vietnamese": "nợ"
  },
  {
    "english": "decade",
    "type": "n",
    "pronounce": "dekeid",
    "vietnamese": "thập kỷ, bộ mười, nhóm mười"
  },
  {
    "english": "decay",
    "type": "n, v",
    "pronounce": "di'kei",
    "vietnamese": "tình trạng suy tàn, suy sụp, tình trạng đổ nát"
  },
  {
    "english": "December (abbr Dec)",
    "type": "n",
    "pronounce": "di'sembə",
    "vietnamese": "tháng mười hai, tháng Chạp"
  },
  {
    "english": "decide",
    "type": "v",
    "pronounce": "di'said",
    "vietnamese": "quyết định, giải quyết, phân xử"
  },
  {
    "english": "decision",
    "type": "n",
    "pronounce": "diˈsiʒn",
    "vietnamese": "sự quyết định, sự giải quyết, sự phân xử"
  },
  {
    "english": "declare",
    "type": "v",
    "pronounce": "di'kleə",
    "vietnamese": "tuyên bố, công bố"
  },
  {
    "english": "decline",
    "type": "n, v",
    "pronounce": "di'klain",
    "vietnamese": "sự suy tàn, sự suy sụp; suy sụp, suy tàn"
  },
  {
    "english": "decorate",
    "type": "v",
    "pronounce": "́dekə ̧reit",
    "vietnamese": "trang hoàng, trang trí"
  },
  {
    "english": "decoration",
    "type": "n",
    "pronounce": "̧dekə ́reiʃən",
    "vietnamese": "sự trang hoàng, đồ trang hoàng, trang trí"
  },
  {
    "english": "decorative",
    "type": "adj",
    "pronounce": "́dekərətiv",
    "vietnamese": "để trang hoàng, để trang trí, để làm cảnh"
  },
  {
    "english": "decrease",
    "type": "v, n",
    "pronounce": "di:kri:s",
    "vietnamese": "giảm bớt, làm suy giảm, sự giảm đi, sự giảm sút"
  },
  {
    "english": "deep",
    "type": "adj, adv",
    "pronounce": "di:p",
    "vietnamese": "sâu, khó lường, bí ẩn"
  },
  {
    "english": "deeply",
    "type": "adv",
    "pronounce": "́di:pli",
    "vietnamese": "sâu, sâu xa, sâu sắc"
  },
  {
    "english": "defeat",
    "type": "v, n",
    "pronounce": "di'fi:t",
    "vietnamese": "đánh thắng, đánh bại; sự thất bại (1 kế hoạch), sự tiêu tan (hyvọng..)"
  },
  {
    "english": "defence",
    "type": "n",
    "pronounce": "di'fens",
    "vietnamese": "cái để bảo vệ, vật để chống đỡ, sự che chở"
  },
  {
    "english": "defend",
    "type": "v",
    "pronounce": "di'fend",
    "vietnamese": "che chở, bảo vệ, bào chữa"
  },
  {
    "english": "define",
    "type": "v",
    "pronounce": "di'fain",
    "vietnamese": "định nghĩa"
  },
  {
    "english": "definite",
    "type": "adj",
    "pronounce": "də'finit",
    "vietnamese": "xác định, định rõ, rõ ràng"
  },
  {
    "english": "definitely",
    "type": "adv",
    "pronounce": "definitli",
    "vietnamese": "rạch ròi, dứt khoát"
  },
  {
    "english": "definition",
    "type": "n",
    "pronounce": "defini∫n",
    "vietnamese": "sự định nghĩa, lời định nghĩa"
  },
  {
    "english": "degree",
    "type": "n",
    "pronounce": "dɪˈgri:",
    "vietnamese": "mức độ, trình độ; bằng cấp; độ"
  },
  {
    "english": "delay",
    "type": "n, v",
    "pronounce": "dɪˈleɪ",
    "vietnamese": "sự chậm trễ, sự trì hoãn, sự cản trở; làm chậm trễ"
  },
  {
    "english": "deliberate",
    "type": "adj",
    "pronounce": "di'libəreit",
    "vietnamese": "thận trọng, có tính toán, chủ tâm, có suy nghĩ cân nhắc"
  },
  {
    "english": "deliberately",
    "type": "adv",
    "pronounce": "di ́libəritli",
    "vietnamese": "thận trọng, có suy nghĩ cân nhắc"
  },
  {
    "english": "delicate",
    "type": "adj",
    "pronounce": "delikeit",
    "vietnamese": "thanh nhã, thanh tú, tế nhị, khó xử"
  },
  {
    "english": "delight",
    "type": "n, v",
    "pronounce": "di'lait",
    "vietnamese": "sự vui thích, sự vui sướng, điều thích thú; làm vui thích, làm say mê"
  },
  {
    "english": "delighted",
    "type": "adj",
    "pronounce": "di'laitid",
    "vietnamese": "vui mừng, hài lòng"
  },
  {
    "english": "deliver",
    "type": "v",
    "pronounce": "di'livə",
    "vietnamese": "cứu khỏi, thoát khỏi, bày tỏ, giãi bày"
  },
  {
    "english": "delivery",
    "type": "n",
    "pronounce": "di'livəri",
    "vietnamese": "sự phân phát, sự phân phối, sự giao hàng; sự bày tỏ, phát biếu"
  },
  {
    "english": "demand",
    "type": "n, v",
    "pronounce": "dɪ.ˈmænd",
    "vietnamese": "sự đòi hỏi, sự yêu cầu; đòi hỏi, yêu cầu"
  },
  {
    "english": "demonstrate",
    "type": "v",
    "pronounce": "ˈdɛmənˌstreɪt",
    "vietnamese": "chứng minh, giải thích; bày tỏ, biểu lộ"
  },
  {
    "english": "dentist",
    "type": "n",
    "pronounce": "dentist",
    "vietnamese": "nha sĩ"
  },
  {
    "english": "deny",
    "type": "v",
    "pronounce": "di'nai",
    "vietnamese": "từ chối, phản đối, phủ nhận"
  },
  {
    "english": "department",
    "type": "n",
    "pronounce": "di'pɑ:tmənt",
    "vietnamese": "cục, sở, ty, ban, khoa; gian hàng, khu bày hàng"
  },
  {
    "english": "departure",
    "type": "n",
    "pronounce": "di'pɑ:tʃə",
    "vietnamese": "sự rời khỏi, sự đi, sự khởi hành"
  },
  {
    "english": "depend",
    "type": "on, v",
    "pronounce": "di'pend",
    "vietnamese": "phụ thuộc, tùy thuộc; dựa vào, ỷ vào, trông mong vào"
  },
  {
    "english": "deposit",
    "type": "n, v",
    "pronounce": "dɪˈpɒzɪt",
    "vietnamese": "vật gửi, tiền gửi, tiền đặt cọc; gửi, đặt cọc"
  },
  {
    "english": "depress",
    "type": "v",
    "pronounce": "di ́pres",
    "vietnamese": "làm chán nản, làm phiền muộn; làm suy giảm"
  },
  {
    "english": "depressed",
    "type": "adj",
    "pronounce": "di-'prest",
    "vietnamese": "chán nản, thất vọng, phiền muộn; suy yếu, đình trệ"
  },
  {
    "english": "depressing",
    "type": "adj",
    "pronounce": "di'presiη",
    "vietnamese": "làm chán nản làm thát vọng, làm trì trệ"
  },
  {
    "english": "depth",
    "type": "n",
    "pronounce": "depθ",
    "vietnamese": "chiều sâu, độ dày"
  },
  {
    "english": "derive",
    "type": "v",
    "pronounce": "di ́raiv",
    "vietnamese": "nhận được từ, lấy được từ; xuất phát từ, bắt nguồn, chuyển hóa từ (from)"
  },
  {
    "english": "describe",
    "type": "v",
    "pronounce": "dɪˈskraɪb",
    "vietnamese": "diễn tả, miêu tả, mô tả"
  },
  {
    "english": "description",
    "type": "n",
    "pronounce": "dɪˈskrɪpʃən",
    "vietnamese": "sự mô tả, sự tả, sự miêu tả"
  },
  {
    "english": "desert",
    "type": "n, v",
    "pronounce": "ˈdɛzərt",
    "vietnamese": "sa mạc; công lao, giá trị; rời bỏ, bỏ trốn"
  },
  {
    "english": "deserted",
    "type": "adj",
    "pronounce": "di'zз:tid",
    "vietnamese": "hoang vắng, không người ở"
  },
  {
    "english": "deserve",
    "type": "v",
    "pronounce": "di'zз:v",
    "vietnamese": "đáng, xứng đáng"
  },
  {
    "english": "design",
    "type": "n, v",
    "pronounce": "di ́zain",
    "vietnamese": "sự thiết kế, kế hoạch, đề cương. phác thảo; phác họa, thiết kế"
  },
  {
    "english": "desire",
    "type": "n, v",
    "pronounce": "di'zaiə",
    "vietnamese": "ước muốn; thèm muốn, ao ước"
  },
  {
    "english": "desk",
    "type": "n",
    "pronounce": "desk",
    "vietnamese": "bàn (học sinh, viết, làm việc)"
  },
  {
    "english": "desperate",
    "type": "adj",
    "pronounce": "despərit",
    "vietnamese": "liều mạng, liều lĩnh; tuyệt vọng"
  },
  {
    "english": "desperately",
    "type": "adv",
    "pronounce": "despəritli",
    "vietnamese": "liều lĩnh, liều mạng"
  },
  {
    "english": "despite",
    "type": "prep",
    "pronounce": "dis'pait",
    "vietnamese": "dù, mặc dù, bất chấp"
  },
  {
    "english": "destroy",
    "type": "v",
    "pronounce": "dis'trɔi",
    "vietnamese": "phá, phá hoại, phá huỷ, tiêu diệt, triệt phá"
  },
  {
    "english": "destruction",
    "type": "n",
    "pronounce": "dis'trʌk∫n",
    "vietnamese": "sự phá hoại, sự phá hủy, sự tiêu diệt"
  },
  {
    "english": "detail",
    "type": "n",
    "pronounce": "(n) ˈditeɪl ; (v) dɪˈteɪl",
    "vietnamese": "chi tiết. in detail: tường tận, tỉ mỉ"
  },
  {
    "english": "detailed",
    "type": "adj",
    "pronounce": "di:teild",
    "vietnamese": "cặn kẽ, tỉ mỉ, nhiều chi tiết"
  },
  {
    "english": "determination",
    "type": "n",
    "pronounce": "di,tə:mi'neiʃn",
    "vietnamese": "sự xác định, sự định rõ; sự quyết định"
  },
  {
    "english": "determine",
    "type": "v",
    "pronounce": "di'tз:min",
    "vietnamese": "xác định, định rõ; quyết định"
  },
  {
    "english": "determined",
    "type": "adj",
    "pronounce": "di ́tə:mind",
    "vietnamese": "đã được xác định, đã được xác định rõ"
  },
  {
    "english": "develop",
    "type": "v",
    "pronounce": "di'veləp",
    "vietnamese": "phát triển, mở rộng; trình bày, bày tỏ"
  },
  {
    "english": "development",
    "type": "n",
    "pronounce": "di’veləpmənt",
    "vietnamese": "sự phát triển, sự trình bày, sự bày tỏ"
  },
  {
    "english": "device",
    "type": "n",
    "pronounce": "di'vais",
    "vietnamese": "kế sách; thiết bị, dụng cụ, máy móc"
  },
  {
    "english": "devote",
    "type": "v",
    "pronounce": "di'vout",
    "vietnamese": "hiến dâng, dành hết cho"
  },
  {
    "english": "devoted",
    "type": "adj",
    "pronounce": "di ́voutid",
    "vietnamese": "hiến cho, dâng cho, dành cho; hết lòng, nhiệt tình"
  },
  {
    "english": "diagram",
    "type": "n",
    "pronounce": "ˈdaɪəˌgræm",
    "vietnamese": "biểu đồ"
  },
  {
    "english": "diamond",
    "type": "n",
    "pronounce": "́daiəmənd",
    "vietnamese": "kim cương"
  },
  {
    "english": "diary",
    "type": "n",
    "pronounce": "daiəri",
    "vietnamese": "sổ nhật ký; lịch ghi nhớ"
  },
  {
    "english": "dictionary",
    "type": "n",
    "pronounce": "dikʃənəri",
    "vietnamese": "từ điển"
  },
  {
    "english": "die",
    "type": "v",
    "pronounce": "daɪ",
    "vietnamese": "chết, tư trần, hy sinh"
  },
  {
    "english": "diet",
    "type": "n",
    "pronounce": "daiət",
    "vietnamese": "chế độ ăn uống, chế độ ăn kiêng"
  },
  {
    "english": "difference",
    "type": "n",
    "pronounce": "ˈdɪfərəns , ˈdɪfrəns",
    "vietnamese": "sự khác nhau"
  },
  {
    "english": "different",
    "type": "adj",
    "pronounce": "difrзnt",
    "vietnamese": "khác, khác biệt, khác nhau"
  },
  {
    "english": "differently",
    "type": "adv",
    "pronounce": "difrзntli",
    "vietnamese": "khác, khác biệt, khác nhau"
  },
  {
    "english": "difficult",
    "type": "adj",
    "pronounce": "difik(ə)lt",
    "vietnamese": "khó, khó khăn, gấy go"
  },
  {
    "english": "difficulty",
    "type": "n",
    "pronounce": "difikəlti",
    "vietnamese": "sự khó khăn, nỗi khó khăn, điều cản trở"
  },
  {
    "english": "dig",
    "type": "v",
    "pronounce": "dɪg",
    "vietnamese": "đào bới, xới"
  },
  {
    "english": "dinner",
    "type": "n",
    "pronounce": "dinə",
    "vietnamese": "bữa trưa, chiều"
  },
  {
    "english": "direct",
    "type": "adj, v",
    "pronounce": "di'rekt; dai'rekt",
    "vietnamese": "trực tiếp, thẳng, thẳng thắn; gửi, viết cho ai, điều khiển"
  },
  {
    "english": "direction",
    "type": "n",
    "pronounce": "di'rek∫n",
    "vietnamese": "sự điều khiển, sự chỉ huy"
  },
  {
    "english": "directly",
    "type": "adv",
    "pronounce": "dai ́rektli",
    "vietnamese": "trực tiếp, thẳng"
  },
  {
    "english": "director",
    "type": "n",
    "pronounce": "di'rektə",
    "vietnamese": "giám đốc, người điều khiển, chỉ huy"
  },
  {
    "english": "dirt",
    "type": "n",
    "pronounce": "də:t",
    "vietnamese": "đồ bẩn thỉu, đồ dơ bẩn, vật rác rưởi"
  },
  {
    "english": "dirty",
    "type": "adj",
    "pronounce": "́də:ti",
    "vietnamese": "bẩn thỉu, dơ bẩn"
  },
  {
    "english": "disabled",
    "type": "adj",
    "pronounce": "dis ́eibld",
    "vietnamese": "bất lực, không có khả năng"
  },
  {
    "english": "disadvantage",
    "type": "n",
    "pronounce": "dɪsədˈvɑntɪdʒ",
    "vietnamese": "sự bất lợi, sự thiệt hại"
  },
  {
    "english": "disagree",
    "type": "v",
    "pronounce": "̧disə ́gri:",
    "vietnamese": "bất đồng, không đồng ý, khác, không giống; không hợp"
  },
  {
    "english": "disagreement",
    "type": "n",
    "pronounce": "̧disəg ́ri:mənt",
    "vietnamese": "sự bất đồng, sự không đồng ý, sự khác nhau"
  },
  {
    "english": "disappear",
    "type": "v",
    "pronounce": "disə'piə",
    "vietnamese": "biến mất, biến đi"
  },
  {
    "english": "disappoint",
    "type": "v",
    "pronounce": "dɪsəˈpɔɪnt",
    "vietnamese": "không làm thỏa ước nguyện, ý mong đợi; thất ước, làm thất bại"
  },
  {
    "english": "disappointed",
    "type": "adj",
    "pronounce": ",disз'pointid",
    "vietnamese": "thất vọng"
  },
  {
    "english": "disappointing",
    "type": "adj",
    "pronounce": "̧disə ́pɔintiη",
    "vietnamese": "làm chán ngán, làm thất vọng"
  },
  {
    "english": "disappointment",
    "type": "n",
    "pronounce": "̧disə ́pɔintmənt",
    "vietnamese": "sự chán ngán, sự thất vọng"
  },
  {
    "english": "disapproval",
    "type": "n",
    "pronounce": "̧disə ́pru:vl",
    "vietnamese": "sự phản đổi, sự không tán thành"
  },
  {
    "english": "disapprove",
    "type": "of, v",
    "pronounce": "̧disə ́pru:v",
    "vietnamese": "không tán thành, phản đối, chê"
  },
  {
    "english": "disapproving",
    "type": "adj",
    "pronounce": "̧disə ́pru:viη",
    "vietnamese": "phản đối"
  },
  {
    "english": "disaster",
    "type": "n",
    "pronounce": "di'zɑ:stə",
    "vietnamese": "tai họa, thảm họa"
  },
  {
    "english": "disc, disk",
    "type": "n",
    "pronounce": "disk",
    "vietnamese": "đĩa"
  },
  {
    "english": "discipline",
    "type": "n",
    "pronounce": "disiplin",
    "vietnamese": "kỷ luật"
  },
  {
    "english": "discount",
    "type": "n",
    "pronounce": "diskaunt",
    "vietnamese": "sự bớt giá, sự chiết khấu, tiền bớt chiết khấu"
  },
  {
    "english": "discover",
    "type": "v",
    "pronounce": "dis'kʌvə",
    "vietnamese": "khám phá, phát hiện ra, nhận ra"
  },
  {
    "english": "discovery",
    "type": "n",
    "pronounce": "dis'kʌvəri",
    "vietnamese": "sự khám phá, sự tìm ra, sự phát hiện ra"
  },
  {
    "english": "discuss",
    "type": "v",
    "pronounce": "dis'kΛs",
    "vietnamese": "thảo luận, tranh luận"
  },
  {
    "english": "discussion",
    "type": "n",
    "pronounce": "dis'kʌʃn",
    "vietnamese": "sự thảo luận, sự tranh luận"
  },
  {
    "english": "disease",
    "type": "n",
    "pronounce": "di'zi:z",
    "vietnamese": "căn bệnh, bệnh tật"
  },
  {
    "english": "disgust",
    "type": "v, n",
    "pronounce": "dis ́gʌst",
    "vietnamese": "làm ghê tởm, làm kinh tởm, làm phẫn nộ"
  },
  {
    "english": "disgusted",
    "type": "adj",
    "pronounce": "dis ́gʌstid",
    "vietnamese": "chán ghét, phẫn nộ"
  },
  {
    "english": "disgusting",
    "type": "adj",
    "pronounce": "dis ́gʌstiη",
    "vietnamese": "làm ghê tởm, kinh tởm"
  },
  {
    "english": "dish",
    "type": "n",
    "pronounce": "diʃ",
    "vietnamese": "đĩa (đựng thức ăn)"
  },
  {
    "english": "dishonest",
    "type": "adj",
    "pronounce": "dis ́ɔnist",
    "vietnamese": "bất lương, không thành thật"
  },
  {
    "english": "dishonestly",
    "type": "adv",
    "pronounce": "dis'onistli",
    "vietnamese": "bất lương, không lương thiện"
  },
  {
    "english": "disk",
    "type": "n",
    "pronounce": "disk",
    "vietnamese": "đĩa, đĩa hát"
  },
  {
    "english": "dislike",
    "type": "v, n",
    "pronounce": "dis'laik",
    "vietnamese": "sự không ưa, không thích, sự ghé"
  },
  {
    "english": "dismiss",
    "type": "v",
    "pronounce": "dis'mis",
    "vietnamese": "giải tán (quân đội, đám đông); sa thải (người làm)"
  },
  {
    "english": "display",
    "type": "v, n",
    "pronounce": "dis'plei",
    "vietnamese": "bày tỏ, phô trương, trưng bày; sự bày ra, phô bày, trưng bày"
  },
  {
    "english": "dissolve",
    "type": "v",
    "pronounce": "dɪˈzɒlv",
    "vietnamese": "tan rã, phân hủy, giải tán"
  },
  {
    "english": "distance",
    "type": "n",
    "pronounce": "distəns",
    "vietnamese": "khoảng cách, tầm xa"
  },
  {
    "english": "distinguish",
    "type": "v",
    "pronounce": "dis ́tiηgwiʃ",
    "vietnamese": "phân biệt, nhận ra, nghe ra"
  },
  {
    "english": "distribute",
    "type": "v",
    "pronounce": "dis'tribju:t",
    "vietnamese": "phân bổ, phân phối, sắp xếp, phân loại"
  },
  {
    "english": "distribution",
    "type": "n",
    "pronounce": ",distri'bju:ʃn",
    "vietnamese": "sự phân bổ, sự phân phối, phân phát, sự sắp xếp"
  },
  {
    "english": "district",
    "type": "n",
    "pronounce": "distrikt",
    "vietnamese": "huyện, quận"
  },
  {
    "english": "disturb",
    "type": "v",
    "pronounce": "dis ́tə:b",
    "vietnamese": "làm mất yên tĩnh, làm náo động, quấy rầy"
  },
  {
    "english": "disturbing",
    "type": "adj",
    "pronounce": "dis ́tə:biη",
    "vietnamese": "xáo trộn"
  },
  {
    "english": "divide",
    "type": "v",
    "pronounce": "di'vaid",
    "vietnamese": "chia, chia ra, phân ra"
  },
  {
    "english": "division",
    "type": "n",
    "pronounce": "dɪ'vɪʒn",
    "vietnamese": "sự chia, sự phân chia, sự phân loại"
  },
  {
    "english": "divorce",
    "type": "n, v",
    "pronounce": "di ́vɔ:s",
    "vietnamese": "sự ly dị"
  },
  {
    "english": "divorced",
    "type": "adj",
    "pronounce": "di'vo:sd",
    "vietnamese": "đã ly dị"
  },
  {
    "english": "do",
    "type": "vauxiliary, v",
    "pronounce": "du:, du",
    "vietnamese": "làm"
  },
  {
    "english": "doctor (abbr Dr)",
    "type": "n",
    "pronounce": "dɔktə",
    "vietnamese": "bác sĩ y khoa, tiến sĩ"
  },
  {
    "english": "document",
    "type": "n",
    "pronounce": "dɒkjʊmənt",
    "vietnamese": "văn kiện, tài liệu, tư liệu"
  },
  {
    "english": "dog",
    "type": "n",
    "pronounce": "dɔg",
    "vietnamese": "chó"
  },
  {
    "english": "dollar",
    "type": "n",
    "pronounce": "́dɔlə",
    "vietnamese": "đô la Mỹ"
  },
  {
    "english": "domestic",
    "type": "adj",
    "pronounce": "də'mestik",
    "vietnamese": "vật nuôi trong nhà, (thuộc) nội trợ, quốc nội"
  },
  {
    "english": "dominate",
    "type": "v",
    "pronounce": "ˈdɒməˌneɪt",
    "vietnamese": "chiếm ưu thế, có ảnh hưởng, chi phối; kiềm chế"
  },
  {
    "english": "door",
    "type": "n",
    "pronounce": "dɔ:",
    "vietnamese": "cửa, cửa ra vào"
  },
  {
    "english": "dot",
    "type": "n",
    "pronounce": "dɔt",
    "vietnamese": "chấm nhỏ, điểm; của hồi môn"
  },
  {
    "english": "double",
    "type": "adj, det, adv, n, v",
    "pronounce": "dʌbl",
    "vietnamese": "đôi, hai, kép; cái gấp đôi, lượng gấp đôi; làm gấp đôi"
  },
  {
    "english": "doubt",
    "type": "n, v",
    "pronounce": "daut",
    "vietnamese": "sự nghi ngờ, sự ngờ vực; nghi ngờ, ngờ vực"
  },
  {
    "english": "down",
    "type": "adv, prep",
    "pronounce": "daun",
    "vietnamese": "xuống"
  },
  {
    "english": "downstairs",
    "type": "adv, adj, n",
    "pronounce": "daun'steзz",
    "vietnamese": "ở dưới nhà, ở tầng dưới; xống gác; tầng dưới"
  },
  {
    "english": "downward",
    "type": "adj",
    "pronounce": "́daun ̧wəd",
    "vietnamese": "xuống, đi xuống"
  },
  {
    "english": "downwards",
    "type": "adv",
    "pronounce": "́daun ̧wədz",
    "vietnamese": "xuống, đi xuống"
  },
  {
    "english": "dozen",
    "type": "ndet",
    "pronounce": "dʌzn",
    "vietnamese": "tá (12)"
  },
  {
    "english": "draft",
    "type": "n, adj, v",
    "pronounce": "dra:ft",
    "vietnamese": "bản phác thảo, sơ đồ thiết kế; phác thảo, thiết kế"
  },
  {
    "english": "drag",
    "type": "v",
    "pronounce": "drӕg",
    "vietnamese": "lôi kéo, kéo lê"
  },
  {
    "english": "drama",
    "type": "n",
    "pronounce": "drɑː.mə",
    "vietnamese": "kịch, tuồng"
  },
  {
    "english": "dramatic",
    "type": "adj",
    "pronounce": "drə ́mætik",
    "vietnamese": "như kịch, như đóng kịch, thích hợp với sân khấu"
  },
  {
    "english": "dramatically",
    "type": "adv",
    "pronounce": "drə'mætikəli",
    "vietnamese": "đột ngột"
  },
  {
    "english": "draw",
    "type": "v",
    "pronounce": "dro:",
    "vietnamese": "vẽ, kéo"
  },
  {
    "english": "drawer",
    "type": "n",
    "pronounce": "́drɔ:ə",
    "vietnamese": "người vẽ, người kéo"
  },
  {
    "english": "drawing",
    "type": "n",
    "pronounce": "dro:iŋ",
    "vietnamese": "bản vẽ, bức vẽ, sự kéo"
  },
  {
    "english": "dream",
    "type": "n, v",
    "pronounce": "dri:m",
    "vietnamese": "giấc mơ, mơ"
  },
  {
    "english": "dress",
    "type": "n, v",
    "pronounce": "dres",
    "vietnamese": "quần áo, mặc (quần áo), ăn mặc"
  },
  {
    "english": "dressed",
    "type": "adj",
    "pronounce": "",
    "vietnamese": "cách ăn mặc"
  },
  {
    "english": "drink",
    "type": "n, v",
    "pronounce": "driɳk",
    "vietnamese": "đồ uống; uống"
  },
  {
    "english": "drive",
    "type": "v, n",
    "pronounce": "draiv",
    "vietnamese": "lái , đua xe; cuộc đua xe (điều khiển)"
  },
  {
    "english": "driver",
    "type": "n",
    "pronounce": "draivə(r)",
    "vietnamese": "người lái xe"
  },
  {
    "english": "driving",
    "type": "n",
    "pronounce": "draiviɳ",
    "vietnamese": "sự lái xe, cuộc đua xe"
  },
  {
    "english": "drop",
    "type": "v, n",
    "pronounce": "drɒp",
    "vietnamese": "chảy nhỏ giọt, rơi, rớt; giọt (nước, máu...)"
  },
  {
    "english": "drug",
    "type": "n",
    "pronounce": "drʌg",
    "vietnamese": "thuốc, dược phẩm; ma túy"
  },
  {
    "english": "drugstore",
    "type": "n",
    "pronounce": "drʌgstɔ:",
    "vietnamese": "hiệu thuốc, cửa hàng dược phẩm"
  },
  {
    "english": "drum",
    "type": "n",
    "pronounce": "drʌm",
    "vietnamese": "cái trống, tiếng trống"
  },
  {
    "english": "enjoy",
    "type": "v",
    "pronounce": "in'dЗoi",
    "vietnamese": "thưởng thức, thích thú cái gì, được hưởng, có được"
  },
  {
    "english": "enjoyable",
    "type": "adj",
    "pronounce": "in ́dʒɔiəbl",
    "vietnamese": "thú vị, thích thú"
  },
  {
    "english": "enjoyment",
    "type": "n",
    "pronounce": "in ́dʒɔimənt",
    "vietnamese": "sự thích thú, sự có được, được hưởng"
  },
  {
    "english": "enormous",
    "type": "adj",
    "pronounce": "i'nɔ:məs",
    "vietnamese": "to lớn, khổng lồ"
  },
  {
    "english": "enough",
    "type": "det, pron, adv",
    "pronounce": "i'nʌf",
    "vietnamese": "đủ"
  },
  {
    "english": "enquiry",
    "type": "n",
    "pronounce": "in'kwaiəri",
    "vietnamese": "sự điều tra, sự thẩm vấn"
  },
  {
    "english": "ensure",
    "type": "v",
    "pronounce": "ɛnˈʃʊər , ɛnˈʃɜr",
    "vietnamese": "bảo đảm, chắc chắn"
  },
  {
    "english": "enter",
    "type": "v",
    "pronounce": "́entə",
    "vietnamese": "đi vào, gia nhập"
  },
  {
    "english": "entertain",
    "type": "v",
    "pronounce": ",entə'tein",
    "vietnamese": "giải trí, tiếp đón, chiêu đãi"
  },
  {
    "english": "entertainer",
    "type": "n",
    "pronounce": "̧entə ́teinə",
    "vietnamese": "người quản trò, người tiếp đãi, chiêu đãi"
  },
  {
    "english": "entertaining",
    "type": "adj",
    "pronounce": ",entə'teiniɳ",
    "vietnamese": "giải trí"
  },
  {
    "english": "entertainment",
    "type": "n",
    "pronounce": "entə'teinm(ə)nt",
    "vietnamese": "sự giải trí, sự tiếp đãi, chiêu đãi"
  },
  {
    "english": "enthusiasm",
    "type": "n",
    "pronounce": "ɛnˈθuziˌæzəm",
    "vietnamese": "sự hăng hái, sự nhiệt tình"
  },
  {
    "english": "enthusiastic",
    "type": "adj",
    "pronounce": "ɛnˌθuziˈæstɪk",
    "vietnamese": "hăng hái, say mê, nhiệt tình"
  },
  {
    "english": "entire",
    "type": "adj",
    "pronounce": "in'taiə",
    "vietnamese": "toàn thể, toàn bộ"
  },
  {
    "english": "entirely",
    "type": "adv",
    "pronounce": "in ́taiəli",
    "vietnamese": "toàn vẹn, trọn vẹn, toàn bộ"
  },
  {
    "english": "entitle",
    "type": "v",
    "pronounce": "in'taitl",
    "vietnamese": "cho tiêu đề, cho tên (sách); cho quyền làm gì"
  },
  {
    "english": "entrance",
    "type": "n",
    "pronounce": "entrəns",
    "vietnamese": "sự đi vào, sự nhậm chức"
  },
  {
    "english": "entry",
    "type": "n",
    "pronounce": "ˈɛntri",
    "vietnamese": "sự ghi vào sổ sách, sự đi vào, sự tiếp nhận (pháp lý)"
  },
  {
    "english": "envelope",
    "type": "n",
    "pronounce": "enviloup",
    "vietnamese": "phong bì"
  },
  {
    "english": "environment",
    "type": "n",
    "pronounce": "in'vaiərənmənt",
    "vietnamese": "môi trường, hoàn cảnh xung quanh"
  },
  {
    "english": "environmental",
    "type": "adj",
    "pronounce": "in,vairən'mentl",
    "vietnamese": "thuộc về môi trường"
  },
  {
    "english": "equal",
    "type": "adj, n, v",
    "pronounce": "́i:kwəl",
    "vietnamese": "ngang, bằng; người ngang hàng, ngang tài, sức; bằng, ngang"
  },
  {
    "english": "equally",
    "type": "adv",
    "pronounce": "i:kwзli",
    "vietnamese": "bằng nhau, ngang bằng"
  },
  {
    "english": "equipment",
    "type": "n",
    "pronounce": "i'kwipmənt",
    "vietnamese": "trang, thiết bị"
  },
  {
    "english": "equivalent",
    "type": "adj, n",
    "pronounce": "i ́kwivələnt",
    "vietnamese": "tương đương; tư, vật tương đương"
  },
  {
    "english": "error",
    "type": "n",
    "pronounce": "erə",
    "vietnamese": "lỗi, sự sai sót, sai lầm"
  },
  {
    "english": "escape",
    "type": "v, n",
    "pronounce": "is'keip",
    "vietnamese": "trốn thoát, thoát khỏi; sự trốn thoát, lỗi thoát"
  },
  {
    "english": "especially",
    "type": "adv",
    "pronounce": "is'peʃəli",
    "vietnamese": "đặc biệt là, nhất là"
  },
  {
    "english": "essay",
    "type": "n",
    "pronounce": "ˈɛseɪ",
    "vietnamese": "bài tiểu luận"
  },
  {
    "english": "essential",
    "type": "adj, n",
    "pronounce": "əˈsɛnʃəl",
    "vietnamese": "bản chất, thực chất, cốt yếu; yếu tố cần thiết"
  },
  {
    "english": "essentially",
    "type": "adv",
    "pronounce": "e ̧senʃi ́əli",
    "vietnamese": "về bản chất, về cơ bản"
  },
  {
    "english": "establish",
    "type": "v",
    "pronounce": "ɪˈstæblɪʃ",
    "vietnamese": "lập, thành lập"
  },
  {
    "english": "estate",
    "type": "n",
    "pronounce": "ɪˈsteɪt",
    "vietnamese": "tài sản, di sản, bất động sản"
  },
  {
    "english": "estimate",
    "type": "n, v",
    "pronounce": "estimit - 'estimeit",
    "vietnamese": "sự ước lượng, đánh giá; ước lượng, đánh giá"
  },
  {
    "english": "etc., et cetera",
    "type": "",
    "pronounce": "et setərə",
    "vietnamese": "vân vân"
  },
  {
    "english": "euro",
    "type": "n",
    "pronounce": "́ju:rou",
    "vietnamese": "đơn vị tiền tệ của liên minh châu Âu"
  },
  {
    "english": "even",
    "type": "adv, adj",
    "pronounce": "i:vn",
    "vietnamese": "ngay cả, ngay, lại còn; bằng phẳng, điềm đạm, ngang bằng"
  },
  {
    "english": "evening",
    "type": "n",
    "pronounce": "i:vniɳ",
    "vietnamese": "buổi chiều, tối"
  },
  {
    "english": "event",
    "type": "n",
    "pronounce": "i'vent",
    "vietnamese": "sự việc, sự kiện"
  },
  {
    "english": "eventually",
    "type": "adv",
    "pronounce": "i ́ventjuəli",
    "vietnamese": "cuối cùng"
  },
  {
    "english": "ever",
    "type": "adv",
    "pronounce": "evə(r)",
    "vietnamese": "từng, từ trước tới giờ"
  },
  {
    "english": "every",
    "type": "det",
    "pronounce": "evəri",
    "vietnamese": "mỗi, mọi"
  },
  {
    "english": "everyone, everybody",
    "type": "pron",
    "pronounce": "́evri ̧wʌn",
    "vietnamese": "mọi người"
  },
  {
    "english": "everything",
    "type": "pron",
    "pronounce": "evriθiɳ",
    "vietnamese": "mọi vật, mọi thứ"
  },
  {
    "english": "everywhere",
    "type": "adv",
    "pronounce": "́evri ̧weə",
    "vietnamese": "mọi nơi"
  },
  {
    "english": "evidence",
    "type": "n",
    "pronounce": "evidəns",
    "vietnamese": "điều hiển nhiên, điều rõ ràng"
  },
  {
    "english": "evil",
    "type": "adj, n",
    "pronounce": "i:vl",
    "vietnamese": "xấu, ác; điều xấu, điều ác, điều tai hại"
  },
  {
    "english": "ex",
    "type": "-",
    "pronounce": "prefix",
    "vietnamese": "tiền tố chỉ bên ngoài"
  },
  {
    "english": "exact",
    "type": "adj",
    "pronounce": "ig ́zækt",
    "vietnamese": "chính xác, đúng"
  },
  {
    "english": "exactly",
    "type": "adv",
    "pronounce": "ig ́zæktli",
    "vietnamese": "chính xác, đúng đắn"
  },
  {
    "english": "exaggerate",
    "type": "v",
    "pronounce": "ig ́zædʒə ̧reit",
    "vietnamese": "cường điệu, phóng đại"
  },
  {
    "english": "exaggerated",
    "type": "adj",
    "pronounce": "ig'zædЗзreit",
    "vietnamese": "cường điệu, phòng đại"
  },
  {
    "english": "exam",
    "type": "n",
    "pronounce": "ig ́zæm",
    "vietnamese": "viết tắt của Examination (xem nghĩa phía dưới)"
  },
  {
    "english": "examination",
    "type": "n",
    "pronounce": "ig ̧zæmi ́neiʃən",
    "vietnamese": "sự thi cử, kỳ thi"
  },
  {
    "english": "examine",
    "type": "v",
    "pronounce": "ɪgˈzæmɪn",
    "vietnamese": "thẩm tra, khám xét, hỏi han (thí sinh)"
  },
  {
    "english": "example",
    "type": "n",
    "pronounce": "ig ́za:mp(ə)l",
    "vietnamese": "thí dụ, ví dụ"
  },
  {
    "english": "excellent",
    "type": "adj",
    "pronounce": "ˈeksələnt",
    "vietnamese": "xuất sắc, xuất chúng"
  },
  {
    "english": "except",
    "type": "prep, conj",
    "pronounce": "ik'sept",
    "vietnamese": "trừ ra, không kể; trừ phi"
  },
  {
    "english": "exception",
    "type": "n",
    "pronounce": "ik'sepʃn",
    "vietnamese": "sự trừ ra, sự loại ra"
  },
  {
    "english": "exchange",
    "type": "v, n",
    "pronounce": "iks ́tʃeindʒ",
    "vietnamese": "trao đổi; sự trao đổi"
  },
  {
    "english": "excite",
    "type": "v",
    "pronounce": "ik'sait",
    "vietnamese": "kích thích, kích động"
  },
  {
    "english": "excited",
    "type": "adj",
    "pronounce": "ɪkˈsaɪtɪd",
    "vietnamese": "bị kích thích, bị kích động"
  },
  {
    "english": "excitement",
    "type": "n",
    "pronounce": "ik ́saitmənt",
    "vietnamese": "sự kích thích, sự kích động"
  },
  {
    "english": "exciting",
    "type": "adj",
    "pronounce": "ik ́saitiη",
    "vietnamese": "hứng thú, thú vị"
  },
  {
    "english": "exclude",
    "type": "v",
    "pronounce": "iks ́klu:d",
    "vietnamese": "ngăn chặn, loại trừ"
  },
  {
    "english": "excluding",
    "type": "prep",
    "pronounce": "iks ́klu:diη",
    "vietnamese": "ngoài ra, trư ra"
  },
  {
    "english": "excuse",
    "type": "n, v",
    "pronounce": "iks ́kju:z",
    "vietnamese": "lời xin lỗi, bào chữa; xin lỗi, tha thứ, tha lỗi"
  },
  {
    "english": "executive",
    "type": "n, adj",
    "pronounce": "ɪgˈzɛkyətɪv",
    "vietnamese": "sự thi hành, chấp hành; (thuộc) sự thi hành, chấp hành"
  },
  {
    "english": "exercise",
    "type": "n, v",
    "pronounce": "eksəsaiz",
    "vietnamese": "bài tập, sự thi hành, sự thực hiện; làm, thi hành, thực hiện"
  },
  {
    "english": "exhibit",
    "type": "v, n",
    "pronounce": "ɪgˈzɪbɪt",
    "vietnamese": "trưng bày, triển lãm; vật trưng bày vật triển lãm"
  },
  {
    "english": "exhibition",
    "type": "n",
    "pronounce": "ˌɛksəˈbɪʃən",
    "vietnamese": "cuộc triển lãm, trưng bày"
  },
  {
    "english": "exist",
    "type": "v",
    "pronounce": "ig'zist",
    "vietnamese": "tồn tại, sống"
  },
  {
    "english": "existence",
    "type": "n",
    "pronounce": "ig'zistəns",
    "vietnamese": "sự tồn tại, sự sống"
  },
  {
    "english": "exit",
    "type": "n",
    "pronounce": "́egzit",
    "vietnamese": "lỗi ra, sự đi ra, thoát ra"
  },
  {
    "english": "expand",
    "type": "v",
    "pronounce": "iks'pænd",
    "vietnamese": "mở rộng, phát triển, nở, giãn ra"
  },
  {
    "english": "expect",
    "type": "v",
    "pronounce": "ik'spekt",
    "vietnamese": "chờ đợi, mong ngóng; liệu trước"
  },
  {
    "english": "expectation",
    "type": "n",
    "pronounce": ",ekspek'tei∫n",
    "vietnamese": "sự mong chờ, sự chờ đợi"
  },
  {
    "english": "expected",
    "type": "adj",
    "pronounce": "iks ́pektid",
    "vietnamese": "được chờ đợi, được hy vọng"
  },
  {
    "english": "expense",
    "type": "n",
    "pronounce": "ɪkˈspɛns",
    "vietnamese": "chi phí"
  },
  {
    "english": "expensive",
    "type": "adj",
    "pronounce": "iks'pensiv",
    "vietnamese": "đắt"
  },
  {
    "english": "experience",
    "type": "n, v",
    "pronounce": "iks'piəriəns",
    "vietnamese": "kinh nghiệm; trải qua, nếm mùi"
  },
  {
    "english": "experienced",
    "type": "adj",
    "pronounce": "eks ́piəriənst",
    "vietnamese": "có kinh nghiệm, tưng trải, giàu kinh nghiệm"
  },
  {
    "english": "experiment",
    "type": "n, v",
    "pronounce": "(n)ɪkˈspɛrəmənt",
    "vietnamese": "cuộc thí nghiệm; thí nghiệm"
  },
  {
    "english": "expert",
    "type": "n, adj",
    "pronounce": ",ekspз'ti:z",
    "vietnamese": "chuyên gia; chuyên môn, thành thạo"
  },
  {
    "english": "explain",
    "type": "v",
    "pronounce": "iks'plein",
    "vietnamese": "giải nghĩa, giải thích"
  },
  {
    "english": "explanation",
    "type": "n",
    "pronounce": ",eksplə'neiʃn",
    "vietnamese": "sự giải nghĩa, giải thích"
  },
  {
    "english": "explode",
    "type": "v",
    "pronounce": "iks'ploud",
    "vietnamese": "đập tan (hy vọng...), làm nổ, nổ"
  },
  {
    "english": "explore",
    "type": "v",
    "pronounce": "iks ́plɔ:",
    "vietnamese": "thăm dò, thám hiểm"
  },
  {
    "english": "explosion",
    "type": "n",
    "pronounce": "iks'plouʤn",
    "vietnamese": "sự nổ, sự phát triển ồ ạt"
  },
  {
    "english": "export",
    "type": "v, n",
    "pronounce": "iks ́pɔ:t",
    "vietnamese": "xuất khẩu; hàng xuất khẩu, sự xuất khẩu"
  },
  {
    "english": "expose",
    "type": "v",
    "pronounce": "ɪkˈspoʊz",
    "vietnamese": "trưng bày, phơi bày"
  },
  {
    "english": "express",
    "type": "v, adj",
    "pronounce": "iks'pres",
    "vietnamese": "diễn tả, biểu lộ, bày tỏ; nhanh, tốc hành"
  },
  {
    "english": "expression",
    "type": "n",
    "pronounce": "iks'preʃn",
    "vietnamese": "sự diễn tả, sự bày tỏ, biểu lộ, sự diễn đạt"
  },
  {
    "english": "extend",
    "type": "v",
    "pronounce": "iks'tend",
    "vietnamese": "giơ, duỗi ra (tay, châ(n).); kéo dài (thời gia(n).), dành cho, gửi lời"
  },
  {
    "english": "extension",
    "type": "n",
    "pronounce": "ɪkstent ʃən",
    "vietnamese": "sự giơ, duỗi; sự kéo dài, sự dành cho, gửi lời"
  },
  {
    "english": "extensive",
    "type": "adj",
    "pronounce": "iks ́tensiv",
    "vietnamese": "rộng rãi, bao quát"
  },
  {
    "english": "extent",
    "type": "nv",
    "pronounce": "ɪkˈstɛnt",
    "vietnamese": "quy mô, phạm vi"
  },
  {
    "english": "extra",
    "type": "adj, n, adv",
    "pronounce": "ekstrə",
    "vietnamese": "thêm, phụ, ngoại; thứ thêm, phụ"
  },
  {
    "english": "extraordinary",
    "type": "adj",
    "pronounce": "iks'trɔ:dnri",
    "vietnamese": "đặc biệt, lạ thường, khác thường"
  },
  {
    "english": "extreme",
    "type": "adj, n",
    "pronounce": "iks'tri:m",
    "vietnamese": "vô cùng, khắc nghiệt, quá khích, cực đoan; sự quá khích"
  },
  {
    "english": "extremely",
    "type": "adv",
    "pronounce": "iks ́tri:mli",
    "vietnamese": "vô cùng, cực độ"
  },
  {
    "english": "eye",
    "type": "n",
    "pronounce": "ai",
    "vietnamese": "mắt"
  },
  {
    "english": "face",
    "type": "n, v",
    "pronounce": "feis",
    "vietnamese": "mặt, thể diện; đương đầu, đối phó đối mặt"
  },
  {
    "english": "facility",
    "type": "n",
    "pronounce": "fəˈsɪlɪti",
    "vietnamese": "điều kiện dễ dàng, sự dễ dàng, thuận lợi"
  },
  {
    "english": "fact",
    "type": "n",
    "pronounce": "fækt",
    "vietnamese": "việc, sự việc, sự kiện"
  },
  {
    "english": "factor",
    "type": "n",
    "pronounce": "fæktə",
    "vietnamese": "nhân tố"
  },
  {
    "english": "factory",
    "type": "n",
    "pronounce": "fæktəri",
    "vietnamese": "nhà máy, xí nghiệp, xưởng"
  },
  {
    "english": "fail",
    "type": "v",
    "pronounce": "feil",
    "vietnamese": "sai, thất bại"
  },
  {
    "english": "failure",
    "type": "n",
    "pronounce": "ˈfeɪlyər",
    "vietnamese": "sự thất bại, người thất bại"
  },
  {
    "english": "faint",
    "type": "adj",
    "pronounce": "feɪnt",
    "vietnamese": "nhút nhát, yếu ớt"
  },
  {
    "english": "faintly",
    "type": "adv",
    "pronounce": "feintli",
    "vietnamese": "nhút nhát, yếu ớt"
  },
  {
    "english": "fair",
    "type": "adj",
    "pronounce": "feə",
    "vietnamese": "hợp lý, công bằng; thuận lợi"
  },
  {
    "english": "fairly",
    "type": "adv",
    "pronounce": "feəli",
    "vietnamese": "hợp lý, công bằng"
  },
  {
    "english": "faith",
    "type": "n",
    "pronounce": "feiθ",
    "vietnamese": "sự tin tưởng, tin cậy; niềm tin, vật đảm bảo"
  },
  {
    "english": "faithful",
    "type": "adj",
    "pronounce": "feiθful",
    "vietnamese": "trung thành, chung thủy, trung thực"
  },
  {
    "english": "faithfully",
    "type": "adv",
    "pronounce": "feiθfuli",
    "vietnamese": "trung thành, chung thủy, trung thực. yours faithfully bạn chân thành"
  },
  {
    "english": "fall",
    "type": "v, n",
    "pronounce": "fɔl",
    "vietnamese": "rơi, ngã, sự rơi, ngã. fall over ngã lộn nhào, bị đổ"
  },
  {
    "english": "FALSE",
    "type": "adj",
    "pronounce": "fo:ls",
    "vietnamese": "sai, nhầm, giả dối"
  },
  {
    "english": "fame",
    "type": "n",
    "pronounce": "feim",
    "vietnamese": "tên tuổi, danh tiếng"
  },
  {
    "english": "familiar",
    "type": "adj",
    "pronounce": "fəˈmiliər",
    "vietnamese": "thân thiết, quen thộc"
  },
  {
    "english": "family",
    "type": "n, adj",
    "pronounce": "ˈfæmili",
    "vietnamese": "gia đình, thuộc gia đình"
  },
  {
    "english": "famous",
    "type": "adj",
    "pronounce": "feiməs",
    "vietnamese": "nổi tiếng"
  },
  {
    "english": "fan",
    "type": "n",
    "pronounce": "fæn",
    "vietnamese": "người hâm mộ"
  },
  {
    "english": "fancy",
    "type": "v, adj",
    "pronounce": "ˈfænsi",
    "vietnamese": "tưởng tượng, cho, nghĩ rằng; tưởng tượng"
  },
  {
    "english": "far",
    "type": "adv, adj",
    "pronounce": "fɑ:",
    "vietnamese": "xa"
  },
  {
    "english": "farm",
    "type": "n",
    "pronounce": "fa:m",
    "vietnamese": "trang trại"
  },
  {
    "english": "farmer",
    "type": "n",
    "pronounce": "fɑ:mə(r)",
    "vietnamese": "nông dân, người chủ trại"
  },
  {
    "english": "farming",
    "type": "n",
    "pronounce": "fɑ:miɳ",
    "vietnamese": "công việc trồng trọt, đồng áng"
  },
  {
    "english": "fashion",
    "type": "n",
    "pronounce": "fæ∫ən",
    "vietnamese": "mốt, thời trang"
  },
  {
    "english": "fashionable",
    "type": "adj",
    "pronounce": "fæʃnəbl",
    "vietnamese": "đúng mốt, hợp thời trang"
  },
  {
    "english": "fast",
    "type": "adj, adv",
    "pronounce": "fa:st",
    "vietnamese": "nhanh"
  },
  {
    "english": "fasten",
    "type": "v",
    "pronounce": "fɑ:sn",
    "vietnamese": "buộc, trói"
  },
  {
    "english": "fat",
    "type": "adj, n",
    "pronounce": "fæt",
    "vietnamese": "béo, béo bở; mỡ, chất béo"
  },
  {
    "english": "father",
    "type": "n",
    "pronounce": "fɑ:ðə",
    "vietnamese": "cha (bố)"
  },
  {
    "english": "faucet",
    "type": "n",
    "pronounce": "ˈfɔsɪt",
    "vietnamese": "vòi (ở thùng rượu....)"
  },
  {
    "english": "fault",
    "type": "n",
    "pronounce": "fɔ:lt",
    "vietnamese": "sự thiết sót, sai sót"
  },
  {
    "english": "favour",
    "type": "n",
    "pronounce": "feivз",
    "vietnamese": "thiện ý, sự quý mến; sự đồng ý; sự chiếu cố. in favour/favor (of): ủng hộ cái gì (to be in favour of something )"
  },
  {
    "english": "favourite",
    "type": "adj, n",
    "pronounce": "feivзrit",
    "vietnamese": "được ưa thích; người (vật) được ưa thích"
  },
  {
    "english": "fear",
    "type": "n, v",
    "pronounce": "fɪər",
    "vietnamese": "sự sợ hãi, e sợ; sợ, lo ngại"
  },
  {
    "english": "feather",
    "type": "n",
    "pronounce": "feðə",
    "vietnamese": "lông chim"
  },
  {
    "english": "feature",
    "type": "n, v",
    "pronounce": "fi:tʃə",
    "vietnamese": "nét đặt biệt, điểm đặc trưng; mô tả nét đặc biệt, đặc trưng của..."
  },
  {
    "english": "February (abbr Feb)",
    "type": "n",
    "pronounce": "́februəri",
    "vietnamese": "tháng 2"
  },
  {
    "english": "federal",
    "type": "adj",
    "pronounce": "fedərəl",
    "vietnamese": "liên bang"
  },
  {
    "english": "fee",
    "type": "n",
    "pronounce": "fi:",
    "vietnamese": "tiền thù lao, học phí"
  },
  {
    "english": "feed",
    "type": "v",
    "pronounce": "fid",
    "vietnamese": "cho ăn, nuôi"
  },
  {
    "english": "feel",
    "type": "v",
    "pronounce": "fi:l",
    "vietnamese": "cảm thấy"
  },
  {
    "english": "feel sick",
    "type": "",
    "pronounce": "",
    "vietnamese": "buồn nôn"
  },
  {
    "english": "feeling",
    "type": "n",
    "pronounce": "fi:liɳ",
    "vietnamese": "sự cảm thấy, cảm giác"
  },
  {
    "english": "fellow",
    "type": "n",
    "pronounce": "felou",
    "vietnamese": "anh chàng (đáng yêu), đồng chí"
  },
  {
    "english": "female",
    "type": "adj, n",
    "pronounce": "́fi:meil",
    "vietnamese": "thuộc giống cái; giống cái"
  },
  {
    "english": "fence",
    "type": "n",
    "pronounce": "fens",
    "vietnamese": "hàng rào"
  },
  {
    "english": "festival",
    "type": "n",
    "pronounce": "festivəl",
    "vietnamese": "lễ hội, đại hội liên hoan"
  },
  {
    "english": "fetch",
    "type": "v",
    "pronounce": "fetʃ",
    "vietnamese": "tìm về, đem về; làm bực mình; làm say mê, quyến rũ"
  },
  {
    "english": "fever",
    "type": "n",
    "pronounce": "fi:və",
    "vietnamese": "cơn sốt, bệnh sốt"
  },
  {
    "english": "few",
    "type": "det, adj, pron",
    "pronounce": "fju:",
    "vietnamese": "ít,vài; một ít, một vài. a few một ít, một vài"
  },
  {
    "english": "field",
    "type": "n",
    "pronounce": "fi:ld",
    "vietnamese": "cánh đồng, bãi chiến trường"
  },
  {
    "english": "fight",
    "type": "v, n",
    "pronounce": "fait",
    "vietnamese": "đấu tranh, chiến đấu; sự đấu tranh, cuộc chiến đấu"
  },
  {
    "english": "fighting",
    "type": "n",
    "pronounce": "́faitiη",
    "vietnamese": "sự chiến đấu, sự đấu tranh"
  },
  {
    "english": "figure",
    "type": "n, v",
    "pronounce": "figə(r)",
    "vietnamese": "hình dáng, nhân vật; hình dung, miêu tả"
  },
  {
    "english": "file",
    "type": "n",
    "pronounce": "fail",
    "vietnamese": "hồ sơ, tài liệu"
  },
  {
    "english": "fill",
    "type": "v",
    "pronounce": "fil",
    "vietnamese": "làm đấy, lấp kín"
  },
  {
    "english": "film",
    "type": "n, v",
    "pronounce": "film",
    "vietnamese": "phim, được dựng thành phim"
  },
  {
    "english": "final",
    "type": "adj, n",
    "pronounce": "fainl",
    "vietnamese": "cuối cùng, cuộc đấu chung kết"
  },
  {
    "english": "finally",
    "type": "adv",
    "pronounce": "́fainəli",
    "vietnamese": "cuối cùng, sau cùng"
  },
  {
    "english": "finance",
    "type": "n, v",
    "pronounce": "fɪˈnæns , ˈfaɪnæns",
    "vietnamese": "tài chính; tài trợ, cấp vốn"
  },
  {
    "english": "financial",
    "type": "adj",
    "pronounce": "fai'næn∫l",
    "vietnamese": "thuộc (tài chính)"
  },
  {
    "english": "find",
    "type": "v",
    "pronounce": "faind",
    "vietnamese": "tìm, tìm thấy. find out sth: khám phá, tìm ra"
  },
  {
    "english": "fine",
    "type": "adj",
    "pronounce": "fain",
    "vietnamese": "tốt, giỏi"
  },
  {
    "english": "finely",
    "type": "adv",
    "pronounce": "́fainli",
    "vietnamese": "đẹp đẽ, tế nhị, cao thượng"
  },
  {
    "english": "finger",
    "type": "n",
    "pronounce": "fiɳgə",
    "vietnamese": "ngón tay"
  },
  {
    "english": "finish",
    "type": "v, n",
    "pronounce": "",
    "vietnamese": "kết thúc, hoàn thành; sự kết thúc, phần cuối"
  },
  {
    "english": "finished",
    "type": "adj",
    "pronounce": "ˈfɪnɪʃt",
    "vietnamese": "hoàn tất, hoàn thành"
  },
  {
    "english": "fire",
    "type": "n, v",
    "pronounce": "faiə",
    "vietnamese": "lửa; đốt cháy. set fire to: đốt cháy cái gì"
  },
  {
    "english": "firm",
    "type": "n, adj, adv",
    "pronounce": "fə:m",
    "vietnamese": "hãng, công ty; chắc, kiên quyết, vũng vàng, mạnh mẽ"
  },
  {
    "english": "firmly",
    "type": "adv",
    "pronounce": "́fə:mli",
    "vietnamese": "vững chắc, kiên quyết"
  },
  {
    "english": "first",
    "type": "det, adv, n",
    "pronounce": "fə:st",
    "vietnamese": "thứ nhất, đầu tiên, trước hết; người, vật đầu tiên, thứ nhất. at first trực tiếp"
  },
  {
    "english": "fish",
    "type": "n, v",
    "pronounce": "fɪʃ",
    "vietnamese": "cá, món cá; câu cá, bắt cá"
  },
  {
    "english": "fishing",
    "type": "n",
    "pronounce": "́fiʃiη",
    "vietnamese": "sự câu cá, sự đánh cá"
  },
  {
    "english": "fit",
    "type": "v, adj",
    "pronounce": "fit",
    "vietnamese": "hợp, vưa; thích hợp, xứng đáng"
  },
  {
    "english": "fix",
    "type": "v",
    "pronounce": "fiks",
    "vietnamese": "đóng, gắn, lắp; sửa chữa, sửa sang"
  },
  {
    "english": "fixed",
    "type": "adj",
    "pronounce": "",
    "vietnamese": "đứng yên, bất động"
  },
  {
    "english": "flag",
    "type": "n",
    "pronounce": "flæg",
    "vietnamese": "quốc kỳ"
  },
  {
    "english": "flame",
    "type": "n",
    "pronounce": "fleim",
    "vietnamese": "ngọn lửa"
  },
  {
    "english": "flash",
    "type": "v, n",
    "pronounce": "flæ∫",
    "vietnamese": "loé sáng, vụt sáng; ánh sáng lóe lên, đèn nháy"
  },
  {
    "english": "flat",
    "type": "adj, n",
    "pronounce": "flæt",
    "vietnamese": "bằng phẳng, bẹt, nhẵn; dãy phòng, căn phòng, mặt phẳng"
  },
  {
    "english": "flavour",
    "type": "n, v",
    "pronounce": "fleivə",
    "vietnamese": "vị, mùi; cho gia vị, làm tăng thêm mùi vị"
  },
  {
    "english": "flesh",
    "type": "n",
    "pronounce": "fle∫",
    "vietnamese": "thịt"
  },
  {
    "english": "flight",
    "type": "n",
    "pronounce": "flait",
    "vietnamese": "sự bỏ chạy, rút chạy; sự bay, chuyến bay"
  },
  {
    "english": "float",
    "type": "v",
    "pronounce": "floʊt",
    "vietnamese": "nổi, trôi, lơ lửng"
  },
  {
    "english": "flood",
    "type": "n, v",
    "pronounce": "flʌd",
    "vietnamese": "lụt, lũ lụtl; tràn đầy, tràn ngập"
  },
  {
    "english": "floor",
    "type": "n",
    "pronounce": "flɔ:",
    "vietnamese": "sàn, tầng (nhà)"
  },
  {
    "english": "flour",
    "type": "n",
    "pronounce": "́flauə",
    "vietnamese": "bột, bột mỳ"
  },
  {
    "english": "flow",
    "type": "n, v",
    "pronounce": "flow",
    "vietnamese": "sự chảy; chảy"
  },
  {
    "english": "flower",
    "type": "n",
    "pronounce": "flauə",
    "vietnamese": "hoa, bông, đóa, cây hoa"
  },
  {
    "english": "flu",
    "type": "n",
    "pronounce": "flu:",
    "vietnamese": "bệnh cúm"
  },
  {
    "english": "fly",
    "type": "v, n",
    "pronounce": "flaɪ",
    "vietnamese": "bay; sự bay, quãng đường bay"
  },
  {
    "english": "flying",
    "type": "adj, n",
    "pronounce": "́flaiiη",
    "vietnamese": "biết bay; sự bay, chuyến bay"
  },
  {
    "english": "focus",
    "type": "v, n",
    "pronounce": "foukəs",
    "vietnamese": "tập trung; trung tâm, trọng tâm ((n)bóng)"
  },
  {
    "english": "fold",
    "type": "v, n",
    "pronounce": "foʊld",
    "vietnamese": "gấp, vén, xắn; nếp gấp"
  },
  {
    "english": "folding",
    "type": "adj",
    "pronounce": "́fouldiη",
    "vietnamese": "gấp lại được"
  },
  {
    "english": "follow",
    "type": "v",
    "pronounce": "fɔlou",
    "vietnamese": "đi theo sau, theo, tiếp theo"
  },
  {
    "english": "following",
    "type": "adj, prep",
    "pronounce": "́fɔlouiη",
    "vietnamese": "tiếp theo, theo sau, sau đây; sau, tiếp theo"
  },
  {
    "english": "food",
    "type": "n",
    "pronounce": "fu:d",
    "vietnamese": "đồ ăn, thức, món ăn"
  },
  {
    "english": "foot",
    "type": "n",
    "pronounce": "fut",
    "vietnamese": "chân, bàn chân"
  },
  {
    "english": "football",
    "type": "n",
    "pronounce": "ˈfʊtˌbɔl",
    "vietnamese": "bóng đá"
  },
  {
    "english": "for",
    "type": "prep",
    "pronounce": "fɔ:,fə",
    "vietnamese": "cho, dành cho..."
  },
  {
    "english": "force",
    "type": "n, v",
    "pronounce": "fɔ:s",
    "vietnamese": "sức mạnh; ép buộc, cưỡng ép"
  },
  {
    "english": "forecast",
    "type": "n, v",
    "pronounce": "fɔ:'kɑ:st",
    "vietnamese": "sự dự đoán, dự báo; dự đoán, dự báo"
  },
  {
    "english": "foreign",
    "type": "adj",
    "pronounce": "fɔrin",
    "vietnamese": "(thuộc) nước ngoài, tư nước ngoài, ở nước ngoài"
  },
  {
    "english": "forest",
    "type": "n",
    "pronounce": "forist",
    "vietnamese": "rừng"
  },
  {
    "english": "forever",
    "type": "adv",
    "pronounce": "fə'revə",
    "vietnamese": "mãi mãi"
  },
  {
    "english": "forget",
    "type": "v",
    "pronounce": "fə'get",
    "vietnamese": "quên"
  },
  {
    "english": "forgive",
    "type": "v",
    "pronounce": "fərˈgɪv",
    "vietnamese": "tha, tha thứ"
  },
  {
    "english": "fork",
    "type": "n",
    "pronounce": "fɔrk",
    "vietnamese": "cái nĩa"
  },
  {
    "english": "form",
    "type": "n, v",
    "pronounce": "fɔ:m",
    "vietnamese": "hình thể, hình dạng, hình thức; làm thành, được tạo thành"
  },
  {
    "english": "formal",
    "type": "adj",
    "pronounce": "fɔ:ml",
    "vietnamese": "hình thức"
  },
  {
    "english": "formally",
    "type": "adv",
    "pronounce": "fo:mзlaiz",
    "vietnamese": "chính thức"
  },
  {
    "english": "former",
    "type": "adj",
    "pronounce": "́fɔ:mə",
    "vietnamese": "trước, cũ, xưa, nguyên"
  },
  {
    "english": "formerly",
    "type": "adv",
    "pronounce": "́fɔ:məli",
    "vietnamese": "trước đây, thuở xưa"
  },
  {
    "english": "formula",
    "type": "n",
    "pronounce": "fɔ:mjulə",
    "vietnamese": "công thức, thể thức, cách thức"
  },
  {
    "english": "fortune",
    "type": "n",
    "pronounce": "ˈfɔrtʃən",
    "vietnamese": "sự giàu có, sự thịnh vượng"
  },
  {
    "english": "forward",
    "type": "adj",
    "pronounce": "ˈfɔrwərd",
    "vietnamese": "ở phía trước, tiến về phía trước"
  },
  {
    "english": "forward, forwards",
    "type": "adv",
    "pronounce": "ˈfɔrwərd",
    "vietnamese": "về tương lai, sau này ở phía trước, tiến về phía trước"
  },
  {
    "english": "found",
    "type": "v",
    "pronounce": "faund",
    "vietnamese": "tìm, tìm thấy"
  },
  {
    "english": "foundation",
    "type": "n",
    "pronounce": "faun'dei∫n",
    "vietnamese": "sự thành lập, sự sáng lập; tổ chức"
  },
  {
    "english": "frame",
    "type": "n, v",
    "pronounce": "freim",
    "vietnamese": "cấu trúc, hệ thống; dàn xếp, bố trí"
  },
  {
    "english": "free",
    "type": "adj, v, adv",
    "pronounce": "fri:",
    "vietnamese": "miễn phí, tự do, giải phóng, trả tự do"
  },
  {
    "english": "freedom",
    "type": "n",
    "pronounce": "fri:dəm",
    "vietnamese": "sự tự do; nền tự do"
  },
  {
    "english": "freely",
    "type": "adv",
    "pronounce": "́fri:li",
    "vietnamese": "tự do, thoải mái"
  },
  {
    "english": "freeze",
    "type": "v",
    "pronounce": "fri:z",
    "vietnamese": "đóng băng, đông lạnh"
  },
  {
    "english": "frequent",
    "type": "adj",
    "pronounce": "ˈfrikwənt",
    "vietnamese": "thường xuyên"
  },
  {
    "english": "frequently",
    "type": "adv",
    "pronounce": "́fri:kwəntli",
    "vietnamese": "thường xuyên"
  },
  {
    "english": "fresh",
    "type": "adj",
    "pronounce": "freʃ",
    "vietnamese": "tươi, tươi tắn"
  },
  {
    "english": "freshly",
    "type": "adv",
    "pronounce": "́freʃli",
    "vietnamese": "tươi mát, khỏe khoắn"
  },
  {
    "english": "Friday (abbr Fri)",
    "type": "n",
    "pronounce": "́fraidi",
    "vietnamese": "thứ Sáu"
  },
  {
    "english": "fridge",
    "type": "n",
    "pronounce": "fridЗ",
    "vietnamese": "tủ lạnh"
  },
  {
    "english": "friend",
    "type": "n",
    "pronounce": "frend",
    "vietnamese": "người bạn"
  },
  {
    "english": "friendly",
    "type": "adj",
    "pronounce": "́frendli",
    "vietnamese": "thân thiện, thân mật"
  },
  {
    "english": "friendship",
    "type": "n",
    "pronounce": "frendʃipn",
    "vietnamese": "tình bạn, tình hữu nghị"
  },
  {
    "english": "frighten",
    "type": "v",
    "pronounce": "ˈfraɪtn",
    "vietnamese": "làm sợ, làm hoảng sợ"
  },
  {
    "english": "frightened",
    "type": "adj",
    "pronounce": "fraitnd",
    "vietnamese": "hoảng sợ, khiếp sợ"
  },
  {
    "english": "frightening",
    "type": "adj",
    "pronounce": "́fraiəniη",
    "vietnamese": "kinh khủng, khủng khiếp"
  },
  {
    "english": "from",
    "type": "prep",
    "pronounce": "frɔm",
    "vietnamese": "frəm/ tư"
  },
  {
    "english": "front",
    "type": "n, adj",
    "pronounce": "frʌnt",
    "vietnamese": "mặt; đằng trước, về phía trước. in front (of): ở phía trước"
  },
  {
    "english": "frozen",
    "type": "adj",
    "pronounce": "frouzn",
    "vietnamese": "lạnh giá"
  },
  {
    "english": "fruit",
    "type": "n",
    "pronounce": "fru:t",
    "vietnamese": "quả, trái cây"
  },
  {
    "english": "fry",
    "type": "v, n",
    "pronounce": "frai",
    "vietnamese": "rán, chiên; thịt rán"
  },
  {
    "english": "fuel",
    "type": "n",
    "pronounce": "ˈfyuəl",
    "vietnamese": "chất đốt, nhiên liệu"
  },
  {
    "english": "full",
    "type": "adj",
    "pronounce": "ful",
    "vietnamese": "đầy, đầy đủ"
  },
  {
    "english": "fully",
    "type": "adv",
    "pronounce": "́fuli",
    "vietnamese": "đầy đủ, hoàn toàn"
  },
  {
    "english": "fun",
    "type": "n, adj",
    "pronounce": "fʌn",
    "vietnamese": "sự vui đùa, sự vui thích; hài hước make fun of: đùa cợt, chế giễu, chế nhạo"
  },
  {
    "english": "function",
    "type": "n, v",
    "pronounce": "ˈfʌŋkʃən",
    "vietnamese": "chức năng; họat động, chạy (máy)"
  },
  {
    "english": "fund",
    "type": "n, v",
    "pronounce": "fʌnd",
    "vietnamese": "kho, quỹ; tài trợ, tiền bạc, để tiền vào công quỹ"
  },
  {
    "english": "fundamental",
    "type": "adj",
    "pronounce": ",fʌndə'mentl",
    "vietnamese": "cơ bản, cơ sở, chủ yếu"
  },
  {
    "english": "funeral",
    "type": "n",
    "pronounce": "ˈfju:nərəl",
    "vietnamese": "lễ tang, đám tang"
  },
  {
    "english": "funny",
    "type": "adj",
    "pronounce": "́fʌni",
    "vietnamese": "buồn cười, khôi hài"
  },
  {
    "english": "fur",
    "type": "n",
    "pronounce": "fə:",
    "vietnamese": "bộ da lông thú"
  },
  {
    "english": "furniture",
    "type": "n",
    "pronounce": "fə:nitʃə",
    "vietnamese": "đồ đạc (trong nhà)"
  },
  {
    "english": "further",
    "type": "adj",
    "pronounce": "fə:ðə",
    "vietnamese": "xa hơn nữa; thêm nữa"
  },
  {
    "english": "further, furthest",
    "type": "adj",
    "pronounce": "",
    "vietnamese": "cấp so sánh của far"
  },
  {
    "english": "future",
    "type": "n, adj",
    "pronounce": "fju:tʃə",
    "vietnamese": "tương lai"
  },
  {
    "english": "gain",
    "type": "v, n",
    "pronounce": "geɪn",
    "vietnamese": "lợi, lợi ích; giành được, kiếm được, đạt tới"
  },
  {
    "english": "gallon",
    "type": "n",
    "pronounce": "gælən",
    "vietnamese": "Galông 1gl = 4, 54 lít ở Anh, 3, 78 lít ở Mỹ"
  },
  {
    "english": "gamble",
    "type": "v, n",
    "pronounce": "gæmbl",
    "vietnamese": "đánh bạc; cuộc đánh bạc"
  },
  {
    "english": "gambling",
    "type": "n",
    "pronounce": "gæmbliɳ",
    "vietnamese": "trò cờ bạc"
  },
  {
    "english": "game",
    "type": "n",
    "pronounce": "geim",
    "vietnamese": "trò chơi"
  },
  {
    "english": "gap",
    "type": "n",
    "pronounce": "gæp",
    "vietnamese": "đèo, lỗ hổng, kẽ hở; chỗ trống"
  },
  {
    "english": "garage",
    "type": "n",
    "pronounce": "́gæra:ʒ",
    "vietnamese": "nhà để ô tô"
  },
  {
    "english": "garbage",
    "type": "n",
    "pronounce": "ˈgɑrbɪdʒ",
    "vietnamese": "lòng, ruột (thú)"
  },
  {
    "english": "garden",
    "type": "n",
    "pronounce": "gɑ:dn",
    "vietnamese": "vườn"
  },
  {
    "english": "gas",
    "type": "n",
    "pronounce": "gæs",
    "vietnamese": "khí, hơi đốt"
  },
  {
    "english": "gasoline",
    "type": "n",
    "pronounce": "gasolin",
    "vietnamese": "dầu lửa, dầu hỏa, xăng"
  },
  {
    "english": "gate",
    "type": "n",
    "pronounce": "geit",
    "vietnamese": "cổng"
  },
  {
    "english": "gather",
    "type": "v",
    "pronounce": "gæðə",
    "vietnamese": "tập hợp; hái, lượm, thu thập"
  },
  {
    "english": "gear",
    "type": "n",
    "pronounce": "giə",
    "vietnamese": "cơ cấu, thiết bị, dụng cụ"
  },
  {
    "english": "general",
    "type": "adj",
    "pronounce": "ʤenər(ə)l",
    "vietnamese": "chung, chung chung; tổng"
  },
  {
    "english": "generally",
    "type": "adv",
    "pronounce": "dʒenərəli",
    "vietnamese": "nói chung, đại thể. in general: nói chung, đại khái"
  },
  {
    "english": "generate",
    "type": "v",
    "pronounce": "dʒenəreit",
    "vietnamese": "sinh, đẻ ra"
  },
  {
    "english": "generation",
    "type": "n",
    "pronounce": "ˌdʒɛnəˈreɪʃən",
    "vietnamese": "sự sinh ra, sự phát sinh ra; thế hệ đời"
  },
  {
    "english": "generous",
    "type": "adj",
    "pronounce": "́dʒenərəs",
    "vietnamese": "rộng lượng, khoan hồng, hào phóng"
  },
  {
    "english": "generously",
    "type": "adv",
    "pronounce": "dʒenərəsli",
    "vietnamese": "rộng lượng, hào phóng"
  },
  {
    "english": "gentle",
    "type": "adj",
    "pronounce": "dʒentl",
    "vietnamese": "hiền lành, dịu dàng, nhẹ nhàng"
  },
  {
    "english": "gentleman",
    "type": "n",
    "pronounce": "ˈdʒɛntlmən",
    "vietnamese": "người quý phái, người thượng lưu"
  },
  {
    "english": "gently",
    "type": "adv",
    "pronounce": "dʤentli",
    "vietnamese": "nhẹ nhàng, êm ái, dịu dàng"
  },
  {
    "english": "genuine",
    "type": "adj",
    "pronounce": "́dʒenjuin",
    "vietnamese": "thành thật, chân thật; xác thực"
  },
  {
    "english": "genuinely",
    "type": "adv",
    "pronounce": "́dʒenjuinli",
    "vietnamese": "thành thật, chân thật"
  },
  {
    "english": "geography",
    "type": "n",
    "pronounce": "dʒi ́ɔgrəfi",
    "vietnamese": "địa lý, khoa địa lý"
  },
  {
    "english": "get",
    "type": "v",
    "pronounce": "get",
    "vietnamese": "được, có được. get on leo, trèo lên. get off: ra khỏi, thoát khỏi"
  },
  {
    "english": "giant",
    "type": "n, adj",
    "pronounce": "ˈdʒaiənt",
    "vietnamese": "người khổng lồ, người phi thường khổng lồ, phi thường"
  },
  {
    "english": "gift",
    "type": "n",
    "pronounce": "gift",
    "vietnamese": "quà tặng"
  },
  {
    "english": "girl",
    "type": "n",
    "pronounce": "g3:l",
    "vietnamese": "con gái"
  },
  {
    "english": "girlfriend",
    "type": "n",
    "pronounce": "gз:lfrend",
    "vietnamese": "bạn gái, người yêu"
  },
  {
    "english": "give",
    "type": "v",
    "pronounce": "giv",
    "vietnamese": "cho, biếu, tặng. give sth away cho phát. give sth out: chia, phân phối give (sth) up bỏ, tư bỏ"
  },
  {
    "english": "give birth",
    "type": "to",
    "pronounce": "",
    "vietnamese": "sinh ra"
  },
  {
    "english": "glad",
    "type": "adj",
    "pronounce": "glæd",
    "vietnamese": "vui lòng, sung sướng"
  },
  {
    "english": "glass",
    "type": "n",
    "pronounce": "glɑ:s",
    "vietnamese": "kính, thủy tinh, cái cốc, ly"
  },
  {
    "english": "glasses",
    "type": "n",
    "pronounce": "",
    "vietnamese": "kính đeo mắt"
  },
  {
    "english": "global",
    "type": "adjv",
    "pronounce": "́gloubl",
    "vietnamese": "toàn cầu, toàn thể, toàn bộ"
  },
  {
    "english": "glove",
    "type": "n",
    "pronounce": "glʌv",
    "vietnamese": "bao tay, găng tay"
  },
  {
    "english": "glue",
    "type": "n, v",
    "pronounce": "glu:",
    "vietnamese": "keo, hồ; gắn lại, dán bằng keo, hồ"
  },
  {
    "english": "go",
    "type": "v",
    "pronounce": "gou",
    "vietnamese": "đi. go down: đi xuống. go up: đi lên. be going to sắp sửa, có ý định"
  },
  {
    "english": "goal",
    "type": "n",
    "pronounce": "goƱl",
    "vietnamese": "mục đích, bàn thắng, khung thành"
  },
  {
    "english": "god",
    "type": "n",
    "pronounce": "gɒd",
    "vietnamese": "thần, Chúa"
  },
  {
    "english": "gold",
    "type": "n, adj",
    "pronounce": "goʊld",
    "vietnamese": "vàng; bằng vàng"
  },
  {
    "english": "good",
    "type": "adj, n",
    "pronounce": "gud",
    "vietnamese": "tốt, hay, tuyệt; điều tốt, điều thiện. good at: tiến bộ ở. good for: có lợi cho"
  },
  {
    "english": "good, well",
    "type": "adj",
    "pronounce": "gud, wel",
    "vietnamese": "tốt, khỏe"
  },
  {
    "english": "goodbye",
    "type": "exclamation, n",
    "pronounce": "̧gud ́bai",
    "vietnamese": "tạm biệt; lời chào tạm biệt"
  },
  {
    "english": "goods",
    "type": "n",
    "pronounce": "gudz",
    "vietnamese": "của cải, tài sản, hàng hóa"
  },
  {
    "english": "govern",
    "type": "v",
    "pronounce": "́gʌvən",
    "vietnamese": "cai trị, thống trị, cầm quyền"
  },
  {
    "english": "government",
    "type": "n",
    "pronounce": "ˈgʌvərnmənt , ˈgʌvərmənt",
    "vietnamese": "chính phủ, nội các; sự cai trị"
  },
  {
    "english": "governor",
    "type": "n",
    "pronounce": "́gʌvənə",
    "vietnamese": "thủ lĩnh, chủ; kẻ thống trị"
  },
  {
    "english": "grab",
    "type": "v",
    "pronounce": "græb",
    "vietnamese": "túm lấy, vồ, chộp lấy"
  },
  {
    "english": "grade",
    "type": "n, v",
    "pronounce": "greɪd",
    "vietnamese": "điểm, điểm số; phân loại, xếp loại"
  },
  {
    "english": "gradual",
    "type": "adj",
    "pronounce": "́grædjuəl",
    "vietnamese": "dần dần, tưng bước một"
  },
  {
    "english": "gradually",
    "type": "adv",
    "pronounce": "grædzuəli",
    "vietnamese": "dần dần, tư tư"
  },
  {
    "english": "grain",
    "type": "n",
    "pronounce": "grein",
    "vietnamese": "thóc lúa, hạt, hột; tính chất, bản chất"
  },
  {
    "english": "gram",
    "type": "n",
    "pronounce": "græm",
    "vietnamese": "đậu xanh"
  },
  {
    "english": "gram, gramme (abbr g, gm)",
    "type": "n",
    "pronounce": "græm",
    "vietnamese": "ngữ pháp"
  },
  {
    "english": "grammar",
    "type": "n",
    "pronounce": "ˈgræmər",
    "vietnamese": "văn phạm"
  },
  {
    "english": "grand",
    "type": "adj",
    "pronounce": "grænd",
    "vietnamese": "rộng lớn, vĩ đại"
  },
  {
    "english": "grandchild",
    "type": "n",
    "pronounce": "́græn ̧tʃaild",
    "vietnamese": "cháu (của ông bà)"
  },
  {
    "english": "granddaughter",
    "type": "n",
    "pronounce": "græn,do:tз",
    "vietnamese": "cháu gái"
  },
  {
    "english": "grandfather",
    "type": "n",
    "pronounce": "́græn ̧fa:ðə",
    "vietnamese": "ông"
  },
  {
    "english": "grandmother",
    "type": "n",
    "pronounce": "græn,mʌðə",
    "vietnamese": "bà"
  },
  {
    "english": "grandparent",
    "type": "n",
    "pronounce": "́græn ̧pɛərənts",
    "vietnamese": "ông bà"
  },
  {
    "english": "grandson",
    "type": "n",
    "pronounce": "́grænsʌn",
    "vietnamese": "cháu trai"
  },
  {
    "english": "grant",
    "type": "v, n",
    "pronounce": "grα:nt",
    "vietnamese": "cho, bán, cấp; sự cho, sự bán, sự cấp"
  },
  {
    "english": "grass",
    "type": "n",
    "pronounce": "grɑ:s",
    "vietnamese": "cỏ; bãi cỏ, đồng cỏ"
  },
  {
    "english": "grateful",
    "type": "adj",
    "pronounce": "́greitful",
    "vietnamese": "biết ơn, dễ chịu, khoan khoái"
  },
  {
    "english": "grave",
    "type": "n, adj",
    "pronounce": "greiv",
    "vietnamese": "mộ, dấu huyền; trang nghiêm, nghiêm trọng"
  },
  {
    "english": "gray",
    "type": "grei",
    "pronounce": "",
    "vietnamese": "xám, hoa râm (tóc)"
  },
  {
    "english": "great",
    "type": "adj",
    "pronounce": "greɪt",
    "vietnamese": "to, lớn, vĩ đại"
  },
  {
    "english": "greatly",
    "type": "adv",
    "pronounce": "́greitli",
    "vietnamese": "rất, lắm; cao thượng, cao cả"
  },
  {
    "english": "green",
    "type": "adj, n",
    "pronounce": "grin",
    "vietnamese": "xanh lá cây"
  },
  {
    "english": "grey",
    "type": "adj",
    "pronounce": "grei",
    "vietnamese": "xám, hoa râm (tóc)"
  },
  {
    "english": "grey, usually gray",
    "type": "adj, n",
    "pronounce": "",
    "vietnamese": "màu xám"
  },
  {
    "english": "groceries",
    "type": "n",
    "pronounce": "ˈgroʊsəri, ˈgroʊsri",
    "vietnamese": "hàng tạp hóa"
  },
  {
    "english": "grocery",
    "type": "n",
    "pronounce": "́grousəri",
    "vietnamese": "cửa hàng tạp phẩm"
  },
  {
    "english": "ground",
    "type": "n",
    "pronounce": "graund",
    "vietnamese": "mặt đất, đất, bãi đất"
  },
  {
    "english": "group",
    "type": "n",
    "pronounce": "gru:p",
    "vietnamese": "nhóm"
  },
  {
    "english": "grow",
    "type": "v",
    "pronounce": "grou",
    "vietnamese": "mọc, mọc lên. grow up lớn lên, trưởng thành"
  },
  {
    "english": "growth",
    "type": "n",
    "pronounce": "grouθ",
    "vietnamese": "sự lớn lên, sự phát triển"
  },
  {
    "english": "guarantee",
    "type": "n, v",
    "pronounce": "gærənˈti",
    "vietnamese": "sự bảo hành, bảo lãnh, người bảo lãnh; cam đoan, bảo đảm"
  },
  {
    "english": "guard",
    "type": "n, v",
    "pronounce": "ga:d",
    "vietnamese": "cái chắn, người bảo vệ; bảo vệ, gác, canh giữ"
  },
  {
    "english": "guess",
    "type": "v, n",
    "pronounce": "ges",
    "vietnamese": "đoán, phỏng đoán; sự đoán, sự ước chưng"
  },
  {
    "english": "guest",
    "type": "n",
    "pronounce": "gest",
    "vietnamese": "khách, khách mời"
  },
  {
    "english": "guide",
    "type": "n, v",
    "pronounce": "gaɪd",
    "vietnamese": "điều chỉ dẫn, người hướng dẫn; dẫn đường, chỉ đường"
  },
  {
    "english": "guilty",
    "type": "adj",
    "pronounce": "ˈgɪlti",
    "vietnamese": "có tội, phạm tội, tội lỗi"
  },
  {
    "english": "gun",
    "type": "n",
    "pronounce": "gʌn",
    "vietnamese": "súng"
  },
  {
    "english": "guy",
    "type": "n",
    "pronounce": "gai",
    "vietnamese": "bù nhìn, anh chàng, gã"
  },
  {
    "english": "habit",
    "type": "n",
    "pronounce": "́hæbit",
    "vietnamese": "thói quen, tập quán"
  },
  {
    "english": "hair",
    "type": "n",
    "pronounce": "heə",
    "vietnamese": "tóc"
  },
  {
    "english": "hairdresser",
    "type": "n",
    "pronounce": "heədresə",
    "vietnamese": "thợ làm tóc"
  },
  {
    "english": "half",
    "type": "det, pron, adv",
    "pronounce": "hɑ:f",
    "vietnamese": "một nửa, phần chia đôi, nửa giờ; nửa"
  },
  {
    "english": "hall",
    "type": "n",
    "pronounce": "hɔ:l",
    "vietnamese": "đại sảnh, tòa (thị chính), hội trường"
  },
  {
    "english": "hammer",
    "type": "n",
    "pronounce": "hæmə",
    "vietnamese": "búa"
  },
  {
    "english": "hand",
    "type": "n, v",
    "pronounce": "hænd",
    "vietnamese": "tay, bàn tay; trao tay, truyền cho"
  },
  {
    "english": "handle",
    "type": "v, n",
    "pronounce": "hændl",
    "vietnamese": "cầm, sờ mó; tay cầm, móc quai"
  },
  {
    "english": "hang",
    "type": "v",
    "pronounce": "hæŋ",
    "vietnamese": "treo, mắc"
  },
  {
    "english": "happen",
    "type": "v",
    "pronounce": "hæpən",
    "vietnamese": "xảy ra, xảy đến"
  },
  {
    "english": "happily",
    "type": "adv",
    "pronounce": "hæpili",
    "vietnamese": "sung sướng, hạnh phúc"
  },
  {
    "english": "happiness",
    "type": "n",
    "pronounce": "hæpinis",
    "vietnamese": "sự sung sướng, hạnh phúc"
  },
  {
    "english": "happy",
    "type": "adj",
    "pronounce": "ˈhæpi",
    "vietnamese": "vui sướng, hạnh phúc"
  },
  {
    "english": "hard",
    "type": "adj, adv",
    "pronounce": "ha:d",
    "vietnamese": "cứng, rắn, hà khắc; hết sức cố gắng, tích cực"
  },
  {
    "english": "hardly",
    "type": "adv",
    "pronounce": "́ha:dli",
    "vietnamese": "khắc nghiệt, nghiêm khắc, tàn tệ, khó khăn"
  },
  {
    "english": "harm",
    "type": "n, v",
    "pronounce": "hɑ:m",
    "vietnamese": "thiệt hại, tổn hao; làm hại, gây thiệt hại"
  },
  {
    "english": "harmful",
    "type": "adj",
    "pronounce": "́ha:mful",
    "vietnamese": "gây tai hại, có hại"
  },
  {
    "english": "harmless",
    "type": "adj",
    "pronounce": "́ha:mlis",
    "vietnamese": "không có hại"
  },
  {
    "english": "hat",
    "type": "n",
    "pronounce": "hæt",
    "vietnamese": "cái mũ"
  },
  {
    "english": "hate",
    "type": "v, n",
    "pronounce": "heit",
    "vietnamese": "ghét; lòng căm ghét, thù hận"
  },
  {
    "english": "hatred",
    "type": "n",
    "pronounce": "heitrid",
    "vietnamese": "lòng căm thì, sự căm ghét"
  },
  {
    "english": "have",
    "type": "vauxiliary, v",
    "pronounce": "hæv, həv",
    "vietnamese": "có"
  },
  {
    "english": "have to",
    "type": "modal, v",
    "pronounce": "",
    "vietnamese": "phải (bắt buộc, có bổn phận phải)"
  },
  {
    "english": "he",
    "type": "n, pro",
    "pronounce": "hi:",
    "vietnamese": "nó, anh ấy, ông ấy"
  },
  {
    "english": "head",
    "type": "n, v",
    "pronounce": "hed",
    "vietnamese": "cái đầu (người, thú); chỉ huy, lãnh đại, dẫn đầu"
  },
  {
    "english": "headache",
    "type": "n",
    "pronounce": "hedeik",
    "vietnamese": "chứng nhức đầu"
  },
  {
    "english": "heal",
    "type": "v",
    "pronounce": "hi:l",
    "vietnamese": "chữa khỏi, làm lành"
  },
  {
    "english": "health",
    "type": "n",
    "pronounce": "hɛlθ",
    "vietnamese": "sức khỏe, thể chất, sự lành mạnh"
  },
  {
    "english": "healthy",
    "type": "adj",
    "pronounce": "helθi",
    "vietnamese": "khỏe mạnh, lành mạnh"
  },
  {
    "english": "hear",
    "type": "v",
    "pronounce": "hiə",
    "vietnamese": "nghe"
  },
  {
    "english": "hearing",
    "type": "n",
    "pronounce": "ˈhɪərɪŋ",
    "vietnamese": "sự nghe, thính giác"
  },
  {
    "english": "heart",
    "type": "n",
    "pronounce": "hɑ:t",
    "vietnamese": "tim, trái tim"
  },
  {
    "english": "heat",
    "type": "n, v",
    "pronounce": "hi:t",
    "vietnamese": "hơi nóng, sức nóng"
  },
  {
    "english": "heating",
    "type": "n",
    "pronounce": "hi:tiη",
    "vietnamese": "sự đốt nóng, sự làm nóng"
  },
  {
    "english": "heaven",
    "type": "n",
    "pronounce": "ˈhɛvən",
    "vietnamese": "thiên đường"
  },
  {
    "english": "heavily",
    "type": "adv",
    "pronounce": "́hevili",
    "vietnamese": "nặng, nặng nề"
  },
  {
    "english": "heavy",
    "type": "adj",
    "pronounce": "hevi",
    "vietnamese": "nặng, nặng nề"
  },
  {
    "english": "heel",
    "type": "n",
    "pronounce": "hi:l",
    "vietnamese": "gót chân"
  },
  {
    "english": "height",
    "type": "n",
    "pronounce": "hait",
    "vietnamese": "chiều cao, độ cao; đỉnh, điểm cao"
  },
  {
    "english": "hell",
    "type": "n",
    "pronounce": "hel",
    "vietnamese": "địa ngục"
  },
  {
    "english": "hello",
    "type": "exclamation, n",
    "pronounce": "hз'lou",
    "vietnamese": "chào, xin chào; lời chào"
  },
  {
    "english": "help",
    "type": "v, n",
    "pronounce": "help",
    "vietnamese": "giúp đỡ; sự giúp đỡ"
  },
  {
    "english": "helpful",
    "type": "adj",
    "pronounce": "́helpful",
    "vietnamese": "có ích; giúp đỡ"
  },
  {
    "english": "hence",
    "type": "adv",
    "pronounce": "hens",
    "vietnamese": "sau đây, kể từ đây; do đó, vì thế"
  },
  {
    "english": "her",
    "type": "pron, det",
    "pronounce": "hз:",
    "vietnamese": "nó, chị ấy, cô ấy, bà ấy"
  },
  {
    "english": "here",
    "type": "adv",
    "pronounce": "hiə",
    "vietnamese": "đây, ở đây"
  },
  {
    "english": "hero",
    "type": "n",
    "pronounce": "hiərou",
    "vietnamese": "người anh hùng"
  },
  {
    "english": "hers",
    "type": "pron",
    "pronounce": "hə:z",
    "vietnamese": "cái của nó, cái của cô ấy, cái của chị ấy, cái của bà ấy"
  },
  {
    "english": "herself",
    "type": "pron",
    "pronounce": "hə: ́self",
    "vietnamese": "chính nó, chính cô ta, chính chị ta chính bà ta"
  },
  {
    "english": "hesitate",
    "type": "v",
    "pronounce": "heziteit",
    "vietnamese": "ngập ngưng, do dự"
  },
  {
    "english": "hi",
    "type": "exclamation",
    "pronounce": "hai",
    "vietnamese": "xin chào"
  },
  {
    "english": "hide",
    "type": "v",
    "pronounce": "haid",
    "vietnamese": "trốn, ẩn nấp; che giấu"
  },
  {
    "english": "high",
    "type": "adj, adv",
    "pronounce": "hai",
    "vietnamese": "cao, ở mức độ cao"
  },
  {
    "english": "highlight",
    "type": "v, n",
    "pronounce": "ˈhaɪˌlaɪt",
    "vietnamese": "làm nổi bật, nêu bật; chỗ nổi bật nhất, đẹp, sáng nhất"
  },
  {
    "english": "highly",
    "type": "adv",
    "pronounce": "́haili",
    "vietnamese": "tốt, cao; hết sức, ở mức độ cao"
  },
  {
    "english": "highway",
    "type": "n",
    "pronounce": "́haiwei",
    "vietnamese": "đường quốc lộ"
  },
  {
    "english": "hill",
    "type": "n",
    "pronounce": "hil",
    "vietnamese": "đồi"
  },
  {
    "english": "him",
    "type": "pron",
    "pronounce": "him",
    "vietnamese": "nó, hắn, ông ấy, anh ấy"
  },
  {
    "english": "himself",
    "type": "pron",
    "pronounce": "him ́self",
    "vietnamese": "chính nó, chính hắn, chính ông ta, chính anh ta"
  },
  {
    "english": "hip",
    "type": "n",
    "pronounce": "hip",
    "vietnamese": "hông"
  },
  {
    "english": "hire",
    "type": "v, n",
    "pronounce": "haiə",
    "vietnamese": "thuê, cho thuê (nhà...); sự thuê, sự cho thuê"
  },
  {
    "english": "his",
    "type": "det, pron",
    "pronounce": "hiz",
    "vietnamese": "của nó, của hắn, của ông ấy, của anh ấy; cái của nó, cái của hắn, cái của ông ấy, cái của anh ấy"
  },
  {
    "english": "historical",
    "type": "adj",
    "pronounce": "his'tɔrikəl",
    "vietnamese": "lịch sử, thuộc lịch sử"
  },
  {
    "english": "history",
    "type": "n",
    "pronounce": "́histəri",
    "vietnamese": "lịch sử, sử học"
  },
  {
    "english": "hit",
    "type": "v, n",
    "pronounce": "hit",
    "vietnamese": "đánh, đấm, ném trúng; đòn, cú đấm"
  },
  {
    "english": "hobby",
    "type": "n",
    "pronounce": "hɒbi",
    "vietnamese": "sở thích riêng"
  },
  {
    "english": "hold",
    "type": "v, n",
    "pronounce": "hould",
    "vietnamese": "cầm, nắm, giữ; sự cầm, sự nắm giữ"
  },
  {
    "english": "hole",
    "type": "n",
    "pronounce": "houl",
    "vietnamese": "lỗ, lỗ trống; hang"
  },
  {
    "english": "holiday",
    "type": "n",
    "pronounce": "hɔlədi",
    "vietnamese": "ngày lễ, ngày nghỉ"
  },
  {
    "english": "hollow",
    "type": "adj",
    "pronounce": "hɔlou",
    "vietnamese": "rỗng, trống rỗng"
  },
  {
    "english": "holy",
    "type": "adj",
    "pronounce": "ˈhoʊli",
    "vietnamese": "linh thiêng; sùng đạo"
  },
  {
    "english": "home",
    "type": "n, adv",
    "pronounce": "hoʊm",
    "vietnamese": "nhà; ở tại nhà, nước mình"
  },
  {
    "english": "homework",
    "type": "n",
    "pronounce": "́houm ̧wə:k",
    "vietnamese": "bài tập về nhà (học sinh), công việc làm ở nhà"
  },
  {
    "english": "honest",
    "type": "adj",
    "pronounce": "ɔnist",
    "vietnamese": "lương thiện, trung thực, chân thật"
  },
  {
    "english": "honestly",
    "type": "adv",
    "pronounce": "ɔnistli",
    "vietnamese": "lương thiện, trung thực, chân thật"
  },
  {
    "english": "honour",
    "type": "n",
    "pronounce": "onз",
    "vietnamese": "danh dự, thanh danh, lòng kính trọng. in honour/honor of: để tỏ lòng tôn kính, trân trọng đối với"
  },
  {
    "english": "hook",
    "type": "n",
    "pronounce": "huk",
    "vietnamese": "cái móc; bản lề; lưỡi câu"
  },
  {
    "english": "hope",
    "type": "v, n",
    "pronounce": "houp",
    "vietnamese": "hy vọng; nguồn hy vọng"
  },
  {
    "english": "horizontal",
    "type": "adj",
    "pronounce": ",hɔri'zɔntl",
    "vietnamese": "(thuộc) chân trời, ở chân trời; ngang, nằm ngang (trục hoành)"
  },
  {
    "english": "horn",
    "type": "n",
    "pronounce": "hɔ:n",
    "vietnamese": "sừng (trâu, bò...)"
  },
  {
    "english": "horror",
    "type": "n",
    "pronounce": "́hɔrə",
    "vietnamese": "điều kinh khủng, sự ghê rợn"
  },
  {
    "english": "horse",
    "type": "n",
    "pronounce": "hɔrs",
    "vietnamese": "ngựa"
  },
  {
    "english": "hospital",
    "type": "n",
    "pronounce": "hɔspitl",
    "vietnamese": "bệnh viện, nhà thương"
  },
  {
    "english": "host",
    "type": "n, v",
    "pronounce": "houst",
    "vietnamese": "chủ nhà, chủ tiệc; dẫn (c.trình), >đăng cai tổ >chức (hội nghị....)"
  },
  {
    "english": "hot",
    "type": "adj",
    "pronounce": "hɒt",
    "vietnamese": "nóng, nóng bức"
  },
  {
    "english": "hotel",
    "type": "n",
    "pronounce": "hou ́tel",
    "vietnamese": "khách sạn"
  },
  {
    "english": "hour",
    "type": "n",
    "pronounce": "auз",
    "vietnamese": "giờ"
  },
  {
    "english": "house",
    "type": "n",
    "pronounce": "haus",
    "vietnamese": "nhà, căn nhà, toàn nhà"
  },
  {
    "english": "household",
    "type": "n, adj",
    "pronounce": "́haushould",
    "vietnamese": "hộ, gia đình; (thuộc) gia đình"
  },
  {
    "english": "housing",
    "type": "n",
    "pronounce": "́hauziη",
    "vietnamese": "nơi ăn chốn ở"
  },
  {
    "english": "how",
    "type": "adv",
    "pronounce": "hau",
    "vietnamese": "thế nào, như thế nào, làm sao, ra >sao"
  },
  {
    "english": "however",
    "type": "adv",
    "pronounce": "hau ́evə",
    "vietnamese": "tuy nhiên, tuy vậy, dù thế nào"
  },
  {
    "english": "huge",
    "type": "adj",
    "pronounce": "hjuːdʒ",
    "vietnamese": "to lớn, khổng lồ"
  },
  {
    "english": "human",
    "type": "adj, n",
    "pronounce": "hju:mən",
    "vietnamese": "(thuộc) con người, loài người"
  },
  {
    "english": "humorous",
    "type": "adj",
    "pronounce": "́hju:mərəs",
    "vietnamese": "hài hước, hóm hỉnh"
  },
  {
    "english": "humour",
    "type": "n",
    "pronounce": "́hju:mə",
    "vietnamese": "sự hài hước, sự hóm hỉnh"
  },
  {
    "english": "hungry",
    "type": "adj",
    "pronounce": "hΔŋgri",
    "vietnamese": "đói"
  },
  {
    "english": "hunt",
    "type": "v",
    "pronounce": "hʌnt",
    "vietnamese": "săn, đi săn"
  },
  {
    "english": "hunting",
    "type": "n",
    "pronounce": "hʌntiɳ",
    "vietnamese": "sự đi săn"
  },
  {
    "english": "hurry",
    "type": "v, n",
    "pronounce": "hɜri , hʌri",
    "vietnamese": "sự vội vàng, sự gấp rút. in a hurry: >vội vàng, hối hả, gấp rút"
  },
  {
    "english": "hurt",
    "type": "v",
    "pronounce": "hɜrt",
    "vietnamese": "làm bị thương, gây thiệt hại"
  },
  {
    "english": "husband",
    "type": "n",
    "pronounce": "́hʌzbənd",
    "vietnamese": "người chồng"
  },
  {
    "english": "i.e.",
    "type": "",
    "pronounce": "",
    "vietnamese": "nghĩa là, tức là ( Id est)"
  },
  {
    "english": "ice",
    "type": "n",
    "pronounce": "ais",
    "vietnamese": "băng, nước đá"
  },
  {
    "english": "ice cream",
    "type": "n",
    "pronounce": "",
    "vietnamese": "kem"
  },
  {
    "english": "idea",
    "type": "n",
    "pronounce": "ai'diз",
    "vietnamese": "ý tưởng, quan niệm"
  },
  {
    "english": "ideal",
    "type": "adj, n",
    "pronounce": "aɪˈdiəl, aɪˈdil",
    "vietnamese": "(thuộc) quan niệm, tư tưởng; >lý >tưởng"
  },
  {
    "english": "ideally",
    "type": "adv",
    "pronounce": "aɪˈdiəli",
    "vietnamese": "lý tưởng, đúng như lý tưởng"
  },
  {
    "english": "identify",
    "type": "v",
    "pronounce": "ai'dentifai",
    "vietnamese": "nhận biết, nhận ra, nhận dạng"
  },
  {
    "english": "identity",
    "type": "n",
    "pronounce": "aɪˈdɛntɪti",
    "vietnamese": "cá tính, nét nhận dạng; tính đồng >nhất, giống hệt"
  },
  {
    "english": "if",
    "type": "conj",
    "pronounce": "if",
    "vietnamese": "nếu, nếu như"
  },
  {
    "english": "ignore",
    "type": "v",
    "pronounce": "ig'no:(r)",
    "vietnamese": "phớt lờ, tỏ ra không biết đến"
  },
  {
    "english": "ill",
    "type": "adj",
    "pronounce": "il",
    "vietnamese": "ốm"
  },
  {
    "english": "illegal",
    "type": "adj",
    "pronounce": "i ́li:gl",
    "vietnamese": "trái luật, bất hợp pháp"
  },
  {
    "english": "illegally",
    "type": "adv",
    "pronounce": "i ́li:gəli",
    "vietnamese": "trái luật, bất hợp pháp"
  },
  {
    "english": "illness",
    "type": "n",
    "pronounce": "́ilnis",
    "vietnamese": "sự đau yếu, ốm, bệnh tật"
  },
  {
    "english": "illustrate",
    "type": "v",
    "pronounce": "́ilə ̧streit",
    "vietnamese": "minh họa, làm rõ ý"
  },
  {
    "english": "image",
    "type": "n",
    "pronounce": "́imidʒ",
    "vietnamese": "ảnh, hình ảnh"
  },
  {
    "english": "imaginary",
    "type": "adj",
    "pronounce": "i ́mædʒinəri",
    "vietnamese": "tưởng tượng, ảo"
  },
  {
    "english": "imagination",
    "type": "n",
    "pronounce": "i,mædʤi'neiʃn",
    "vietnamese": "trí tưởng tượng, sự tưởng tượng"
  },
  {
    "english": "imagine",
    "type": "v",
    "pronounce": "i'mæʤin",
    "vietnamese": "tưởng tượng, hình dung; tưởng >rằng, cho rằng"
  },
  {
    "english": "immediate",
    "type": "adj",
    "pronounce": "i'mi:djət",
    "vietnamese": "lập tức, >tức thì"
  },
  {
    "english": "immediately",
    "type": "adv",
    "pronounce": "i'mi:djətli",
    "vietnamese": "ngay lập tức"
  },
  {
    "english": "immoral",
    "type": "adj",
    "pronounce": "i ́mɔrəl",
    "vietnamese": "trái đạo đức, luân lý; xấu xa"
  },
  {
    "english": "impact",
    "type": "n",
    "pronounce": "ɪmpækt",
    "vietnamese": "sự và chạm, sự tác động, ảnh >hưởng"
  },
  {
    "english": "impatient",
    "type": "adj",
    "pronounce": "im'peiʃən",
    "vietnamese": "thiếu kiên nhẫn, nóng vội"
  },
  {
    "english": "impatiently",
    "type": "adv",
    "pronounce": "im'pei∫зns",
    "vietnamese": "nóng lòng, sốt ruột"
  },
  {
    "english": "implication",
    "type": "n",
    "pronounce": "̧impli ́keiʃən",
    "vietnamese": "sự lôi kéo, sự liên can, điều gợi ý"
  },
  {
    "english": "imply",
    "type": "v",
    "pronounce": "im'plai",
    "vietnamese": "ngụ ý, bao hàm"
  },
  {
    "english": "import",
    "type": "n, v",
    "pronounce": "",
    "vietnamese": "import sự nhập, sự nhập khẩu; >nhập, nhập khẩu"
  },
  {
    "english": "importance",
    "type": "n",
    "pronounce": "im'pɔ:təns",
    "vietnamese": "sự quan trọng, tầm quan trọng"
  },
  {
    "english": "important",
    "type": "adj",
    "pronounce": "im'pɔ:tənt",
    "vietnamese": "quan trọng, hệ trọng"
  },
  {
    "english": "importantly",
    "type": "adv",
    "pronounce": "im'pɔ:təntli",
    "vietnamese": "quan trọng, trọng yếu"
  },
  {
    "english": "impose",
    "type": "v",
    "pronounce": "im'pouz",
    "vietnamese": "đánh (thuế...), bắt gánh vác; đánh >tráo, lợi dụng"
  },
  {
    "english": "impossible",
    "type": "adj",
    "pronounce": "im'pɔsəbl",
    "vietnamese": "không thể làm được, không thể >xảy ra"
  },
  {
    "english": "impress",
    "type": "v",
    "pronounce": "im'pres",
    "vietnamese": "ghi, khắc, in sâu vào; gây ấn >tượng, làm cảm động"
  },
  {
    "english": "impressed",
    "type": "adj",
    "pronounce": "",
    "vietnamese": "được ghi, khắc, in sâu vào"
  },
  {
    "english": "impression",
    "type": "n",
    "pronounce": "ɪmˈprɛʃən",
    "vietnamese": "ấn tượng, cảm giác; sự in, đóng >dấu"
  },
  {
    "english": "impressive",
    "type": "adj",
    "pronounce": "im'presiv",
    "vietnamese": "gây ấn tượng mạnh; hùng vĩ, oai >vệ"
  },
  {
    "english": "improve",
    "type": "v",
    "pronounce": "im'pru:v",
    "vietnamese": "cải thiện, cái tiến, mở mang"
  },
  {
    "english": "improvement",
    "type": "n",
    "pronounce": "im'pru:vmənt",
    "vietnamese": "sự cải thiện, sự cải tiến, sự mở >mang"
  },
  {
    "english": "in",
    "type": "prep, adv",
    "pronounce": "in",
    "vietnamese": "ở, tại, trong; vào"
  },
  {
    "english": "in addition",
    "type": "to",
    "pronounce": "",
    "vietnamese": "thêm vào"
  },
  {
    "english": "in case of",
    "type": "",
    "pronounce": "",
    "vietnamese": "nếu......"
  },
  {
    "english": "in control of",
    "type": "",
    "pronounce": "",
    "vietnamese": "trong sự >điều khiển của. under >control dưới sự điều khiển của"
  },
  {
    "english": "in exchange for",
    "type": "",
    "pronounce": "",
    "vietnamese": "trong việc trao đổi về"
  },
  {
    "english": "inability",
    "type": "n",
    "pronounce": "̧inə ́biliti",
    "vietnamese": "sự bất lực, bất tài"
  },
  {
    "english": "inch",
    "type": "n",
    "pronounce": "intʃ",
    "vietnamese": "insơ (đơn vị đo chiều dài Anh >bằng 2, 54 cm)"
  },
  {
    "english": "incident",
    "type": "n",
    "pronounce": "́insidənt",
    "vietnamese": "việc xảy ra, >việc có liên quan"
  },
  {
    "english": "include",
    "type": "v",
    "pronounce": "in'klu:d",
    "vietnamese": "bao gồm, tính cả"
  },
  {
    "english": "including",
    "type": "prep",
    "pronounce": "in ́klu:diη",
    "vietnamese": "bao gồm, kể cả"
  },
  {
    "english": "income",
    "type": "n",
    "pronounce": "inkəm",
    "vietnamese": "lợi tức, thu nhập"
  },
  {
    "english": "increase",
    "type": "v, n",
    "pronounce": "in'kri:s",
    "vietnamese": "tăng, tăng thêm; sự tăng, sự tăng >thêm"
  },
  {
    "english": "increasingly",
    "type": "adv",
    "pronounce": "in ́kri:siηli",
    "vietnamese": "tăng >thêm"
  },
  {
    "english": "indeed",
    "type": "adv",
    "pronounce": "ɪnˈdid",
    "vietnamese": "thật vậy, quả thật"
  },
  {
    "english": "independence",
    "type": "n",
    "pronounce": ",indi'pendəns",
    "vietnamese": "sự độc lập, nền độc lập"
  },
  {
    "english": "independent",
    "type": "adj",
    "pronounce": ",indi'pendənt",
    "vietnamese": "độc lập"
  },
  {
    "english": "independently",
    "type": "adv",
    "pronounce": ",indi'pendзntli",
    "vietnamese": "độc lập"
  },
  {
    "english": "index",
    "type": "n",
    "pronounce": "indeks",
    "vietnamese": "chỉ số, sự biểu thị"
  },
  {
    "english": "indicate",
    "type": "v",
    "pronounce": "́indikeit",
    "vietnamese": "chỉ, cho biết; biểu thị, trình bày >ngắn gọn"
  },
  {
    "english": "indication",
    "type": "n",
    "pronounce": ",indi'kei∫n",
    "vietnamese": "sự chỉ, sự biểu thị, sự biểu lộ"
  },
  {
    "english": "indirect",
    "type": "adj",
    "pronounce": "̧indi ́rekt",
    "vietnamese": "gián tiếp"
  },
  {
    "english": "indirectly",
    "type": "adv",
    "pronounce": ",indi'rektli",
    "vietnamese": "gián tiếp"
  },
  {
    "english": "individual",
    "type": "adj, n",
    "pronounce": "indivídʤuəl",
    "vietnamese": "riêng, riêng biệt; cá nhân"
  },
  {
    "english": "indoor",
    "type": "adj",
    "pronounce": "́in ̧dɔ:",
    "vietnamese": "trong nhà"
  },
  {
    "english": "indoors",
    "type": "adv",
    "pronounce": "̧in ́dɔ:z",
    "vietnamese": "ở trong nhà"
  },
  {
    "english": "industrial",
    "type": "adj",
    "pronounce": "in ́dʌstriəl",
    "vietnamese": "(thuộc) công nghiệp, kỹ nghệ"
  },
  {
    "english": "industry",
    "type": "n",
    "pronounce": "indəstri",
    "vietnamese": "công nghiệp, kỹ nghệ"
  },
  {
    "english": "inevitable",
    "type": "adj",
    "pronounce": "in ́evitəbl",
    "vietnamese": "không thể tránh được, chắc chắn >xảy ra; vẫn thường thấy, nghe"
  },
  {
    "english": "inevitably",
    "type": "adv",
    "pronounce": "in’evitəbli",
    "vietnamese": "chắc chắn, chắc hẳn"
  },
  {
    "english": "infect",
    "type": "v",
    "pronounce": "in'fekt",
    "vietnamese": "nhiễm, tiêm nhiễm, đầu độc, lan truyền"
  },
  {
    "english": "infected",
    "type": "adj",
    "pronounce": "",
    "vietnamese": "bị nhiễm, bị đầu độc"
  },
  {
    "english": "infection",
    "type": "n",
    "pronounce": "in'fekʃn",
    "vietnamese": "sự nhiễm, sự đầu độc"
  },
  {
    "english": "infectious",
    "type": "adj",
    "pronounce": "in ́fekʃəs",
    "vietnamese": "lây, nhiễm"
  },
  {
    "english": "influence",
    "type": "n, v",
    "pronounce": "ˈɪnfluəns",
    "vietnamese": "sự ảnh hưởng, sự tác dụng; ảnh hưởng, tác động"
  },
  {
    "english": "inform",
    "type": "v",
    "pronounce": "in'fo:m",
    "vietnamese": "báo cho biết, cung cấp tin tức"
  },
  {
    "english": "informal",
    "type": "adj",
    "pronounce": "in ́fɔ:məl",
    "vietnamese": "không chính thức, không nghi thức"
  },
  {
    "english": "information",
    "type": "n",
    "pronounce": ",infə'meinʃn",
    "vietnamese": "tin tức, tài liệu, kiến thức"
  },
  {
    "english": "ingredient",
    "type": "n",
    "pronounce": "in'gri:diənt",
    "vietnamese": "phần hợp thành, thành phần"
  },
  {
    "english": "initial",
    "type": "adj, n",
    "pronounce": "i'ni∫зl",
    "vietnamese": "ban đầu, lúc đầu; chữ đầu (của 1 tên gọi)"
  },
  {
    "english": "initially",
    "type": "adv",
    "pronounce": "i ́niʃəli",
    "vietnamese": "vào lúc ban đầu, ban đầu"
  },
  {
    "english": "initiative",
    "type": "n",
    "pronounce": "ɪˈnɪʃiətɪv , ɪˈnɪʃətɪv",
    "vietnamese": "bước đầu, sự khởi đầu"
  },
  {
    "english": "injure",
    "type": "v",
    "pronounce": "in'dӡə(r)",
    "vietnamese": "làm tổn thương, làm hại, xúc phạm"
  },
  {
    "english": "injured",
    "type": "adj",
    "pronounce": "́indʒə:d",
    "vietnamese": "bị tổn thương, bị xúc phạm"
  },
  {
    "english": "injury",
    "type": "n",
    "pronounce": "indʤəri",
    "vietnamese": "sự làm tổn thương, làm hại; điều hại, điều tổn hại"
  },
  {
    "english": "ink",
    "type": "n",
    "pronounce": "iηk",
    "vietnamese": "mực"
  },
  {
    "english": "inner",
    "type": "adj",
    "pronounce": "inə",
    "vietnamese": "ở trong, nội bộ; thân cận"
  },
  {
    "english": "innocent",
    "type": "adj",
    "pronounce": "inəsnt",
    "vietnamese": "vô tội, trong trắng, ngây thơ"
  },
  {
    "english": "insect",
    "type": "n",
    "pronounce": "insekt",
    "vietnamese": "sâu bọ, côn trùng"
  },
  {
    "english": "insert",
    "type": "v",
    "pronounce": "insə:t",
    "vietnamese": "chèn vào, lồng vào"
  },
  {
    "english": "inside",
    "type": "prep, adv, n, adj",
    "pronounce": "in'said",
    "vietnamese": "mặt trong, phía, phần trong; ở trong, nội bộ"
  },
  {
    "english": "insist",
    "type": "on, v",
    "pronounce": "in'sist",
    "vietnamese": "cứ nhất định, cứ khăng khăng"
  },
  {
    "english": "install",
    "type": "v",
    "pronounce": "in'stɔ:l",
    "vietnamese": "đặt (hệ thống máy móc, thiết bị...)"
  },
  {
    "english": "instance",
    "type": "n",
    "pronounce": "instəns",
    "vietnamese": "thí dị, ví dụ; trường hợp cá biệt. for instance ví dụ chẳng hạn"
  },
  {
    "english": "instead",
    "type": "adv",
    "pronounce": "in'sted",
    "vietnamese": "để thay thế. instead of thay cho"
  },
  {
    "english": "institute",
    "type": "n",
    "pronounce": "ˈ ́institju:t",
    "vietnamese": "viện, học viện"
  },
  {
    "english": "institution",
    "type": "n",
    "pronounce": "insti'tju:ʃn",
    "vietnamese": "sự thành lập, lập; cơ quan, trụ sở"
  },
  {
    "english": "instruction",
    "type": "n",
    "pronounce": "ɪn'strʌkʃn",
    "vietnamese": "sự dạy, tài liệu cung cấp"
  },
  {
    "english": "instrument",
    "type": "n",
    "pronounce": "instrumənt",
    "vietnamese": "dụng cụ âm nhạc khí"
  },
  {
    "english": "insult",
    "type": "v, n",
    "pronounce": "insʌlt",
    "vietnamese": "lăng mạ, xỉ nhục; lời lăng mạ, sự xỉ nhục"
  },
  {
    "english": "insulting",
    "type": "adj",
    "pronounce": "in ́sʌltiη",
    "vietnamese": "lăng mạ, xỉ nhục"
  },
  {
    "english": "insurance",
    "type": "n",
    "pronounce": "in'ʃuərəns",
    "vietnamese": "sự bảo hiểm"
  },
  {
    "english": "intelligence",
    "type": "n",
    "pronounce": "in'telidʒəns",
    "vietnamese": "sự hiểu biết, trí thông minh"
  },
  {
    "english": "intelligent",
    "type": "adj",
    "pronounce": "in,teli'dЗen∫зl",
    "vietnamese": "thông minh, sáng trí"
  },
  {
    "english": "intend",
    "type": "v",
    "pronounce": "in'tend",
    "vietnamese": "ý định, có ý định"
  },
  {
    "english": "intended",
    "type": "adj",
    "pronounce": "in ́tendid",
    "vietnamese": "có ý định, có dụng ý"
  },
  {
    "english": "intention",
    "type": "n",
    "pronounce": "in'tenʃn",
    "vietnamese": "ý định, mục đích"
  },
  {
    "english": "interest",
    "type": "n, v",
    "pronounce": "ˈɪntərest",
    "vietnamese": "sự thích thú, sự quan tâm, chú ý; làm quan tâm, làm chú ý"
  },
  {
    "english": "interested",
    "type": "adj",
    "pronounce": "có thích thú, có quan tâm,",
    "vietnamese": "có chú ý"
  },
  {
    "english": "interesting",
    "type": "adj",
    "pronounce": "intristiŋ",
    "vietnamese": "làm thích thú, làm quan tâm, làm chú ý"
  },
  {
    "english": "interior",
    "type": "n, adj",
    "pronounce": "in'teriə",
    "vietnamese": "phần trong, phía trong; ở trong, ở phía trong"
  },
  {
    "english": "internal",
    "type": "adj",
    "pronounce": "in'tə:nl",
    "vietnamese": "ở trong, bên trong, nội địa"
  },
  {
    "english": "international",
    "type": "adj",
    "pronounce": "intə'næʃən(ə)l",
    "vietnamese": "quốc tế"
  },
  {
    "english": "internet",
    "type": "n",
    "pronounce": "intə,net",
    "vietnamese": "liên mạng"
  },
  {
    "english": "interpret",
    "type": "v",
    "pronounce": "in'tз:prit",
    "vietnamese": "giải thích"
  },
  {
    "english": "interpretation",
    "type": "n",
    "pronounce": "in,tə:pri'teiʃn",
    "vietnamese": "sự giải thích"
  },
  {
    "english": "interrupt",
    "type": "v",
    "pronounce": "ɪntǝ'rʌpt",
    "vietnamese": "làm gián đoạn, ngắt lời"
  },
  {
    "english": "interruption",
    "type": "n",
    "pronounce": ",intə'rʌp∫n",
    "vietnamese": "sự gián đoạn, sự ngắt lời"
  },
  {
    "english": "interval",
    "type": "n",
    "pronounce": "ˈɪntərvəl",
    "vietnamese": "khoảng (khoãng thời gian), khoảng cách"
  },
  {
    "english": "interview",
    "type": "n, v",
    "pronounce": "intəvju:",
    "vietnamese": "cuộc phỏng vấn, sự gặp mặt; phỏng vấn, nói chuyện riêng"
  },
  {
    "english": "into",
    "type": "prep",
    "pronounce": "intu",
    "vietnamese": "vào, vào trong"
  },
  {
    "english": "introduce",
    "type": "v",
    "pronounce": "intrədju:s",
    "vietnamese": "giới thiệu"
  },
  {
    "english": "introduction",
    "type": "n",
    "pronounce": "̧intrə ́dʌkʃən",
    "vietnamese": "sự giới thiệu, lời giới thiệu"
  },
  {
    "english": "invent",
    "type": "v",
    "pronounce": "in'vent",
    "vietnamese": "phát minh, sáng chế"
  },
  {
    "english": "invention",
    "type": "n",
    "pronounce": "ɪnˈvɛnʃən",
    "vietnamese": "sự phát minh, sự sáng chế"
  },
  {
    "english": "invest",
    "type": "v",
    "pronounce": "in'vest",
    "vietnamese": "đầu tư"
  },
  {
    "english": "investigate",
    "type": "v",
    "pronounce": "in'vestigeit",
    "vietnamese": "điều tra, nghiên cứu"
  },
  {
    "english": "investigation",
    "type": "n",
    "pronounce": "in ̧vesti ́geiʃən",
    "vietnamese": "sự điều tra, nghiên cứu"
  },
  {
    "english": "investment",
    "type": "n",
    "pronounce": "in'vestmənt",
    "vietnamese": "sự đầu tư, vốn đầu tư"
  },
  {
    "english": "invitation",
    "type": "n",
    "pronounce": ",invi'teiʃn",
    "vietnamese": "lời mời, sự mời"
  },
  {
    "english": "invite",
    "type": "v",
    "pronounce": "in'vait",
    "vietnamese": "mời"
  },
  {
    "english": "involve",
    "type": "v",
    "pronounce": "ɪnˈvɒlv",
    "vietnamese": "bao gồm, bao hàm; thu hút, dồn tâm trí. involved in để hết tâm trí vào"
  },
  {
    "english": "involvement",
    "type": "n",
    "pronounce": "in'vɔlvmənt",
    "vietnamese": "sự gồm, sự bao hàm; sự để, dồn hết tâm trí vào"
  },
  {
    "english": "iron",
    "type": "n, v",
    "pronounce": "aɪən",
    "vietnamese": "sắt; bọc sắt"
  },
  {
    "english": "irritate",
    "type": "v",
    "pronounce": "́iri ̧teit",
    "vietnamese": "làm phát cáu, chọc tức"
  },
  {
    "english": "irritated",
    "type": "adj",
    "pronounce": "iriteitid",
    "vietnamese": "tức giận, cáu tiết"
  },
  {
    "english": "irritating",
    "type": "adj",
    "pronounce": "́iriteitiη",
    "vietnamese": "làm phát cáu, chọc tức"
  },
  {
    "english": "island",
    "type": "n",
    "pronounce": "́ailənd",
    "vietnamese": "hòn đảo"
  },
  {
    "english": "issue",
    "type": "n, v",
    "pronounce": "ɪʃuː; also ɪsjuː",
    "vietnamese": "sự phát ra, sự phát sinh; phát hành, đưa ra"
  },
  {
    "english": "it",
    "type": "n, det, pro",
    "pronounce": "it",
    "vietnamese": "cái đó, điều đó, con vật đó"
  },
  {
    "english": "item",
    "type": "n",
    "pronounce": "aitəm",
    "vietnamese": "tin tức; khoả(n)., mó(n).., tiết mục"
  },
  {
    "english": "its",
    "type": "det",
    "pronounce": "its",
    "vietnamese": "của cái đó, của điều đó, của con vật đó; cái của điều đó, cái của con vật đó"
  },
  {
    "english": "itself",
    "type": "pron",
    "pronounce": "it ́self",
    "vietnamese": "chính cái đó, chính điều đó, chính con vật đó"
  },
  {
    "english": "jacket",
    "type": "n",
    "pronounce": "dʤækit",
    "vietnamese": "áo vét"
  },
  {
    "english": "jam",
    "type": "n",
    "pronounce": "dʒæm",
    "vietnamese": "mứt, sự mắc kẹt, sự kẹt (máy...)"
  },
  {
    "english": "January (abbrJan)",
    "type": "n",
    "pronounce": "ʤænjuəri",
    "vietnamese": "tháng giêng"
  },
  {
    "english": "jealous",
    "type": "adj",
    "pronounce": "ʤeləs",
    "vietnamese": "ghen,, ghen tị"
  },
  {
    "english": "jeans",
    "type": "n",
    "pronounce": "dЗeins",
    "vietnamese": "quần bò, quần zin"
  },
  {
    "english": "jelly",
    "type": "n",
    "pronounce": "́dʒeli",
    "vietnamese": "thạch"
  },
  {
    "english": "jewellery",
    "type": "n",
    "pronounce": "dʤu:əlri",
    "vietnamese": "nữ trang, kim hoàn"
  },
  {
    "english": "job",
    "type": "n",
    "pronounce": "dʒɔb",
    "vietnamese": "việc, việc làm"
  },
  {
    "english": "join",
    "type": "v",
    "pronounce": "ʤɔin",
    "vietnamese": "gia nhập, tham gia; nối, chắp, ghép"
  },
  {
    "english": "",
    "type": "",
    "pronounce": "",
    "vietnamese": ""
  },
  {
    "english": "joint",
    "type": "adj, n",
    "pronounce": "dʒɔɪnt",
    "vietnamese": "chung (giữa 2 người hoặc hơn); chỗ nối, đầu nối"
  },
  {
    "english": "jointly",
    "type": "adv",
    "pronounce": "ˈdʒɔɪntli",
    "vietnamese": "cùng nhau, cùng chung"
  },
  {
    "english": "joke",
    "type": "n, v",
    "pronounce": "dʒouk",
    "vietnamese": "trò cười, lời nói đùa; nói đùa, giễu cợt"
  },
  {
    "english": "journalist",
    "type": "n",
    "pronounce": "́dʒə:nəlist",
    "vietnamese": "nhà báo"
  },
  {
    "english": "journey",
    "type": "n",
    "pronounce": "dʤə:ni",
    "vietnamese": "cuộc hành trình (đường bộ); quãng đường, chặng đường đi"
  },
  {
    "english": "joy",
    "type": "n",
    "pronounce": "dʒɔɪ",
    "vietnamese": "niềm vui, sự vui mừng"
  },
  {
    "english": "judge",
    "type": "n, v",
    "pronounce": "dʒʌdʒ",
    "vietnamese": "xét xử, phân xử; quan tòa, thẩm phán"
  },
  {
    "english": "judgement",
    "type": "n",
    "pronounce": "dʤʌdʤmənt",
    "vietnamese": "sự xét xử"
  },
  {
    "english": "juice",
    "type": "n",
    "pronounce": "ʤu:s",
    "vietnamese": "nước ép (rau, củ, quả)"
  },
  {
    "english": "July (abbr Jul)",
    "type": "n",
    "pronounce": "dʒu ́lai",
    "vietnamese": "tháng 7"
  },
  {
    "english": "jump",
    "type": "v, n",
    "pronounce": "dʒʌmp",
    "vietnamese": "nhảy; sự nhảy, bước nhảy"
  },
  {
    "english": "June (abbr Jun)",
    "type": "n",
    "pronounce": "dЗu:n",
    "vietnamese": "tháng 6"
  },
  {
    "english": "junior",
    "type": "adj, n",
    "pronounce": "́dʒu:niə",
    "vietnamese": "trẻ hơn, ít tuổi hơn; người ít tuổi hơn"
  },
  {
    "english": "just",
    "type": "adv",
    "pronounce": "dʤʌst",
    "vietnamese": "đúng, vưa đủ; vưa mới, chỉ"
  },
  {
    "english": "justice",
    "type": "n",
    "pronounce": "dʤʌstis",
    "vietnamese": "sự công bằng"
  },
  {
    "english": "justified",
    "type": "adj",
    "pronounce": "dʒʌstɪfaɪd",
    "vietnamese": "hợp lý, được chứng minh là đúng"
  },
  {
    "english": "justify",
    "type": "v",
    "pronounce": "́dʒʌsti ̧fai",
    "vietnamese": "bào chữa, biện hộ"
  },
  {
    "english": "keen",
    "type": "adj",
    "pronounce": "ki:n",
    "vietnamese": "sắc, bén. keen on: say mê, ưa thích"
  },
  {
    "english": "keep",
    "type": "v",
    "pronounce": "ki:p",
    "vietnamese": "giữ, giữ lại"
  },
  {
    "english": "key",
    "type": "n, adj",
    "pronounce": "ki:",
    "vietnamese": "chìa khóa, khóa, thuộc (khóa)"
  },
  {
    "english": "keyboard",
    "type": "n",
    "pronounce": "ki:bɔ:d",
    "vietnamese": "bàn phím"
  },
  {
    "english": "kick",
    "type": "v, n",
    "pronounce": "kick",
    "vietnamese": "đá; cú đá"
  },
  {
    "english": "kid",
    "type": "n",
    "pronounce": "kid",
    "vietnamese": "con dê non"
  },
  {
    "english": "kill",
    "type": "v",
    "pronounce": "kil",
    "vietnamese": "giết, tiêu diệt"
  },
  {
    "english": "killing",
    "type": "n",
    "pronounce": "́kiliη",
    "vietnamese": "sự giết chóc, sự tàn sát"
  },
  {
    "english": "kilogram, kilogramme, kilo (abbr kg)",
    "type": "n",
    "pronounce": "́kilou ̧græm",
    "vietnamese": "Kilôgam"
  },
  {
    "english": "kilometre",
    "type": "n",
    "pronounce": "́kilə ̧mi:tə",
    "vietnamese": "Kilômet"
  },
  {
    "english": "kilometre, kilometer (abbr k, km)",
    "type": "n",
    "pronounce": "́kilə ̧mi:tə",
    "vietnamese": "Kilômet"
  },
  {
    "english": "kind",
    "type": "n, adj",
    "pronounce": "kaind",
    "vietnamese": "loại, giống; tử tế, có lòng tốt"
  },
  {
    "english": "kindly",
    "type": "adv",
    "pronounce": "́kaindli",
    "vietnamese": "tử tế, tốt bụng"
  },
  {
    "english": "kindness",
    "type": "n",
    "pronounce": "kaindnis",
    "vietnamese": "sự tử tế, lòng tốt"
  },
  {
    "english": "king",
    "type": "n",
    "pronounce": "kiɳ",
    "vietnamese": "vua, quốc vương"
  },
  {
    "english": "kiss",
    "type": "v, n",
    "pronounce": "kis",
    "vietnamese": "hôn, cái hôn"
  },
  {
    "english": "kitchen",
    "type": "n",
    "pronounce": "́kitʃin",
    "vietnamese": "bếp"
  },
  {
    "english": "knee",
    "type": "n",
    "pronounce": "ni:",
    "vietnamese": "đầu gối"
  },
  {
    "english": "knife",
    "type": "n",
    "pronounce": "naif",
    "vietnamese": "con dao"
  },
  {
    "english": "knit",
    "type": "v",
    "pronounce": "nit",
    "vietnamese": "đan, thêu"
  },
  {
    "english": "knitted",
    "type": "adj",
    "pronounce": "nitid",
    "vietnamese": "được đan, được thêu"
  },
  {
    "english": "knitting",
    "type": "n",
    "pronounce": "́nitiη",
    "vietnamese": "việc đan; hàng dệt kim"
  },
  {
    "english": "knock",
    "type": "v, n",
    "pronounce": "nɔk",
    "vietnamese": "đánh, đập; cú đánh"
  },
  {
    "english": "knot",
    "type": "n",
    "pronounce": "nɔt",
    "vietnamese": "cái nơ; điểm nút, điểm trung tâm"
  },
  {
    "english": "know",
    "type": "v",
    "pronounce": "nou",
    "vietnamese": "biết"
  },
  {
    "english": "knowledge",
    "type": "n",
    "pronounce": "nɒliʤ",
    "vietnamese": "sự hiểu biết, tri thức"
  },
  {
    "english": "label",
    "type": "n, v",
    "pronounce": "leibl",
    "vietnamese": "nhãn, mác; dán nhãn, ghi mác"
  },
  {
    "english": "laboratory, lab",
    "type": "n",
    "pronounce": "ˈlæbrəˌtɔri",
    "vietnamese": "phòng thí nghiệm"
  },
  {
    "english": "labour",
    "type": "n",
    "pronounce": "leibз",
    "vietnamese": "lao động; công việc"
  },
  {
    "english": "lack",
    "type": "of, n, v",
    "pronounce": "læk",
    "vietnamese": "sự thiếu; thiếu"
  },
  {
    "english": "lacking",
    "type": "adj",
    "pronounce": "lækiη",
    "vietnamese": "ngu đần, ngây ngô"
  },
  {
    "english": "lady",
    "type": "n",
    "pronounce": "ˈleɪdi",
    "vietnamese": "người yêu, vợ, quý bà, tiểu thư"
  },
  {
    "english": "lake",
    "type": "n",
    "pronounce": "leik",
    "vietnamese": "hồ"
  },
  {
    "english": "lamp",
    "type": "n",
    "pronounce": "læmp",
    "vietnamese": "đèn"
  },
  {
    "english": "land",
    "type": "n, v",
    "pronounce": "lænd",
    "vietnamese": "đất, đất canh tác, đất đai"
  },
  {
    "english": "landscape",
    "type": "n",
    "pronounce": "lændskeip",
    "vietnamese": "phong cảnh"
  },
  {
    "english": "lane",
    "type": "n",
    "pronounce": "lein",
    "vietnamese": "đường nhỏ (làng, hẻm phố)"
  },
  {
    "english": "language",
    "type": "n",
    "pronounce": "ˈlæŋgwɪdʒ",
    "vietnamese": "ngôn ngữ"
  },
  {
    "english": "large",
    "type": "adj",
    "pronounce": "la:dʒ",
    "vietnamese": "rộng, lớn, to"
  },
  {
    "english": "largely",
    "type": "adv",
    "pronounce": "́la:dʒli",
    "vietnamese": "phong phú, ở mức độ lớn"
  },
  {
    "english": "last",
    "type": "det, , adv, n, v",
    "pronounce": "lɑ:st",
    "vietnamese": "lần cuối, sau cùng; người cuối cùng; cuối cùng, rốt hết; kéo dài"
  },
  {
    "english": "late",
    "type": "adj, adv",
    "pronounce": "leit",
    "vietnamese": "trễ, muộn"
  },
  {
    "english": "later",
    "type": "adv, adj",
    "pronounce": "leɪtə(r)",
    "vietnamese": "chậm hơn"
  },
  {
    "english": "latest",
    "type": "adj, n",
    "pronounce": "leitist",
    "vietnamese": "muộn nhất, chậm nhất, gần đây nhất"
  },
  {
    "english": "latter",
    "type": "adj, n",
    "pronounce": "́lætə",
    "vietnamese": "sau cùng, gần đây, mới đây"
  },
  {
    "english": "laugh",
    "type": "v, n",
    "pronounce": "lɑ:f",
    "vietnamese": "cười; tiếng cười"
  },
  {
    "english": "launch",
    "type": "v, n",
    "pronounce": "lɔ:ntʃ",
    "vietnamese": "hạ thủy (tàu); khai trương; sự hạ thủy, buổi giới thiệu sản phầm"
  },
  {
    "english": "law",
    "type": "n",
    "pronounce": "lo:",
    "vietnamese": "luật"
  },
  {
    "english": "lawyer",
    "type": "n",
    "pronounce": "ˈlɔyər , ˈlɔɪər",
    "vietnamese": "luật sư"
  },
  {
    "english": "lay",
    "type": "v",
    "pronounce": "lei",
    "vietnamese": "xếp, đặt, bố trí"
  },
  {
    "english": "layer",
    "type": "n",
    "pronounce": "leiə",
    "vietnamese": "lớp"
  },
  {
    "english": "lazy",
    "type": "adj",
    "pronounce": "leizi",
    "vietnamese": "lười biếng"
  },
  {
    "english": "lead",
    "type": "v, n",
    "pronounce": "li:d",
    "vietnamese": "lãnh đạo, dẫn dắt; sự lãnh đạo, sự hướng dẫn"
  },
  {
    "english": "leader",
    "type": "n",
    "pronounce": "́li:də",
    "vietnamese": "người lãnh đạo, lãnh tụ"
  },
  {
    "english": "leading",
    "type": "adj",
    "pronounce": "́li:diη",
    "vietnamese": "lãnh đạo, dẫn đầu"
  },
  {
    "english": "leaf",
    "type": "n",
    "pronounce": "li:f",
    "vietnamese": "lá cây, lá (vàng...)"
  },
  {
    "english": "league",
    "type": "n",
    "pronounce": "li:g",
    "vietnamese": "liên minh, liên hoàn"
  },
  {
    "english": "lean",
    "type": "v",
    "pronounce": "li:n",
    "vietnamese": "nghiêng, dựa, ỷ vào"
  },
  {
    "english": "learn",
    "type": "v",
    "pronounce": "lə:n",
    "vietnamese": "học, nghiên cứu"
  },
  {
    "english": "least",
    "type": "det, pron, adv",
    "pronounce": "li:st",
    "vietnamese": "tối thiểu; ít nhất. at least: ít ra, ít nhất, chí ít"
  },
  {
    "english": "leather",
    "type": "n",
    "pronounce": "leðə",
    "vietnamese": "da thuộc"
  },
  {
    "english": "leave",
    "type": "v",
    "pronounce": "li:v",
    "vietnamese": "bỏ đi, rời đi, để lại. leave out bỏ quên, bỏ sót"
  },
  {
    "english": "lecture",
    "type": "n",
    "pronounce": "lekt∫ə(r)",
    "vietnamese": "bài diễn thuyết, bài thuyết trình, bài nói chuyện"
  },
  {
    "english": "left",
    "type": "adj, adv, n",
    "pronounce": "left",
    "vietnamese": "bên trái; về phía trái"
  },
  {
    "english": "leg",
    "type": "n",
    "pronounce": "́leg",
    "vietnamese": "chân (người, thú, bà(n)..)"
  },
  {
    "english": "legal",
    "type": "adj",
    "pronounce": "ˈligəl",
    "vietnamese": "hợp pháp"
  },
  {
    "english": "legally",
    "type": "adv",
    "pronounce": "li:gзlizm",
    "vietnamese": "hợp pháp"
  },
  {
    "english": "lemon",
    "type": "n",
    "pronounce": "́lemən",
    "vietnamese": "quả chanh"
  },
  {
    "english": "lend",
    "type": "v",
    "pronounce": "lend",
    "vietnamese": "cho vay, cho mượn"
  },
  {
    "english": "length",
    "type": "n",
    "pronounce": "leɳθ",
    "vietnamese": "chiều dài, độ dài"
  },
  {
    "english": "less",
    "type": "det, pron, adv",
    "pronounce": "les",
    "vietnamese": "nhỏ bé, ít hơn; số lượng ít hơn"
  },
  {
    "english": "lesson",
    "type": "n",
    "pronounce": "lesn",
    "vietnamese": "bài học"
  },
  {
    "english": "let",
    "type": "v",
    "pronounce": "lεt",
    "vietnamese": "cho phép, để cho"
  },
  {
    "english": "letter",
    "type": "n",
    "pronounce": "letə",
    "vietnamese": "thư; chữ cái, mẫu tự"
  },
  {
    "english": "level",
    "type": "n, adj",
    "pronounce": "levl",
    "vietnamese": "trình độ, cấp, vị trí; bằng, ngang bằng"
  },
  {
    "english": "library",
    "type": "n",
    "pronounce": "laibrəri",
    "vietnamese": "thư viện"
  },
  {
    "english": "licence",
    "type": "n",
    "pronounce": "ˈlaɪsəns",
    "vietnamese": "bằng, chứng chỉ, bằng cử nhân; sự cho phép"
  },
  {
    "english": "license",
    "type": "v",
    "pronounce": "laisзns",
    "vietnamese": "cấp chứng chỉ, cấp bằng, cho phép"
  },
  {
    "english": "lid",
    "type": "n",
    "pronounce": "lid",
    "vietnamese": "nắp, vung (xoong, nồi..); mi mắt (eyelid)"
  },
  {
    "english": "lie",
    "type": "v, n",
    "pronounce": "lai",
    "vietnamese": "nói dối; lời nói dối, sự dối trá"
  },
  {
    "english": "life",
    "type": "n",
    "pronounce": "laif",
    "vietnamese": "đời, sự sống"
  },
  {
    "english": "lift",
    "type": "v, n",
    "pronounce": "lift",
    "vietnamese": "giơ lên, nhấc lên; sự nâng, sự nhấc lên"
  },
  {
    "english": "light",
    "type": "n, adj, v",
    "pronounce": "lait",
    "vietnamese": "ánh sáng; nhẹ, nhẹ nhàng; đốt, thắp sáng"
  },
  {
    "english": "lightly",
    "type": "adv",
    "pronounce": "́laitli",
    "vietnamese": "nhẹ nhàng"
  },
  {
    "english": "like",
    "type": "prep, vconj",
    "pronounce": "laik",
    "vietnamese": "giống như; thích; như"
  },
  {
    "english": "likely",
    "type": "adj, adv",
    "pronounce": "́laikli",
    "vietnamese": "có thể đúng, có thể xảy ra, có khả năng; có thể, chắc vậy"
  },
  {
    "english": "limit",
    "type": "n, v",
    "pronounce": "limit",
    "vietnamese": "giới hạn, ranh giới; giới hạn, hạn chế"
  },
  {
    "english": "limited",
    "type": "adj",
    "pronounce": "ˈlɪmɪtɪd",
    "vietnamese": "hạn chế, có giới hạn"
  },
  {
    "english": "line",
    "type": "n",
    "pronounce": "lain",
    "vietnamese": "dây, đường, tuyến"
  },
  {
    "english": "link",
    "type": "n, v",
    "pronounce": "lɪɳk",
    "vietnamese": "mắt xích, mối liên lạc; liên kết, kết nối"
  },
  {
    "english": "lip",
    "type": "n",
    "pronounce": "lip",
    "vietnamese": "môi"
  },
  {
    "english": "liquid",
    "type": "n, adj",
    "pronounce": "likwid",
    "vietnamese": "chất lỏng; lỏng, êm ái, du dương, không vững"
  },
  {
    "english": "list",
    "type": "n, v",
    "pronounce": "list",
    "vietnamese": "danh sách; ghi vào danh sách"
  },
  {
    "english": "listen",
    "type": "to, v",
    "pronounce": "lisn",
    "vietnamese": "nghe, lắng nghe"
  },
  {
    "english": "literature",
    "type": "n",
    "pronounce": "ˈlɪtərətʃər",
    "vietnamese": "văn chương, văn học"
  },
  {
    "english": "litre",
    "type": "n",
    "pronounce": "́li:tə",
    "vietnamese": "lít"
  },
  {
    "english": "little",
    "type": "adj, det, pron, adv",
    "pronounce": "lit(ə)l",
    "vietnamese": "nhỏ, bé, chút ít; không nhiều; một chút"
  },
  {
    "english": "live",
    "type": "adj, adv",
    "pronounce": "liv",
    "vietnamese": "sống, hoạt động"
  },
  {
    "english": "lively",
    "type": "adj",
    "pronounce": "laivli",
    "vietnamese": "sống, sinh động"
  },
  {
    "english": "living",
    "type": "adj",
    "pronounce": "liviŋ",
    "vietnamese": "sống, đang sống"
  },
  {
    "english": "load",
    "type": "n, v",
    "pronounce": "loud",
    "vietnamese": "gánh nặng, vật nặng; chất, chở"
  },
  {
    "english": "loan",
    "type": "n",
    "pronounce": "ləʊn",
    "vietnamese": "sự vay mượn"
  },
  {
    "english": "local",
    "type": "adj",
    "pronounce": "ləʊk(ə)l",
    "vietnamese": "địa phương, bộ phận, cục bộ"
  },
  {
    "english": "locally",
    "type": "adv",
    "pronounce": "ˈloʊkəli",
    "vietnamese": "có tính chat địa phương, cục bộ"
  },
  {
    "english": "locate",
    "type": "v",
    "pronounce": "loʊˈkeɪt",
    "vietnamese": "xác định vị trí, định vị"
  },
  {
    "english": "located",
    "type": "adj",
    "pronounce": "loʊˈkeɪtid",
    "vietnamese": "định vị"
  },
  {
    "english": "location",
    "type": "n",
    "pronounce": "louk ́eiʃən",
    "vietnamese": "vị trí, sự định vị"
  },
  {
    "english": "lock",
    "type": "v, n",
    "pronounce": "lɔk",
    "vietnamese": "khóa; khóa"
  },
  {
    "english": "logic",
    "type": "n",
    "pronounce": "lɔdʤik",
    "vietnamese": "lô gic"
  },
  {
    "english": "logical",
    "type": "adj",
    "pronounce": "lɔdʤikəl",
    "vietnamese": "hợp lý, hợp logic"
  },
  {
    "english": "lonely",
    "type": "adj",
    "pronounce": "́lounli",
    "vietnamese": "cô đơn, bơ vơ"
  },
  {
    "english": "long",
    "type": "adj, adv",
    "pronounce": "lɔɳ",
    "vietnamese": "dài, xa; lâu"
  },
  {
    "english": "look",
    "type": "v, n",
    "pronounce": "luk",
    "vietnamese": "nhìn; cái nhìn"
  },
  {
    "english": "look after",
    "type": "",
    "pronounce": "",
    "vietnamese": "trông nom, chăm sóc. look at: nhìn, ngắm, xem. look for tìm kiếm. look forward to: mong đợi cách hân hoan"
  },
  {
    "english": "loose",
    "type": "adj",
    "pronounce": "lu:s",
    "vietnamese": "lỏng, không chặt"
  },
  {
    "english": "loosely",
    "type": "adv",
    "pronounce": "́lu:sli",
    "vietnamese": "lỏng lẻo"
  },
  {
    "english": "lord",
    "type": "n",
    "pronounce": "lɔrd",
    "vietnamese": "Chúa, vua"
  },
  {
    "english": "lorry",
    "type": "n",
    "pronounce": "́lɔ:ri",
    "vietnamese": "xe tải"
  },
  {
    "english": "lose",
    "type": "v",
    "pronounce": "lu:z",
    "vietnamese": "mất, thua, lạc"
  },
  {
    "english": "loss",
    "type": "n",
    "pronounce": "lɔs , lɒs",
    "vietnamese": "sự mất, sự thua"
  },
  {
    "english": "lost",
    "type": "adj",
    "pronounce": "lost",
    "vietnamese": "thua, mất"
  },
  {
    "english": "lot, a lot",
    "type": "pron, det, , adv",
    "pronounce": "lɒt",
    "vietnamese": "số lượng lớn; rất nhiều"
  },
  {
    "english": "loud",
    "type": "adj, adv",
    "pronounce": "laud",
    "vietnamese": "to, inh ỏi, ầm ĩ; to, lớn (nói)"
  },
  {
    "english": "loudly",
    "type": "adv",
    "pronounce": "laudili",
    "vietnamese": "ầm ĩ, inh ỏi"
  },
  {
    "english": "love",
    "type": "n, v",
    "pronounce": "lʌv",
    "vietnamese": "tình yêu, lòng yêu thương; yêu, thích"
  },
  {
    "english": "lovely",
    "type": "adj",
    "pronounce": "ˈlʌvli",
    "vietnamese": "đẹp, xinh xắn, có duyên"
  },
  {
    "english": "lover",
    "type": "n",
    "pronounce": "́lʌvə",
    "vietnamese": "người yêu, người tình"
  },
  {
    "english": "low",
    "type": "adj, adv",
    "pronounce": "lou",
    "vietnamese": "thấp, bé, lùn"
  },
  {
    "english": "loyal",
    "type": "adj",
    "pronounce": "lɔiəl",
    "vietnamese": "trung thành, trung kiên"
  },
  {
    "english": "luck",
    "type": "n",
    "pronounce": "lʌk",
    "vietnamese": "may mắn, vận may"
  },
  {
    "english": "lucky",
    "type": "adj",
    "pronounce": "lʌki",
    "vietnamese": "gặp may, gặp may mắn, hạnh phúc"
  },
  {
    "english": "luggage",
    "type": "n",
    "pronounce": "lʌgiʤ",
    "vietnamese": "hành lý"
  },
  {
    "english": "lump",
    "type": "n",
    "pronounce": "lΛmp",
    "vietnamese": "cục, tảng, miếng; cái bướu"
  },
  {
    "english": "lunch",
    "type": "n",
    "pronounce": "lʌntʃ",
    "vietnamese": "bữa ăn trưa"
  },
  {
    "english": "lung",
    "type": "n",
    "pronounce": "lʌη",
    "vietnamese": "phổi"
  },
  {
    "english": "machine",
    "type": "n",
    "pronounce": "mə'ʃi:n",
    "vietnamese": "máy, máy móc"
  },
  {
    "english": "machinery",
    "type": "n",
    "pronounce": "mə'ʃi:nəri",
    "vietnamese": "máy móc, thiết bị"
  },
  {
    "english": "mad",
    "type": "adj",
    "pronounce": "mæd",
    "vietnamese": "điên, mất trí; bực điên người"
  },
  {
    "english": "magazine",
    "type": "n",
    "pronounce": ",mægə'zi:n",
    "vietnamese": "tạp chí"
  },
  {
    "english": "magic",
    "type": "n, adj",
    "pronounce": "mæʤik",
    "vietnamese": "ma thuật, ảo thuật; (thuộc) ma thuật, ảo thuật"
  },
  {
    "english": "mail",
    "type": "n, v",
    "pronounce": "meil",
    "vietnamese": "thư tư, bưu kiện; gửi qua bưu điện"
  },
  {
    "english": "main",
    "type": "adj",
    "pronounce": "mein",
    "vietnamese": "chính, chủ yếu, trọng yếu nhất"
  },
  {
    "english": "mainly",
    "type": "adv",
    "pronounce": "́meinli",
    "vietnamese": "chính, chủ yếu, phần lớn"
  },
  {
    "english": "maintain",
    "type": "v",
    "pronounce": "mein ́tein",
    "vietnamese": "giữ gìn, duy trì, bảo vệ"
  },
  {
    "english": "major",
    "type": "adj",
    "pronounce": "ˈmeɪdʒər",
    "vietnamese": "lớn, nhiều hơn, trọng đại, chủ yếu"
  },
  {
    "english": "majority",
    "type": "n",
    "pronounce": "mə'dʒɔriti",
    "vietnamese": "phần lớn, đa số, ưu thế"
  },
  {
    "english": "make",
    "type": "v, n",
    "pronounce": "meik",
    "vietnamese": "làm, chế tạo; sự chế tạo. make sth up: làm thành, cấu thành, gộp thành"
  },
  {
    "english": "make friends with",
    "type": "",
    "pronounce": "",
    "vietnamese": "kết bạn với"
  },
  {
    "english": "make-up",
    "type": "n",
    "pronounce": "́meik ̧ʌp",
    "vietnamese": "đồ hóa trang, son phấn"
  },
  {
    "english": "male",
    "type": "adj, n",
    "pronounce": "meil",
    "vietnamese": "trai, trống, đực; con trai, đàn ông, con trống, đực"
  },
  {
    "english": "mall",
    "type": "n",
    "pronounce": "mɔ:l",
    "vietnamese": "búa"
  },
  {
    "english": "man",
    "type": "n",
    "pronounce": "mæn",
    "vietnamese": "con người; đàn ông"
  },
  {
    "english": "manage",
    "type": "v",
    "pronounce": "mæniʤ",
    "vietnamese": "quản lý, trông nom, điều khiển"
  },
  {
    "english": "management",
    "type": "n",
    "pronounce": "mænidʒmənt",
    "vietnamese": "sự quản lý, sự trông nom, sự điều khiển"
  },
  {
    "english": "manager",
    "type": "n",
    "pronounce": "ˈmænɪdʒər",
    "vietnamese": "người quản lý, giám đốc"
  },
  {
    "english": "manner",
    "type": "n",
    "pronounce": "mænз",
    "vietnamese": "cách, lối, kiểu; dáng, vẻ, thái độ"
  },
  {
    "english": "manufacture",
    "type": "v, n",
    "pronounce": ",mænju'fæktʃə",
    "vietnamese": "sản xuất, chế tạo"
  },
  {
    "english": "manufacturer",
    "type": "n",
    "pronounce": "̧mæni ́fæktʃərə",
    "vietnamese": "người chế tạo, người sản xuất"
  },
  {
    "english": "manufacturing",
    "type": "n",
    "pronounce": "̧mænju ́fæktʃəriη",
    "vietnamese": "sự sản xuất, sự chế tạo"
  },
  {
    "english": "many",
    "type": "det, pron",
    "pronounce": "meni",
    "vietnamese": "nhiều"
  },
  {
    "english": "map",
    "type": "n",
    "pronounce": "mæp",
    "vietnamese": "bản đồ"
  },
  {
    "english": "March (abbr Mar)",
    "type": "n",
    "pronounce": "mɑ:tʃ",
    "vietnamese": "tháng ba"
  },
  {
    "english": "mark",
    "type": "n, v",
    "pronounce": "mɑ:k",
    "vietnamese": "dấu, nhãn, nhãn mác; đánh dấu, ghi dấu"
  },
  {
    "english": "market",
    "type": "n",
    "pronounce": "mɑ:kit",
    "vietnamese": "chợ, thị trường"
  },
  {
    "english": "marketing",
    "type": "n",
    "pronounce": "mα:kitiη",
    "vietnamese": "ma-kết-tinh"
  },
  {
    "english": "marriage",
    "type": "n",
    "pronounce": "ˈmærɪdʒ",
    "vietnamese": "sự cưới xin, sự kết hôn, lễ cưới"
  },
  {
    "english": "married",
    "type": "adj",
    "pronounce": "́mærid",
    "vietnamese": "cưới, kết hôn"
  },
  {
    "english": "marry",
    "type": "v",
    "pronounce": "mæri",
    "vietnamese": "cưới (vợ), lấy (chồng)"
  },
  {
    "english": "mass",
    "type": "n, adj",
    "pronounce": "mæs",
    "vietnamese": "khối, khối lượng; quần chúng, đại chúng"
  },
  {
    "english": "massive",
    "type": "adj",
    "pronounce": "mæsiv",
    "vietnamese": "to lớn, đồ sộ"
  },
  {
    "english": "master",
    "type": "n",
    "pronounce": "mɑ:stə",
    "vietnamese": "chủ, chủ nhân, thầy giáo, thạc sĩ"
  },
  {
    "english": "match",
    "type": "n, v",
    "pronounce": "mætʃ",
    "vietnamese": "trận thi đấu, đối thủ, địch thủ; đối chọi, sánh được"
  },
  {
    "english": "matching",
    "type": "adj",
    "pronounce": "́mætʃiη",
    "vietnamese": "tính địch thù, thi đấu"
  },
  {
    "english": "mate",
    "type": "n, v",
    "pronounce": "meit",
    "vietnamese": "bạn, bạn nghề; giao phối"
  },
  {
    "english": "material",
    "type": "n, adj",
    "pronounce": "mə ́tiəriəl",
    "vietnamese": "nguyên vật liệu; vật chất, hữu hình"
  },
  {
    "english": "mathematics, also maths",
    "type": "n",
    "pronounce": ",mæθi'mætiks",
    "vietnamese": "toán học, môn toán"
  },
  {
    "english": "matter",
    "type": "n, v",
    "pronounce": "mætə",
    "vietnamese": "chất, vật chất; có ý nghĩa, có tính chất quan trọng"
  },
  {
    "english": "maximum",
    "type": "adj, n",
    "pronounce": "́mæksiməm",
    "vietnamese": "cực độ, tối đa; trị số cực đại, lượng cực đại, cực độ"
  },
  {
    "english": "may",
    "type": "v, modal",
    "pronounce": "mei",
    "vietnamese": "có thể, có lẽ"
  },
  {
    "english": "May",
    "type": "n",
    "pronounce": "mei",
    "vietnamese": "tháng 5"
  },
  {
    "english": "maybe",
    "type": "adv",
    "pronounce": "́mei ̧bi:",
    "vietnamese": "có thể, có lẽ"
  },
  {
    "english": "mayor",
    "type": "n",
    "pronounce": "mɛə",
    "vietnamese": "thị trưởng"
  },
  {
    "english": "me",
    "type": "n, pro",
    "pronounce": "mi:",
    "vietnamese": "tôi, tao, tớ"
  },
  {
    "english": "meal",
    "type": "n",
    "pronounce": "mi:l",
    "vietnamese": "bữa ăn"
  },
  {
    "english": "mean",
    "type": "v",
    "pronounce": "mi:n",
    "vietnamese": "nghĩa, có nghĩa là"
  },
  {
    "english": "meaning",
    "type": "n",
    "pronounce": "mi:niɳ",
    "vietnamese": "ý, ý nghĩa"
  },
  {
    "english": "means",
    "type": "n",
    "pronounce": "mi:nz",
    "vietnamese": "của cải, tài sản, phương tiện. by means: of bằng phương tiện"
  },
  {
    "english": "meanwhile",
    "type": "adv",
    "pronounce": "miː(n)waɪl",
    "vietnamese": "trong lúc đó, trong lúc ấy"
  },
  {
    "english": "measure",
    "type": "v, n",
    "pronounce": "meʤə",
    "vietnamese": "đo, đo lường; sự đo lường, đơn vị đo lường"
  },
  {
    "english": "measurement",
    "type": "n",
    "pronounce": "məʤəmənt",
    "vietnamese": "sự đo lường, phép đo"
  },
  {
    "english": "meat",
    "type": "n",
    "pronounce": "mi:t",
    "vietnamese": "thịt"
  },
  {
    "english": "media",
    "type": "n",
    "pronounce": "́mi:diə",
    "vietnamese": "phương tiện truyền thông đại chúng"
  },
  {
    "english": "medical",
    "type": "adj",
    "pronounce": "medikə",
    "vietnamese": "(thuộc) y học"
  },
  {
    "english": "medicine",
    "type": "n",
    "pronounce": "medisn",
    "vietnamese": "y học, y khoa; thuốc"
  },
  {
    "english": "medium",
    "type": "adj, n",
    "pronounce": "mi:djəm",
    "vietnamese": "trung bình, trung, vưa; sự trung gian, sự môi giới"
  },
  {
    "english": "meet",
    "type": "v",
    "pronounce": "mi:t",
    "vietnamese": "gặp, gặp gỡ"
  },
  {
    "english": "meeting",
    "type": "n",
    "pronounce": "mi:tiɳ",
    "vietnamese": "cuộc mít tinh, cuộc biểu tình"
  },
  {
    "english": "melt",
    "type": "v",
    "pronounce": "mɛlt",
    "vietnamese": "tan ra, chảy ra; làm tan chảy ra"
  },
  {
    "english": "member",
    "type": "n",
    "pronounce": "membə",
    "vietnamese": "thành viên, hội viên"
  },
  {
    "english": "membership",
    "type": "n",
    "pronounce": "membəʃip",
    "vietnamese": "tư cách hội viên, địa vị hội viên"
  },
  {
    "english": "memory",
    "type": "n",
    "pronounce": "meməri",
    "vietnamese": "bộ nhớ, trí nhớ, kỉ niệm. in memory of: sự tưởng nhớ"
  },
  {
    "english": "mental",
    "type": "adj",
    "pronounce": "mentl",
    "vietnamese": "(thuộc) trí tuệ, trí óc; mất trí"
  },
  {
    "english": "mentally",
    "type": "adv",
    "pronounce": "́mentəli",
    "vietnamese": "về mặt tinh thần"
  },
  {
    "english": "mention",
    "type": "v",
    "pronounce": "menʃn",
    "vietnamese": "kể ra, nói đến, đề cập"
  },
  {
    "english": "menu",
    "type": "n",
    "pronounce": "menju",
    "vietnamese": "thực đơn"
  },
  {
    "english": "mere",
    "type": "adj",
    "pronounce": "miə",
    "vietnamese": "chỉ là"
  },
  {
    "english": "merely",
    "type": "adv",
    "pronounce": "miəli",
    "vietnamese": "chỉ, đơn thuần"
  },
  {
    "english": "mess",
    "type": "n",
    "pronounce": "mes",
    "vietnamese": "tình trạng bưa bộn, tình trạng lộn xộn; người nhếch nhác, bẩn thỉu"
  },
  {
    "english": "message",
    "type": "n",
    "pronounce": "ˈmɛsɪdʒ",
    "vietnamese": "tin nhắn, thư tín, điện thông báo, thông điệp"
  },
  {
    "english": "metal",
    "type": "n",
    "pronounce": "metl",
    "vietnamese": "kim loại"
  },
  {
    "english": "method",
    "type": "n",
    "pronounce": "meθəd",
    "vietnamese": "phương pháp, cách thức"
  },
  {
    "english": "metre",
    "type": "n",
    "pronounce": "́mi:tə",
    "vietnamese": "mét"
  },
  {
    "english": "mid",
    "type": "-",
    "pronounce": "combiningform",
    "vietnamese": "tiền tố: một nửa"
  },
  {
    "english": "midday",
    "type": "n",
    "pronounce": "́mid ́dei",
    "vietnamese": "trưa, buổi trưa"
  },
  {
    "english": "middle",
    "type": "n, adj",
    "pronounce": "midl",
    "vietnamese": "giữa, ở giữa"
  },
  {
    "english": "midnight",
    "type": "n",
    "pronounce": "midnait",
    "vietnamese": "nửa đêm, 12h đêm"
  },
  {
    "english": "might",
    "type": "modal, v",
    "pronounce": "mait",
    "vietnamese": "qk. may có thể, có lẽ"
  },
  {
    "english": "mild",
    "type": "adj",
    "pronounce": "maɪld",
    "vietnamese": "nhẹ, êm dịu, ôn hòa"
  },
  {
    "english": "mile",
    "type": "n",
    "pronounce": "mail",
    "vietnamese": "dặm (đo lường)"
  },
  {
    "english": "military",
    "type": "adj",
    "pronounce": "militəri",
    "vietnamese": "(thuộc) quân đội, quân sự"
  },
  {
    "english": "milk",
    "type": "n",
    "pronounce": "milk",
    "vietnamese": "sữa"
  },
  {
    "english": "milligram, milligramme (abbr mg)",
    "type": "n",
    "pronounce": "́mili ̧græm",
    "vietnamese": "mi-li-gam"
  },
  {
    "english": "mind",
    "type": "n, v",
    "pronounce": "maid",
    "vietnamese": "tâm trí, tinh thần, trí tuệ; chú ý, để ý, chăm sóc, quan tâm"
  },
  {
    "english": "mine",
    "type": "pron, n",
    "pronounce": "",
    "vietnamese": "của tôi"
  },
  {
    "english": "mineral",
    "type": "n, adj",
    "pronounce": "ˈmɪnərəl , ˈmɪnrəl",
    "vietnamese": "công nhân, thợ mỏ; khoáng"
  },
  {
    "english": "minimum",
    "type": "adj, n",
    "pronounce": "miniməm",
    "vietnamese": "tối thiểu; số lượng tối thiểu, mức tối thiểu"
  },
  {
    "english": "minister",
    "type": "n",
    "pronounce": "́ministə",
    "vietnamese": "bộ trưởng"
  },
  {
    "english": "ministry",
    "type": "n",
    "pronounce": "́ministri",
    "vietnamese": "bộ"
  },
  {
    "english": "minor",
    "type": "adj",
    "pronounce": "́mainə",
    "vietnamese": "nhỏ hơn, thứ yếu, không quan trọng"
  },
  {
    "english": "minority",
    "type": "n",
    "pronounce": "mai ́nɔriti",
    "vietnamese": "phần ít, thiểu số"
  },
  {
    "english": "minute",
    "type": "n",
    "pronounce": "minit",
    "vietnamese": "phút"
  },
  {
    "english": "mirror",
    "type": "n",
    "pronounce": "ˈmɪrər",
    "vietnamese": "gương"
  },
  {
    "english": "miss",
    "type": "v, n",
    "pronounce": "mis",
    "vietnamese": "lỡ, trượt; sự trượt, sự thiếu vắng"
  },
  {
    "english": "missing",
    "type": "adj",
    "pronounce": "́misiη",
    "vietnamese": "vắng, thiếu, thất lạc"
  },
  {
    "english": "mistake",
    "type": "n, v",
    "pronounce": "mis'teik",
    "vietnamese": "lỗi, sai lầm, lỗi lầm; phạm lỗi, phạm sai lầm"
  },
  {
    "english": "mistaken",
    "type": "adj",
    "pronounce": "mis ́teiken",
    "vietnamese": "sai lầm, hiểu lầm"
  },
  {
    "english": "mix",
    "type": "v, n",
    "pronounce": "miks",
    "vietnamese": "pha, trộn lẫn; sự pha trộn"
  },
  {
    "english": "mixed",
    "type": "adj",
    "pronounce": "mikst",
    "vietnamese": "lẫn lộn, pha trộn"
  },
  {
    "english": "mixture",
    "type": "n",
    "pronounce": "ˈmɪkstʃər",
    "vietnamese": "sự pha trộn, sự hỗn hợp"
  },
  {
    "english": "mobile",
    "type": "adj",
    "pronounce": "məʊbail; 'məʊbi:l",
    "vietnamese": "chuyển động, di động"
  },
  {
    "english": "mobile phone (mobile)",
    "type": "n",
    "pronounce": "",
    "vietnamese": "điện thoại đi động"
  },
  {
    "english": "model",
    "type": "n",
    "pronounce": "ˈmɒdl",
    "vietnamese": "mẫu, kiểu mẫu"
  },
  {
    "english": "modern",
    "type": "adj",
    "pronounce": "mɔdən",
    "vietnamese": "hiện đại, tân tiến"
  },
  {
    "english": "moment",
    "type": "n",
    "pronounce": "məum(ə)nt",
    "vietnamese": "chốc, lát"
  },
  {
    "english": "Monday (abbr Mon)",
    "type": "n",
    "pronounce": "mʌndi",
    "vietnamese": "thứ 2"
  },
  {
    "english": "money",
    "type": "n",
    "pronounce": "mʌni",
    "vietnamese": "tiền"
  },
  {
    "english": "monitor",
    "type": "n, v",
    "pronounce": "mɔnitə",
    "vietnamese": "lớp trưởng, màn hình máy tính; nghe, ghi phát thanh, giám sát"
  },
  {
    "english": "month",
    "type": "n",
    "pronounce": "mʌnθ",
    "vietnamese": "tháng"
  },
  {
    "english": "mood",
    "type": "n",
    "pronounce": "mu:d",
    "vietnamese": "lối, thức, điệu, tâm trạng, tính khí"
  },
  {
    "english": "moon",
    "type": "n",
    "pronounce": "mu:n",
    "vietnamese": "mặt trăng"
  },
  {
    "english": "moral",
    "type": "adj",
    "pronounce": "ˈmɔrəl , ˈmɒrəl",
    "vietnamese": "(thuộc) đạo đức, luân lý, phẩm hạnh; có đạo đức"
  },
  {
    "english": "morally",
    "type": "adv",
    "pronounce": "",
    "vietnamese": "có đạo đức"
  },
  {
    "english": "more",
    "type": "det, pron, adv",
    "pronounce": "mɔ:",
    "vietnamese": "hơn, nhiều hơn"
  },
  {
    "english": "moreover",
    "type": "adv",
    "pronounce": "mɔ: ́rouvə",
    "vietnamese": "hơn nữa, ngoài ra, vả lại"
  },
  {
    "english": "morning",
    "type": "n",
    "pronounce": "mɔ:niɳ",
    "vietnamese": "buổi sáng"
  },
  {
    "english": "most",
    "type": "det, pro, n, adv",
    "pronounce": "moust",
    "vietnamese": "lớn nhất, nhiều nhất; nhất, hơn cả"
  },
  {
    "english": "mostly",
    "type": "adv",
    "pronounce": "́moustli",
    "vietnamese": "hầu hết, chủ yếu là"
  },
  {
    "english": "mother",
    "type": "n",
    "pronounce": "mΔðз",
    "vietnamese": "mẹ"
  },
  {
    "english": "motion",
    "type": "n",
    "pronounce": "́mouʃən",
    "vietnamese": "sự chuyển động, sụ di động"
  },
  {
    "english": "motor",
    "type": "n",
    "pronounce": "́moutə",
    "vietnamese": "động cơ mô tô"
  },
  {
    "english": "motorcycle",
    "type": "n",
    "pronounce": "moutə,saikl",
    "vietnamese": "xe mô tô"
  },
  {
    "english": "mount",
    "type": "v, n",
    "pronounce": "maunt",
    "vietnamese": "leo, trèo; núi"
  },
  {
    "english": "mountain",
    "type": "n",
    "pronounce": "ˈmaʊntən",
    "vietnamese": "núi"
  },
  {
    "english": "mouse",
    "type": "n",
    "pronounce": "maus - mauz",
    "vietnamese": "chuột"
  },
  {
    "english": "mouth",
    "type": "n",
    "pronounce": "mauθ - mauð",
    "vietnamese": "miệng"
  },
  {
    "english": "move",
    "type": "v, n",
    "pronounce": "mu:v",
    "vietnamese": "di chuyển, chuyển động; sự di chuyển, sự chuyển động"
  },
  {
    "english": "movement",
    "type": "n",
    "pronounce": "mu:vmənt",
    "vietnamese": "sự chuyển động, sự hoạt động; cử động, động tác"
  },
  {
    "english": "movie",
    "type": "n",
    "pronounce": "́mu:vi",
    "vietnamese": "phim xi nê"
  },
  {
    "english": "movie theater",
    "type": "n",
    "pronounce": "",
    "vietnamese": "rạp chiếu phim"
  },
  {
    "english": "moving",
    "type": "adj",
    "pronounce": "mu:viɳ",
    "vietnamese": "động, hoạt động"
  },
  {
    "english": "Mr",
    "type": "",
    "pronounce": "",
    "vietnamese": "Ông, ngài"
  },
  {
    "english": "Mrs",
    "type": "",
    "pronounce": "",
    "vietnamese": "Cô"
  },
  {
    "english": "Ms",
    "type": "",
    "pronounce": "",
    "vietnamese": "Bà, Cô"
  },
  {
    "english": "much",
    "type": "det, pron, adv",
    "pronounce": "mʌtʃ",
    "vietnamese": "nhiều, lắm"
  },
  {
    "english": "mud",
    "type": "n",
    "pronounce": "mʌd",
    "vietnamese": "bùn"
  },
  {
    "english": "multiply",
    "type": "v",
    "pronounce": "mʌltiplai",
    "vietnamese": "nhân lên, làm tăng lên nhiều lần; sinh sôi nảy nở"
  },
  {
    "english": "mum",
    "type": "n",
    "pronounce": "mʌm",
    "vietnamese": "mẹ"
  },
  {
    "english": "murder",
    "type": "n, v",
    "pronounce": "mə:də",
    "vietnamese": "tội giết người, tội ám sát; giết người, ám sát"
  },
  {
    "english": "muscle",
    "type": "n",
    "pronounce": "mʌsl",
    "vietnamese": "cơ, bắp thịt"
  },
  {
    "english": "museum",
    "type": "n",
    "pronounce": "mju: ́ziəm",
    "vietnamese": "bảo tàng"
  },
  {
    "english": "music",
    "type": "n",
    "pronounce": "mju:zik",
    "vietnamese": "nhạc, âm nhạc"
  },
  {
    "english": "musical",
    "type": "adj",
    "pronounce": "ˈmyuzɪkəl",
    "vietnamese": "(thuộc) nhạc, âm nhạc; du dương, êm ái"
  },
  {
    "english": "musician",
    "type": "n",
    "pronounce": "mju:'ziʃn",
    "vietnamese": "nhạc sĩ"
  },
  {
    "english": "must",
    "type": "v, modal",
    "pronounce": "mʌst",
    "vietnamese": "phải, cần, nên làm"
  },
  {
    "english": "my",
    "type": "det",
    "pronounce": "mai",
    "vietnamese": "của tôi"
  },
  {
    "english": "myself",
    "type": "pron",
    "pronounce": "mai'self",
    "vietnamese": "tự tôi, chính tôi"
  },
  {
    "english": "mysterious",
    "type": "adj",
    "pronounce": "mis'tiəriəs",
    "vietnamese": "thần bí, huyền bí, khó hiểu"
  },
  {
    "english": "mystery",
    "type": "n",
    "pronounce": "mistəri",
    "vietnamese": "điều huyền bí, điều thần bí"
  },
  {
    "english": "nail",
    "type": "n",
    "pronounce": "neil",
    "vietnamese": "móng (tay, chân) móng vuốt"
  },
  {
    "english": "naked",
    "type": "adj",
    "pronounce": "neikid",
    "vietnamese": "trần, khỏa thân, trơ trụi"
  },
  {
    "english": "name",
    "type": "n, v",
    "pronounce": "neim",
    "vietnamese": "tên; đặt tên, gọi tên"
  },
  {
    "english": "narrow",
    "type": "adj",
    "pronounce": "nærou",
    "vietnamese": "hẹp, chật hẹp"
  },
  {
    "english": "nation",
    "type": "n",
    "pronounce": "nei∫n",
    "vietnamese": "dân tộc, quốc gia"
  },
  {
    "english": "national",
    "type": "adj",
    "pronounce": "næʃən(ə)l",
    "vietnamese": "(thuộc) quốc gia, dân tộc"
  },
  {
    "english": "natural",
    "type": "adj",
    "pronounce": "nætʃrəl",
    "vietnamese": "(thuộc) tự nhiên, thiên nhiên"
  },
  {
    "english": "naturally",
    "type": "adv",
    "pronounce": "næt∫rəli",
    "vietnamese": "vốn, tự nhiên, đương nhiên"
  },
  {
    "english": "nature",
    "type": "n",
    "pronounce": "neitʃə",
    "vietnamese": "tự nhiên, thiên nhiên"
  },
  {
    "english": "navy",
    "type": "n",
    "pronounce": "neivi",
    "vietnamese": "hải quân"
  },
  {
    "english": "near",
    "type": "adj, adv, prep",
    "pronounce": "niə",
    "vietnamese": "gần, cận; ở gần"
  },
  {
    "english": "nearby",
    "type": "adj, adv",
    "pronounce": "́niə ̧bai",
    "vietnamese": "gần"
  },
  {
    "english": "nearly",
    "type": "adv",
    "pronounce": "́niəli",
    "vietnamese": "gần, sắp, suýt"
  },
  {
    "english": "neat",
    "type": "adj",
    "pronounce": "ni:t",
    "vietnamese": "sạch, ngăn nắp; rành mạch"
  },
  {
    "english": "neatly",
    "type": "adv",
    "pronounce": "ni:tli",
    "vietnamese": "gọn gàng, ngăn nắp"
  },
  {
    "english": "necessarily",
    "type": "adv",
    "pronounce": "́nesisərili",
    "vietnamese": "tất yếu, nhất thiết"
  },
  {
    "english": "necessary",
    "type": "adj",
    "pronounce": "nesəseri",
    "vietnamese": "cần, cần thiết, thiết yếu"
  },
  {
    "english": "neck",
    "type": "n",
    "pronounce": "nek",
    "vietnamese": "cổ"
  },
  {
    "english": "need",
    "type": "v, modal verb, n",
    "pronounce": "ni:d",
    "vietnamese": "cần, đòi hỏi; sự cần"
  },
  {
    "english": "needle",
    "type": "n",
    "pronounce": "́ni:dl",
    "vietnamese": "cái kim, mũi nhọn"
  },
  {
    "english": "negative",
    "type": "adj",
    "pronounce": "́negətiv",
    "vietnamese": "phủ định"
  },
  {
    "english": "neighbour",
    "type": "n",
    "pronounce": "neibə",
    "vietnamese": "hàng xóm"
  },
  {
    "english": "neighbourhood",
    "type": "n",
    "pronounce": "́neibəhud",
    "vietnamese": "hàng xóm, làng giềng"
  },
  {
    "english": "neither",
    "type": "det, pron, adv",
    "pronounce": "naiðə",
    "vietnamese": "không này mà cũng không kia"
  },
  {
    "english": "nephew",
    "type": "n",
    "pronounce": "́nevju:",
    "vietnamese": "cháu trai (con anh, chị, em)"
  },
  {
    "english": "nerve",
    "type": "n",
    "pronounce": "nɜrv",
    "vietnamese": "khí lực, thần kinh, can đảm"
  },
  {
    "english": "nervous",
    "type": "adj",
    "pronounce": "ˈnɜrvəs",
    "vietnamese": "hoảng sợ, dễ bị kích thích, hay lo lắng"
  },
  {
    "english": "nervously",
    "type": "adv",
    "pronounce": "nз:vзstli",
    "vietnamese": "bồn chồn, lo lắng"
  },
  {
    "english": "nest",
    "type": "n, v",
    "pronounce": "nest",
    "vietnamese": "tổ, ổ; làm tổ"
  },
  {
    "english": "net",
    "type": "n",
    "pronounce": "net",
    "vietnamese": "lưới, mạng"
  },
  {
    "english": "network",
    "type": "n",
    "pronounce": "netwə:k",
    "vietnamese": "mạng lưới, hệ thống"
  },
  {
    "english": "never",
    "type": "adv",
    "pronounce": "nevə",
    "vietnamese": "không bao giờ, không khi nào"
  },
  {
    "english": "nevertheless",
    "type": "adv",
    "pronounce": ",nevəðə'les",
    "vietnamese": "tuy nhiên, tuy thế mà"
  },
  {
    "english": "new",
    "type": "adj",
    "pronounce": "nju:",
    "vietnamese": "mới, mới mẻ, mới lạ"
  },
  {
    "english": "newly",
    "type": "adv",
    "pronounce": "́nju:li",
    "vietnamese": "mới"
  },
  {
    "english": "news",
    "type": "n",
    "pronounce": "nju:z",
    "vietnamese": "tin, tin tức"
  },
  {
    "english": "newspaper",
    "type": "n",
    "pronounce": "nju:zpeipə",
    "vietnamese": "báo"
  },
  {
    "english": "next",
    "type": "adj, adv, n",
    "pronounce": "nekst",
    "vietnamese": "sát, gần, bên cạnh; lần sau, tiếp nữa. next to: Gần"
  },
  {
    "english": "nice",
    "type": "adj",
    "pronounce": "nais",
    "vietnamese": "đẹp, thú vị, dễ chịu"
  },
  {
    "english": "nicely",
    "type": "adv",
    "pronounce": "́naisli",
    "vietnamese": "thú vị, dễ chịu"
  },
  {
    "english": "niece",
    "type": "n",
    "pronounce": "ni:s",
    "vietnamese": "cháu gái"
  },
  {
    "english": "night",
    "type": "n",
    "pronounce": "nait",
    "vietnamese": "đêm, tối"
  },
  {
    "english": "no",
    "type": "exclamation, det",
    "pronounce": "nou",
    "vietnamese": "không"
  },
  {
    "english": "nobody (noone)",
    "type": "pron",
    "pronounce": "noubədi",
    "vietnamese": "không ai, không người nào"
  },
  {
    "english": "noise",
    "type": "n",
    "pronounce": "nɔiz",
    "vietnamese": "tiếng ồn, sự huyên náo"
  },
  {
    "english": "noisily",
    "type": "adv",
    "pronounce": "́nɔizili",
    "vietnamese": "ồn ào, huyên náo"
  },
  {
    "english": "noisy",
    "type": "adj",
    "pronounce": "́nɔizi",
    "vietnamese": "ồn ào, huyên náo"
  },
  {
    "english": "non",
    "type": "-",
    "pronounce": "prefix",
    "vietnamese": "không"
  },
  {
    "english": "none",
    "type": "n, pro",
    "pronounce": "nʌn",
    "vietnamese": "không ai, không người, vật gì"
  },
  {
    "english": "nonsense",
    "type": "n",
    "pronounce": "́nɔnsəns",
    "vietnamese": "lời nói vô lý, vô nghĩa"
  },
  {
    "english": "nor",
    "type": "adv, conj",
    "pronounce": "no:",
    "vietnamese": "cũng không"
  },
  {
    "english": "normal",
    "type": "adj, n",
    "pronounce": "nɔ:məl",
    "vietnamese": "thường, bình thường; tình trạng bình thường"
  },
  {
    "english": "normally",
    "type": "adv",
    "pronounce": "no:mзli",
    "vietnamese": "thông thường, như thường lệ"
  },
  {
    "english": "north",
    "type": "n, adj, adv",
    "pronounce": "nɔ:θ",
    "vietnamese": "phía bắc, phương bắc"
  },
  {
    "english": "northern",
    "type": "adj",
    "pronounce": "nɔ:ðən",
    "vietnamese": "Bắc"
  },
  {
    "english": "nose",
    "type": "n",
    "pronounce": "nouz",
    "vietnamese": "mũi"
  },
  {
    "english": "not",
    "type": "adv",
    "pronounce": "nɔt",
    "vietnamese": "không"
  },
  {
    "english": "note",
    "type": "n, v",
    "pronounce": "nout",
    "vietnamese": "lời ghi, lời ghi chép; ghi chú, ghi chép"
  },
  {
    "english": "nothing",
    "type": "pron",
    "pronounce": "ˈnʌθɪŋ",
    "vietnamese": "không gì, không cái gì"
  },
  {
    "english": "notice",
    "type": "n, v",
    "pronounce": "nəƱtis",
    "vietnamese": "thông báo, yết thị; chú ý, để ý, nhận biết. take notice of chú ý"
  },
  {
    "english": "noticeable",
    "type": "adj",
    "pronounce": "ˈnoʊtɪsəbəl",
    "vietnamese": "đáng chú ý, đáng để ý"
  },
  {
    "english": "novel",
    "type": "n",
    "pronounce": "ˈnɒvəl",
    "vietnamese": "tiểu thuyết, truyện"
  },
  {
    "english": "November (abbr Nov)",
    "type": "n",
    "pronounce": "nou ́vembə",
    "vietnamese": "tháng 11"
  },
  {
    "english": "now",
    "type": "adv",
    "pronounce": "nau",
    "vietnamese": "bây giờ, hiện giờ, hiện nay"
  },
  {
    "english": "nowhere",
    "type": "adv",
    "pronounce": "́nou ̧wɛə",
    "vietnamese": "không nơi nào, không ở đâu"
  },
  {
    "english": "nuclear",
    "type": "adj",
    "pronounce": "nju:kliз",
    "vietnamese": "(thuộc) hạt nhân"
  },
  {
    "english": "number (abbr No)",
    "type": "no, n",
    "pronounce": "́nʌmbə",
    "vietnamese": "số"
  },
  {
    "english": "nurse",
    "type": "n",
    "pronounce": "nə:s",
    "vietnamese": "y tá"
  },
  {
    "english": "nut",
    "type": "n",
    "pronounce": "nʌt",
    "vietnamese": "quả hạch; đầu"
  },
  {
    "english": "o clock",
    "type": "adv",
    "pronounce": "klɔk",
    "vietnamese": "đúng giờ"
  },
  {
    "english": "obey",
    "type": "v",
    "pronounce": "o'bei",
    "vietnamese": "vâng lời, tuân theo, tuân lệnh"
  },
  {
    "english": "object",
    "type": "n, v",
    "pronounce": "(n) ˈɒbdʒɛkt ; (v) əbˈdʒɛkt",
    "vietnamese": "vật, vật thể, đối tượng; phản đối,chống lại"
  },
  {
    "english": "objective",
    "type": "n, adj",
    "pronounce": "əb ́dʒektiv",
    "vietnamese": "mục tiêu, mục đích; (thuộc) mục tiêu, khách quan"
  },
  {
    "english": "observation",
    "type": "n",
    "pronounce": "obzə:'vei∫(ə)n",
    "vietnamese": "sự quan sát, sự theo dõi"
  },
  {
    "english": "observe",
    "type": "v",
    "pronounce": "əbˈzə:v",
    "vietnamese": "quan sát, theo dõi"
  },
  {
    "english": "obtain",
    "type": "v",
    "pronounce": "əb'tein",
    "vietnamese": "đạt được, giành được"
  },
  {
    "english": "obvious",
    "type": "adj",
    "pronounce": "ɒbviəs",
    "vietnamese": "rõ ràng, rành mạch, hiển nhiên"
  },
  {
    "english": "obviously",
    "type": "adv",
    "pronounce": "ɔbviəsli",
    "vietnamese": "một cách rõ ràng, có thể thấy được"
  },
  {
    "english": "occasion",
    "type": "n",
    "pronounce": "əˈkeɪʒən",
    "vietnamese": "dịp, cơ hội"
  },
  {
    "english": "occasionally",
    "type": "adv",
    "pronounce": "з'keiЗnзli",
    "vietnamese": "thỉnh thoảng, đôi khi"
  },
  {
    "english": "occupied",
    "type": "adj",
    "pronounce": "ɔkjupaid",
    "vietnamese": "đang sử dụng, đầy (người)"
  },
  {
    "english": "occupy",
    "type": "v",
    "pronounce": "ɔkjupai",
    "vietnamese": "giữ, chiếm lĩnh, chiếm giữ"
  },
  {
    "english": "occur",
    "type": "v",
    "pronounce": "ə'kə:",
    "vietnamese": "xảy ra, xảy đến, xuất hiện"
  },
  {
    "english": "ocean",
    "type": "n",
    "pronounce": "əuʃ(ə)n",
    "vietnamese": "đại dương"
  },
  {
    "english": "October (abbr Oct)",
    "type": "n",
    "pronounce": "ɔk ́toubə",
    "vietnamese": "tháng 10"
  },
  {
    "english": "odd",
    "type": "adj",
    "pronounce": "ɔd",
    "vietnamese": "kỳ quặc, kỳ cục, lẻ (số)"
  },
  {
    "english": "oddly",
    "type": "adv",
    "pronounce": "́ɔdli",
    "vietnamese": "kỳ quặc, kỳ cục, lẻ (số)"
  },
  {
    "english": "of",
    "type": "prep",
    "pronounce": "ɔv",
    "vietnamese": "của"
  },
  {
    "english": "off",
    "type": "adv, prep",
    "pronounce": "ɔ:f",
    "vietnamese": "tắt; khỏi, cách, rời"
  },
  {
    "english": "offence",
    "type": "n",
    "pronounce": "ə'fens",
    "vietnamese": "sự vi phạm, sự phạm tội"
  },
  {
    "english": "offend",
    "type": "v",
    "pronounce": "ə ́fend",
    "vietnamese": "xúc phạm, làm bực mình, làm khó chịu"
  },
  {
    "english": "offensive",
    "type": "n, adj",
    "pronounce": "ə ́fensiv",
    "vietnamese": "sự tấn công, cuộc tấn công, sỉ nhục"
  },
  {
    "english": "offer",
    "type": "v, n",
    "pronounce": "́ɔfə",
    "vietnamese": "biếu, tặng, cho; sự trả giá"
  },
  {
    "english": "office",
    "type": "n",
    "pronounce": "ɔfis",
    "vietnamese": "cơ quấn, văn phòng, bộ"
  },
  {
    "english": "officer",
    "type": "n",
    "pronounce": "́ɔfisə",
    "vietnamese": "viên chức, cảnh sát, sĩ quấn"
  },
  {
    "english": "official",
    "type": "adj, n",
    "pronounce": "ə'fiʃəl",
    "vietnamese": "(thuộc) chính quyền, văn phòng; viên chức, công chức"
  },
  {
    "english": "officially",
    "type": "adv",
    "pronounce": "ə'fi∫əli",
    "vietnamese": "một cách trịnh trọng, một cách chính thức"
  },
  {
    "english": "often",
    "type": "adv",
    "pronounce": "ɔ:fn",
    "vietnamese": "thường, hay, luôn"
  },
  {
    "english": "oh",
    "type": "exclamation",
    "pronounce": "ou",
    "vietnamese": "chao, ôi chao, chà, này.."
  },
  {
    "english": "oil",
    "type": "n",
    "pronounce": "ɔɪl",
    "vietnamese": "dầu"
  },
  {
    "english": "OK (okay)",
    "type": "exclamation, adj, adv",
    "pronounce": "əʊkei",
    "vietnamese": "đồng ý, tán thành"
  },
  {
    "english": "old",
    "type": "adj",
    "pronounce": "ould",
    "vietnamese": "già"
  },
  {
    "english": "old-fashioned",
    "type": "adj",
    "pronounce": "",
    "vietnamese": "lỗi thời"
  },
  {
    "english": "on",
    "type": "adv",
    "pronounce": "on, prep",
    "vietnamese": "trên, ở trên; tiếp tục, tiếp diễn"
  },
  {
    "english": "once",
    "type": "adv, conj",
    "pronounce": "wʌns",
    "vietnamese": "một lần; khi mà, ngay khi, một khi"
  },
  {
    "english": "one number",
    "type": "det, pron",
    "pronounce": "wʌn",
    "vietnamese": "một; một người, một vật nào đó"
  },
  {
    "english": "onion",
    "type": "n",
    "pronounce": "ˈʌnjən",
    "vietnamese": "củ hành"
  },
  {
    "english": "only",
    "type": "adj, adv",
    "pronounce": "ounli",
    "vietnamese": "chỉ có 1, duy nhất; chỉ, mới"
  },
  {
    "english": "onto",
    "type": "prep",
    "pronounce": "́ɔntu",
    "vietnamese": "về phía trên, lên trên"
  },
  {
    "english": "open",
    "type": "adj, v",
    "pronounce": "oupən",
    "vietnamese": "mở, ngỏ; mở, bắt đầu, khai mạc"
  },
  {
    "english": "opening",
    "type": "n",
    "pronounce": "́oupniη",
    "vietnamese": "khe hở, lỗ; sự bắt đầu, sự khai mạc, lễ khánh thành"
  },
  {
    "english": "openly",
    "type": "adv",
    "pronounce": "́oupənli",
    "vietnamese": "công khai, thẳng thắn"
  },
  {
    "english": "operate",
    "type": "v",
    "pronounce": "ɔpəreit",
    "vietnamese": "hoạt động, điều khiển"
  },
  {
    "english": "operation",
    "type": "n",
    "pronounce": ",ɔpə'reiʃn",
    "vietnamese": "sự hoạt động, quá trình hoạt động"
  },
  {
    "english": "opinion",
    "type": "n",
    "pronounce": "ə'pinjən",
    "vietnamese": "ý kiến, quan điểm"
  },
  {
    "english": "opponent",
    "type": "n",
    "pronounce": "əpəʊ.nənt",
    "vietnamese": "địch thủ, đối thủ, kẻ thù"
  },
  {
    "english": "opportunity",
    "type": "n",
    "pronounce": "ɒpərˈtunɪti ,ˌɒpərˈtyunɪti",
    "vietnamese": "cơ hội, thời cơ"
  },
  {
    "english": "oppose",
    "type": "v",
    "pronounce": "əˈpoʊz",
    "vietnamese": "đối kháng, đối chọi, đối lập; chống đối, phản đối"
  },
  {
    "english": "opposed to",
    "type": "",
    "pronounce": "ə ́pouzd",
    "vietnamese": "chống lại, phản đối"
  },
  {
    "english": "opposing",
    "type": "adj",
    "pronounce": "з'pouziη",
    "vietnamese": "tính đối kháng, đối chọi"
  },
  {
    "english": "opposite",
    "type": "adj, adv, nprep",
    "pronounce": "ɔpəzit",
    "vietnamese": "đối nhau, ngược nhau; trước mặt, đối diện; điều trái ngược"
  },
  {
    "english": "opposition",
    "type": "n",
    "pronounce": "̧ɔpə ́ziʃən",
    "vietnamese": "sự đối lập, sự đối nhau; sự chống lại, sự phản đối; phe đối lập"
  },
  {
    "english": "option",
    "type": "n",
    "pronounce": "ɔpʃn",
    "vietnamese": "sự lựa chọn"
  },
  {
    "english": "orange",
    "type": "n, adj",
    "pronounce": "ɒrɪndʒ",
    "vietnamese": "quả cam; có màu da cam"
  },
  {
    "english": "order",
    "type": "n, v",
    "pronounce": "ɔ:də",
    "vietnamese": "thứ, bậc; ra lệnh. in order to hợp lệ"
  },
  {
    "english": "ordinary",
    "type": "adj",
    "pronounce": "o:dinəri",
    "vietnamese": "thường, thông thường"
  },
  {
    "english": "organ",
    "type": "n",
    "pronounce": "ɔ:gən",
    "vietnamese": "đàn óoc gan"
  },
  {
    "english": "organization",
    "type": "n",
    "pronounce": ",ɔ:gənai'zeiʃn",
    "vietnamese": "tổ chức, cơ quan; sự tổ chức"
  },
  {
    "english": "organize",
    "type": "v",
    "pronounce": "́ɔ:gə ̧naiz",
    "vietnamese": "tổ chức, thiết lập"
  },
  {
    "english": "organized",
    "type": "adj",
    "pronounce": "o:gзnaizd",
    "vietnamese": "có trật tự, ngăn nắp, được sắp xếp, được tổ chức"
  },
  {
    "english": "origin",
    "type": "n",
    "pronounce": "ɔridӡin",
    "vietnamese": "gốc, nguồn gốc, căn nguyên"
  },
  {
    "english": "original",
    "type": "adj, n",
    "pronounce": "ə'ridʒənl",
    "vietnamese": "(thuộc) gốc, nguồn gốc, căn nguyên; nguyên bản"
  },
  {
    "english": "originally",
    "type": "adv",
    "pronounce": "ə'ridʒnəli",
    "vietnamese": "một cách sáng tạo, mới mẻ, độc đáo; khởi đầu, đầu tiên"
  },
  {
    "english": "other",
    "type": "adj, pron",
    "pronounce": "ˈʌðər",
    "vietnamese": "khác"
  },
  {
    "english": "otherwise",
    "type": "adv",
    "pronounce": "́ʌðə ̧waiz",
    "vietnamese": "khác, cách khác; nếu không thì...; mặt khác"
  },
  {
    "english": "ought to",
    "type": "v, modal",
    "pronounce": "ɔ:t",
    "vietnamese": "phải, nên, hẳn là"
  },
  {
    "english": "our",
    "type": "det",
    "pronounce": "auə",
    "vietnamese": "của chúng ta, thuộc chúng ta, của chúng tôi, của chúng mình"
  },
  {
    "english": "ours",
    "type": "n",
    "pronounce": "auəz, pro",
    "vietnamese": "của chúng ta, thuộc chúng ta, của chúng tôi, của chúng mình"
  },
  {
    "english": "ourselves",
    "type": "pron",
    "pronounce": "́awə ́selvz",
    "vietnamese": "bản thân chúng ta, bản thân chúng tôi, bản thân chúng mình; tự chúng mình"
  },
  {
    "english": "out",
    "type": "of, adv, prep",
    "pronounce": "aut",
    "vietnamese": "ngoài, ở ngoài, ra ngoài"
  },
  {
    "english": "outdoor",
    "type": "adj",
    "pronounce": "autdɔ:",
    "vietnamese": "ngoài trời, ở ngoài"
  },
  {
    "english": "outdoors",
    "type": "adv",
    "pronounce": "̧aut ́dɔ:z",
    "vietnamese": "ở ngoài trời, ở ngoài nhà"
  },
  {
    "english": "outer",
    "type": "adj",
    "pronounce": "",
    "vietnamese": "ở phía ngoài, ở xa hơn"
  },
  {
    "english": "outline",
    "type": "v, n",
    "pronounce": "́aut ̧lain",
    "vietnamese": "vẽ, phác tảo; đường nét, hình dáng, nét ngoài"
  },
  {
    "english": "output",
    "type": "n",
    "pronounce": "autput",
    "vietnamese": "sự sản xuất; sản phẩm, sản lượng"
  },
  {
    "english": "outside",
    "type": "n, adj, prep, adv",
    "pronounce": "aut'said",
    "vietnamese": "bề ngoài, bên ngoài; ở ngoài; ngoài"
  },
  {
    "english": "outstanding",
    "type": "adj",
    "pronounce": "̧aut ́stændiη",
    "vietnamese": "nổi bật, đáng chú ý; còn tồn lại"
  },
  {
    "english": "oven",
    "type": "n",
    "pronounce": "ʌvn",
    "vietnamese": "lò (nướng)"
  },
  {
    "english": "over",
    "type": "adv, prep",
    "pronounce": "ouvə",
    "vietnamese": "bên trên, vượt qua; lên, lên trên"
  },
  {
    "english": "overall",
    "type": "adj, adv",
    "pronounce": "(adv) ˈoʊvərˈɔl",
    "vietnamese": "toàn bộ, toàn thể; tất cả, bao gồm"
  },
  {
    "english": "overcome",
    "type": "v",
    "pronounce": "oʊvərˈkʌm",
    "vietnamese": "thắng, chiến thắng, khắc phục, đánh bại (khó khăn)"
  },
  {
    "english": "owe",
    "type": "v",
    "pronounce": "ou",
    "vietnamese": "nợ, hàm ơn; có được (cái gì)"
  },
  {
    "english": "own",
    "type": "adj, pron, v",
    "pronounce": "oun",
    "vietnamese": "của chính mình, tự mình; nhận, nhìn nhận"
  },
  {
    "english": "owner",
    "type": "n",
    "pronounce": "́ounə",
    "vietnamese": "người chủ, chủ nhân"
  },
  {
    "english": "p.m. (PM)",
    "type": "",
    "pronounce": "pip'emз",
    "vietnamese": "quá trưa, chiều, tối"
  },
  {
    "english": "pace",
    "type": "n",
    "pronounce": "peis",
    "vietnamese": "bước chân, bước"
  },
  {
    "english": "pack",
    "type": "v, n",
    "pronounce": "pæk",
    "vietnamese": "gói, bọc; bó, gói"
  },
  {
    "english": "package",
    "type": "n, v",
    "pronounce": "pæk.ɪdʒ",
    "vietnamese": "gói đồ, bưu kiện; đóng gói, đóng kiện"
  },
  {
    "english": "packaging",
    "type": "n",
    "pronounce": "pækidzŋ",
    "vietnamese": "bao bì"
  },
  {
    "english": "packet",
    "type": "n",
    "pronounce": "pækit",
    "vietnamese": "gói nhỏ"
  },
  {
    "english": "page",
    "type": "n",
    "pronounce": "peidʒ",
    "vietnamese": "trang (sách)"
  },
  {
    "english": "pain",
    "type": "n",
    "pronounce": "pein",
    "vietnamese": "sự đau đớn, sự đau khổ"
  },
  {
    "english": "painful",
    "type": "adj",
    "pronounce": "peinful",
    "vietnamese": "đau đớn, đau khổ"
  },
  {
    "english": "paint",
    "type": "n, v",
    "pronounce": "peint",
    "vietnamese": "sơn, vôi màu; sơn, quét sơn"
  },
  {
    "english": "painter",
    "type": "n",
    "pronounce": "peintə",
    "vietnamese": "họa sĩ"
  },
  {
    "english": "painting",
    "type": "n",
    "pronounce": "peintiɳ",
    "vietnamese": "sự sơn; bức họa, bức tranh"
  },
  {
    "english": "pair",
    "type": "n",
    "pronounce": "pɛə",
    "vietnamese": "đôi, cặp"
  },
  {
    "english": "palace",
    "type": "n",
    "pronounce": "ˈpælɪs",
    "vietnamese": "cung điện, lâu đài"
  },
  {
    "english": "pale",
    "type": "adj",
    "pronounce": "peil",
    "vietnamese": "taí, nhợt"
  },
  {
    "english": "pan",
    "type": "n",
    "pronounce": "pæn - pɑ:n",
    "vietnamese": "xoong, chảo"
  },
  {
    "english": "panel",
    "type": "n",
    "pronounce": "pænl",
    "vietnamese": "ván ô (cửa, tường), pa nô"
  },
  {
    "english": "pants",
    "type": "n",
    "pronounce": "pænts",
    "vietnamese": "quần lót, quần đùi dài"
  },
  {
    "english": "paper",
    "type": "n",
    "pronounce": "́peipə",
    "vietnamese": "giấy"
  },
  {
    "english": "parallel",
    "type": "adj",
    "pronounce": "pærəlel",
    "vietnamese": "song song, tương đương"
  },
  {
    "english": "parent",
    "type": "n",
    "pronounce": "peərənt",
    "vietnamese": "cha, mẹ"
  },
  {
    "english": "park",
    "type": "n, v",
    "pronounce": "pa:k",
    "vietnamese": "công viên, vườn hoa; khoanh vùng thành công viên"
  },
  {
    "english": "parliament",
    "type": "n",
    "pronounce": "pɑ:ləmənt",
    "vietnamese": "nghi viện, quốc hội"
  },
  {
    "english": "part",
    "type": "n",
    "pronounce": "pa:t",
    "vietnamese": "phần, bộ phận"
  },
  {
    "english": "particular",
    "type": "adj",
    "pronounce": "pə ́tikjulə",
    "vietnamese": "riêng biệt, cá biệt"
  },
  {
    "english": "particularly",
    "type": "adv",
    "pronounce": "pə ́tikjuləli",
    "vietnamese": "một cách đặc biệt, cá biệt, riêng biệt"
  },
  {
    "english": "partly",
    "type": "adv",
    "pronounce": "́pa:tli",
    "vietnamese": "đến chừng mực nào đó, phần nào đó"
  },
  {
    "english": "partner",
    "type": "n",
    "pronounce": "pɑ:tnə",
    "vietnamese": "đối tác, cộng sự"
  },
  {
    "english": "partnership",
    "type": "n",
    "pronounce": "́pa:tnəʃip",
    "vietnamese": "sự chung phần, sự cộng tác"
  },
  {
    "english": "party",
    "type": "n",
    "pronounce": "ˈpɑrti",
    "vietnamese": "tiệc, buổi liên hoan; đảng"
  },
  {
    "english": "pass",
    "type": "v",
    "pronounce": "́pa:s",
    "vietnamese": "qua, vượt qua, ngang qua"
  },
  {
    "english": "passage",
    "type": "n",
    "pronounce": "ˈpæsɪdʒ",
    "vietnamese": "sự đi qua, sự trôi qua; hành lang"
  },
  {
    "english": "passenger",
    "type": "n",
    "pronounce": "pæsindʤə",
    "vietnamese": "hành khách"
  },
  {
    "english": "passing",
    "type": "n, adj",
    "pronounce": "́pa:siη",
    "vietnamese": "sự đi qua, sự trôi qua; thoáng qua ngắn ngủi"
  },
  {
    "english": "passport",
    "type": "n",
    "pronounce": "́pa:spɔ:t",
    "vietnamese": "hộ chiếu"
  },
  {
    "english": "past",
    "type": "adj, n, prep, adv",
    "pronounce": "pɑ:st",
    "vietnamese": "quá khứ, dĩ vãng; quá, qua"
  },
  {
    "english": "path",
    "type": "n",
    "pronounce": "pɑ:θ",
    "vietnamese": "đường mòn; hướng đi"
  },
  {
    "english": "patience",
    "type": "n",
    "pronounce": "́peiʃəns",
    "vietnamese": "tính kiên nhẫn, nhẫn nại, kiên trì, sự chịu đựng"
  },
  {
    "english": "patient",
    "type": "n, adj",
    "pronounce": "peiʃənt",
    "vietnamese": "bệnh nhân; kiên nhẫn, nhẫn nại, bền chí"
  },
  {
    "english": "pattern",
    "type": "n",
    "pronounce": "pætə(r)n",
    "vietnamese": "mẫu, khuôn mẫu"
  },
  {
    "english": "pause",
    "type": "v, n",
    "pronounce": "pɔ:z",
    "vietnamese": "tạm nghỉ, dưng; sự tạm nghỉ, sự tạm ngưng"
  },
  {
    "english": "pause",
    "type": "v, n",
    "pronounce": "pɔ:z",
    "vietnamese": "tạm nghỉ, dưng; sự tạm nghỉ, sự tạm ngưng"
  },
  {
    "english": "pay",
    "type": "v, n",
    "pronounce": "pei",
    "vietnamese": "trả, thanh toán, nộp; tiền lương"
  },
  {
    "english": "pay attention to",
    "type": "",
    "pronounce": "",
    "vietnamese": "chú ý tới"
  },
  {
    "english": "payment",
    "type": "n",
    "pronounce": "peim(ə)nt",
    "vietnamese": "sự trả tiền, số tiền trả, tiền bồi thường"
  },
  {
    "english": "peace",
    "type": "n",
    "pronounce": "pi:s",
    "vietnamese": "hòa bình, sự hòa thuận"
  },
  {
    "english": "peaceful",
    "type": "adj",
    "pronounce": "pi:sfl",
    "vietnamese": "hòa bình, thái bình, yên tĩnh"
  },
  {
    "english": "peak",
    "type": "n",
    "pronounce": "pi:k",
    "vietnamese": "lưỡi trai; đỉnh, chóp"
  },
  {
    "english": "pen",
    "type": "n",
    "pronounce": "pen",
    "vietnamese": "bút"
  },
  {
    "english": "pence",
    "type": "n",
    "pronounce": "pens",
    "vietnamese": "đồng xu"
  },
  {
    "english": "pencil",
    "type": "n",
    "pronounce": "́pensil",
    "vietnamese": "bút chì"
  },
  {
    "english": "penny",
    "type": "n",
    "pronounce": "́peni",
    "vietnamese": "đồng xu"
  },
  {
    "english": "pension",
    "type": "n",
    "pronounce": "penʃn",
    "vietnamese": "tiền trợ cấp, lương hưu"
  },
  {
    "english": "people",
    "type": "n",
    "pronounce": "ˈpipəl",
    "vietnamese": "dân tộc, dòng giống; người"
  },
  {
    "english": "pepper",
    "type": "n",
    "pronounce": "́pepə",
    "vietnamese": "hạt tiêu, cây ớt"
  },
  {
    "english": "per",
    "type": "prep",
    "pronounce": "pə:",
    "vietnamese": "cho mỗi"
  },
  {
    "english": "per cent (percent)",
    "type": "usn, adj,",
    "pronounce": "adv",
    "vietnamese": "phần trăm"
  },
  {
    "english": "perfect",
    "type": "adj",
    "pronounce": "pə'fekt",
    "vietnamese": "hoàn hảo"
  },
  {
    "english": "perfectly",
    "type": "adv",
    "pronounce": "́pə:fiktli",
    "vietnamese": "một cách hoàn hảo"
  },
  {
    "english": "perform",
    "type": "v",
    "pronounce": "pə ́fɔ:m",
    "vietnamese": "biểu diễn; làm, thực hiện"
  },
  {
    "english": "performance",
    "type": "n",
    "pronounce": "pə'fɔ:məns",
    "vietnamese": "sự làm, sự thực hiện, sự thi hành, sự biểu diễn"
  },
  {
    "english": "performer",
    "type": "n",
    "pronounce": "pə ́fɔ:mə",
    "vietnamese": "người biểu diễn, người trình diễn"
  },
  {
    "english": "perhaps",
    "type": "adv",
    "pronounce": "pə'hæps",
    "vietnamese": "có thể, có lẽ"
  },
  {
    "english": "period",
    "type": "n",
    "pronounce": "piəriəd",
    "vietnamese": "kỳ, thời kỳ, thời gian; thời đại"
  },
  {
    "english": "permanent",
    "type": "adj",
    "pronounce": "pə:mənənt",
    "vietnamese": "lâu dài, vĩnh cửu, thường xuyên"
  },
  {
    "english": "permanently",
    "type": "adv",
    "pronounce": "pə:mənəntli",
    "vietnamese": "cách thường xuyên, vĩnh cửu"
  },
  {
    "english": "permission",
    "type": "n",
    "pronounce": "pə'miʃn",
    "vietnamese": "sự cho phép, giấy phép"
  },
  {
    "english": "permit",
    "type": "v",
    "pronounce": "pə:mit",
    "vietnamese": "cho  phép, cho cơ hội"
  },
  {
    "english": "person",
    "type": "n",
    "pronounce": "ˈpɜrsən",
    "vietnamese": "con người, người"
  },
  {
    "english": "personal",
    "type": "adj",
    "pronounce": "pə:snl",
    "vietnamese": "cá nhân, tư, riêng tư"
  },
  {
    "english": "personality",
    "type": "n",
    "pronounce": "pə:sə'næləti",
    "vietnamese": "nhân cách, tính cách; nhân phẩm, cá tính"
  },
  {
    "english": "personally",
    "type": "adv",
    "pronounce": "́pə:sənəli",
    "vietnamese": "đích thân, bản thân, về phần tôi, đối với tôi"
  },
  {
    "english": "persuade",
    "type": "v",
    "pronounce": "pə'sweid",
    "vietnamese": "thuyết phục"
  },
  {
    "english": "pet",
    "type": "n",
    "pronounce": "pet",
    "vietnamese": "cơn nóng giận; vật cưng, người được yêu thích"
  },
  {
    "english": "petrol",
    "type": "n",
    "pronounce": "ˈpɛtrəl",
    "vietnamese": "xăng dầu"
  },
  {
    "english": "phase",
    "type": "n",
    "pronounce": "feiz",
    "vietnamese": "tuần trăng; giai đoạn, thời kỳ"
  },
  {
    "english": "philosophy",
    "type": "n",
    "pronounce": "fɪˈlɒsəfi",
    "vietnamese": "triết học, triết lý"
  },
  {
    "english": "photocopy",
    "type": "n, v",
    "pronounce": "́foutə ̧kɔpi",
    "vietnamese": "bản sao chụp; sao chụp"
  },
  {
    "english": "photograph (photo)",
    "type": "n, v",
    "pronounce": "́foutə ̧gra:f",
    "vietnamese": "ảnh, bức ảnh; chụp ảnh"
  },
  {
    "english": "photographer",
    "type": "n",
    "pronounce": "fə ́tɔgrəfə",
    "vietnamese": "thợ chụp ảnh, nhà nhiếp ảnh"
  },
  {
    "english": "photography",
    "type": "n",
    "pronounce": "fə ́tɔgrəfi",
    "vietnamese": "thuật chụp ảnh, nghề nhiếp ảnh"
  },
  {
    "english": "phrase",
    "type": "n",
    "pronounce": "freiz",
    "vietnamese": "câu; thành ngữ, cụm tư"
  },
  {
    "english": "physical",
    "type": "adj",
    "pronounce": "́fizikl",
    "vietnamese": "vật chất; (thuộc) cơ thể, thân thể"
  },
  {
    "english": "physically",
    "type": "adv",
    "pronounce": "́fizikli",
    "vietnamese": "về thân thể, theo luật tự nhiên"
  },
  {
    "english": "physics",
    "type": "n",
    "pronounce": "fiziks",
    "vietnamese": "vật lý học"
  },
  {
    "english": "piano",
    "type": "n",
    "pronounce": "pjænou",
    "vietnamese": "đàn pianô, dương cầm"
  },
  {
    "english": "pick",
    "type": "v",
    "pronounce": "pik",
    "vietnamese": "cuốc (đất); đào, khoét (lỗ). pick sth up cuốc, vỡ, xé"
  },
  {
    "english": "picture",
    "type": "n",
    "pronounce": "piktʃə",
    "vietnamese": "bức vẽ, bức họa"
  },
  {
    "english": "piece",
    "type": "n",
    "pronounce": "pi:s",
    "vietnamese": "mảnh, mẩu; đồng tiền"
  },
  {
    "english": "pig",
    "type": "n",
    "pronounce": "pig",
    "vietnamese": "con lợn"
  },
  {
    "english": "pile",
    "type": "n, v",
    "pronounce": "paɪl",
    "vietnamese": "cọc, chồng, đống, pin; đóng cọc, chất chồng"
  },
  {
    "english": "pill",
    "type": "n",
    "pronounce": "́pil",
    "vietnamese": "viên thuốc"
  },
  {
    "english": "pilot",
    "type": "n",
    "pronounce": "́paiələt",
    "vietnamese": "phi công"
  },
  {
    "english": "pin",
    "type": "n, v",
    "pronounce": "pin",
    "vietnamese": "đinh ghim; ghim., kẹp"
  },
  {
    "english": "pink",
    "type": "adj, n",
    "pronounce": "piηk",
    "vietnamese": "màu hồng; hoa cẩm chướng, tình trạng tốt, hoàn hảo"
  },
  {
    "english": "pint",
    "type": "n",
    "pronounce": "paint",
    "vietnamese": "Panh (đơn vị (đo lường) ở Anh bằng 0, 58 lít; ở Mỹ bằng 0, 473 lít); panh, lít (bia, sữa) a pint of beer + một panh bia"
  },
  {
    "english": "pipe",
    "type": "n",
    "pronounce": "paip",
    "vietnamese": "ống dẫn (khí, nước...)"
  },
  {
    "english": "pitch",
    "type": "n",
    "pronounce": "pit∫",
    "vietnamese": "sân (chơi các môn thể thao); đầu hắc ín"
  },
  {
    "english": "pity",
    "type": "n",
    "pronounce": "́piti",
    "vietnamese": "lòng thương hại, điều đáng tiếc, đáng thương"
  },
  {
    "english": "place",
    "type": "n, v",
    "pronounce": "pleis",
    "vietnamese": "nơi, địa điểm; quảng trường. take place: xảy ra, được cử hành, được tổ chức"
  },
  {
    "english": "plain",
    "type": "adj",
    "pronounce": "plein",
    "vietnamese": "ngay thẳng, đơn giản, chất phác"
  },
  {
    "english": "plan",
    "type": "n, v",
    "pronounce": "plæn",
    "vietnamese": "bản đồ, kế hoạch; vẽ bản đồ, lập kế hoạch, dự kiến"
  },
  {
    "english": "plane",
    "type": "n",
    "pronounce": "plein",
    "vietnamese": "mặt phẳng, mặt bằng, máy bay"
  },
  {
    "english": "planet",
    "type": "n",
    "pronounce": "́plænit",
    "vietnamese": "hành tinh"
  },
  {
    "english": "planning",
    "type": "n",
    "pronounce": "plænniη",
    "vietnamese": "sự lập kế hoạch, sự quy hoạch"
  },
  {
    "english": "plant",
    "type": "n, v",
    "pronounce": "plænt , plɑnt",
    "vietnamese": "thực vật, sự mọc lên; trồng, gieo"
  },
  {
    "english": "plastic",
    "type": "n, adj",
    "pronounce": "plæstik",
    "vietnamese": "chất dẻo, làm bằng chất dẻo"
  },
  {
    "english": "plate",
    "type": "n",
    "pronounce": "pleit",
    "vietnamese": "bản, tấm kim loại"
  },
  {
    "english": "platform",
    "type": "n",
    "pronounce": "plætfɔ:m",
    "vietnamese": "nền, bục, bệ; thềm, sân ga"
  },
  {
    "english": "play",
    "type": "v, n",
    "pronounce": "plei",
    "vietnamese": "chơi, đánh; sự vui chơi, trò chơi, trận đấu"
  },
  {
    "english": "player",
    "type": "n",
    "pronounce": "pleiз",
    "vietnamese": "người chơi 1 trò chơi nào đó (nhạc cụ)"
  },
  {
    "english": "pleasant",
    "type": "adj",
    "pronounce": "pleznt",
    "vietnamese": "vui vẻ, dễ thương; dịu dàng, thân mật"
  },
  {
    "english": "pleasantly",
    "type": "adv",
    "pronounce": "plezəntli",
    "vietnamese": "vui vẻ, dễ thương; thân mật"
  },
  {
    "english": "please",
    "type": "exclamation, v",
    "pronounce": "pli:z",
    "vietnamese": "làm vui lòng, vưa lòng, mong... vui lòng, xin mời"
  },
  {
    "english": "pleased",
    "type": "adj",
    "pronounce": "pli:zd",
    "vietnamese": "hài lòng"
  },
  {
    "english": "pleasing",
    "type": "adj",
    "pronounce": "́pli:siη",
    "vietnamese": "mang lại niềm vui thích; dễ chịu"
  },
  {
    "english": "pleasure",
    "type": "n",
    "pronounce": "ˈplɛʒuə(r)",
    "vietnamese": "niềm vui thích, điều thích thú, điều thú vị; ý muốn, ý thích"
  },
  {
    "english": "plenty",
    "type": "n, adv, n, det, pro",
    "pronounce": "plenti",
    "vietnamese": "nhiều (s.k.lượng); chỉ sự thưa; sự sung túc, sự p.phú"
  },
  {
    "english": "plot",
    "type": "n, v",
    "pronounce": "plɔt",
    "vietnamese": "mảnh đất nhỏ, sơ đồ, đồ thị, đồ án; vẽ sơ đồ, dựng đồ án"
  },
  {
    "english": "plug",
    "type": "n",
    "pronounce": "plʌg",
    "vietnamese": "nút (thùng, chậu, bồ(n)..), cái phíc cắm"
  },
  {
    "english": "plus",
    "type": "n, adj ,conj, prep",
    "pronounce": "plʌs",
    "vietnamese": "cộng với (số, người...); dấu cộng; cộng, thêm vào"
  },
  {
    "english": "pocket",
    "type": "n",
    "pronounce": "pɔkit",
    "vietnamese": "túi (quần áo, trong xe hơi), túi tiền"
  },
  {
    "english": "poem",
    "type": "n",
    "pronounce": "pouim",
    "vietnamese": "bài thơ"
  },
  {
    "english": "poetry",
    "type": "n",
    "pronounce": "pouitri",
    "vietnamese": "thi ca; chất thơ"
  },
  {
    "english": "point",
    "type": "n, v",
    "pronounce": "",
    "vietnamese": "mũi nhọn, điểm; vót, làm nhọn, chấm (câu..)"
  },
  {
    "english": "pointed",
    "type": "adj",
    "pronounce": "́pɔintid",
    "vietnamese": "nhọn, có đầu nhọn"
  },
  {
    "english": "poison",
    "type": "n, v",
    "pronounce": "ˈpɔɪzən",
    "vietnamese": "chất độc, thuốc độc; đánh thuốc độc, tẩm thuốc độc"
  },
  {
    "english": "poisonous",
    "type": "adj",
    "pronounce": "pɔɪ.zə(n)əs",
    "vietnamese": "độc, có chất độc, gây chết, bệnh"
  },
  {
    "english": "pole",
    "type": "n",
    "pronounce": "poul",
    "vietnamese": "người Ba Lan; cực (nam châm, trái đat...)"
  },
  {
    "english": "police",
    "type": "n",
    "pronounce": "pə'li:s",
    "vietnamese": "cảnh sát, công an"
  },
  {
    "english": "policy",
    "type": "n",
    "pronounce": "pol.ə si",
    "vietnamese": "chính sách"
  },
  {
    "english": "polish",
    "type": "n, v",
    "pronounce": "pouliʃ",
    "vietnamese": "nước bóng, nước láng; đánh bóng, làm cho láng"
  },
  {
    "english": "polite",
    "type": "adj",
    "pronounce": "pəˈlaɪt",
    "vietnamese": "lễ phép, lịch sự"
  },
  {
    "english": "politely",
    "type": "adv",
    "pronounce": "pəˈlaɪtli",
    "vietnamese": "lễ phép, lịch sự"
  },
  {
    "english": "political",
    "type": "adj",
    "pronounce": "pə'litikl",
    "vietnamese": "về chính trị, về chính phủ, có tính chính trị"
  },
  {
    "english": "politically",
    "type": "adv",
    "pronounce": "pə'litikəli",
    "vietnamese": "về mặt chính trị; khôn ngoan, thận trọng; sảo quyệt"
  },
  {
    "english": "politician",
    "type": "n",
    "pronounce": "̧pɔli ́tiʃən",
    "vietnamese": "nhà chính trị, chính khách"
  },
  {
    "english": "politics",
    "type": "n",
    "pronounce": "pɔlitiks",
    "vietnamese": "họat động chính trị, đời sống chính trị, quan điểm chính trị"
  },
  {
    "english": "pollution",
    "type": "n",
    "pronounce": "pəˈluʃən",
    "vietnamese": "sự ô nhiễm"
  },
  {
    "english": "pool",
    "type": "n",
    "pronounce": "pu:l",
    "vietnamese": "vũng nước; bể bơi, hồ bơi"
  },
  {
    "english": "poor",
    "type": "adj",
    "pronounce": "puə",
    "vietnamese": "nghèo"
  },
  {
    "english": "pop",
    "type": "n, v",
    "pronounce": "pɒp; NAmE pɑːp",
    "vietnamese": "tiếp bốp, phong cách dân gian hiện đại; nổ bốp"
  },
  {
    "english": "",
    "type": "",
    "pronounce": "",
    "vietnamese": ""
  },
  {
    "english": "popular",
    "type": "adj",
    "pronounce": "́pɔpjulə",
    "vietnamese": "có tính đại chúng, (thuộc) nhân dân; được nhiều người ưa chuộng"
  },
  {
    "english": "population",
    "type": "n",
    "pronounce": ",pɔpju'leiʃn",
    "vietnamese": "dân cư, dân số; mật độ dân số"
  },
  {
    "english": "port",
    "type": "n",
    "pronounce": "pɔ:t",
    "vietnamese": "cảng"
  },
  {
    "english": "pose",
    "type": "v, n",
    "pronounce": "pouz",
    "vietnamese": "đưa ra, đề ra, đặt; sự đặt, đề ra"
  },
  {
    "english": "position",
    "type": "n",
    "pronounce": "pəˈzɪʃən",
    "vietnamese": "vị trí, chỗ"
  },
  {
    "english": "positive",
    "type": "adj",
    "pronounce": "pɔzətiv",
    "vietnamese": "khẳng định, xác thực, rõ ràng, tích cực, lạc quan"
  },
  {
    "english": "possess",
    "type": "v",
    "pronounce": "pə'zes",
    "vietnamese": "có, chiếm hữu"
  },
  {
    "english": "possession",
    "type": "n",
    "pronounce": "pə'zeʃn",
    "vietnamese": "quyền sở hữu, vật sở hữu"
  },
  {
    "english": "possibility",
    "type": "n",
    "pronounce": "̧pɔsi ́biliti",
    "vietnamese": "khả năng, triển vọng"
  },
  {
    "english": "possible",
    "type": "adj",
    "pronounce": "pɔsibəl",
    "vietnamese": "có thể, có thể thực hiện"
  },
  {
    "english": "possibly",
    "type": "adv",
    "pronounce": "́pɔsibli",
    "vietnamese": "có lẽ, có thể, có thể chấp nhận được"
  },
  {
    "english": "post",
    "type": "n, v",
    "pronounce": "poʊst",
    "vietnamese": "thư, bưu kiện; gửi thư"
  },
  {
    "english": "post office",
    "type": "n",
    "pronounce": "ɔfis",
    "vietnamese": "bưu điện"
  },
  {
    "english": "pot",
    "type": "n",
    "pronounce": "pɒt",
    "vietnamese": "can, bình, lọ..."
  },
  {
    "english": "potato",
    "type": "n",
    "pronounce": "pə'teitou",
    "vietnamese": "khoai tây"
  },
  {
    "english": "potential",
    "type": "adj, n",
    "pronounce": "pəˈtɛnʃəl",
    "vietnamese": "tiềm năng; khả năng, tiềm lực"
  },
  {
    "english": "potentially",
    "type": "adv",
    "pronounce": "pəˈtɛnʃəlli",
    "vietnamese": "tiềm năng, tiềm ẩn"
  },
  {
    "english": "pound",
    "type": "n",
    "pronounce": "paund",
    "vietnamese": "pao - đơn vị đo lường"
  },
  {
    "english": "pour",
    "type": "v",
    "pronounce": "pɔ:",
    "vietnamese": "rót, đổ, giội"
  },
  {
    "english": "powder",
    "type": "n",
    "pronounce": "paudə",
    "vietnamese": "bột, bụi"
  },
  {
    "english": "power",
    "type": "n",
    "pronounce": "ˈpauə(r)",
    "vietnamese": "khả năng, tài năng, năng lực; sức mạnh, nội lực; quyền lực"
  },
  {
    "english": "powerful",
    "type": "adj",
    "pronounce": "́pauəful",
    "vietnamese": "hùng mạnh, hùng cường"
  },
  {
    "english": "practical",
    "type": "adj",
    "pronounce": "ˈpræktɪkəl",
    "vietnamese": "thực hành; thực tế"
  },
  {
    "english": "practically",
    "type": "adv",
    "pronounce": "́præktikəli",
    "vietnamese": "về mặt thực hành; thực tế"
  },
  {
    "english": "practice",
    "type": "n",
    "pronounce": "́præktis",
    "vietnamese": "thực hành, thực tiễn"
  },
  {
    "english": "practise",
    "type": "v",
    "pronounce": "́præktis",
    "vietnamese": "thực hành, tập luyện"
  },
  {
    "english": "praise",
    "type": "n, v",
    "pronounce": "preiz",
    "vietnamese": "sự ca ngợi, sự tán dương, lòng tôn kính, tôn thờ; khen ngợi, tán dương"
  },
  {
    "english": "prayer",
    "type": "n",
    "pronounce": "prɛər",
    "vietnamese": "sự cầu nguyện"
  },
  {
    "english": "precise",
    "type": "adj",
    "pronounce": "pri ́sais",
    "vietnamese": "rõ ràng, chính xác; tỉ mỉ, kỹ tính"
  },
  {
    "english": "precisely",
    "type": "adv",
    "pronounce": "pri ́saisli",
    "vietnamese": "đúng, chính xác, cần thận"
  },
  {
    "english": "predict",
    "type": "v",
    "pronounce": "pri'dikt",
    "vietnamese": "báo trước, tiên đoán, dự báo"
  },
  {
    "english": "prefer",
    "type": "v",
    "pronounce": "pri'fə:",
    "vietnamese": "thích hơn"
  },
  {
    "english": "preference",
    "type": "n",
    "pronounce": "prefərəns",
    "vietnamese": "sự thích hơn, sự ưa hơn; cái được ưa thích hơn"
  },
  {
    "english": "pregnant",
    "type": "adj",
    "pronounce": "pregnənt",
    "vietnamese": "mang thai; giàu trí tưởng tượng, sáng tạo"
  },
  {
    "english": "premises",
    "type": "n",
    "pronounce": "premis",
    "vietnamese": "biệt thự"
  },
  {
    "english": "preparation",
    "type": "n",
    "pronounce": "̧prepə ́reiʃən",
    "vietnamese": "sự sửa soạn, sự chuẩn bị"
  },
  {
    "english": "prepare",
    "type": "v",
    "pronounce": "pri ́peə",
    "vietnamese": "sửa soạn, chuẩn bị"
  },
  {
    "english": "prepared",
    "type": "adj",
    "pronounce": "pri'peəd",
    "vietnamese": "đã được chuẩn bị"
  },
  {
    "english": "presence",
    "type": "n",
    "pronounce": "prezns",
    "vietnamese": "sự hiện diện, sự có mặt; người, vât hiện diện"
  },
  {
    "english": "present",
    "type": "adj, n, v",
    "pronounce": "(v)pri'zent",
    "vietnamese": "có mặt, hiện diện; hiện nay, hiện thời; bày tỏ, giới thiệu, trình bày"
  },
  {
    "english": "presentation",
    "type": "n",
    "pronounce": ",prezen'teiʃn",
    "vietnamese": "bài thuyết trình, sự trình diện, sự giới thiệu"
  },
  {
    "english": "preserve",
    "type": "v",
    "pronounce": "pri'zə:v",
    "vietnamese": "bảo quản, giữ gìn"
  },
  {
    "english": "president",
    "type": "n",
    "pronounce": "́prezidənt",
    "vietnamese": "hiệu trưởng, chủ tịnh, tổng thống"
  },
  {
    "english": "press",
    "type": "n, v",
    "pronounce": "pres",
    "vietnamese": "sự ép, sự nén, sự ấn; ép, nén, bóp, ấn"
  },
  {
    "english": "pressure",
    "type": "n",
    "pronounce": "preʃə",
    "vietnamese": "sức ép, áp lực, áp suất"
  },
  {
    "english": "presumably",
    "type": "adv",
    "pronounce": "pri'zju:məbli",
    "vietnamese": "có thể được, có lẽ"
  },
  {
    "english": "pretend",
    "type": "v",
    "pronounce": "pri'tend",
    "vietnamese": "giả vờ, giả bộ, làm ra vẻ"
  },
  {
    "english": "pretty",
    "type": "adv, adj",
    "pronounce": "priti",
    "vietnamese": "khá, vưa phải; xinh, xinh xắn;, đẹp"
  },
  {
    "english": "prevent",
    "type": "v",
    "pronounce": "pri'vent",
    "vietnamese": "ngăn cản, ngăn chặn, ngăn ngưa"
  },
  {
    "english": "previous",
    "type": "adj",
    "pronounce": "ˈpriviəs",
    "vietnamese": "vội vàng, hấp tấp; trước (vd. ngày hôm trước), ưu tiên"
  },
  {
    "english": "previously",
    "type": "adv",
    "pronounce": "́pri:viəsli",
    "vietnamese": "trước, trước đây"
  },
  {
    "english": "price",
    "type": "n",
    "pronounce": "prais",
    "vietnamese": "giá"
  },
  {
    "english": "pride",
    "type": "n",
    "pronounce": "praid",
    "vietnamese": "sự kiêu hãnh, sự hãnh diện; tính kiêu căng, tự phụ"
  },
  {
    "english": "priest",
    "type": "n",
    "pronounce": "pri:st",
    "vietnamese": "linh mục, thầy tu"
  },
  {
    "english": "primarily",
    "type": "adv",
    "pronounce": "́praimərili",
    "vietnamese": "trước hết, đầu tiên"
  },
  {
    "english": "primary",
    "type": "adj",
    "pronounce": "praiməri",
    "vietnamese": "nguyên thủy, đầu tiên; thời cổ đại, nguyên sinh; sơ cấp, tiểu học"
  },
  {
    "english": "prime minister",
    "type": "n",
    "pronounce": "́ministə",
    "vietnamese": "thủ tướng"
  },
  {
    "english": "prince",
    "type": "n",
    "pronounce": "prins",
    "vietnamese": "hoàn tử"
  },
  {
    "english": "princess",
    "type": "n",
    "pronounce": "prin'ses",
    "vietnamese": "công chúa"
  },
  {
    "english": "principle",
    "type": "n",
    "pronounce": "ˈprɪnsəpəl",
    "vietnamese": "cơ bản, chủ yếu; nguyên lý, nguyên tắc"
  },
  {
    "english": "print",
    "type": "v, n",
    "pronounce": "print",
    "vietnamese": "in, xuất bản; sự in ra"
  },
  {
    "english": "printer",
    "type": "n",
    "pronounce": "́printə",
    "vietnamese": "máy in, thợ in"
  },
  {
    "english": "printing",
    "type": "n",
    "pronounce": "́printiη",
    "vietnamese": "sự in, thuật in, kỹ sảo in"
  },
  {
    "english": "prior",
    "type": "adj",
    "pronounce": "praɪə(r)",
    "vietnamese": "trước, ưu tiên"
  },
  {
    "english": "priority",
    "type": "n",
    "pronounce": "prai ́ɔriti",
    "vietnamese": "sự ưu tế, quyền ưu tiên"
  },
  {
    "english": "prison",
    "type": "n",
    "pronounce": "ˈprɪzən",
    "vietnamese": "nhà tù"
  },
  {
    "english": "prisoner",
    "type": "n",
    "pronounce": "ˈprɪzənə(r)",
    "vietnamese": "tù nhân"
  },
  {
    "english": "private",
    "type": "adj",
    "pronounce": "ˈpraɪvɪt",
    "vietnamese": "cá nhân, riêng"
  },
  {
    "english": "privately",
    "type": "adv",
    "pronounce": "ˈpraɪvɪtli",
    "vietnamese": "riêng tư, cá nhân"
  },
  {
    "english": "prize",
    "type": "n",
    "pronounce": "praiz",
    "vietnamese": "giải, giải thưởng"
  },
  {
    "english": "probable",
    "type": "adj",
    "pronounce": "́prɔbəbl",
    "vietnamese": "có thể, có khả năng"
  },
  {
    "english": "probably",
    "type": "adv",
    "pronounce": "́prɔbəbli",
    "vietnamese": "hầu như chắc chắn"
  },
  {
    "english": "problem",
    "type": "n",
    "pronounce": "prɔbləm",
    "vietnamese": "vấn đề, điều khó giải quyết"
  },
  {
    "english": "procedure",
    "type": "n",
    "pronounce": "prə ́si:dʒə",
    "vietnamese": "thủ tục"
  },
  {
    "english": "proceed",
    "type": "v",
    "pronounce": "proceed",
    "vietnamese": "tiến lên, theo duổi, tiếp diễn"
  },
  {
    "english": "process",
    "type": "n, v",
    "pronounce": "prouses",
    "vietnamese": "quá trình, sự tiến triển, quy trình; chế biến, gia công, xử lý"
  },
  {
    "english": "produce",
    "type": "v",
    "pronounce": "prɔdju:s",
    "vietnamese": "sản xuất, chế tạo"
  },
  {
    "english": "producer",
    "type": "n",
    "pronounce": "prə ́dju:sə",
    "vietnamese": "nhà sản xuất"
  },
  {
    "english": "product",
    "type": "n",
    "pronounce": "́prɔdʌkt",
    "vietnamese": "sản phẩm"
  },
  {
    "english": "production",
    "type": "n",
    "pronounce": "prə ́dʌkʃən",
    "vietnamese": "sự sản xuất, chế tạo"
  },
  {
    "english": "profession",
    "type": "n",
    "pronounce": "prə ́feʃ(ə)n",
    "vietnamese": "nghề, nghề nghiệp"
  },
  {
    "english": "professional",
    "type": "adj, n",
    "pronounce": "prə'feʃənl",
    "vietnamese": "(thuộc) nghề, nghề nghiệp; chuyên nghiệp"
  },
  {
    "english": "professor",
    "type": "n",
    "pronounce": "prəˈfɛsər",
    "vietnamese": "giáo sư, giảng viên"
  },
  {
    "english": "profit",
    "type": "n",
    "pronounce": "ˈprɒfɪt",
    "vietnamese": "thuận lợi, lợi ích, lợi nhuận"
  },
  {
    "english": "program",
    "type": "n, v",
    "pronounce": "́prougræm",
    "vietnamese": "chương trình; lên chương trình"
  },
  {
    "english": "programme",
    "type": "n",
    "pronounce": "́prougræm",
    "vietnamese": "chương trình"
  },
  {
    "english": "progress",
    "type": "n, v",
    "pronounce": "prougres",
    "vietnamese": "sự tiến tới, sự tiến triển; tiến bộ, tiến triển, phát triển"
  },
  {
    "english": "project",
    "type": "n, v",
    "pronounce": "(n) ˈprɒdʒɛkt",
    "vietnamese": "đề án, dự án, kế hoạch; dự kiến, kế hoạch"
  },
  {
    "english": "promise",
    "type": "v, n",
    "pronounce": "",
    "vietnamese": "hứa, lời hứa"
  },
  {
    "english": "promote",
    "type": "v",
    "pronounce": "prəˈmoʊt",
    "vietnamese": "thăng chức, thăng cấp"
  },
  {
    "english": "promotion",
    "type": "n",
    "pronounce": "prə'mou∫n",
    "vietnamese": "sự thăng chức, sự thăng cấp"
  },
  {
    "english": "prompt",
    "type": "adj, v",
    "pronounce": "prɒmpt",
    "vietnamese": "mau lẹ, nhanh chóng; xúi, giục, nhắc nhở"
  },
  {
    "english": "promptly",
    "type": "adv",
    "pronounce": "́prɔmptli",
    "vietnamese": "mau lẹ, ngay lập tức"
  },
  {
    "english": "pronounce",
    "type": "v",
    "pronounce": "prəˈnaʊns",
    "vietnamese": "tuyên bố, thông báo, phát âm"
  },
  {
    "english": "pronunciation",
    "type": "n",
    "pronounce": "prə ̧nʌnsi ́eiʃən",
    "vietnamese": "sự phát âm"
  },
  {
    "english": "proof",
    "type": "n",
    "pronounce": "pru:f",
    "vietnamese": "chứng, chứng cớ, bằng chứng; sự kiểm chứng"
  },
  {
    "english": "proper",
    "type": "adj",
    "pronounce": "prɔpə",
    "vietnamese": "đúng, thích đáng, thích hợp"
  },
  {
    "english": "properly",
    "type": "adv",
    "pronounce": "́prɔpəli",
    "vietnamese": "một cách đúng đắn, một cách thích đáng"
  },
  {
    "english": "property",
    "type": "n",
    "pronounce": "prɔpəti",
    "vietnamese": "tài sản, của cải; đất đấi, nhà cửa, bất động sản"
  },
  {
    "english": "proportion",
    "type": "n",
    "pronounce": "prə'pɔ:ʃn",
    "vietnamese": "sự cân xứng, sự cân đối"
  },
  {
    "english": "proposal",
    "type": "n",
    "pronounce": "prə'pouzl",
    "vietnamese": "sự đề nghị, đề xuất"
  },
  {
    "english": "propose",
    "type": "v",
    "pronounce": "prǝ'prouz",
    "vietnamese": "đề nghị, đề xuat, đưa ra"
  },
  {
    "english": "prospect",
    "type": "n",
    "pronounce": "́prɔspekt",
    "vietnamese": "viễn cảnh, toàn cảnh; triển vọng, mong chờ"
  },
  {
    "english": "protect",
    "type": "v",
    "pronounce": "prə'tekt",
    "vietnamese": "bảo vệ, che chở"
  },
  {
    "english": "protection",
    "type": "n",
    "pronounce": "prə'tek∫n",
    "vietnamese": "sự bảo vệ, sự che chở"
  },
  {
    "english": "react",
    "type": "v",
    "pronounce": "ri ́ækt",
    "vietnamese": "tác động trở lại, phản ứng"
  },
  {
    "english": "reaction",
    "type": "n",
    "pronounce": "ri:'ækʃn",
    "vietnamese": "sự phản ứng; sự phản tác dụng"
  },
  {
    "english": "read",
    "type": "v",
    "pronounce": "ri:d",
    "vietnamese": "đọc"
  },
  {
    "english": "reader",
    "type": "n",
    "pronounce": "́ri:də",
    "vietnamese": "người đọc, độc giả"
  },
  {
    "english": "reading",
    "type": "n",
    "pronounce": "́ri:diη",
    "vietnamese": "sự đọc"
  },
  {
    "english": "ready",
    "type": "adj",
    "pronounce": "redi",
    "vietnamese": "sẵn sàng"
  },
  {
    "english": "real",
    "type": "adj",
    "pronounce": "riəl",
    "vietnamese": "thực, thực tế, có thật"
  },
  {
    "english": "realistic",
    "type": "adj",
    "pronounce": "ri:ə'listik; BrE also riə",
    "vietnamese": "hiện thực"
  },
  {
    "english": "reality",
    "type": "n",
    "pronounce": "ri:'æliti",
    "vietnamese": "sự thật, thực tế, thực tại"
  },
  {
    "english": "realize",
    "type": "v",
    "pronounce": "riəlaiz",
    "vietnamese": "thực hiện, thực hành; thấy rõ, hiểu rõ, nhận thức rõ (việc gì...)"
  },
  {
    "english": "really",
    "type": "adv",
    "pronounce": "riəli",
    "vietnamese": "thực, thực ra, thực sự"
  },
  {
    "english": "rear",
    "type": "n, adj",
    "pronounce": "rɪər",
    "vietnamese": "phía sau; ở đằng sau, ở đằng sau"
  },
  {
    "english": "reason",
    "type": "n",
    "pronounce": "ri:zn",
    "vietnamese": "lý do, lý lẽ"
  },
  {
    "english": "reasonable",
    "type": "adj",
    "pronounce": "́ri:zənəbl",
    "vietnamese": "có lý, hợp lý"
  },
  {
    "english": "reasonably",
    "type": "adv",
    "pronounce": "́ri:zənəblli",
    "vietnamese": "hợp lý"
  },
  {
    "english": "recall",
    "type": "v",
    "pronounce": "ri ́kɔ:l",
    "vietnamese": "gọi về, triệu hồi; nhắc lại, gợi lại"
  },
  {
    "english": "receipt",
    "type": "n",
    "pronounce": "ri ́si:t",
    "vietnamese": "công thức; đơn thuốc"
  },
  {
    "english": "receive",
    "type": "v",
    "pronounce": "ri'si:v",
    "vietnamese": "nhận, lĩnh, thu"
  },
  {
    "english": "recent",
    "type": "adj",
    "pronounce": "́ri:sənt",
    "vietnamese": "gần đây, mới đây"
  },
  {
    "english": "recently",
    "type": "adv",
    "pronounce": "́ri:səntli",
    "vietnamese": "gần đây, mới đây"
  },
  {
    "english": "reception",
    "type": "n",
    "pronounce": "ri'sep∫n",
    "vietnamese": "sự nhận, sự tiếp nhận, sự đón tiếp"
  },
  {
    "english": "reckon",
    "type": "v",
    "pronounce": "rekən",
    "vietnamese": "tính, đếm"
  },
  {
    "english": "recognition",
    "type": "n",
    "pronounce": ",rekəg'niʃn",
    "vietnamese": "sự công nhận, sự thưa nhận"
  },
  {
    "english": "recognize",
    "type": "v",
    "pronounce": "rekəgnaiz",
    "vietnamese": "nhận ra, nhận diện; công nhận, thưa nhận"
  },
  {
    "english": "recommend",
    "type": "v",
    "pronounce": "rekə'mend",
    "vietnamese": "giới thiệu, tiến cử; đề nghị, khuyên bảo"
  },
  {
    "english": "record",
    "type": "n, v",
    "pronounce": "́rekɔ:d",
    "vietnamese": "bản ghi, sự ghi, bản thu, sự thu; thu, ghi chép"
  },
  {
    "english": "recording",
    "type": "n",
    "pronounce": "ri ́kɔ:diη",
    "vietnamese": "sự ghi, sự thu âm"
  },
  {
    "english": "recover",
    "type": "v",
    "pronounce": "ri:'kʌvə",
    "vietnamese": "lấy lại, giành lại"
  },
  {
    "english": "red",
    "type": "adj, n",
    "pronounce": "red",
    "vietnamese": "đỏ; màu đỏ"
  },
  {
    "english": "reduce",
    "type": "v",
    "pronounce": "ri'dju:s",
    "vietnamese": "giảm, giảm bớt"
  },
  {
    "english": "reduction",
    "type": "n",
    "pronounce": "ri ́dʌkʃən",
    "vietnamese": "sự giảm giá, sự hạ giá"
  },
  {
    "english": "refer to",
    "type": "v",
    "pronounce": "",
    "vietnamese": "xem, tham khảo; ám chỉ, nhắc đến"
  },
  {
    "english": "reference",
    "type": "n",
    "pronounce": "refərəns",
    "vietnamese": "sự tham khảo, hỏi ý kiến"
  },
  {
    "english": "reflect",
    "type": "v",
    "pronounce": "ri'flekt",
    "vietnamese": "phản hồi, phản ánh"
  },
  {
    "english": "reform",
    "type": "v, n",
    "pronounce": "ri ́fɔ:m",
    "vietnamese": "cải cách, cải thiện, cải tạo; sự cải cách, sự cải thiện, cải tạo"
  },
  {
    "english": "refrigerator",
    "type": "n",
    "pronounce": "ri'fridЗзreitз",
    "vietnamese": "tủ lạnh"
  },
  {
    "english": "refusal",
    "type": "n",
    "pronounce": "ri ́fju:zl",
    "vietnamese": "sự từ chối, sự khước từ"
  },
  {
    "english": "refuse",
    "type": "v",
    "pronounce": "rɪˈfyuz",
    "vietnamese": "từ chối, khước từ"
  },
  {
    "english": "regard",
    "type": "v, n",
    "pronounce": "ri'gɑ:d",
    "vietnamese": "nhìn, đánh giá; cái nhìn, sự quan tâm, sự chú ý (lời chúc tụng cuối thư)"
  },
  {
    "english": "regarding",
    "type": "prep",
    "pronounce": "ri ́ga:diη",
    "vietnamese": "về, về việc, đối với (vấn đề...)"
  },
  {
    "english": "region",
    "type": "n",
    "pronounce": "ri:dʒən",
    "vietnamese": "vùng, miền"
  },
  {
    "english": "regional",
    "type": "adj",
    "pronounce": "ˈridʒənl",
    "vietnamese": "vùng, địa phương"
  },
  {
    "english": "register",
    "type": "v, n",
    "pronounce": "redʤistə",
    "vietnamese": "đăng ký, ghi vào sổ; sổ, sổ sách, máy ghi"
  },
  {
    "english": "regret",
    "type": "v, n",
    "pronounce": "ri'gret",
    "vietnamese": "đáng tiếc, hối tiếc; lòng thương tiếc, sự hối tiếc"
  },
  {
    "english": "regular",
    "type": "adj",
    "pronounce": "rəgjulə",
    "vietnamese": "thường xuyên, đều đặn"
  },
  {
    "english": "regularly",
    "type": "adv",
    "pronounce": "́regjuləli",
    "vietnamese": "đều đặn, thường xuyên"
  },
  {
    "english": "regulation",
    "type": "n",
    "pronounce": "̧regju ́leiʃən",
    "vietnamese": "sự điều chỉnh, điều lệ, quy tắc"
  },
  {
    "english": "reject",
    "type": "v",
    "pronounce": "ri:ʤekt",
    "vietnamese": "không chấp nhận, loại bỏ, bác bỏ"
  },
  {
    "english": "relate",
    "type": "v",
    "pronounce": "ri'leit",
    "vietnamese": "kể lại, thuật lại; liên hệ, liên quan"
  },
  {
    "english": "related",
    "type": "to, adj",
    "pronounce": "ri'leitid",
    "vietnamese": "có liên quan, có quan hệ với ai, cái gì"
  },
  {
    "english": "relation",
    "type": "n",
    "pronounce": "ri'leiʃn",
    "vietnamese": "mối quan hệ, sự liên quan, liên lạc"
  },
  {
    "english": "relationship",
    "type": "n",
    "pronounce": "ri'lei∫ən∫ip",
    "vietnamese": "mối quan hệ, mối liên lạc"
  },
  {
    "english": "relative",
    "type": "adj, n",
    "pronounce": "relətiv",
    "vietnamese": "có liên quấn đến; người có họ, đạ từ quan hệ"
  },
  {
    "english": "relatively",
    "type": "adv",
    "pronounce": "relətivli",
    "vietnamese": "có liên quan, có quan hệ"
  },
  {
    "english": "relax",
    "type": "v",
    "pronounce": "ri ́læks",
    "vietnamese": "giải trí, nghỉ ngơi"
  },
  {
    "english": "relaxed",
    "type": "adj",
    "pronounce": "ri ́lækst",
    "vietnamese": "thanh thản, thoải mái"
  },
  {
    "english": "relaxing",
    "type": "adj",
    "pronounce": "ri'læksiɳ",
    "vietnamese": "làm giảm, bớt căng thẳng"
  },
  {
    "english": "release",
    "type": "v, n",
    "pronounce": "ri'li:s",
    "vietnamese": "làm nhẹ, bớt, thả, phóng thích, phát hành; sự giải thoát, thoát khỏi, sự thả, phóng thích, sự phát hành"
  },
  {
    "english": "relevant",
    "type": "adj",
    "pronounce": "́reləvənt",
    "vietnamese": "thích hợp, có liên quan"
  },
  {
    "english": "relief",
    "type": "n",
    "pronounce": "ri'li:f",
    "vietnamese": "sự giảm nhẹ, sự làm cho khuây khỏa; sự trợ cấp; sự đền bù"
  },
  {
    "english": "religion",
    "type": "n",
    "pronounce": "rɪˈlɪdʒən",
    "vietnamese": "tôn giáo"
  },
  {
    "english": "religious",
    "type": "adj",
    "pronounce": "ri'lidʒəs",
    "vietnamese": "(thuộc) tôn giáo"
  },
  {
    "english": "rely on",
    "type": "v",
    "pronounce": "ri ́lai",
    "vietnamese": "tin vào, tin cậy, tin tưởng vào"
  },
  {
    "english": "remain",
    "type": "v",
    "pronounce": "riˈmein",
    "vietnamese": "còn lại, vẫn còn như cũ"
  },
  {
    "english": "remaining",
    "type": "adj",
    "pronounce": "ri ́meiniη",
    "vietnamese": "còn lại"
  },
  {
    "english": "remains",
    "type": "n",
    "pronounce": "re'meins",
    "vietnamese": "đồ thưa, cái còn lại"
  },
  {
    "english": "remark",
    "type": "n, v",
    "pronounce": "ri'mɑ:k",
    "vietnamese": "sự nhận xét, phê bình, sự để ý, chú ý; nhận xét, phê bình, để ý, chú ý"
  },
  {
    "english": "remarkable",
    "type": "adj",
    "pronounce": "ri'ma:kəb(ə)l",
    "vietnamese": "đáng chú ý, đáng để ý; khác thường"
  },
  {
    "english": "remarkably",
    "type": "adv",
    "pronounce": "ri'ma:kəb(ə)li",
    "vietnamese": "đáng chú ý, đáng để ý; khác thường"
  },
  {
    "english": "remember",
    "type": "v",
    "pronounce": "rɪˈmɛmbər",
    "vietnamese": "nhớ, nhớ lại"
  },
  {
    "english": "remind",
    "type": "v",
    "pronounce": "riˈmaind",
    "vietnamese": "nhắc nhở, gợi nhớ"
  },
  {
    "english": "remote",
    "type": "adj",
    "pronounce": "ri'mout",
    "vietnamese": "xa, xa xôi, xa cách"
  },
  {
    "english": "removal",
    "type": "n",
    "pronounce": "ri'mu:vəl",
    "vietnamese": "viêc di chuyển, việc dọn nhà, dời đi"
  },
  {
    "english": "remove",
    "type": "v",
    "pronounce": "ri'mu:v",
    "vietnamese": "dời đi, di chuyển"
  },
  {
    "english": "rent",
    "type": "n, v",
    "pronounce": "rent",
    "vietnamese": "sự thuê mướn; cho thuê, thuê"
  },
  {
    "english": "rented",
    "type": "adj",
    "pronounce": "rentid",
    "vietnamese": "được thuê, được mướn"
  },
  {
    "english": "repair",
    "type": "v, n",
    "pronounce": "ri'peə",
    "vietnamese": "sửa chữa, chỉnh tu; sự sửa chữa, sự chỉnh tu"
  },
  {
    "english": "repeat",
    "type": "v",
    "pronounce": "ri'pi:t",
    "vietnamese": "nhắc lại, lặp lại"
  },
  {
    "english": "repeated",
    "type": "adj",
    "pronounce": "ri ́pi:tid",
    "vietnamese": "được nhắc lại, được lặp lại"
  },
  {
    "english": "repeatedly",
    "type": "adv",
    "pronounce": "ri ́pi:tidli",
    "vietnamese": "lặp đi lặp lại nhiều lần"
  },
  {
    "english": "replace",
    "type": "v",
    "pronounce": "rɪpleɪs",
    "vietnamese": "thay thế"
  },
  {
    "english": "reply",
    "type": "n, v",
    "pronounce": "ri'plai",
    "vietnamese": "sự trả lời, sự hồi âm; trả lời, hồi âm"
  },
  {
    "english": "report",
    "type": "v, n",
    "pronounce": "ri'pɔ:t",
    "vietnamese": "báo cáo, tường trình; bản báo cáo, bản tường trình"
  },
  {
    "english": "represent",
    "type": "v",
    "pronounce": "repri'zent",
    "vietnamese": "miêu tả, hình dung; đại diện, thay mặt"
  },
  {
    "english": "representative",
    "type": "n, adj",
    "pronounce": ",repri'zentətiv",
    "vietnamese": "điều tiêu biểu, tượng trưng, mẫu; miêu tả, biểu hiện, đại diện, tượng trưng"
  },
  {
    "english": "reproduce",
    "type": "v",
    "pronounce": ",ri:prə'dju:s",
    "vietnamese": "tái sản xuất"
  },
  {
    "english": "reputation",
    "type": "n",
    "pronounce": ",repju:'teiʃn",
    "vietnamese": "sự nổi tiếng, nổi danh"
  },
  {
    "english": "request",
    "type": "n, v",
    "pronounce": "ri'kwest",
    "vietnamese": "lời thỉnh cầu, lời đề nghị, yêu cầu; thỉnh cầu, đề nghị, yêu cầu"
  },
  {
    "english": "require",
    "type": "v",
    "pronounce": "ri'kwaiə(r)",
    "vietnamese": "đòi hỏi, yêu cầu, quy định"
  },
  {
    "english": "requirement",
    "type": "n",
    "pronounce": "rɪˈkwaɪərmənt",
    "vietnamese": "nhu cầu, sự đòi hỏi; luật lệ, thủ tục"
  },
  {
    "english": "rescue",
    "type": "v, n",
    "pronounce": "́reskju:",
    "vietnamese": "giải thoát, cứu nguy; sự giải thoát, sự cứu nguy"
  },
  {
    "english": "research",
    "type": "n",
    "pronounce": "ri'sз:tʃ",
    "vietnamese": "sự nghiên cứu"
  },
  {
    "english": "reservation",
    "type": "n",
    "pronounce": "rez.əveɪ.ʃən",
    "vietnamese": "sự hạn chế, điều kiện hạn chế"
  },
  {
    "english": "reserve",
    "type": "v, n",
    "pronounce": "ri'zЗ:v",
    "vietnamese": "dự trữ, để dành, đặt trước, đăng ký trước; sự dự trữ, sự để dành, sự đặt trước, sự đăng ký trước"
  },
  {
    "english": "resident",
    "type": "n, adj",
    "pronounce": "rezidənt",
    "vietnamese": "người sinh sống, trú ngụ, khách trọ; có nhà ở, cư trú, thường trú"
  },
  {
    "english": "resist",
    "type": "v",
    "pronounce": "ri'zist",
    "vietnamese": "chống lại, phản đổi, kháng cự"
  },
  {
    "english": "resistance",
    "type": "n",
    "pronounce": "ri ́zistəns",
    "vietnamese": "sự chống lại, sự phản đối, sự kháng cự"
  },
  {
    "english": "resolve",
    "type": "v",
    "pronounce": "ri'zɔlv",
    "vietnamese": "quyết định, kiên quyết (làm gì); giải quyết (vấn đề, khó khă(n).)"
  },
  {
    "english": "resort",
    "type": "n",
    "pronounce": "ri ́zɔ:t",
    "vietnamese": "kế sách, phương kế"
  },
  {
    "english": "resource",
    "type": "n",
    "pronounce": "ri'so:s",
    "vietnamese": "tài nguyên; kế sách, thủ đoạn"
  },
  {
    "english": "respect",
    "type": "n, v",
    "pronounce": "riˈspekt",
    "vietnamese": "sự kính trọng, sự lễ phép; tôn trọng, kính trọng, khâm phục"
  },
  {
    "english": "respond",
    "type": "v",
    "pronounce": "ri'spond",
    "vietnamese": "hưởng ứng, phản ứng lại, trả lời"
  },
  {
    "english": "response",
    "type": "n",
    "pronounce": "rɪˈspɒns",
    "vietnamese": "sự trả lời, câu trả lời, sự hưởng ứng, sự đáp lại"
  },
  {
    "english": "responsibility",
    "type": "n",
    "pronounce": "ris,ponsз'biliti",
    "vietnamese": "trách nhiệm, sự chịu trách nhiệm"
  },
  {
    "english": "responsible",
    "type": "adj",
    "pronounce": "ri'spɔnsəbl",
    "vietnamese": "chịu trách nhiệm về, chịu trách nhiệm trước ai, gì"
  },
  {
    "english": "rest",
    "type": "n, v",
    "pronounce": "rest",
    "vietnamese": "sự nghỉ ngơi, lúc nghỉ; nghỉ, nghỉ ngơi. the rest vật, cái còn lại, những người, cái khác"
  },
  {
    "english": "restaurant",
    "type": "n",
    "pronounce": "́restərɔn",
    "vietnamese": "nhà hàng ăn, hiệu ăn"
  },
  {
    "english": "restore",
    "type": "v",
    "pronounce": "ris ́tɔ:",
    "vietnamese": "hoàn lại, trả lại; sửa chữa lại, phục hồi lại"
  },
  {
    "english": "restrict",
    "type": "v",
    "pronounce": "ris ́trikt",
    "vietnamese": "hạn chế, giới hạn"
  },
  {
    "english": "restricted",
    "type": "adj",
    "pronounce": "ris ́triktid",
    "vietnamese": "bị hạn chế, có giới hạn; vùng cấm"
  },
  {
    "english": "restriction",
    "type": "n",
    "pronounce": "ri'strik∫n",
    "vietnamese": "sự hạn chế, sự giới hạn"
  },
  {
    "english": "result",
    "type": "n, v",
    "pronounce": "ri'zʌlt",
    "vietnamese": "kết quả; bởi, do.. mà ra, kết quả là..."
  },
  {
    "english": "retain",
    "type": "v",
    "pronounce": "ri'tein",
    "vietnamese": "giữ lại, nhớ được"
  },
  {
    "english": "retire",
    "type": "v",
    "pronounce": "ri ́taiə",
    "vietnamese": "rời bỏ, rút về; thôi, nghỉ việc, về hưu"
  },
  {
    "english": "retired",
    "type": "adj",
    "pronounce": "ri ́taiəd",
    "vietnamese": "ẩn dật, hẻo lánh, đã về hưu, đã nghỉ việc"
  },
  {
    "english": "retirement",
    "type": "n",
    "pronounce": "rɪˈtaɪərmənt",
    "vietnamese": "sự ẩn dật, nơi hẻo lánh, sự về hưu, sự nghỉ việc"
  },
  {
    "english": "return",
    "type": "v, n",
    "pronounce": "ri'tə:n",
    "vietnamese": "trở lại, trở về; sự trở lại, sự trở về"
  },
  {
    "english": "reveal",
    "type": "v",
    "pronounce": "riˈvi:l",
    "vietnamese": "bộc lộ, biểu lộ, tiết lộ; phát hiện, khám phá"
  },
  {
    "english": "reverse",
    "type": "v, n",
    "pronounce": "ri'və:s",
    "vietnamese": "đảo, ngược lại; điều trái ngược, mặt trái"
  },
  {
    "english": "review",
    "type": "n, v",
    "pronounce": "ri ́vju:",
    "vietnamese": "sự xem lại, sự xét lại; làm lại, xem xét lại"
  },
  {
    "english": "revise",
    "type": "v",
    "pronounce": "ri'vaiz",
    "vietnamese": "đọc lại, xem lại, sửa lại, ôn lại"
  },
  {
    "english": "revision",
    "type": "n",
    "pronounce": "ri ́viʒən",
    "vietnamese": "sự xem lại, sự đọc lại, sự sửa lại, sự ôn lại"
  },
  {
    "english": "revolution",
    "type": "n",
    "pronounce": ",revə'lu:ʃn",
    "vietnamese": "cuộc cách mạng"
  },
  {
    "english": "reward",
    "type": "n, v",
    "pronounce": "ri'wɔ:d",
    "vietnamese": "sự thưởng, phần thưởng; thưởng, thưởng công"
  },
  {
    "english": "rhythm",
    "type": "n",
    "pronounce": "riðm",
    "vietnamese": "nhịp điệu"
  },
  {
    "english": "rice",
    "type": "n",
    "pronounce": "raɪs",
    "vietnamese": "gạo, thóc, cơm; cây lúa"
  },
  {
    "english": "rich",
    "type": "adj",
    "pronounce": "ritʃ",
    "vietnamese": "giàu, giàu có"
  },
  {
    "english": "rid",
    "type": "v",
    "pronounce": "rid",
    "vietnamese": "giải thoát (get rid of : tống khứ)"
  },
  {
    "english": "ride",
    "type": "v, n",
    "pronounce": "raid",
    "vietnamese": "đi, cưỡi (ngựa); sự đi, đường đi"
  },
  {
    "english": "rider",
    "type": "n",
    "pronounce": "́raidə",
    "vietnamese": "người cưỡi ngựa, người đi xe đạp"
  },
  {
    "english": "ridiculous",
    "type": "adj",
    "pronounce": "rɪˈdɪkyələs",
    "vietnamese": "buồn cười, lố bịch, lố lăng"
  },
  {
    "english": "riding",
    "type": "n",
    "pronounce": "́raidiη",
    "vietnamese": "môn thể thấo cưỡi ngựa, sự đi xe (bus, điện, xe đạp)"
  },
  {
    "english": "right",
    "type": "adj, adv, n",
    "pronounce": "rait",
    "vietnamese": "thẳng, phải, tốt; ngấy, đúng; điều thiện, điều phải, tốt, bên phải"
  },
  {
    "english": "rightly",
    "type": "adv",
    "pronounce": "́raitli",
    "vietnamese": "đúng, phải, có lý"
  },
  {
    "english": "ring",
    "type": "n, v",
    "pronounce": "riɳ",
    "vietnamese": "chiếc nhẫn, đeo nhẫn cho ai"
  },
  {
    "english": "rise",
    "type": "n, v",
    "pronounce": "raiz",
    "vietnamese": "sự lên, sự tăng lương, thăng cấp; dậy, đứng lên, mọc (mặt trời), thành đạt"
  },
  {
    "english": "risk",
    "type": "n, v",
    "pronounce": "risk",
    "vietnamese": "sự liều, mạo hiểm; liều"
  },
  {
    "english": "rival",
    "type": "n, adj",
    "pronounce": "raivl",
    "vietnamese": "đối thủ, địch thủ; đối địch, cạnh tranh"
  },
  {
    "english": "river",
    "type": "n",
    "pronounce": "rivə",
    "vietnamese": "sông"
  },
  {
    "english": "road",
    "type": "n",
    "pronounce": "roʊd",
    "vietnamese": "con đường, đường phố"
  },
  {
    "english": "rob",
    "type": "v",
    "pronounce": "rɔb",
    "vietnamese": "cướp, lấy trộm"
  },
  {
    "english": "rock",
    "type": "n",
    "pronounce": "rɔk",
    "vietnamese": "đá"
  },
  {
    "english": "role",
    "type": "n",
    "pronounce": "roul",
    "vietnamese": "vai (diễn), vai trò"
  },
  {
    "english": "roll",
    "type": "n, v",
    "pronounce": "roul",
    "vietnamese": "cuốn, cuộn, sự lăn tròn; lăn, cuốn, quấn, cuộn"
  },
  {
    "english": "romantic",
    "type": "adj",
    "pronounce": "roʊˈmæntɪk",
    "vietnamese": "lãng mạn"
  },
  {
    "english": "roof",
    "type": "n",
    "pronounce": "ru:f",
    "vietnamese": "mái nhà, nóc"
  },
  {
    "english": "room",
    "type": "n",
    "pronounce": "rum",
    "vietnamese": "phòng, buồng"
  },
  {
    "english": "root",
    "type": "n",
    "pronounce": "ru:t",
    "vietnamese": "gốc, rễ"
  },
  {
    "english": "rope",
    "type": "n",
    "pronounce": "roʊp",
    "vietnamese": "dây cáp, dây thưng, xâu, chuỗi"
  },
  {
    "english": "rough",
    "type": "adj",
    "pronounce": "rᴧf",
    "vietnamese": "gồ ghề, lởm chởm"
  },
  {
    "english": "roughly",
    "type": "adv",
    "pronounce": "rʌfli",
    "vietnamese": "gồ ghề, lởm chởm"
  },
  {
    "english": "round",
    "type": "adj, adv, prep, n",
    "pronounce": "raund",
    "vietnamese": "tròn, vòng quanh, xung quanh"
  },
  {
    "english": "rounded",
    "type": "adj",
    "pronounce": "́raundid",
    "vietnamese": "bị làm thành tròn; phát triển đầy đủ"
  },
  {
    "english": "route",
    "type": "n",
    "pronounce": "ru:t",
    "vietnamese": "đường đi, lộ trình, tuyến đường"
  },
  {
    "english": "routine",
    "type": "n, adj",
    "pronounce": "ru:'ti:n",
    "vietnamese": "thói thường, lệ thường, thủ tục; thường lệ, thông thường"
  },
  {
    "english": "row",
    "type": "n",
    "pronounce": "rou",
    "vietnamese": "hàng, dãy"
  },
  {
    "english": "royal",
    "type": "adj",
    "pronounce": "ˈrɔɪəl",
    "vietnamese": "(thuộc) vua, nữ hoàng, hoàng gia"
  },
  {
    "english": "rub",
    "type": "v",
    "pronounce": "rʌb",
    "vietnamese": "cọ xát, xoa bóp, nghiền, tán"
  },
  {
    "english": "rubber",
    "type": "n",
    "pronounce": "́rʌbə",
    "vietnamese": "cao su"
  },
  {
    "english": "rubbish",
    "type": "n",
    "pronounce": "ˈrʌbɪʃ",
    "vietnamese": "vật vô giá trị, bỏ đi, rác rưởi"
  },
  {
    "english": "rude",
    "type": "adj",
    "pronounce": "ru:d",
    "vietnamese": "bất lịch sự, thô lỗ; thô sơ, đơn giản"
  },
  {
    "english": "rudely",
    "type": "adv",
    "pronounce": "ru:dli",
    "vietnamese": "bất lịch sự, thô lỗ; thô sơ, đơn giản"
  },
  {
    "english": "ruin",
    "type": "v, n",
    "pronounce": "ru:in",
    "vietnamese": "làm hỏng, làm đổ nát, làm phá sản; sự hỏng, sự đổ nát, sự phá sản"
  },
  {
    "english": "ruined",
    "type": "adj",
    "pronounce": "ru:ind",
    "vietnamese": "bị hỏng, bị đổ nát, bị phá sản"
  },
  {
    "english": "rule",
    "type": "n, v",
    "pronounce": "ru:l",
    "vietnamese": "quy tắc, điều lệ, luật lệ; cai trị, chỉ huy, điều khiển"
  },
  {
    "english": "ruler",
    "type": "n",
    "pronounce": "́ru:lə",
    "vietnamese": "người cai trị, người trị vì; thước kẻ"
  },
  {
    "english": "rumour",
    "type": "n",
    "pronounce": "ˈrumər",
    "vietnamese": "tin đồn, lời đồn"
  },
  {
    "english": "run",
    "type": "v, n",
    "pronounce": "rʌn",
    "vietnamese": "chạy; sự chạy"
  },
  {
    "english": "runner",
    "type": "n",
    "pronounce": "́rʌnə",
    "vietnamese": "người chạy"
  },
  {
    "english": "running",
    "type": "n",
    "pronounce": "rʌniɳ",
    "vietnamese": "sự chạy, cuộc chạy đua"
  },
  {
    "english": "rural",
    "type": "adj",
    "pronounce": "́ruərəl",
    "vietnamese": "(thuộc) nông thôn, vùng nông thôn"
  },
  {
    "english": "rush",
    "type": "v, n",
    "pronounce": "rʌ∫",
    "vietnamese": "xông lên, lao vào, xô đẩy; sự xông lên, sự lao vào, sự xô đẩy"
  },
  {
    "english": "sack",
    "type": "n, v",
    "pronounce": "sæk",
    "vietnamese": "bao tải; đóng bao, bỏ vào bao"
  },
  {
    "english": "sad",
    "type": "adj",
    "pronounce": "sæd",
    "vietnamese": "buồn, buồn bã"
  },
  {
    "english": "sadly",
    "type": "adv",
    "pronounce": "sædli",
    "vietnamese": "một cách buồn bã, đáng buồn là, không may mà"
  },
  {
    "english": "sadness",
    "type": "n",
    "pronounce": "sædnis",
    "vietnamese": "sự buồn rầu, sự buồn bã"
  },
  {
    "english": "safe",
    "type": "adj",
    "pronounce": "seif",
    "vietnamese": "an toàn, chắc chắn, đáng tin"
  },
  {
    "english": "safely",
    "type": "adv",
    "pronounce": "seifli",
    "vietnamese": "an toàn, chắc chắn, đáng tin"
  },
  {
    "english": "safety",
    "type": "n",
    "pronounce": "seifti",
    "vietnamese": "sự an toàn, sự chắc chăn"
  },
  {
    "english": "sail",
    "type": "v, n",
    "pronounce": "seil",
    "vietnamese": "đi tàu, thuyền, nhổ neo; buồm, cánh buồm, chuyến đi bằng thuyền buồm"
  },
  {
    "english": "sailing",
    "type": "n",
    "pronounce": "seiliɳ",
    "vietnamese": "sự đi thuyền"
  },
  {
    "english": "sailor",
    "type": "n",
    "pronounce": "seilə",
    "vietnamese": "thủy thủ"
  },
  {
    "english": "salad",
    "type": "n",
    "pronounce": "sæləd",
    "vietnamese": "sa lát (xà lách trộng dầu dấm); rau sống"
  },
  {
    "english": "salary",
    "type": "n",
    "pronounce": "ˈsæləri",
    "vietnamese": "tiền lương"
  },
  {
    "english": "sale",
    "type": "n",
    "pronounce": "seil",
    "vietnamese": "việc bán hàng"
  },
  {
    "english": "salt",
    "type": "n",
    "pronounce": "sɔ:lt",
    "vietnamese": "muối"
  },
  {
    "english": "salty",
    "type": "adj",
    "pronounce": "́sɔ:lti",
    "vietnamese": "chứa vị muối, có muối, mặn"
  },
  {
    "english": "same",
    "type": "adj, pron",
    "pronounce": "seim",
    "vietnamese": "đều đều, đơn điệu; cũng như thế, vẫn cái đó"
  },
  {
    "english": "sample",
    "type": "n",
    "pronounce": "́sa:mpl",
    "vietnamese": "mẫu, hàng mẫu"
  },
  {
    "english": "sand",
    "type": "n",
    "pronounce": "sænd",
    "vietnamese": "cát"
  },
  {
    "english": "satisfaction",
    "type": "n",
    "pronounce": ",sætis'fæk∫n",
    "vietnamese": "sự làm cho thỏa mãn, toại nguyện sự trả nợ, bồi thường"
  },
  {
    "english": "satisfied",
    "type": "adj",
    "pronounce": "sætisfaid",
    "vietnamese": "cảm thấy hài lòng, vưa ý, thoả mãn"
  },
  {
    "english": "satisfy",
    "type": "v",
    "pronounce": "sætisfai",
    "vietnamese": "làm thỏa mãn, hài lòng; trả (nợ), chuộc tội"
  },
  {
    "english": "satisfying",
    "type": "adj",
    "pronounce": "sætisfaiiη",
    "vietnamese": "đem lại sự thỏa mãn, làm thỏa mãn, làm vưa ý"
  },
  {
    "english": "Saturday (abbr Sat)",
    "type": "n",
    "pronounce": "sætədi",
    "vietnamese": "thứ 7"
  },
  {
    "english": "sauce",
    "type": "n",
    "pronounce": "sɔ:s",
    "vietnamese": "nước xốt, nước chấm"
  },
  {
    "english": "save",
    "type": "v",
    "pronounce": "seiv",
    "vietnamese": "cứu, lưu"
  },
  {
    "english": "saving",
    "type": "n",
    "pronounce": "́seiviη",
    "vietnamese": "sự cứu, sự tiết kiệm"
  },
  {
    "english": "say",
    "type": "v",
    "pronounce": "sei",
    "vietnamese": "nói"
  },
  {
    "english": "scale",
    "type": "n",
    "pronounce": "skeɪl",
    "vietnamese": "vảy (cá..), tỷ lệ"
  },
  {
    "english": "scare",
    "type": "v, n",
    "pronounce": "skɛə",
    "vietnamese": "làm kinh hãi, sợ hãi, dọa; sự sợ hãi, sự kinh hoàng"
  },
  {
    "english": "scared",
    "type": "adj",
    "pronounce": "skerd",
    "vietnamese": "bị hoảng sợ, bị sợ hãi"
  },
  {
    "english": "scene",
    "type": "n",
    "pronounce": "si:n",
    "vietnamese": "cảnh, phong cảnh"
  },
  {
    "english": "schedule",
    "type": "n, v",
    "pronounce": "́ʃkedju:l",
    "vietnamese": "kế hoạch làm việc, bản liệt kê; lên thời khóa biểu, lên kế hoạch"
  },
  {
    "english": "scheme",
    "type": "n",
    "pronounce": "ski:m",
    "vietnamese": "sự sắp xếp, sự phối hợp; kế hoạch thực hiện; lược đồ, sơ đồ"
  },
  {
    "english": "school",
    "type": "n",
    "pronounce": "sku:l",
    "vietnamese": "đàn cá, bầy cá, trường học, học đường"
  },
  {
    "english": "science",
    "type": "n",
    "pronounce": "saiəns",
    "vietnamese": "khoa học, khoa học tự nhiên"
  },
  {
    "english": "scientific",
    "type": "adj",
    "pronounce": ",saiən'tifik",
    "vietnamese": "(thuộc) khoa học, có tính khoa học"
  },
  {
    "english": "scientist",
    "type": "n",
    "pronounce": "saiəntist",
    "vietnamese": "nhà khoa học"
  },
  {
    "english": "scissors",
    "type": "n",
    "pronounce": "́sizəz",
    "vietnamese": "cái kéo"
  },
  {
    "english": "score",
    "type": "n, v",
    "pronounce": "skɔ:",
    "vietnamese": "điểm số, bản thắng, tỷ số; đạt được, thành công, cho điểm"
  },
  {
    "english": "scratch",
    "type": "v, n",
    "pronounce": "skrætʃ",
    "vietnamese": "cào, làm xước da; sự cào, sự trầy xước da"
  },
  {
    "english": "scream",
    "type": "v, n",
    "pronounce": "skri:m",
    "vietnamese": "gào thét, kêu lên; tiếng thét, tiếng kêu to"
  },
  {
    "english": "screen",
    "type": "n",
    "pronounce": "skrin",
    "vietnamese": "màn che, màn ảnh, màn hình; phim ảnh nói chung"
  },
  {
    "english": "screw",
    "type": "n, v",
    "pronounce": "skru:",
    "vietnamese": "đinh vít, đinh ốc; bắt vít, bắt ốc"
  },
  {
    "english": "sea",
    "type": "n",
    "pronounce": "si:",
    "vietnamese": "biển"
  },
  {
    "english": "seal",
    "type": "n, v",
    "pronounce": "si:l",
    "vietnamese": "hải cẩu; săn hải cẩu"
  },
  {
    "english": "search",
    "type": "n, v",
    "pronounce": "sə:t∫",
    "vietnamese": "sự tìm kiếm, sự thăm dò, sự điều tra; tìm kiếm, thăm dò, điều tra"
  },
  {
    "english": "season",
    "type": "n",
    "pronounce": "́si:zən",
    "vietnamese": "mùa"
  },
  {
    "english": "seat",
    "type": "n",
    "pronounce": "si:t",
    "vietnamese": "ghế, chỗ ngồi"
  },
  {
    "english": "second",
    "type": "det, adv, n",
    "pronounce": "ˈsɛkənd",
    "vietnamese": "thứ hai, ở vị trí thứ 2, thứ nhì; người về nhì"
  },
  {
    "english": "secondary",
    "type": "adj",
    "pronounce": "́sekəndəri",
    "vietnamese": "trung học, thứ yếu"
  },
  {
    "english": "secret",
    "type": "adj, n",
    "pronounce": "si:krit",
    "vietnamese": "bí mật; điều bí mật"
  },
  {
    "english": "secretary",
    "type": "n",
    "pronounce": "sekrətri",
    "vietnamese": "thư ký"
  },
  {
    "english": "secretly",
    "type": "adv",
    "pronounce": "si:kritli",
    "vietnamese": "bí mật, riêng tư"
  },
  {
    "english": "section",
    "type": "n",
    "pronounce": "sekʃn",
    "vietnamese": "mục, phần"
  },
  {
    "english": "sector",
    "type": "n",
    "pronounce": "ˈsɛktər",
    "vietnamese": "khu vực, lĩnh vực"
  },
  {
    "english": "secure",
    "type": "adj, v",
    "pronounce": "si'kjuə",
    "vietnamese": "chắc chắn, đảm bảo; bảo đảm, giữ an ninh"
  },
  {
    "english": "security",
    "type": "n",
    "pronounce": "siˈkiuəriti",
    "vietnamese": "sự an toàn, sự an ninh"
  },
  {
    "english": "see",
    "type": "v",
    "pronounce": "si:",
    "vietnamese": "nhìn, nhìn thấy, quan sát"
  },
  {
    "english": "seed",
    "type": "n",
    "pronounce": "sid",
    "vietnamese": "hạt, hạt giống"
  },
  {
    "english": "seek",
    "type": "v",
    "pronounce": "si:k",
    "vietnamese": "tìm, tìm kiếm, theo đuổi"
  },
  {
    "english": "seem linking",
    "type": "v",
    "pronounce": "si:m",
    "vietnamese": "có vẻ như, dường như"
  },
  {
    "english": "select",
    "type": "v",
    "pronounce": "si ́lekt",
    "vietnamese": "chọn lựa, chọn lọc"
  },
  {
    "english": "selection",
    "type": "n",
    "pronounce": "si'lekʃn",
    "vietnamese": "sự lựa chọn, sự chọc lọc"
  },
  {
    "english": "self",
    "type": "n",
    "pronounce": "self",
    "vietnamese": "bản thân mình"
  },
  {
    "english": "self",
    "type": "-",
    "pronounce": "combiningform",
    "vietnamese": "tự bản thân mình, cái tôi"
  },
  {
    "english": "sell",
    "type": "v",
    "pronounce": "sel",
    "vietnamese": "bán"
  },
  {
    "english": "senate",
    "type": "n",
    "pronounce": "́senit",
    "vietnamese": "thượng nghi viện, ban giám hiệu"
  },
  {
    "english": "senator",
    "type": "n",
    "pronounce": "ˈsɛnətər",
    "vietnamese": "thượng nghị sĩ"
  },
  {
    "english": "send",
    "type": "v",
    "pronounce": "send",
    "vietnamese": "gửi, phái đi"
  },
  {
    "english": "senior",
    "type": "adj, n",
    "pronounce": "si:niə",
    "vietnamese": "nhiều tuổi hơn, dành cho trẻ em trên 11t; người lớn tuổi hơn, sinh viên năm cuối trường trung học, cấo đẳng"
  },
  {
    "english": "sense",
    "type": "n",
    "pronounce": "sens",
    "vietnamese": "giác quan, tri giác, cảm giác"
  },
  {
    "english": "sensible",
    "type": "adj",
    "pronounce": "sensəbl",
    "vietnamese": "có óc xét đoán; hiểu, nhận biết được"
  },
  {
    "english": "sensitive",
    "type": "adj",
    "pronounce": "sensitiv",
    "vietnamese": "dễ bị thương, dễ bị hỏng; dễ bị xúc phạm"
  },
  {
    "english": "sentence",
    "type": "n",
    "pronounce": "sentəns",
    "vietnamese": "câu"
  },
  {
    "english": "separate",
    "type": "adj, v",
    "pronounce": "seprət",
    "vietnamese": "khác nhau, riêng biệt; làm rời, tách ra, chia tay"
  },
  {
    "english": "separated",
    "type": "adj",
    "pronounce": "seprətid",
    "vietnamese": "ly thân"
  },
  {
    "english": "separately",
    "type": "adv",
    "pronounce": "seprətli",
    "vietnamese": "không cùng nhau, thành người riêng, vật riêng"
  },
  {
    "english": "separation",
    "type": "n",
    "pronounce": "̧sepə ́reiʃən",
    "vietnamese": "sự chia cắt, phân ly, sự chia tay, ly thân"
  },
  {
    "english": "September",
    "type": "n",
    "pronounce": "sep ́tembə",
    "vietnamese": "tháng 9"
  },
  {
    "english": "series",
    "type": "n",
    "pronounce": "ˈsɪəriz",
    "vietnamese": "loạt, dãy, chuỗi"
  },
  {
    "english": "serious",
    "type": "adj",
    "pronounce": "siəriəs",
    "vietnamese": "đứng đắn, nghiêm trang"
  },
  {
    "english": "seriously",
    "type": "adv",
    "pronounce": "siəriəsli",
    "vietnamese": "đứng đắn, nghiêm trang"
  },
  {
    "english": "servant",
    "type": "n",
    "pronounce": "sə:vənt",
    "vietnamese": "người hầu, đầy tớ"
  },
  {
    "english": "serve",
    "type": "v",
    "pronounce": "sɜ:v",
    "vietnamese": "phục vụ, phụng sự"
  },
  {
    "english": "service",
    "type": "n",
    "pronounce": "sə:vis",
    "vietnamese": "sự phục vụ, sự hầu hạ"
  },
  {
    "english": "session",
    "type": "n",
    "pronounce": "seʃn",
    "vietnamese": "buổi họp, phiên họp, buổi, phiên"
  },
  {
    "english": "set",
    "type": "n, v",
    "pronounce": "set",
    "vietnamese": "bộ, bọn, đám, lũ; đặt để, bố trí"
  },
  {
    "english": "settle",
    "type": "v",
    "pronounce": "ˈsɛtl",
    "vietnamese": "giải quyết, dàn xếp, hòa giải, đặt, bố trí"
  },
  {
    "english": "several",
    "type": "det, pron",
    "pronounce": "sevrəl",
    "vietnamese": "vài"
  },
  {
    "english": "severe",
    "type": "adj",
    "pronounce": "səˈvɪər",
    "vietnamese": "khắt khe, gay gắt (thái độ, cư xử); giản dị, mộc mạc (kiểu cách, trang phục, dung nhan)"
  },
  {
    "english": "severely",
    "type": "adv",
    "pronounce": "sə ́virli",
    "vietnamese": "khắt khe, gay gắt (thái độ, cư xử); giản dị, mộc mạc (kiểu cách, trang phục, dung nhan)"
  },
  {
    "english": "sew",
    "type": "v",
    "pronounce": "soʊ",
    "vietnamese": "may, khâu"
  },
  {
    "english": "sewing",
    "type": "n",
    "pronounce": "́souiη",
    "vietnamese": "sự khâu, sự may vá"
  },
  {
    "english": "sex",
    "type": "n",
    "pronounce": "seks",
    "vietnamese": "giới, giống"
  },
  {
    "english": "sexual",
    "type": "adj",
    "pronounce": "seksjuəl",
    "vietnamese": "giới tính, các vấn đề sinh lý"
  },
  {
    "english": "sexually",
    "type": "adv",
    "pronounce": "sekSJli",
    "vietnamese": "giới tính, các vấn đề sinh lý"
  },
  {
    "english": "shade",
    "type": "n",
    "pronounce": "ʃeid",
    "vietnamese": "bóng, bóng tối"
  },
  {
    "english": "shadow",
    "type": "n",
    "pronounce": "ˈʃædəu",
    "vietnamese": "bóng, bóng tối, bóng râm, bóng mát"
  },
  {
    "english": "shake",
    "type": "v, n",
    "pronounce": "ʃeik",
    "vietnamese": "rung, lắc, giũ; sự rung, sự lắc, sự giũ"
  },
  {
    "english": "shall",
    "type": "v, modal",
    "pronounce": "ʃæl",
    "vietnamese": "dự đoán tương lai: sẽ"
  },
  {
    "english": "shallow",
    "type": "adj",
    "pronounce": "ʃælou",
    "vietnamese": "nông, cạn"
  },
  {
    "english": "shame",
    "type": "n",
    "pronounce": "ʃeɪm",
    "vietnamese": "sự xấu hổ, thẹn thùng, sự ngượng"
  },
  {
    "english": "shape",
    "type": "n, v",
    "pronounce": "ʃeip",
    "vietnamese": "hình, hình dạng, hình thù"
  },
  {
    "english": "shaped",
    "type": "adj",
    "pronounce": "ʃeipt",
    "vietnamese": "có hình dáng được chỉ rõ"
  },
  {
    "english": "share",
    "type": "v, n",
    "pronounce": "ʃeə",
    "vietnamese": "đóng góp, tham gia, chia sẻ; phần đóng góp, phần tham gia, phần chia sẻ"
  },
  {
    "english": "sharp",
    "type": "adj",
    "pronounce": "ʃɑrp",
    "vietnamese": "sắc, nhọn, bén"
  },
  {
    "english": "sharply",
    "type": "adv",
    "pronounce": "ʃɑrpli",
    "vietnamese": "sắc, nhọn, bén"
  },
  {
    "english": "shave",
    "type": "v",
    "pronounce": "ʃeiv",
    "vietnamese": "cạo (râu), bào, đẽo (gỗ)"
  },
  {
    "english": "she",
    "type": "n, pro",
    "pronounce": "ʃi:",
    "vietnamese": "nó, bà ấy, chị ấy, cô ấy..."
  },
  {
    "english": "sheep",
    "type": "n",
    "pronounce": "ʃi:p",
    "vietnamese": "con cừu"
  },
  {
    "english": "sheet",
    "type": "n",
    "pronounce": "ʃi:t",
    "vietnamese": "chăn, khăn trải giường; lá, tấm, phiến, tờ"
  },
  {
    "english": "shelf",
    "type": "n",
    "pronounce": "ʃɛlf",
    "vietnamese": "kệ, ngăn, giá"
  },
  {
    "english": "shell",
    "type": "n",
    "pronounce": "ʃɛl",
    "vietnamese": "vỏ, mai; vẻ bề ngoài"
  },
  {
    "english": "shelter",
    "type": "n, v",
    "pronounce": "ʃeltə",
    "vietnamese": "sự nương tựa, sự che chở, sự ẩn náu; che chở, bảo vệ"
  },
  {
    "english": "shift",
    "type": "v, n",
    "pronounce": "ʃift",
    "vietnamese": "đổi chỗ, dời chỗ, chuyển, giao; sự thấy đổi, sự luân phiên"
  },
  {
    "english": "shine",
    "type": "v",
    "pronounce": "ʃain",
    "vietnamese": "chiếu sáng, tỏa sáng"
  },
  {
    "english": "shiny",
    "type": "adj",
    "pronounce": "∫aini",
    "vietnamese": "sáng chói, bóng"
  },
  {
    "english": "ship",
    "type": "n",
    "pronounce": "ʃɪp",
    "vietnamese": "tàu, tàu thủy"
  },
  {
    "english": "shirt",
    "type": "n",
    "pronounce": "ʃɜːt",
    "vietnamese": "áo sơ mi"
  },
  {
    "english": "shock",
    "type": "n, v",
    "pronounce": "Sok",
    "vietnamese": "sự đụng chạm, va chạm, sự kích động, sự choáng; chạm mạnh, va mạnh, gây sốc"
  },
  {
    "english": "shocked",
    "type": "adj",
    "pronounce": "Sok",
    "vietnamese": "bị kích động, bị va chạm, bị sốc"
  },
  {
    "english": "shocking",
    "type": "adj",
    "pronounce": "́ʃɔkiη",
    "vietnamese": "gây ra căm phẫn, tồi tệ, gây kích động"
  },
  {
    "english": "shoe",
    "type": "n",
    "pronounce": "ʃu:",
    "vietnamese": "giày"
  },
  {
    "english": "shoot",
    "type": "v",
    "pronounce": "ʃut",
    "vietnamese": "vụt qua, chạy qua, ném, phóng, bắn; đâm ra, trồi ra"
  },
  {
    "english": "shooting",
    "type": "n",
    "pronounce": "∫u:tiη",
    "vietnamese": "sự bắn, sự phóng đi"
  },
  {
    "english": "shop",
    "type": "n, v",
    "pronounce": "ʃɔp",
    "vietnamese": "cửa hàng; đi mua hàng, đi chợ"
  },
  {
    "english": "shopping",
    "type": "n",
    "pronounce": "ʃɔpiɳ",
    "vietnamese": "sự mua sắm"
  },
  {
    "english": "short",
    "type": "adj",
    "pronounce": "ʃɔ:t",
    "vietnamese": "ngắn, cụt"
  },
  {
    "english": "shortly",
    "type": "adv",
    "pronounce": "́ʃɔ:tli",
    "vietnamese": "trong thời gian ngắn, sớm"
  },
  {
    "english": "shot",
    "type": "n",
    "pronounce": "ʃɔt",
    "vietnamese": "đạn, viên đạn"
  },
  {
    "english": "should",
    "type": "v, modal",
    "pronounce": "ʃud, ʃəd, ʃd",
    "vietnamese": "nên"
  },
  {
    "english": "shoulder",
    "type": "n",
    "pronounce": "ʃouldə",
    "vietnamese": "vai"
  },
  {
    "english": "shout",
    "type": "v, n",
    "pronounce": "ʃaʊt",
    "vietnamese": "hò hét, reo hò; sự la hét, sự hò reo"
  },
  {
    "english": "show",
    "type": "v, n",
    "pronounce": "ʃou",
    "vietnamese": "biểu diễn, trưng bày; sự biểu diễn sự bày tỏ"
  },
  {
    "english": "shower",
    "type": "n",
    "pronounce": "́ʃouə",
    "vietnamese": "vòi hoa sen, sự tắm vòi hoa sen"
  },
  {
    "english": "shut",
    "type": "v, adj",
    "pronounce": "ʃʌt",
    "vietnamese": "đóng, khép, đậy; tính khép kín"
  },
  {
    "english": "shy",
    "type": "adj",
    "pronounce": "ʃaɪ",
    "vietnamese": "nhút nhát, e thẹn"
  },
  {
    "english": "sick",
    "type": "adj",
    "pronounce": "sick",
    "vietnamese": "ốm, đau, bệnh"
  },
  {
    "english": "side",
    "type": "n",
    "pronounce": "said",
    "vietnamese": "mặt, mặt phẳng"
  },
  {
    "english": "side",
    "type": "n",
    "pronounce": "sait",
    "vietnamese": "chỗ, vị trí"
  },
  {
    "english": "sideways",
    "type": "adj, adv",
    "pronounce": "́saidwə:dz",
    "vietnamese": "ngang, từ một bên; sang bên"
  },
  {
    "english": "sight",
    "type": "n",
    "pronounce": "sait",
    "vietnamese": "cảnh đẹp; sự nhìn"
  },
  {
    "english": "sign",
    "type": "n, v",
    "pronounce": "sain",
    "vietnamese": "dấu, dấu hiệu, kí hiệu; đánh dấu, viết ký hiệu"
  },
  {
    "english": "signal",
    "type": "n, v",
    "pronounce": "signəl",
    "vietnamese": "dấu hiệu, tín hiệu; ra hiệu, báo hiệu"
  },
  {
    "english": "signature",
    "type": "n",
    "pronounce": "ˈsɪgnətʃər , ˈsɪgnəˌtʃʊər",
    "vietnamese": "chữ ký"
  },
  {
    "english": "significant",
    "type": "adj",
    "pronounce": "sɪgˈnɪfɪkənt",
    "vietnamese": "nhiều ý nghĩa, quan trọng"
  },
  {
    "english": "significantly",
    "type": "adv",
    "pronounce": "sig'nifikəntli",
    "vietnamese": "đáng kể"
  },
  {
    "english": "silence",
    "type": "n",
    "pronounce": "ˈsaɪləns",
    "vietnamese": "sự im lặng, sự yên tĩnh"
  },
  {
    "english": "silent",
    "type": "adj",
    "pronounce": "ˈsaɪlənt",
    "vietnamese": "im lặng, yên tĩnh"
  },
  {
    "english": "silk",
    "type": "n",
    "pronounce": "silk",
    "vietnamese": "tơ, chỉ, lụa"
  },
  {
    "english": "silly",
    "type": "adj",
    "pronounce": "́sili",
    "vietnamese": "ngớ ngẩn, ngu ngốc, khờ dại"
  },
  {
    "english": "silver",
    "type": "n, adj",
    "pronounce": "silvə",
    "vietnamese": "bạc, đồng bạc; làm bằng bạc, trắng như bạc"
  },
  {
    "english": "similar",
    "type": "adj",
    "pronounce": "́similə",
    "vietnamese": "giống như, tương tự như"
  },
  {
    "english": "similarly",
    "type": "adv",
    "pronounce": "́similəli",
    "vietnamese": "tương tự, giống nhau"
  },
  {
    "english": "simple",
    "type": "adj",
    "pronounce": "simpl",
    "vietnamese": "đơn, đơn giản, dễ dàng"
  },
  {
    "english": "simply",
    "type": "adv",
    "pronounce": "́simpli",
    "vietnamese": "một cách dễ dàng, giản dị"
  },
  {
    "english": "since",
    "type": "prep, conj, adv",
    "pronounce": "sins",
    "vietnamese": "từ, từ khi, từ lúc đó; từ đó, từ lúc ấy"
  },
  {
    "english": "sincere",
    "type": "adj",
    "pronounce": "sin ́siə",
    "vietnamese": "thật thà, thẳng thắng, chân thành"
  },
  {
    "english": "sincerely",
    "type": "adv",
    "pronounce": "sin'siəli",
    "vietnamese": "một cách chân thành"
  },
  {
    "english": "sing",
    "type": "v",
    "pronounce": "siɳ",
    "vietnamese": "hát, ca hát"
  },
  {
    "english": "singer",
    "type": "n",
    "pronounce": "́siηə",
    "vietnamese": "ca sĩ"
  },
  {
    "english": "singing",
    "type": "n",
    "pronounce": "́siηiη",
    "vietnamese": "sự hát, tiếng hát"
  },
  {
    "english": "single",
    "type": "adj",
    "pronounce": "siɳgl",
    "vietnamese": "đơn, đơn độc, đơn lẻ"
  },
  {
    "english": "sink",
    "type": "v",
    "pronounce": "sɪŋk",
    "vietnamese": "chìm, lún, đắm"
  },
  {
    "english": "sir",
    "type": "n",
    "pronounce": "sə:",
    "vietnamese": "xưng hô lịch sự Ngài, Ông"
  },
  {
    "english": "sister",
    "type": "n",
    "pronounce": "sistə",
    "vietnamese": "chị, em gái"
  },
  {
    "english": "sit",
    "type": "v",
    "pronounce": "sit",
    "vietnamese": "ngồi. sit down: ngồi xuống"
  },
  {
    "english": "situation",
    "type": "n",
    "pronounce": ",sit∫u'ei∫n",
    "vietnamese": "hoàn cảnh, địa thế, vị trí"
  },
  {
    "english": "size",
    "type": "n",
    "pronounce": "saiz",
    "vietnamese": "cỡ. đã được định cỡ"
  },
  {
    "english": "skilful",
    "type": "adj",
    "pronounce": "́skilful",
    "vietnamese": "tài giỏi, khéo tay"
  },
  {
    "english": "skilfully",
    "type": "adv",
    "pronounce": "́skilfulli",
    "vietnamese": "tài giỏi, khéo tay"
  },
  {
    "english": "skill",
    "type": "n",
    "pronounce": "skil",
    "vietnamese": "kỹ năng, kỹ sảo"
  },
  {
    "english": "skilled",
    "type": "adj",
    "pronounce": "skild",
    "vietnamese": "có kỹ năng, có kỹ sảo, khẻo tay; có kinh nghiệm,, lành nghề"
  },
  {
    "english": "skin",
    "type": "n",
    "pronounce": "skin",
    "vietnamese": "da, vỏ"
  },
  {
    "english": "skirt",
    "type": "n",
    "pronounce": "skɜːrt",
    "vietnamese": "váy, đầm"
  },
  {
    "english": "sky",
    "type": "n",
    "pronounce": "skaɪ",
    "vietnamese": "trời, bầu trời"
  },
  {
    "english": "sleep",
    "type": "v, n",
    "pronounce": "sli:p",
    "vietnamese": "ngủ; giấc ngủ"
  },
  {
    "english": "sleeve",
    "type": "n",
    "pronounce": "sli:v",
    "vietnamese": "tay áo, ống tay"
  },
  {
    "english": "slice",
    "type": "n, v",
    "pronounce": "slais",
    "vietnamese": "miếng, lát mỏng; cắt ra thành miếng mỏng, lát mỏng"
  },
  {
    "english": "slide",
    "type": "v",
    "pronounce": "slaid",
    "vietnamese": "trượtc, chuyển động nhẹ, lướt qua"
  },
  {
    "english": "slight",
    "type": "adj",
    "pronounce": "slait",
    "vietnamese": "mỏng manh, thon, gầy"
  },
  {
    "english": "slightly",
    "type": "adv",
    "pronounce": "slaitli",
    "vietnamese": "mảnh khảnh, mỏng manh, yếu ớt"
  },
  {
    "english": "slip",
    "type": "v",
    "pronounce": "slip",
    "vietnamese": "trượt, tuột, trôi qua, chạy qua"
  },
  {
    "english": "slope",
    "type": "n, v",
    "pronounce": "sloup",
    "vietnamese": "dốc, đường dốc, độ dốc; nghiêng, dốc"
  },
  {
    "english": "slow",
    "type": "adj",
    "pronounce": "slou",
    "vietnamese": "chậm, chậm chạp"
  },
  {
    "english": "slowly",
    "type": "adv",
    "pronounce": "slouli",
    "vietnamese": "một cách chậm chạp, chậm dần"
  },
  {
    "english": "small",
    "type": "adj",
    "pronounce": "smɔ:l",
    "vietnamese": "nhỏ, bé"
  },
  {
    "english": "smart",
    "type": "adj",
    "pronounce": "sma:t",
    "vietnamese": "mạnh, ác liệt, khéo léo, khôn khéo"
  },
  {
    "english": "smash",
    "type": "v, n",
    "pronounce": "smæʃ",
    "vietnamese": "đập, vỡ tan thành mảnh; sự đập, vỡ tàn thành mảnh"
  },
  {
    "english": "smell",
    "type": "v, n",
    "pronounce": "smɛl",
    "vietnamese": "ngửi; sự ngửi, khứu giác"
  },
  {
    "english": "smile",
    "type": "v, n",
    "pronounce": "smail",
    "vietnamese": "cười, mỉm cười; nụ cười, vẻ tươi cười"
  },
  {
    "english": "smoke",
    "type": "n, v",
    "pronounce": "smouk",
    "vietnamese": "khói, hơi thuốc; hút thuốc, bốc khói, hơi"
  },
  {
    "english": "smoking",
    "type": "n",
    "pronounce": "smoukiη",
    "vietnamese": "sự hút thuốc"
  },
  {
    "english": "smooth",
    "type": "adj",
    "pronounce": "smu:ð",
    "vietnamese": "nhẵn, trơn, mượt mà"
  },
  {
    "english": "smoothly",
    "type": "adv",
    "pronounce": "smu:ðli",
    "vietnamese": "một cách êm ả, trôi chảy"
  },
  {
    "english": "snake",
    "type": "n",
    "pronounce": "sneik",
    "vietnamese": "con rắn; người nham hiểm, xảo trá"
  },
  {
    "english": "snow",
    "type": "n, v",
    "pronounce": "snou",
    "vietnamese": "tuyết; tuyết rơi"
  },
  {
    "english": "so",
    "type": "adv, conj",
    "pronounce": "sou",
    "vietnamese": "như vậy, như thế; vì thế, vì vậy, vì thế cho nên so thất để, để cho, để mà"
  },
  {
    "english": "soap",
    "type": "n",
    "pronounce": "soup",
    "vietnamese": "xà phòng"
  },
  {
    "english": "social",
    "type": "adj",
    "pronounce": "sou∫l",
    "vietnamese": "có tính xã hội"
  },
  {
    "english": "socially",
    "type": "adv",
    "pronounce": "́souʃəli",
    "vietnamese": "có tính xã hội"
  },
  {
    "english": "society",
    "type": "n",
    "pronounce": "sə'saiəti",
    "vietnamese": "xã hội"
  },
  {
    "english": "sock",
    "type": "n",
    "pronounce": "sɔk",
    "vietnamese": "tất ngắn, miếng lót giày"
  },
  {
    "english": "soft",
    "type": "adj",
    "pronounce": "sɔft",
    "vietnamese": "mềm, dẻo"
  },
  {
    "english": "softly",
    "type": "adv",
    "pronounce": "sɔftli",
    "vietnamese": "một cách mềm dẻo"
  },
  {
    "english": "software",
    "type": "n",
    "pronounce": "sɔfweз",
    "vietnamese": "phần mềm (m.tính)"
  },
  {
    "english": "soil",
    "type": "n",
    "pronounce": "sɔɪl",
    "vietnamese": "đất trồng; vết bẩn"
  },
  {
    "english": "soldier",
    "type": "n",
    "pronounce": "souldʤə",
    "vietnamese": "lính, quân nhân"
  },
  {
    "english": "solid",
    "type": "adj, n",
    "pronounce": "sɔlid",
    "vietnamese": "rắn; thể rắn, chất rắn"
  },
  {
    "english": "solution",
    "type": "n",
    "pronounce": "sə'lu:ʃn",
    "vietnamese": "sự giải quyết, giải pháp"
  },
  {
    "english": "solve",
    "type": "v",
    "pronounce": "sɔlv",
    "vietnamese": "giải, giải thích, giải quyết"
  },
  {
    "english": "some",
    "type": "det, pron",
    "pronounce": "sʌm",
    "vietnamese": "một it, một vài"
  },
  {
    "english": "somebody, someone",
    "type": "pron",
    "pronounce": "sʌmbədi",
    "vietnamese": "người nào đó"
  },
  {
    "english": "somehow",
    "type": "adv",
    "pronounce": "́sʌm ̧hau",
    "vietnamese": "không biết làm sao, bằng cách này hay cách khác"
  },
  {
    "english": "something",
    "type": "pron",
    "pronounce": "sʌmθiɳ",
    "vietnamese": "một điều gì đó, một việc gì đó, mộ cái gì đó"
  },
  {
    "english": "spend",
    "type": "v",
    "pronounce": "spɛnd",
    "vietnamese": "tiêu, xài"
  },
  {
    "english": "spice",
    "type": "n",
    "pronounce": "spais",
    "vietnamese": "gia vị"
  },
  {
    "english": "spicy",
    "type": "adj",
    "pronounce": "́spaisi",
    "vietnamese": "có gia vị"
  },
  {
    "english": "spider",
    "type": "n",
    "pronounce": "́spaidə",
    "vietnamese": "con nhện"
  },
  {
    "english": "spin",
    "type": "v",
    "pronounce": "spin",
    "vietnamese": "quay, quay tròn"
  },
  {
    "english": "spirit",
    "type": "n",
    "pronounce": "ˈspɪrɪt",
    "vietnamese": "tinh thần, tâm hồn, linh hồn"
  },
  {
    "english": "spiritual",
    "type": "adj",
    "pronounce": "spiritjuəl",
    "vietnamese": "(thuộc) tinh thần, linh hồn"
  },
  {
    "english": "spite",
    "type": "n",
    "pronounce": "spait",
    "vietnamese": "sự giận, sự hận thù; in spite of: mặc dù, bất chấp"
  },
  {
    "english": "split",
    "type": "v, n",
    "pronounce": "split",
    "vietnamese": "chẻ, tách, chia ra; sự chẻ, sự tách sự chia ra"
  },
  {
    "english": "spoil",
    "type": "v",
    "pronounce": "spɔil",
    "vietnamese": "cướp, cướp đọat"
  },
  {
    "english": "spoken",
    "type": "adj",
    "pronounce": "spoukn",
    "vietnamese": "nói theo 1 cách nào đó"
  },
  {
    "english": "spoon",
    "type": "n",
    "pronounce": "spu:n",
    "vietnamese": "cái thìa"
  },
  {
    "english": "sport",
    "type": "n",
    "pronounce": "spɔ:t",
    "vietnamese": "thể thao"
  },
  {
    "english": "spot",
    "type": "n",
    "pronounce": "spɔt",
    "vietnamese": "dấu, đốm, vết"
  },
  {
    "english": "spray",
    "type": "n, v",
    "pronounce": "spreɪ",
    "vietnamese": "máy, ống, bình phụt, bơm, phun; bơm, phun, xịt"
  },
  {
    "english": "spread",
    "type": "v",
    "pronounce": "spred",
    "vietnamese": "trải, căng ra, bày ra; truyền bá"
  },
  {
    "english": "spring",
    "type": "n",
    "pronounce": "sprɪŋ",
    "vietnamese": "mùa xuân"
  },
  {
    "english": "square",
    "type": "adj, n",
    "pronounce": "skweə",
    "vietnamese": "vuông, vuông vắn; dạng hình vuông, hình vuông"
  },
  {
    "english": "squeeze",
    "type": "v, n",
    "pronounce": "skwi:z",
    "vietnamese": "ép, vắt, xiết; sự ép, sự vắt, sự xiết"
  },
  {
    "english": "stable",
    "type": "adj, n",
    "pronounce": "steibl",
    "vietnamese": "ổn định, bình tĩnh, vững vàng; chuồng ngựa"
  },
  {
    "english": "staff",
    "type": "n",
    "pronounce": "sta:f",
    "vietnamese": "gậy"
  },
  {
    "english": "stage",
    "type": "n",
    "pronounce": "steɪdʒ",
    "vietnamese": "tầng, bệ, sân khấu, giai đoạn"
  },
  {
    "english": "stair",
    "type": "n",
    "pronounce": "steə",
    "vietnamese": "bậc thang"
  },
  {
    "english": "stamp",
    "type": "n, v",
    "pronounce": "stæmp",
    "vietnamese": "tem; dán tem"
  },
  {
    "english": "stand",
    "type": "v, n",
    "pronounce": "stænd",
    "vietnamese": "đứng, sự đứng. stand up: đứng đậy"
  },
  {
    "english": "standard",
    "type": "n, adj",
    "pronounce": "stændəd",
    "vietnamese": "tiêu chuẩn, chuẩn, mãu; làm tiêu chuẩn, phù hợp với tiêu chuẩn"
  },
  {
    "english": "star",
    "type": "n, v",
    "pronounce": "stɑ:",
    "vietnamese": "ngôi sao, dán sao, trang trí hình sao, đánh dấu sao"
  },
  {
    "english": "stare",
    "type": "v, n",
    "pronounce": "steə(r)",
    "vietnamese": "nhìm chằm chằm; sự nhìn chằm chằm"
  },
  {
    "english": "start",
    "type": "v, n",
    "pronounce": "stɑ:t",
    "vietnamese": "bắt đầu, khởi động; sự bắt đầu, sự khởi đầu, khởi hành"
  },
  {
    "english": "state",
    "type": "n, adj, v",
    "pronounce": "steit",
    "vietnamese": "nhà nước, quốc gia, chính quyền; (thuộc) nhà nước, có liên quan đến nhà nước; phát biểu, tuyên bố"
  },
  {
    "english": "statement",
    "type": "n",
    "pronounce": "steitmənt",
    "vietnamese": "sự bày tỏ, sự phát biểu; sự tuyên bố, sự trình bày"
  },
  {
    "english": "station",
    "type": "n",
    "pronounce": "steiʃn",
    "vietnamese": "trạm, điểm, đồn"
  },
  {
    "english": "statue",
    "type": "n",
    "pronounce": "stæt∫u:",
    "vietnamese": "tượng"
  },
  {
    "english": "status",
    "type": "n",
    "pronounce": "ˈsteɪtəs , ˈstætəs",
    "vietnamese": "tình trạng"
  },
  {
    "english": "stay",
    "type": "v, n",
    "pronounce": "stei",
    "vietnamese": "ở lại, lưu lại; sự ở lại, sự lưu lại"
  },
  {
    "english": "steadily",
    "type": "adv",
    "pronounce": "stedili",
    "vietnamese": "vững chắc, vững vàng, kiên định"
  },
  {
    "english": "steady",
    "type": "adj",
    "pronounce": "stedi",
    "vietnamese": "vững chắc, vững vàng, kiến định"
  },
  {
    "english": "steal",
    "type": "v",
    "pronounce": "sti:l",
    "vietnamese": "ăn cắp, ăn trộm"
  },
  {
    "english": "steam",
    "type": "n",
    "pronounce": "stim",
    "vietnamese": "hơi nước"
  },
  {
    "english": "steel",
    "type": "n",
    "pronounce": "sti:l",
    "vietnamese": "thép, ngành thép"
  },
  {
    "english": "steep",
    "type": "adj",
    "pronounce": "sti:p",
    "vietnamese": "dốc, dốc đứng"
  },
  {
    "english": "steeply",
    "type": "adv",
    "pronounce": "sti:pli",
    "vietnamese": "dốc, cheo leo"
  },
  {
    "english": "steer",
    "type": "v",
    "pronounce": "stiə",
    "vietnamese": "lái (tàu, ô tô...)"
  },
  {
    "english": "step",
    "type": "n, v",
    "pronounce": "step",
    "vietnamese": "bước; bước, bước đi"
  },
  {
    "english": "stick",
    "type": "v, n",
    "pronounce": "stick",
    "vietnamese": "đâm, thọc, chọc, cắm, dính; cái gậy, qua củi, cán"
  },
  {
    "english": "stick out, stick for",
    "type": "",
    "pronounce": "",
    "vietnamese": "đòi, đạt được cái gì"
  },
  {
    "english": "sticky",
    "type": "adj",
    "pronounce": "stiki",
    "vietnamese": "dính; sánh; bầy nhầy, nhớp nháp"
  },
  {
    "english": "stiff",
    "type": "adj",
    "pronounce": "stif",
    "vietnamese": "cứng, cứng rắn, kiên quyết"
  },
  {
    "english": "stiffly",
    "type": "adv",
    "pronounce": "stifli",
    "vietnamese": "cứng, cứng rắn, kiên quyết"
  },
  {
    "english": "still",
    "type": "adv, adj",
    "pronounce": "stil",
    "vietnamese": "đứng yên; vẫn, vẫn còn"
  },
  {
    "english": "sting",
    "type": "v, n",
    "pronounce": "stiɳ",
    "vietnamese": "châm, chích, đốt; ngòi, vòi (ong, muỗi), nọc (rắn); sự châm, chích.."
  },
  {
    "english": "stir",
    "type": "v",
    "pronounce": "stə:",
    "vietnamese": "khuấy, đảo"
  },
  {
    "english": "stock",
    "type": "n",
    "pronounce": "stə:",
    "vietnamese": "kho sự trữ, hàng dự trữ, vốn"
  },
  {
    "english": "stomach",
    "type": "n",
    "pronounce": "ˈstʌmək",
    "vietnamese": "dạ dày"
  },
  {
    "english": "stone",
    "type": "n",
    "pronounce": "stoun",
    "vietnamese": "đá"
  },
  {
    "english": "stop",
    "type": "v, n",
    "pronounce": "stɔp",
    "vietnamese": "dừng, ngưng, nghỉ, thôi; sự dừng, sự ngưng, sự đỗ lại"
  },
  {
    "english": "store",
    "type": "n, v",
    "pronounce": "stɔ:",
    "vietnamese": "cửa hàng, kho hàng; tích trữ, cho vào kho"
  },
  {
    "english": "storm",
    "type": "n",
    "pronounce": "stɔ:m",
    "vietnamese": "cơn giông, b~o"
  },
  {
    "english": "story",
    "type": "n",
    "pronounce": "stɔ:ri",
    "vietnamese": "chuyện, câu chuyện"
  },
  {
    "english": "stove",
    "type": "n",
    "pronounce": "stouv",
    "vietnamese": "bếp lò, lò sưởi"
  },
  {
    "english": "straight",
    "type": "adv, adj",
    "pronounce": "streɪt",
    "vietnamese": "thẳng, không cong"
  },
  {
    "english": "strain",
    "type": "n",
    "pronounce": "strein",
    "vietnamese": "sự căng thẳng, sự căng"
  },
  {
    "english": "strange",
    "type": "adj",
    "pronounce": "streindʤ",
    "vietnamese": "xa lạ, chưa quen"
  },
  {
    "english": "strangely",
    "type": "adv",
    "pronounce": "streindʤli",
    "vietnamese": "lạ, xa lạ, chưa quen"
  },
  {
    "english": "stranger",
    "type": "n",
    "pronounce": "streinʤə",
    "vietnamese": "người lạ"
  },
  {
    "english": "strategy",
    "type": "n",
    "pronounce": "strætəʤɪ",
    "vietnamese": "chiến lược"
  },
  {
    "english": "stream",
    "type": "n",
    "pronounce": "stri:m",
    "vietnamese": "dòng suối"
  },
  {
    "english": "strength",
    "type": "n",
    "pronounce": "streɳθ",
    "vietnamese": "sức mạnh, sức khỏe"
  },
  {
    "english": "stress",
    "type": "n, v",
    "pronounce": "",
    "vietnamese": "sự căng thẳng; căng thẳng, ép, làm căng"
  },
  {
    "english": "stressed",
    "type": "adj",
    "pronounce": "strest",
    "vietnamese": "bị căng thẳng, bị ép, bị căng"
  },
  {
    "english": "stretch",
    "type": "v",
    "pronounce": "strɛtʃ",
    "vietnamese": "căng ra, duỗi ra, kéo dài ra"
  },
  {
    "english": "strict",
    "type": "adj",
    "pronounce": "strikt",
    "vietnamese": "nghiêm khắc, chặt chẽ,, khắt khe"
  },
  {
    "english": "strictly",
    "type": "adv",
    "pronounce": "striktli",
    "vietnamese": "một cách nghiêm khắc"
  },
  {
    "english": "strike",
    "type": "v, n",
    "pronounce": "straik",
    "vietnamese": "đánh, đập, bãi công, đình công; cuộc bãi công, cuộc đình công"
  },
  {
    "english": "striking",
    "type": "adj",
    "pronounce": "straikiɳ",
    "vietnamese": "nổi bật, gây ấn tượng"
  },
  {
    "english": "string",
    "type": "n",
    "pronounce": "strɪŋ",
    "vietnamese": "dây, sợi dây"
  },
  {
    "english": "strip",
    "type": "v, n",
    "pronounce": "strip",
    "vietnamese": "cởi, lột (quần áo); sự cởi quần áo"
  },
  {
    "english": "stripe",
    "type": "n",
    "pronounce": "straɪp",
    "vietnamese": "sọc, vằn, viền"
  },
  {
    "english": "striped",
    "type": "adj",
    "pronounce": "straipt",
    "vietnamese": "có sọc, có vằn"
  },
  {
    "english": "stroke",
    "type": "n, v",
    "pronounce": "strouk",
    "vietnamese": "cú đánh, cú đòn; cái vuốt ve, sự vuốt ve; vuốt ve"
  },
  {
    "english": "strong",
    "type": "adj",
    "pronounce": "strɔŋ , strɒŋ",
    "vietnamese": "khỏe, mạnh, bền, vững, chắc chắn"
  },
  {
    "english": "strongly",
    "type": "adv",
    "pronounce": "strɔŋli",
    "vietnamese": "khỏe, chắc chắn"
  },
  {
    "english": "structure",
    "type": "n",
    "pronounce": "strʌkt∫ə",
    "vietnamese": "kết cấu, cấu trúc"
  },
  {
    "english": "struggle",
    "type": "v, n",
    "pronounce": "strʌg(ə)l",
    "vietnamese": "đấu tranh; cuộc đấu tranh, cuộc chiến đấu"
  },
  {
    "english": "student",
    "type": "n",
    "pronounce": "stju:dnt",
    "vietnamese": "sinh viên"
  },
  {
    "english": "studio",
    "type": "n",
    "pronounce": "́stju:diou",
    "vietnamese": "xưởng phim, trường quay; phòng thu"
  },
  {
    "english": "study",
    "type": "n, v",
    "pronounce": "stʌdi",
    "vietnamese": "sự học tập, sự nghiên cứu; học tập, nghiên cứu"
  },
  {
    "english": "stuff",
    "type": "n",
    "pronounce": "stʌf",
    "vietnamese": "chất liệu, chất"
  },
  {
    "english": "stupid",
    "type": "adj",
    "pronounce": "ˈstupɪd , ˈstyupɪd",
    "vietnamese": "ngu ngốc, ngu đần, ngớ ngẩn"
  },
  {
    "english": "style",
    "type": "n",
    "pronounce": "stail",
    "vietnamese": "phong cách, kiểu, mẫu, loại"
  },
  {
    "english": "subject",
    "type": "n",
    "pronounce": "ˈsʌbdʒɪkt",
    "vietnamese": "chủ đề, đề tài; chủ ngữ"
  },
  {
    "english": "substance",
    "type": "n",
    "pronounce": "sʌbstəns",
    "vietnamese": "chất liệu; bản chất; nội dung"
  },
  {
    "english": "substantial",
    "type": "adj",
    "pronounce": "səb ́stænʃəl",
    "vietnamese": "thực tế, đáng kể, quan trọng"
  },
  {
    "english": "substantially",
    "type": "adv",
    "pronounce": "səb ́stænʃəli",
    "vietnamese": "về thực chất, về căn bản"
  },
  {
    "english": "substitute",
    "type": "n, v",
    "pronounce": "́sʌbsti ̧tju:t",
    "vietnamese": "người, vật thay thế; thay thế"
  },
  {
    "english": "succeed",
    "type": "v",
    "pronounce": "s>ək'si:d",
    "vietnamese": "nối tiếp, kế tiếp; kế nghiệp, kế vị"
  },
  {
    "english": "success",
    "type": "n",
    "pronounce": "sək'si:d",
    "vietnamese": "sự thành công, sự thành đạt"
  },
  {
    "english": "successful",
    "type": "adj",
    "pronounce": "səkˈsɛsfəl",
    "vietnamese": "thành công, thắng lợi, thành đạt"
  },
  {
    "english": "successfully",
    "type": "adv",
    "pronounce": "səkˈsɛsfəlli",
    "vietnamese": "thành công, thắng lợi, thành đạt"
  },
  {
    "english": "such",
    "type": "det, pron",
    "pronounce": "sʌtʃ",
    "vietnamese": "như thế, như vậy, như là. such as đến nỗi, đến mức"
  },
  {
    "english": "suck",
    "type": "v",
    "pronounce": "sʌk",
    "vietnamese": "bú, hút; hấp thụ, tiếp thu"
  },
  {
    "english": "sudden",
    "type": "adj",
    "pronounce": "sʌdn",
    "vietnamese": "thình lình, đột ngột"
  },
  {
    "english": "suddenly",
    "type": "adv",
    "pronounce": "sʌdnli",
    "vietnamese": "thình lình, đột ngột"
  },
  {
    "english": "suf",
    "type": "n",
    "pronounce": "stri:t",
    "vietnamese": "phố, đường phố"
  },
  {
    "english": "suffer",
    "type": "v",
    "pronounce": "sΛfə(r)",
    "vietnamese": "chịu đựng, chịu thiệt hại, đấu khổ"
  },
  {
    "english": "suffering",
    "type": "n",
    "pronounce": "sΛfəriŋ",
    "vietnamese": "sự đau đớn, sự đau khổ"
  },
  {
    "english": "sufficient",
    "type": "adj",
    "pronounce": "sə'fi∫nt",
    "vietnamese": "(+ for) đủ, thích đáng"
  },
  {
    "english": "sufficiently",
    "type": "adv",
    "pronounce": "sə'fiʃəntli",
    "vietnamese": "đủ, thích đáng"
  },
  {
    "english": "sugar",
    "type": "n",
    "pronounce": "ʃugə",
    "vietnamese": "đường"
  },
  {
    "english": "suggest",
    "type": "v",
    "pronounce": "sə'dʤest",
    "vietnamese": "đề nghị, đề xuất; gợi"
  },
  {
    "english": "suggestion",
    "type": "n",
    "pronounce": "sə'dʤestʃn",
    "vietnamese": "sự đề nghị, sự đề xuất, sự khêu gợi"
  },
  {
    "english": "suit",
    "type": "n, v",
    "pronounce": "su:t",
    "vietnamese": "bộ com lê, trang phục; thích hợp, quen, hợp với"
  },
  {
    "english": "suitable",
    "type": "adj",
    "pronounce": "́su:təbl",
    "vietnamese": "hợp, phù hợp, thích hợp với"
  },
  {
    "english": "suitcase",
    "type": "n",
    "pronounce": "́su:t ̧keis",
    "vietnamese": "va li"
  },
  {
    "english": "suited",
    "type": "adj",
    "pronounce": "́su:tid",
    "vietnamese": "hợp, phù hợp, thích hợp với"
  },
  {
    "english": "sum",
    "type": "n",
    "pronounce": "sʌm",
    "vietnamese": "tổng, toàn bộ"
  },
  {
    "english": "summary",
    "type": "n",
    "pronounce": "ˈsʌməri",
    "vietnamese": "bản tóm tắt"
  },
  {
    "english": "summer",
    "type": "n",
    "pronounce": "ˈsʌmər",
    "vietnamese": "mùa hè"
  },
  {
    "english": "sun",
    "type": "n",
    "pronounce": "sʌn",
    "vietnamese": "mặt trời"
  },
  {
    "english": "Sunday",
    "type": "n",
    "pronounce": "́sʌndi",
    "vietnamese": "Chủ nhật"
  },
  {
    "english": "superior",
    "type": "adj",
    "pronounce": "su:'piəriə(r)",
    "vietnamese": "cao, chất lượng cao"
  },
  {
    "english": "supermarket",
    "type": "n",
    "pronounce": "́su:pə ̧ma:kit",
    "vietnamese": "siêu thị"
  },
  {
    "english": "supply",
    "type": "n, v",
    "pronounce": "sə'plai",
    "vietnamese": "sự cung cấp, nguồn cung cấp; cung cấp, đáp ứng, tiếp tế"
  },
  {
    "english": "support",
    "type": "n, v",
    "pronounce": "sə ́pɔ:t",
    "vietnamese": "sự chống đỡ, sự ủng hộ; chống đỡ, ủng hộ"
  },
  {
    "english": "supporter",
    "type": "n",
    "pronounce": "sə ́pɔ:tə",
    "vietnamese": "vật chống đỡ; người cổ vũ, người ủng hộ"
  },
  {
    "english": "suppose",
    "type": "v",
    "pronounce": "sə'pəƱz",
    "vietnamese": "cho rằng, tin rằng, nghĩ rằng"
  },
  {
    "english": "sure",
    "type": "adj, adv",
    "pronounce": "ʃuə",
    "vietnamese": "chắc chắn, xác thực. make sure chắc chắn, làm cho chắc chắn"
  },
  {
    "english": "surely",
    "type": "adv",
    "pronounce": "́ʃuəli",
    "vietnamese": "chắc chắn"
  },
  {
    "english": "surface",
    "type": "n",
    "pronounce": "ˈsɜrfɪs",
    "vietnamese": "mặt, bề mặt"
  },
  {
    "english": "surname",
    "type": "n",
    "pronounce": "ˈsɜrˌneɪm",
    "vietnamese": "họ"
  },
  {
    "english": "surprise",
    "type": "n, v",
    "pronounce": "sə'praiz",
    "vietnamese": "sự ngạc nhiên, sự bất ngờ; làm ngạc nhiên, gây bất ngờ"
  },
  {
    "english": "surprised",
    "type": "adj",
    "pronounce": "sə: ́praizd",
    "vietnamese": "ngạc nhiên (+ at)"
  },
  {
    "english": "surprising",
    "type": "adj",
    "pronounce": "sə: ́praiziη",
    "vietnamese": "làm ngạc nhiên, làm bất ngờ"
  },
  {
    "english": "surprisingly",
    "type": "adv",
    "pronounce": "sə'praiziηli",
    "vietnamese": "làm ngạc nhiên, làm bất ngờ"
  },
  {
    "english": "surround",
    "type": "v",
    "pronounce": "sə'raƱnd",
    "vietnamese": "vây quanh, bao quanh"
  },
  {
    "english": "surrounding",
    "type": "adj",
    "pronounce": "sə.ˈrɑʊ(n)diɳ",
    "vietnamese": "sự vây quanh, sự bao quanh"
  },
  {
    "english": "surroundings",
    "type": "n",
    "pronounce": "sə ́raundiηz",
    "vietnamese": "vùng xung quanh, môi trường xung quanh"
  },
  {
    "english": "survey",
    "type": "n, v",
    "pronounce": "sə:vei",
    "vietnamese": "sự nhìn chung, sự khảo sát; quan sát, nhìn chung, khảo sát, nghiên cứu"
  },
  {
    "english": "survive",
    "type": "v",
    "pronounce": "sə'vaivə",
    "vietnamese": "sống lâu hơn, tiếp tục sống, sống sót"
  },
  {
    "english": "suspect",
    "type": "v, n",
    "pronounce": "səs ́pekt",
    "vietnamese": "nghi ngờ, hoài nghi; người khả nghi, người bị tình nghi"
  },
  {
    "english": "suspicion",
    "type": "n",
    "pronounce": "səs'pi∫n",
    "vietnamese": "sự nghi ngờ, sự ngờ vực"
  },
  {
    "english": "suspicious",
    "type": "adj",
    "pronounce": "səs ́piʃəs",
    "vietnamese": "có sự nghi ngờ, tỏ ra nghi ngờ, khả nghi"
  },
  {
    "english": "swallow",
    "type": "v",
    "pronounce": "swɔlou",
    "vietnamese": "nuốt, nuốt chửng"
  },
  {
    "english": "swear",
    "type": "v",
    "pronounce": "sweə",
    "vietnamese": "chửi rủa, nguyền rủa; thề, hứa"
  },
  {
    "english": "swearing",
    "type": "n",
    "pronounce": "",
    "vietnamese": "lời thề, lời nguyền rủa, lời thề hứa"
  },
  {
    "english": "sweat",
    "type": "n, v",
    "pronounce": "swet",
    "vietnamese": "mồ hôi; đổ mồ hôi"
  },
  {
    "english": "sweater",
    "type": "n",
    "pronounce": "swetз",
    "vietnamese": "người ra mồ hôi,, kẻ bóc lột lấo động"
  },
  {
    "english": "sweep",
    "type": "v",
    "pronounce": "swi:p",
    "vietnamese": "quét"
  },
  {
    "english": "sweet",
    "type": "adj, n",
    "pronounce": "swi:t",
    "vietnamese": "ngọt, có vị ngọt; sự ngọt bùi, đồ ngọt"
  },
  {
    "english": "swell",
    "type": "v",
    "pronounce": "swel",
    "vietnamese": "phồng, sưng lên"
  },
  {
    "english": "swelling",
    "type": "n",
    "pronounce": "́sweliη",
    "vietnamese": "sự sưng lên, sự phồng ra"
  },
  {
    "english": "swim",
    "type": "v",
    "pronounce": "swim",
    "vietnamese": "bơi lội"
  },
  {
    "english": "swimming",
    "type": "n",
    "pronounce": "́swimiη",
    "vietnamese": "sự bơi lội"
  },
  {
    "english": "swimming pool",
    "type": "n",
    "pronounce": "",
    "vietnamese": "bể nước"
  },
  {
    "english": "swing",
    "type": "n, v",
    "pronounce": "swiŋ",
    "vietnamese": "sự đu đưa, lúc lắc; đánh đu, đu đưa, lúc lắc"
  },
  {
    "english": "switch",
    "type": "n, v",
    "pronounce": "switʃ",
    "vietnamese": "công tắc, roi; tắt, bật, đánh bằng gậy, roi. switch sth off ngắt điện. switch sth on bật điện"
  },
  {
    "english": "swollen",
    "type": "adj",
    "pronounce": "́swoulən",
    "vietnamese": "sưng phồng, phình căng"
  },
  {
    "english": "swollen swell",
    "type": "v",
    "pronounce": "́swoulən, swel",
    "vietnamese": "phồng lên, sưng lên"
  },
  {
    "english": "symbol",
    "type": "n",
    "pronounce": "simbl",
    "vietnamese": "biểu tượng, ký hiệu"
  },
  {
    "english": "sympathetic",
    "type": "adj",
    "pronounce": "̧simpə ́θetik",
    "vietnamese": "đồng cảm, đáng mến, dễ thương"
  },
  {
    "english": "sympathy",
    "type": "n",
    "pronounce": "́simpəθi",
    "vietnamese": "sự đồng cảm, sự đồng ý"
  },
  {
    "english": "system",
    "type": "n",
    "pronounce": "sistim",
    "vietnamese": "hệ thống, chế độ"
  },
  {
    "english": "table",
    "type": "n",
    "pronounce": "teibl",
    "vietnamese": "cái bàn"
  },
  {
    "english": "tablet",
    "type": "n",
    "pronounce": "tæblit",
    "vietnamese": "tấm, bản, thẻ phiến"
  },
  {
    "english": "tackle",
    "type": "v, n",
    "pronounce": "tækl or 'teikl",
    "vietnamese": "giải quyết, khắc phục, bàn thảo; đồ dùng, dụng cụ"
  },
  {
    "english": "tail",
    "type": "n",
    "pronounce": "teil",
    "vietnamese": "đuôi, đoạn cuối"
  },
  {
    "english": "take",
    "type": "v",
    "pronounce": "teik",
    "vietnamese": "sự cầm nắm, sự lấy. take sth off: cởi, bỏ cái gì, lấy đi cái gì"
  },
  {
    "english": "take care of",
    "type": "",
    "pronounce": "",
    "vietnamese": "sự giữ gìn. care for trông nom, chăm sóc"
  },
  {
    "english": "take part in",
    "type": "",
    "pronounce": "",
    "vietnamese": "tham gia (vào)"
  },
  {
    "english": "take sth over",
    "type": "",
    "pronounce": "",
    "vietnamese": "chở, chuyển cái gì; tiếp quản, kế tục cái gì"
  },
  {
    "english": "talk",
    "type": "v, n",
    "pronounce": "tɔ:k",
    "vietnamese": "nói chuyện, trò chuyện; cuộc trò chuyện, cuộc thảo luận"
  },
  {
    "english": "tall",
    "type": "adj",
    "pronounce": "tɔ:l",
    "vietnamese": "cao"
  },
  {
    "english": "tank",
    "type": "n",
    "pronounce": "tæŋk",
    "vietnamese": "thùng, két, bể"
  },
  {
    "english": "tap",
    "type": "v, n",
    "pronounce": "tæp",
    "vietnamese": "mở vòi, đóng vòi; vòi, khóa"
  },
  {
    "english": "tape",
    "type": "n",
    "pronounce": "teip",
    "vietnamese": "băng, băng ghi âm; dải, dây"
  },
  {
    "english": "target",
    "type": "n",
    "pronounce": "ta:git",
    "vietnamese": "bia, mục tiêu, đích"
  },
  {
    "english": "task",
    "type": "n",
    "pronounce": "tɑːsk",
    "vietnamese": "nhiệm vụ, nghĩa vụ, bài tập, công tác, công việc"
  },
  {
    "english": "taste",
    "type": "n, v",
    "pronounce": "teist",
    "vietnamese": "vị, vị giác; nếm"
  },
  {
    "english": "tax",
    "type": "n, v",
    "pronounce": "tæks",
    "vietnamese": "thuế; đánh thuế"
  },
  {
    "english": "taxi",
    "type": "n",
    "pronounce": "tæksi",
    "vietnamese": "xe tắc xi"
  },
  {
    "english": "tea",
    "type": "n",
    "pronounce": "ti:",
    "vietnamese": "cây chè, trà, chè"
  },
  {
    "english": "teach",
    "type": "v",
    "pronounce": "ti:tʃ",
    "vietnamese": "dạy"
  },
  {
    "english": "teacher",
    "type": "n",
    "pronounce": "ti:t∫ə",
    "vietnamese": "giáo viên"
  },
  {
    "english": "teaching",
    "type": "n",
    "pronounce": "ti:t∫iŋ",
    "vietnamese": "sự dạy, công việc dạy học"
  },
  {
    "english": "team",
    "type": "n",
    "pronounce": "ti:m",
    "vietnamese": "đội, nhóm"
  },
  {
    "english": "tear",
    "type": "v, n",
    "pronounce": "tiə",
    "vietnamese": "xé, làm rắch; chỗ rách, miếng xe; nước mắt"
  },
  {
    "english": "technical",
    "type": "adj",
    "pronounce": "teknikl",
    "vietnamese": "(thuộc) kỹ thuật, chuyên môn"
  },
  {
    "english": "technique",
    "type": "n",
    "pronounce": "tek'ni:k",
    "vietnamese": "kỹ sảo, kỹ thuật, phương pháp kỹ thuật"
  },
  {
    "english": "technology",
    "type": "n",
    "pronounce": "tek'nɔlədʤi",
    "vietnamese": "kỹ thuật học, công nghệ học"
  },
  {
    "english": "telephone (phone)",
    "type": "n, v",
    "pronounce": "́telefoun",
    "vietnamese": "máy điện thoại, gọi điện thoại"
  },
  {
    "english": "television (TV)",
    "type": "n",
    "pronounce": "́televiʒn",
    "vietnamese": "vô tuyến truyền hình"
  },
  {
    "english": "tell",
    "type": "v",
    "pronounce": "tel",
    "vietnamese": "nói, nói với"
  },
  {
    "english": "temperature",
    "type": "n",
    "pronounce": "́tempritʃə",
    "vietnamese": "nhiệt độ"
  },
  {
    "english": "temporarily",
    "type": "adv",
    "pronounce": "tempзrзlti",
    "vietnamese": "tạm"
  },
  {
    "english": "temporary",
    "type": "adj",
    "pronounce": "ˈtɛmpəˌrɛri",
    "vietnamese": "tạm thời, nhất thời"
  },
  {
    "english": "tend",
    "type": "v",
    "pronounce": "tend",
    "vietnamese": "trông nom, chăm sóc, giữ gìn, hầu hạ"
  },
  {
    "english": "tendency",
    "type": "n",
    "pronounce": "ˈtɛndənsi",
    "vietnamese": "xu hướng, chiều hướng, khuynh hướng"
  },
  {
    "english": "tension",
    "type": "n",
    "pronounce": "tenʃn",
    "vietnamese": "sự căng, độ căng, tình trạng căng"
  },
  {
    "english": "tent",
    "type": "n",
    "pronounce": "tent",
    "vietnamese": "lều, rạp"
  },
  {
    "english": "term",
    "type": "n",
    "pronounce": "tɜ:m",
    "vietnamese": "giới hạn, kỳ hạn, khóa, kỳ học"
  },
  {
    "english": "terrible",
    "type": "adj",
    "pronounce": "terəbl",
    "vietnamese": "khủng khiếp, ghê sợ"
  },
  {
    "english": "terribly",
    "type": "adv",
    "pronounce": "terəbli",
    "vietnamese": "tồi tệ, không chịu nổi"
  },
  {
    "english": "test",
    "type": "n, v",
    "pronounce": "test",
    "vietnamese": "bài kiểm tra, sự thử nghiệm, xét nghiệm; kiểm tra, xét nghiệm, thử nghiệm"
  },
  {
    "english": "text",
    "type": "n",
    "pronounce": "tɛkst",
    "vietnamese": "nguyên văn, bản văn, chủ đề, đề"
  },
  {
    "english": "than",
    "type": "prep, conj",
    "pronounce": "ðæn",
    "vietnamese": "hơn"
  },
  {
    "english": "thank",
    "type": "v",
    "pronounce": "θæŋk",
    "vietnamese": "cám ơn"
  },
  {
    "english": "thank you",
    "type": "exclamation, n",
    "pronounce": "",
    "vietnamese": "cảm ơn bạn (ông bà, anh chị...)"
  },
  {
    "english": "thanks",
    "type": "exclamation, n",
    "pronounce": "θæŋks",
    "vietnamese": "sự cảm ơn, lời cảm ơn"
  },
  {
    "english": "that",
    "type": "pron, conj, det",
    "pronounce": "ðæt",
    "vietnamese": "người ấy, đó, vật ấy, đó; rằng, là"
  },
  {
    "english": "the",
    "type": "",
    "pronounce": "ði:, ði, ðз",
    "vietnamese": "cái, con, người, ấy này...."
  },
  {
    "english": "theatre",
    "type": "n",
    "pronounce": "ˈθiətər",
    "vietnamese": "rạp hát, nhà hát"
  },
  {
    "english": "their",
    "type": "det",
    "pronounce": "ðea(r)",
    "vietnamese": "của chúng, của chúng nó, của họ"
  },
  {
    "english": "theirs",
    "type": "n, pro",
    "pronounce": "ðeəz",
    "vietnamese": "của chúng, của chúng nó, của họ"
  },
  {
    "english": "them",
    "type": "n, pro",
    "pronounce": "ðem",
    "vietnamese": "chúng, chúng nó, họ"
  },
  {
    "english": "theme",
    "type": "n",
    "pronounce": "θi:m",
    "vietnamese": "đề tài, chủ đề"
  },
  {
    "english": "themselves",
    "type": "n, pro",
    "pronounce": "ðəm'selvz",
    "vietnamese": "tự chúng, tự họ, tự"
  },
  {
    "english": "then",
    "type": "adv",
    "pronounce": "ðen",
    "vietnamese": "khi đó, lúc đó, tiếp đó"
  },
  {
    "english": "theory",
    "type": "n",
    "pronounce": "θiəri",
    "vietnamese": "lý thuyết, học thuyết"
  },
  {
    "english": "there",
    "type": "adv",
    "pronounce": "ðeз",
    "vietnamese": "ở nơi đó, tại nơi đó"
  },
  {
    "english": "therefore",
    "type": "adv",
    "pronounce": "ðeəfɔ:(r)",
    "vietnamese": "bởi vậy, cho nên, vì thế"
  },
  {
    "english": "they",
    "type": "n, pro",
    "pronounce": "ðei",
    "vietnamese": "chúng, chúng nó, họ; những cái ấy"
  },
  {
    "english": "thick",
    "type": "adj",
    "pronounce": "θik",
    "vietnamese": "dày; đậm"
  },
  {
    "english": "thickly",
    "type": "adv",
    "pronounce": "θikli",
    "vietnamese": "dày; dày đặc; thành lớp dày"
  },
  {
    "english": "thickness",
    "type": "n",
    "pronounce": "́θiknis",
    "vietnamese": "tính chất dày, độ dày, bề dày"
  },
  {
    "english": "thief",
    "type": "n",
    "pronounce": "θi:f",
    "vietnamese": "kẻ trộm, kẻ cắp"
  },
  {
    "english": "thin",
    "type": "adj",
    "pronounce": "θin",
    "vietnamese": "mỏng, mảnh"
  },
  {
    "english": "thing",
    "type": "n",
    "pronounce": "θiŋ",
    "vietnamese": "cái, đồ, vật"
  },
  {
    "english": "think",
    "type": "v",
    "pronounce": "θiŋk",
    "vietnamese": "nghĩ, suy nghĩ"
  },
  {
    "english": "thinking",
    "type": "n",
    "pronounce": "θiŋkiŋ",
    "vietnamese": "sự suy nghĩ, ý nghĩ"
  },
  {
    "english": "thirsty",
    "type": "adj",
    "pronounce": "́θə:sti",
    "vietnamese": "khát, cảm thấy khát"
  },
  {
    "english": "this",
    "type": "n, det, pro",
    "pronounce": "ðis",
    "vietnamese": "cái này, điều này, việc này"
  },
  {
    "english": "thorough",
    "type": "adj",
    "pronounce": "θʌrə",
    "vietnamese": "cẩn thận, kỹ lưỡng"
  },
  {
    "english": "thoroughly",
    "type": "adv",
    "pronounce": "θʌrəli",
    "vietnamese": "kỹ lưỡng, thấu đáo, triệt để"
  },
  {
    "english": "though",
    "type": "adv, conj",
    "pronounce": "ðəʊ",
    "vietnamese": "dù, dù cho, mặc dù; mặc dù, tuy >nhiên, tuy vậy"
  },
  {
    "english": "thought",
    "type": "n",
    "pronounce": "θɔ:t",
    "vietnamese": "sự suy nghĩ, khả >năng suy nghĩ; ý >nghĩ, tư tưởng, tư duy"
  },
  {
    "english": "thread",
    "type": "n",
    "pronounce": "θred",
    "vietnamese": "chỉ, sợi chỉ, sợi dây"
  },
  {
    "english": "threat",
    "type": "n",
    "pronounce": "θrɛt",
    "vietnamese": "sự đe dọa, lời đe dọa"
  },
  {
    "english": "threaten",
    "type": "v",
    "pronounce": "θretn",
    "vietnamese": "dọa, đe dọa"
  },
  {
    "english": "threatening",
    "type": "adj",
    "pronounce": "́θretəniη",
    "vietnamese": "sự đe dọa, sự hăm dọa"
  },
  {
    "english": "throat",
    "type": "n",
    "pronounce": "θrout",
    "vietnamese": "cổ, cổ họng"
  },
  {
    "english": "through",
    "type": "adv, prep",
    "pronounce": "θru:",
    "vietnamese": "qua, xuyên qua"
  },
  {
    "english": "throughout",
    "type": "adv, prep",
    "pronounce": "θru:'aut",
    "vietnamese": "khắp, suốt"
  },
  {
    "english": "throw",
    "type": "v",
    "pronounce": "θrou",
    "vietnamese": "ném, vứt, quăng. throw sth away: >ném đi, vứt đi, liệng đi"
  },
  {
    "english": "thumb",
    "type": "n",
    "pronounce": "θʌm",
    "vietnamese": "ngón tay cái"
  },
  {
    "english": "Thursday (abbr Thur, Thurs)",
    "type": "n",
    "pronounce": "́θə:zdi",
    "vietnamese": "thứ 5"
  },
  {
    "english": "thus",
    "type": "adv",
    "pronounce": "ðʌs",
    "vietnamese": "như vậy, như thế, do đó"
  },
  {
    "english": "ticket",
    "type": "n",
    "pronounce": "tikit",
    "vietnamese": "vé"
  },
  {
    "english": "tidy",
    "type": "adj, v",
    "pronounce": "́taidi",
    "vietnamese": "sạch sẽ, ngăn nắp, gọn gàng; làm >cho sạch sẽ, gọn gàng, ngăn nắp"
  },
  {
    "english": "tie",
    "type": "v, n",
    "pronounce": "tai",
    "vietnamese": "buộc, cột, trói; dây buộc, dây trói, >dây giày. tie sth up có quan hệ >mật >thiết, gắn chặt"
  },
  {
    "english": "tight",
    "type": "adj, adv",
    "pronounce": "tait",
    "vietnamese": "kín, chặt, chật"
  },
  {
    "english": "tightly",
    "type": "adv",
    "pronounce": "taitli",
    "vietnamese": "chặt chẽ, sít sao"
  },
  {
    "english": "till, until",
    "type": "til",
    "pronounce": "cho đến khi, tới lúc mà",
    "vietnamese": ""
  },
  {
    "english": "time",
    "type": "n",
    "pronounce": "taim",
    "vietnamese": "thời gian, thì giờ"
  },
  {
    "english": "timetable",
    "type": "n",
    "pronounce": "́taimteibl",
    "vietnamese": "kế hoạch làm việc, thời gian biểu"
  },
  {
    "english": "tin",
    "type": "n",
    "pronounce": "tɪn",
    "vietnamese": "thiếc"
  },
  {
    "english": "tiny",
    "type": "adj",
    "pronounce": "taini",
    "vietnamese": "rất nhỏ, nhỏ xíu"
  },
  {
    "english": "tip",
    "type": "n, v",
    "pronounce": "tip",
    "vietnamese": "đầu, mút, đỉnh, chóp; bịt đầu, lắp >đầu vào"
  },
  {
    "english": "tire",
    "type": "v",
    "pronounce": "taiз",
    "vietnamese": "làm mệt mỏi, trở nên mệt nhọc; >lốp, vỏ xe"
  },
  {
    "english": "tired",
    "type": "adj",
    "pronounce": "taɪəd",
    "vietnamese": "mệt, muốn ngủ, nhàm chán"
  },
  {
    "english": "tiring",
    "type": "adj",
    "pronounce": "́taiəriη",
    "vietnamese": "sự mệt mỏi, sự mệt nhọc"
  },
  {
    "english": "title",
    "type": "n",
    "pronounce": "taɪtl",
    "vietnamese": "đầu đề, tiêu đề; tước vị, tư cách"
  },
  {
    "english": "to",
    "type": "prep,",
    "pronounce": "tu:, tu, tз",
    "vietnamese": "theo hướng, tới"
  },
  {
    "english": "today",
    "type": "adv, n",
    "pronounce": "tə'dei",
    "vietnamese": "vào ngày này; hôm nay, ngày nay"
  },
  {
    "english": "toe",
    "type": "n",
    "pronounce": "tou",
    "vietnamese": "ngón chân (người)"
  },
  {
    "english": "together",
    "type": "adv",
    "pronounce": "tə'geðə",
    "vietnamese": "cùng nhau, cùng với"
  },
  {
    "english": "toilet",
    "type": "n",
    "pronounce": "́tɔilit",
    "vietnamese": "nhà vệ sinh; sự trang điểm (rửa >mặt, ăn mặc, chải tóc...)"
  },
  {
    "english": "tomato",
    "type": "n",
    "pronounce": "tə ́ma:tou",
    "vietnamese": "cà chua"
  },
  {
    "english": "tomorrow",
    "type": "adv, n",
    "pronounce": "tə'mɔrou",
    "vietnamese": "vào ngày mai; ngày mai"
  },
  {
    "english": "ton",
    "type": "n",
    "pronounce": "tΔn",
    "vietnamese": "tấn"
  },
  {
    "english": "tone",
    "type": "n",
    "pronounce": "toun",
    "vietnamese": "tiếng, giọng"
  },
  {
    "english": "tongue",
    "type": "n",
    "pronounce": "tʌη",
    "vietnamese": "lưỡi"
  },
  {
    "english": "tonight",
    "type": "adv, n",
    "pronounce": "tə ́nait",
    "vietnamese": "vào đêm nay, vào tối nay; đêm >nay, tối nay"
  },
  {
    "english": "tonne",
    "type": "n",
    "pronounce": "tʌn",
    "vietnamese": "tấn"
  },
  {
    "english": "too",
    "type": "adv",
    "pronounce": "tu:",
    "vietnamese": "cũng"
  },
  {
    "english": "tool",
    "type": "n",
    "pronounce": "tu:l",
    "vietnamese": "dụng cụ, đồ dùng"
  },
  {
    "english": "tooth",
    "type": "n",
    "pronounce": "tu:θ",
    "vietnamese": "răng"
  },
  {
    "english": "top",
    "type": "n, adj",
    "pronounce": "tɒp",
    "vietnamese": "chóp, đỉnh; đứng đầu, trên hết"
  },
  {
    "english": "topic",
    "type": "n",
    "pronounce": "tɒpɪk",
    "vietnamese": "đề tài, chủ đề"
  },
  {
    "english": "total",
    "type": "adj, n",
    "pronounce": "toutl",
    "vietnamese": "tổng cộng, toàn bộ; tổng số, toàn >bộ số lượng"
  },
  {
    "english": "totally",
    "type": "adv",
    "pronounce": "toutli",
    "vietnamese": "hoàn toàn"
  },
  {
    "english": "touch",
    "type": "v, n",
    "pronounce": "tʌtʃ",
    "vietnamese": "sờ, mó, tiếp xúc; sự sờ, sự mó, sự >tiếp xúc"
  },
  {
    "english": "tough",
    "type": "adj",
    "pronounce": "tʌf",
    "vietnamese": "chắc, bền, dai"
  },
  {
    "english": "tour",
    "type": "n, v",
    "pronounce": "tuə",
    "vietnamese": "cuộc đo du lịch, cuộc đi dạo, >chuyến du lịch; đi du lịch"
  },
  {
    "english": "tourist",
    "type": "n",
    "pronounce": "tuərist",
    "vietnamese": "khách du lịch"
  },
  {
    "english": "towards",
    "type": "prep",
    "pronounce": "tə ́wɔ:dz",
    "vietnamese": "theo hướng, về hướng"
  },
  {
    "english": "towel",
    "type": "n",
    "pronounce": "taʊəl",
    "vietnamese": "khăn tắm, khăn lấu"
  },
  {
    "english": "tower",
    "type": "n",
    "pronounce": "tauə",
    "vietnamese": "tháp"
  },
  {
    "english": "town",
    "type": "n",
    "pronounce": "taun",
    "vietnamese": "thị trấn, thị xã, thành phố nhỏ"
  },
  {
    "english": "toy",
    "type": "n, adj",
    "pronounce": "tɔi",
    "vietnamese": "đồ chơi, đồ trang trí; thể loại đồ >chơi"
  },
  {
    "english": "trace",
    "type": "v, n",
    "pronounce": "treis",
    "vietnamese": "phát hiện, tìm thấy, vạch, chỉ ra, >phác họa; dấu, vết, một chút"
  },
  {
    "english": "track",
    "type": "n",
    "pronounce": "træk",
    "vietnamese": "phần của đĩa; đường mòn, đường >đua"
  },
  {
    "english": "trade",
    "type": "n, v",
    "pronounce": "treid",
    "vietnamese": "thương mại, buôn bán; buôn bán, >trao đổi"
  },
  {
    "english": "trading",
    "type": "n",
    "pronounce": "treidiη",
    "vietnamese": "sự kinh >doanh, việc mua bán"
  },
  {
    "english": "tradition",
    "type": "n",
    "pronounce": "trə ́diʃən",
    "vietnamese": "truyền thống"
  },
  {
    "english": "traditional",
    "type": "adj",
    "pronounce": "trə ́diʃənəl",
    "vietnamese": "theo truyền thống, theo lối cổ"
  },
  {
    "english": "traditionally",
    "type": "adv",
    "pronounce": "trə ́diʃənəlli",
    "vietnamese": "(thuộc) truyền thống, là truyền >thống"
  },
  {
    "english": "traffic",
    "type": "n",
    "pronounce": "træfik",
    "vietnamese": "sự đi lại, sự giao thông, sự >chuyển >động"
  },
  {
    "english": "train",
    "type": "n, v",
    "pronounce": "trein",
    "vietnamese": "xe lửa, tàu hỏa; dạy, rèn luyện, >đào tạo"
  },
  {
    "english": "training",
    "type": "n",
    "pronounce": "trainiŋ",
    "vietnamese": "sự dạy dỗ, sự huấn luyện, sự đào >tạo"
  },
  {
    "english": "transfer",
    "type": "v, n",
    "pronounce": "trænsfə:",
    "vietnamese": "dời, di chuyển; sự di chuyển, sự >dời chỗ"
  },
  {
    "english": "transform",
    "type": "v",
    "pronounce": "træns'fɔ:m",
    "vietnamese": "thay đổi, biến đổi"
  },
  {
    "english": "translate",
    "type": "v",
    "pronounce": "træns ́leit",
    "vietnamese": "dịch, biên dịch, phiên dịch"
  },
  {
    "english": "translation",
    "type": "n",
    "pronounce": "træns'leiʃn",
    "vietnamese": "sự dịch"
  },
  {
    "english": "transparent",
    "type": "adj",
    "pronounce": "træns ́pærənt",
    "vietnamese": "trong suốt; dễ hiểu, sáng sủa"
  },
  {
    "english": "transport",
    "type": "n",
    "pronounce": "trænspɔ:t",
    "vietnamese": "sự vận chuyển, sự vận tải; >phương tiện đi lại"
  },
  {
    "english": "trap",
    "type": "n, v",
    "pronounce": "træp",
    "vietnamese": "đồ đạc, hành lý; bẫy, cạm bãy; >bẫy, giữ, chặn lại"
  },
  {
    "english": "travel",
    "type": "v, n",
    "pronounce": "trævl",
    "vietnamese": "đi lại, đi du lịch, di chuyển; sự đi, >những chuyến đi"
  },
  {
    "english": "traveller",
    "type": "n",
    "pronounce": "trævlə",
    "vietnamese": "người đi, lữ khách"
  },
  {
    "english": "treat",
    "type": "v",
    "pronounce": "tri:t",
    "vietnamese": "đối xử, đối đãi, cư xử"
  },
  {
    "english": "treatment",
    "type": "n",
    "pronounce": "tri:tmənt",
    "vietnamese": "sự đối xử, >sự cư xử"
  },
  {
    "english": "tree",
    "type": "n",
    "pronounce": "tri:",
    "vietnamese": "cây"
  },
  {
    "english": "trend",
    "type": "n",
    "pronounce": "trend",
    "vietnamese": "phương hướng, xu hướng, chiều >hướng"
  },
  {
    "english": "trial",
    "type": "n",
    "pronounce": "traiəl",
    "vietnamese": "sự thử nghiệm, cuộc thử nghiệm"
  },
  {
    "english": "triangle",
    "type": "n",
    "pronounce": "́trai ̧æηgl",
    "vietnamese": "hình tam giác"
  },
  {
    "english": "trick",
    "type": "n, v",
    "pronounce": "trik",
    "vietnamese": "mưu mẹo, thủ đoạn, trò lừa gạt, >lừa gạt"
  },
  {
    "english": "trip",
    "type": "n,v",
    "pronounce": "trip",
    "vietnamese": "cuộc dạo chơi, cuộc du ngoạn; đi >dạo, du ngoạn"
  },
  {
    "english": "tropical",
    "type": "adj",
    "pronounce": "́trɔpikəl",
    "vietnamese": "nhiệt đới"
  },
  {
    "english": "trouble",
    "type": "n",
    "pronounce": "trʌbl",
    "vietnamese": "điều lo lắng, điều muộn phiền"
  },
  {
    "english": "trousers",
    "type": "n",
    "pronounce": "́trauzə:z",
    "vietnamese": "quần tây"
  },
  {
    "english": "truck",
    "type": "n",
    "pronounce": "trʌk",
    "vietnamese": "sự trao đổi, sự đổi chác"
  },
  {
    "english": "TRUE",
    "type": "adj",
    "pronounce": "tru:",
    "vietnamese": "đúng, thật"
  },
  {
    "english": "truly",
    "type": "adv",
    "pronounce": "tru:li",
    "vietnamese": "đúng sự thật, đích thực, thực sự"
  },
  {
    "english": "trust",
    "type": "n, v",
    "pronounce": "trʌst",
    "vietnamese": "niềm tin, sự phó thác; tin, tin cậy, phó thác"
  },
  {
    "english": "truth",
    "type": "n",
    "pronounce": "tru:θ",
    "vietnamese": "sự thật"
  },
  {
    "english": "try",
    "type": "v",
    "pronounce": "trai",
    "vietnamese": "thử, cố gắng"
  },
  {
    "english": "tube",
    "type": "n",
    "pronounce": "tju:b",
    "vietnamese": "ống, tuýp"
  },
  {
    "english": "Tuesday (abbr Tue, Tues)",
    "type": "n",
    "pronounce": "́tju:zdi",
    "vietnamese": "thứ 3"
  },
  {
    "english": "tune",
    "type": "n, v",
    "pronounce": "tun , tyun",
    "vietnamese": "điệu, giai điệu; lên dây, so dây (đàn)"
  },
  {
    "english": "tunnel",
    "type": "n",
    "pronounce": "tʌnl",
    "vietnamese": "đường hầm, hang"
  },
  {
    "english": "turn",
    "type": "v, n",
    "pronounce": "tə:n",
    "vietnamese": "quay, xoay, vặn; sự quay, vòng quay"
  },
  {
    "english": "TV television",
    "type": "",
    "pronounce": "",
    "vietnamese": "vô tuyến truyền hình"
  },
  {
    "english": "twice",
    "type": "adv",
    "pronounce": "twaɪs",
    "vietnamese": "hai lần"
  },
  {
    "english": "twin",
    "type": "n, adj",
    "pronounce": "twɪn",
    "vietnamese": "sinh đôi, tạo thành cặp; cặp song sinh"
  },
  {
    "english": "twist",
    "type": "v, n",
    "pronounce": "twist",
    "vietnamese": "xoắn, cuộn, quắn; sự xoắn, vòng xoắn"
  },
  {
    "english": "twisted",
    "type": "adj",
    "pronounce": "twistid",
    "vietnamese": "được xoắn, được cuộn"
  },
  {
    "english": "type",
    "type": "n, v",
    "pronounce": "taip",
    "vietnamese": "loại, kiểu, mẫu; phân loại, xếp loại"
  },
  {
    "english": "typical",
    "type": "adj",
    "pronounce": "́tipikəl",
    "vietnamese": "tiêu biểu, điển hình, đặc trưng"
  },
  {
    "english": "typically",
    "type": "adv",
    "pronounce": "́tipikəlli",
    "vietnamese": "điển hình, tiêu biểu"
  },
  {
    "english": "tyre",
    "type": "n",
    "pronounce": "taiз",
    "vietnamese": "lốp, vỏ xe"
  },
  {
    "english": "ugly",
    "type": "adj",
    "pronounce": "ʌgli",
    "vietnamese": "xấu xí, xấu xa"
  },
  {
    "english": "ultimate",
    "type": "adj",
    "pronounce": "ˈʌltəmɪt",
    "vietnamese": "cuối cùng, sau cùng"
  },
  {
    "english": "ultimately",
    "type": "adv",
    "pronounce": "́ʌltimətli",
    "vietnamese": "cuối cùng, sau cùng"
  },
  {
    "english": "umbrella",
    "type": "n",
    "pronounce": "ʌm'brelə",
    "vietnamese": "ô, dù"
  },
  {
    "english": "unable",
    "type": "adj",
    "pronounce": "ʌn'eibl",
    "vietnamese": "không có năng lực, không có tài, không thể, không có khẳ năng"
  },
  {
    "english": "unacceptable",
    "type": "adj",
    "pronounce": "ʌnək'septəbl",
    "vietnamese": "không chấp nhận được"
  },
  {
    "english": "unacceptable",
    "type": "",
    "pronounce": "̧ʌnək ́septəbl",
    "vietnamese": "không thể chấp nhận"
  },
  {
    "english": "uncertain",
    "type": "adj",
    "pronounce": "ʌn'sə:tn",
    "vietnamese": "thiếu chính xác, không chắc chắn"
  },
  {
    "english": "uncertain, certain",
    "type": "",
    "pronounce": "ʌn'sə:tn",
    "vietnamese": "không chắc chắn, khôn biết rõ ràng"
  },
  {
    "english": "uncle",
    "type": "n",
    "pronounce": "ʌηkl",
    "vietnamese": "chú, bác"
  },
  {
    "english": "uncomfortable",
    "type": "adj",
    "pronounce": "ʌη ́tkʌmfətəbl",
    "vietnamese": "bất tiện, khó chịu, không thoải má"
  },
  {
    "english": "uncomfortable",
    "type": "",
    "pronounce": "ʌη ́kʌmfətəbl",
    "vietnamese": "bất tiện, không tiện lợi"
  },
  {
    "english": "unconscious",
    "type": "adj",
    "pronounce": "ʌn'kɔnʃəs",
    "vietnamese": "bất tỉnh, không có ý thức, không biết rõ"
  },
  {
    "english": "unconscious",
    "type": "",
    "pronounce": "ʌn'kɔnʃəs",
    "vietnamese": "bất tỉnh, ngất đi"
  },
  {
    "english": "uncontrolled",
    "type": "adj",
    "pronounce": "ʌnkən'trould",
    "vietnamese": "không bị điều khiển, không bị kiểm tra, không bị hạn chế"
  },
  {
    "english": "uncontrolled",
    "type": "",
    "pronounce": "ʌnkən'trould",
    "vietnamese": "không bị kiềm chế, không bị kiểm tra"
  },
  {
    "english": "under",
    "type": "adv, prep",
    "pronounce": "ʌndə",
    "vietnamese": "dưới, ở dưới; ở phía dưới, về phía dưới"
  },
  {
    "english": "underground",
    "type": "adj, adv",
    "pronounce": "ʌndəgraund",
    "vietnamese": "dưới mặt đất, ngầm dưới đất; ngầm"
  },
  {
    "english": "underneath",
    "type": "prep, adv",
    "pronounce": "̧ʌndə ́ni:θ",
    "vietnamese": "dưới, bên dưới"
  },
  {
    "english": "understand",
    "type": "v",
    "pronounce": "ʌndə'stænd",
    "vietnamese": "hiểu, nhận thức"
  },
  {
    "english": "understanding",
    "type": "n",
    "pronounce": "ʌndərˈstændɪŋ",
    "vietnamese": "trí tuệ, sự hiểu biết"
  },
  {
    "english": "underwater",
    "type": "adj, adv",
    "pronounce": "́ʌndə ̧wɔtə",
    "vietnamese": "ở dưới mặt nước, dưới mặt nước"
  },
  {
    "english": "underwear",
    "type": "n",
    "pronounce": "ʌndəweə",
    "vietnamese": "quần lót"
  },
  {
    "english": "undo",
    "type": "v",
    "pronounce": "ʌn ́du:",
    "vietnamese": "tháo, gỡ; xóa bỏ, hủy bỏ"
  },
  {
    "english": "unemployed",
    "type": "adj",
    "pronounce": "̧ʌnim ́plɔid",
    "vietnamese": "thất nghiệp; không dùng, không sử dụng được"
  },
  {
    "english": "unemployment",
    "type": "n",
    "pronounce": "Δnim'ploimзnt",
    "vietnamese": "sự thất nghiệp, nạn thất nghiệp"
  },
  {
    "english": "unexpected",
    "type": "adj",
    "pronounce": "̧ʌniks ́pektid",
    "vietnamese": "bất ngờ, gây ngạc nhiên"
  },
  {
    "english": "unexpectedly",
    "type": "adv",
    "pronounce": "Δniks'pektid",
    "vietnamese": "bất ngờ, gây ngạc nhiên"
  },
  {
    "english": "unfair",
    "type": "adj",
    "pronounce": "ʌn ́fɛə",
    "vietnamese": "gian lận, không công bằng; bất lợi"
  },
  {
    "english": "Unfair, unfairly",
    "type": "",
    "pronounce": "ʌn ́fɛə",
    "vietnamese": "không đúng, không công bằng, gian lận"
  },
  {
    "english": "unfairly",
    "type": "adv",
    "pronounce": "ʌn ́fɛəli",
    "vietnamese": "gian lận, không công bằng; bất lợi"
  },
  {
    "english": "unfortunate",
    "type": "adj",
    "pronounce": "Λnfo:'t∫əneit",
    "vietnamese": "không may, rủi ro, bất hạnh"
  },
  {
    "english": "unfortunately",
    "type": "adv",
    "pronounce": "ʌn ́fɔ:tʃənətli",
    "vietnamese": "một cách đáng tiếc, một cách không may"
  },
  {
    "english": "unfriendly",
    "type": "adj",
    "pronounce": "ʌn ́frendli",
    "vietnamese": "không thân thiện, không có thiện cảm"
  },
  {
    "english": "unhappiness",
    "type": "n",
    "pronounce": "ʌn ́hæpinis",
    "vietnamese": "nỗi buồn, sự bất hạnh"
  },
  {
    "english": "unhappy",
    "type": "adj",
    "pronounce": "ʌn ́hæpi",
    "vietnamese": "buồn rầu, khốn khổ"
  },
  {
    "english": "uniform",
    "type": "n, adj",
    "pronounce": "ˈjunəˌfɔrm",
    "vietnamese": "đồng phục; đều, giống nhấu, đồng dạng"
  },
  {
    "english": "unimportant",
    "type": "adj",
    "pronounce": "̧ʌnim ́pɔ:tənt",
    "vietnamese": "khônh quan trọng, không trọng đạ"
  },
  {
    "english": "union",
    "type": "n",
    "pronounce": "ju:njən",
    "vietnamese": "liên hiệp, sự đoàn kết, sự hiệp nhất"
  },
  {
    "english": "unique",
    "type": "adj",
    "pronounce": "ju: ́ni:k",
    "vietnamese": "độc nhất vô nhị"
  },
  {
    "english": "unit",
    "type": "n",
    "pronounce": "ju:nit",
    "vietnamese": "đơn vị"
  },
  {
    "english": "unite",
    "type": "v",
    "pronounce": "ju: ́nait",
    "vietnamese": "liên kết, hợp nhất, hợp lại, kết thân"
  },
  {
    "english": "united",
    "type": "adj",
    "pronounce": "ju:'naitid",
    "vietnamese": "liên minh, đoàn kết, chung, thống nhất"
  },
  {
    "english": "universe",
    "type": "n",
    "pronounce": "ju:nivə:s",
    "vietnamese": "vũ trụ"
  },
  {
    "english": "university",
    "type": "n",
    "pronounce": "̧ju:ni ́və:siti",
    "vietnamese": "trường đại học"
  },
  {
    "english": "unkind",
    "type": "adj",
    "pronounce": "ʌn ́kaind",
    "vietnamese": "độc ác, tàn nhẫn"
  },
  {
    "english": "unknown",
    "type": "adj",
    "pronounce": "ʌn'noun",
    "vietnamese": "không biết"
  },
  {
    "english": "unless",
    "type": "conj",
    "pronounce": "ʌn ́les",
    "vietnamese": "trừ phi, trừ khi, nếu không"
  },
  {
    "english": "unlike",
    "type": "prep, adj",
    "pronounce": "ʌn ́laik",
    "vietnamese": "khác, không giống"
  },
  {
    "english": "unlikely",
    "type": "adj",
    "pronounce": "ʌnˈlaɪkli",
    "vietnamese": "không thể xảy ra, không chắc xảy ra"
  },
  {
    "english": "unload",
    "type": "v",
    "pronounce": "ʌn ́loud",
    "vietnamese": "cất gánh nặng, dỡ hàng"
  },
  {
    "english": "unlucky",
    "type": "adj",
    "pronounce": "ʌn ́lʌki",
    "vietnamese": "không gặp may, bất hạnh"
  },
  {
    "english": "unnecessary",
    "type": "adj",
    "pronounce": "ʌn'nesisəri",
    "vietnamese": "không cần thiết, không mong muốn"
  },
  {
    "english": "unpleasant",
    "type": "adj",
    "pronounce": "ʌn'plezənt",
    "vietnamese": "không dễ chịu, khó chịu, khó ưa"
  },
  {
    "english": "unreasonable",
    "type": "adj",
    "pronounce": "ʌnˈrizənəbəl",
    "vietnamese": "vô lý"
  },
  {
    "english": "unsteady",
    "type": "adj",
    "pronounce": "ʌn ́stedi",
    "vietnamese": "không chắc, không ổn định"
  },
  {
    "english": "unsuccessful",
    "type": "adj",
    "pronounce": "̧ʌnsək ́sesful",
    "vietnamese": "không thành công, thất bại"
  },
  {
    "english": "untidy",
    "type": "adj",
    "pronounce": "ʌn ́taidi",
    "vietnamese": "không gọn gàng, không ngăn nắp, lộn xộn"
  },
  {
    "english": "until, till",
    "type": "conj, prep",
    "pronounce": "ʌn ́til",
    "vietnamese": "trước khi, cho đến khi"
  },
  {
    "english": "Unusual",
    "type": "",
    "pronounce": "ʌn ́ju:ʒuəl",
    "vietnamese": "hiếm, khác thường"
  },
  {
    "english": "unusually",
    "type": "adv",
    "pronounce": "ʌn ́ju:ʒuəlli",
    "vietnamese": "cực kỳ, khác thường"
  },
  {
    "english": "Unwilling",
    "type": "",
    "pronounce": "ʌn ́wiliη",
    "vietnamese": "không muốn, không có ý định"
  },
  {
    "english": "unwillingly",
    "type": "adv",
    "pronounce": "ʌn ́wiliηgli",
    "vietnamese": "không sẵn lòng, miễn cưỡng"
  },
  {
    "english": "up",
    "type": "adv, prep",
    "pronounce": "Λp",
    "vietnamese": "ở trên, lên trên, lên"
  },
  {
    "english": "upon",
    "type": "prep",
    "pronounce": "ə ́pɔn",
    "vietnamese": "trên, ở trên"
  },
  {
    "english": "upper",
    "type": "adj",
    "pronounce": "́ʌpə",
    "vietnamese": "cao hơn"
  },
  {
    "english": "upset",
    "type": "v, adj",
    "pronounce": "ʌpˈsɛt",
    "vietnamese": "làm đổ, đánh đổ"
  },
  {
    "english": "upsetting",
    "type": "adj",
    "pronounce": "ʌp ́setiη",
    "vietnamese": "tính đánh đổ, làm đổ"
  },
  {
    "english": "upside down",
    "type": "adv",
    "pronounce": "́ʌp ̧said",
    "vietnamese": "lộn ngược"
  },
  {
    "english": "upstairs",
    "type": "adv, adj, n",
    "pronounce": "́ʌp ́stɛəz",
    "vietnamese": "ở tên gác, cư ngụ ở tầng gác; tầng trên, gác"
  },
  {
    "english": "upward",
    "type": "adj",
    "pronounce": "ʌpwəd",
    "vietnamese": "lên, hướng lên, đi lên"
  },
  {
    "english": "upwards",
    "type": "adv",
    "pronounce": "",
    "vietnamese": "lên, hướng lên, đi lên, về phía trên"
  },
  {
    "english": "urban",
    "type": "adj",
    "pronounce": "ˈɜrbən",
    "vietnamese": "(thuộc) thành phố, khu vực"
  },
  {
    "english": "urge",
    "type": "v, n",
    "pronounce": "ə:dʒ",
    "vietnamese": "thúc, giục, giục giã; sự thúc đẩy, sự thôi thúc"
  },
  {
    "english": "urgent",
    "type": "adj",
    "pronounce": "ˈɜrdʒənt",
    "vietnamese": "gấp, khẩn cấp"
  },
  {
    "english": "us",
    "type": "n, pro",
    "pronounce": "ʌs",
    "vietnamese": "chúng tôi, chúng ta; tôi và anh"
  },
  {
    "english": "use",
    "type": "v, n",
    "pronounce": "ju:s",
    "vietnamese": "sử dụng, dùng; sự dùng, sự sử dụng"
  },
  {
    "english": "used",
    "type": "adj",
    "pronounce": "ju:st",
    "vietnamese": "đã dùng, đã sử dụng. used to sth/to doing sth: sử dụng cái gì, sử dụng để làm cái gì"
  },
  {
    "english": "used to",
    "type": "modal, v",
    "pronounce": "",
    "vietnamese": "đã quen dùng"
  },
  {
    "english": "useful",
    "type": "adj",
    "pronounce": "́ju:sful",
    "vietnamese": "hữu ích, giúp ích"
  },
  {
    "english": "useless",
    "type": "adj",
    "pronounce": "ju:slis",
    "vietnamese": "vô ích, vô dụng"
  },
  {
    "english": "user",
    "type": "n",
    "pronounce": "́ju:zə",
    "vietnamese": "người dùng, người sử dụng"
  },
  {
    "english": "usual",
    "type": "adj",
    "pronounce": "ju:ʒl",
    "vietnamese": "thông thường, thường dùng"
  },
  {
    "english": "usually",
    "type": "adv",
    "pronounce": "ju:ʒəli",
    "vietnamese": "thường thường"
  },
  {
    "english": "vacation",
    "type": "n",
    "pronounce": "və'kei∫n",
    "vietnamese": "kỳ nghỉ hè, kỳ nghỉ lễ; ngày nghỉ, ngày lễ"
  },
  {
    "english": "valid",
    "type": "adj",
    "pronounce": "vælɪd",
    "vietnamese": "chắc chắn, hiệu quả, hợp lý"
  },
  {
    "english": "valley",
    "type": "n",
    "pronounce": "væli",
    "vietnamese": "thung lũng"
  },
  {
    "english": "valuable",
    "type": "adj",
    "pronounce": "væljuəbl",
    "vietnamese": "có giá trị lớn, đáng giá"
  },
  {
    "english": "value",
    "type": "n, v",
    "pronounce": "vælju:",
    "vietnamese": "giá trị, ước tính, định giá"
  },
  {
    "english": "van",
    "type": "n",
    "pronounce": "væn",
    "vietnamese": "tiền đội, quân tiên phong, xe tải"
  },
  {
    "english": "variation",
    "type": "n",
    "pronounce": "̧veəri ́eiʃən",
    "vietnamese": "sự biến đổi, sự thay đổi mức độ, sự khác nhau"
  },
  {
    "english": "varied",
    "type": "adj",
    "pronounce": "veərid",
    "vietnamese": "thuộc nhiều loại khác nhau, những vẻ đa dạng"
  },
  {
    "english": "variety",
    "type": "n",
    "pronounce": "və'raiəti",
    "vietnamese": "sự đa dạng, trạng thái khác nhau"
  },
  {
    "english": "various",
    "type": "adj",
    "pronounce": "veri.əs",
    "vietnamese": "khác nhau, thuộc về nhiều loại"
  },
  {
    "english": "vary",
    "type": "v",
    "pronounce": "veəri",
    "vietnamese": "thay đổi, làm cho khác nhau, biến đổi"
  },
  {
    "english": "vast",
    "type": "adj",
    "pronounce": "vɑ:st",
    "vietnamese": "rộng lớn, mênh mông"
  },
  {
    "english": "vegetable",
    "type": "n",
    "pronounce": "ˈvɛdʒtəbəl , vɛdʒɪtəbəl",
    "vietnamese": "rau, thực vật"
  },
  {
    "english": "vehicle",
    "type": "n",
    "pronounce": "vi:hikl",
    "vietnamese": "xe cộ"
  },
  {
    "english": "venture",
    "type": "n, v",
    "pronounce": "ventʃə",
    "vietnamese": "dự án kinh doanh, công việc kinh doanh; liều, mạo hiểm, cả gan"
  },
  {
    "english": "version",
    "type": "n",
    "pronounce": "və:∫n",
    "vietnamese": "bản dịch sang một ngôn ngữ khác"
  },
  {
    "english": "vertical",
    "type": "adj",
    "pronounce": "ˈvɜrtɪkəl",
    "vietnamese": "thẳng đứng, đứng"
  },
  {
    "english": "very",
    "type": "adv",
    "pronounce": "veri",
    "vietnamese": "rất, lắm"
  },
  {
    "english": "via",
    "type": "prep",
    "pronounce": "vaiə",
    "vietnamese": "qua, theo đường"
  },
  {
    "english": "victim",
    "type": "n",
    "pronounce": "viktim",
    "vietnamese": "nạn nhân"
  },
  {
    "english": "victory",
    "type": "n",
    "pronounce": "viktəri",
    "vietnamese": "chiến thắng"
  },
  {
    "english": "video",
    "type": "n",
    "pronounce": "vidiou",
    "vietnamese": "video"
  },
  {
    "english": "view",
    "type": "n, v",
    "pronounce": "vju:",
    "vietnamese": "sự nhìn, tầm nhìn; nhìn thấy, xem, quan sát"
  },
  {
    "english": "village",
    "type": "n",
    "pronounce": "ˈvɪlɪdʒ",
    "vietnamese": "làng, xã"
  },
  {
    "english": "violence",
    "type": "n",
    "pronounce": "ˈvaɪələns",
    "vietnamese": "sự ác liệt, sự dữ dội; bạo lực"
  },
  {
    "english": "violent",
    "type": "adj",
    "pronounce": "vaiələnt",
    "vietnamese": "mãnh liệt, mạnh mẽ, hung dữ"
  },
  {
    "english": "violently",
    "type": "adv",
    "pronounce": "vaiзlзntli",
    "vietnamese": "mãnh liệt, dữ dội"
  },
  {
    "english": "virtually",
    "type": "adv",
    "pronounce": "və:tjuəli",
    "vietnamese": "thực sự, hầu như, gần như"
  },
  {
    "english": "virus",
    "type": "n",
    "pronounce": "vaiərəs",
    "vietnamese": "vi rút"
  },
  {
    "english": "visible",
    "type": "adj",
    "pronounce": "vizəbl",
    "vietnamese": "hữu hình, thấy được"
  },
  {
    "english": "vision",
    "type": "n", 
    "pronounce": "viʒn",
    "vietnamese": "sự nhìn, thị lực"
  },
  {
    "english": "visit",
    "type": "v, n",
    "pronounce": "vizun",
    "vietnamese": "đi thăm hỏi, đến chơi, tham quan; sự đi thăm, sự thăm viếng"
  },
  {
    "english": "visitor",
    "type": "n",
    "pronounce": "vizitə",
    "vietnamese": "khách, du khách"
  },
  {
    "english": "vital",
    "type": "adj",
    "pronounce": "vaitl",
    "vietnamese": "(thuộc) sự sống, cần cho sự sống"
  },
  {
    "english": "vocabulary",
    "type": "n",
    "pronounce": "və ́kæbjuləri",
    "vietnamese": "từ vựng"
  },
  {
    "english": "voice",
    "type": "n",
    "pronounce": "vɔis",
    "vietnamese": "tiếng, giọng nói"
  },
  {
    "english": "volume",
    "type": "n",
    "pronounce": "́vɔlju:m",
    "vietnamese": "thế tích, quyển, tập"
  },
  {
    "english": "vote",
    "type": "n, v",
    "pronounce": "voʊt",
    "vietnamese": "sự bỏ phiếu, sự bầu cử; bỏ phiếu, bầu cử"
  },
  {
    "english": "wage",
    "type": "n",
    "pronounce": "weiʤ",
    "vietnamese": "tiền lương, tiền công"
  },
  {
    "english": "waist",
    "type": "n",
    "pronounce": "weist",
    "vietnamese": "eo, chỗ thắt lưng"
  },
  {
    "english": "wait",
    "type": "v",
    "pronounce": "weit",
    "vietnamese": "chờ đợi"
  },
  {
    "english": "waiter, waitress",
    "type": "n",
    "pronounce": "weitə",
    "vietnamese": "người hầu bàn, người đợi, người trông chờ"
  },
  {
    "english": "wake up",
    "type": "v",
    "pronounce": "weik",
    "vietnamese": "thức dậy, tỉnh thức"
  },
  {
    "english": "walk",
    "type": "v, n",
    "pronounce": "wɔ:k",
    "vietnamese": "đi, đi bộ; sự đi bộ, sự đi dạo"
  },
  {
    "english": "walking",
    "type": "n",
    "pronounce": "wɔ:kiɳ",
    "vietnamese": "sự đi, sự đi bộ"
  },
  {
    "english": "wall",
    "type": "n",
    "pronounce": "wɔ:l",
    "vietnamese": "tường, vách"
  },
  {
    "english": "wallet",
    "type": "n",
    "pronounce": "wolit",
    "vietnamese": "cái ví"
  },
  {
    "english": "wander",
    "type": "v, n",
    "pronounce": "wɔndə",
    "vietnamese": "đi lang thang; sự đi lang thang"
  },
  {
    "english": "want",
    "type": "v",
    "pronounce": "wɔnt",
    "vietnamese": "muốn"
  },
  {
    "english": "war",
    "type": "n",
    "pronounce": "wɔ:",
    "vietnamese": "chiến tranh"
  },
  {
    "english": "warm",
    "type": "adj, v",
    "pronounce": "wɔ:m",
    "vietnamese": "ấm, ấm áp; làm cho nóng, hâm nóng"
  },
  {
    "english": "warmth",
    "type": "n",
    "pronounce": "wɔ:mθ",
    "vietnamese": "trạng thái ấm, sự ấm áp; hơi ấm"
  },
  {
    "english": "warn",
    "type": "v",
    "pronounce": "wɔ:n",
    "vietnamese": "báo cho biết, cảnh báo"
  },
  {
    "english": "warning",
    "type": "n",
    "pronounce": "wɔ:niɳ",
    "vietnamese": "sự báo trước, lời cảnh báo"
  },
  {
    "english": "wash",
    "type": "v",
    "pronounce": "wɒʃ , wɔʃ",
    "vietnamese": "rửa, giặt"
  },
  {
    "english": "washing",
    "type": "n",
    "pronounce": "wɔʃiɳ",
    "vietnamese": "sự tắm rửa, sự giặt"
  },
  {
    "english": "waste",
    "type": "v, n, adj",
    "pronounce": "weɪst",
    "vietnamese": "lãng phí, uổng phí; vùng hoang vu sa mạc; bỏ hoang"
  },
  {
    "english": "watch",
    "type": "v, n",
    "pronounce": "wɔtʃ",
    "vietnamese": "nhìn, theo dõi, quan sát; sự canh gác, sự canh phòng"
  },
  {
    "english": "water",
    "type": "n",
    "pronounce": "wɔ:tə",
    "vietnamese": "nước"
  },
  {
    "english": "wave",
    "type": "n, v",
    "pronounce": "weɪv",
    "vietnamese": "sóng, gợn nước; gợn sóng, uốn thành sóng"
  },
  {
    "english": "way",
    "type": "n",
    "pronounce": "wei",
    "vietnamese": "đường, đường đi"
  },
  {
    "english": "we",
    "type": "pron",
    "pronounce": "wi:",
    "vietnamese": "chúng tôi, chúng ta"
  },
  {
    "english": "weak",
    "type": "adj",
    "pronounce": "wi:k",
    "vietnamese": "yếu, yếu ớt"
  },
  {
    "english": "weakness",
    "type": "n",
    "pronounce": "́wi:knis",
    "vietnamese": "tình trạng yếu đuối, yếu ớt"
  },
  {
    "english": "wealth",
    "type": "n",
    "pronounce": "welθ",
    "vietnamese": "sự giàu có, sự giàu sang"
  },
  {
    "english": "weapon",
    "type": "n",
    "pronounce": "wepən",
    "vietnamese": "vũ khí"
  },
  {
    "english": "wear",
    "type": "v",
    "pronounce": "weə",
    "vietnamese": "mặc, mang, đeo"
  },
  {
    "english": "weather",
    "type": "n",
    "pronounce": "weθə",
    "vietnamese": "thời tiết"
  },
  {
    "english": "web",
    "type": "n",
    "pronounce": "wɛb",
    "vietnamese": "mạng, lưới"
  },
  {
    "english": "website",
    "type": "n",
    "pronounce": "",
    "vietnamese": "không gian liên tới với Internet"
  },
  {
    "english": "wedding",
    "type": "n",
    "pronounce": "ˈwɛdɪŋ",
    "vietnamese": "lễ cưới, hôn lễ"
  },
  {
    "english": "Wednesday",
    "type": "n",
    "pronounce": "́wensdei",
    "vietnamese": "thứ 4"
  },
  {
    "english": "week",
    "type": "n",
    "pronounce": "wi:k",
    "vietnamese": "tuần, tuần lễ"
  },
  {
    "english": "weekend",
    "type": "n",
    "pronounce": "̧wi:k ́end",
    "vietnamese": "cuối tuần"
  },
  {
    "english": "weekly",
    "type": "adj",
    "pronounce": "́wi:kli",
    "vietnamese": "mỗi tuần một lần, hàng tuần"
  },
  {
    "english": "weigh",
    "type": "v",
    "pronounce": "wei",
    "vietnamese": "cân, cân nặng"
  },
  {
    "english": "weight",
    "type": "n",
    "pronounce": "weit",
    "vietnamese": "trọng lượng"
  },
  {
    "english": "welcome",
    "type": "v, adj, n, exclamation",
    "pronounce": "welkʌm",
    "vietnamese": "chào mừng, hoan nghênh"
  },
  {
    "english": "well",
    "type": "adv, adj, exclamation",
    "pronounce": "wel",
    "vietnamese": "tốt, giỏi; ôi, may quá!"
  },
  {
    "english": "well known",
    "type": "adj",
    "pronounce": "́wel ́noun",
    "vietnamese": "nổi tiếng, được nhiều người biết đến"
  },
  {
    "english": "west",
    "type": "n, adj, adv",
    "pronounce": "west",
    "vietnamese": "phía Tây, theo phía tây, về hướng tây"
  },
  {
    "english": "western",
    "type": "adj",
    "pronounce": "westn",
    "vietnamese": "về phía tây, của phía tây"
  },
  {
    "english": "wet",
    "type": "adj",
    "pronounce": "wɛt",
    "vietnamese": "ướt, ẩm ướt"
  },
  {
    "english": "what",
    "type": "n, det, pro",
    "pronounce": "wʌt",
    "vietnamese": "gì, thế nào"
  },
  {
    "english": "whatever",
    "type": "n, det, pro",
    "pronounce": "wɔt ́evə",
    "vietnamese": "bất cứ thứ gì, bất kể thứ gì"
  },
  {
    "english": "wheel",
    "type": "n",
    "pronounce": "wil",
    "vietnamese": "bánh xe"
  },
  {
    "english": "when",
    "type": "adv, pron, conj",
    "pronounce": "wen",
    "vietnamese": "khi, lúc, vào lúc nào"
  },
  {
    "english": "whenever",
    "type": "conj",
    "pronounce": "wen'evə",
    "vietnamese": "bất cứ lúc nào, lúc nào"
  },
  {
    "english": "where",
    "type": "adv, conj",
    "pronounce": "weər",
    "vietnamese": "đâu, ở đâu; nơi mà"
  },
  {
    "english": "whereas",
    "type": "conj",
    "pronounce": "weə'ræz",
    "vietnamese": "nhưng trái lại, trong khi mà"
  },
  {
    "english": "wherever",
    "type": "conj",
    "pronounce": "weər'evə(r)",
    "vietnamese": "ở bất cứ nơi nào, ở bất cứ nơi đâu"
  },
  {
    "english": "whether",
    "type": "conj",
    "pronounce": "́weðə",
    "vietnamese": "có..không; có... chăng; không biết có.. không"
  },
  {
    "english": "which",
    "type": "n, det, pro",
    "pronounce": "witʃ",
    "vietnamese": "nào, bất cứ.. nào; ấy, đó"
  },
  {
    "english": "while",
    "type": "n, conj",
    "pronounce": "wail",
    "vietnamese": "trong lúc, trong khi; lúc, chốc, lát"
  },
  {
    "english": "whilst",
    "type": "conj",
    "pronounce": "wailst",
    "vietnamese": "trong lúc, trong khi"
  },
  {
    "english": "whisper",
    "type": "v, n",
    "pronounce": "́wispə",
    "vietnamese": "nói thì thầm, xì xào; tiếng nói thì thầm, tiếng xì xào"
  },
  {
    "english": "whistle",
    "type": "n, v",
    "pronounce": "wisl",
    "vietnamese": "sự huýt sáo, sự thổi còi; huýt sáo, thổi còi"
  },
  {
    "english": "white",
    "type": "adj, n",
    "pronounce": "wai:t",
    "vietnamese": "trắng; màu trắng"
  },
  {
    "english": "who",
    "type": "n, pro",
    "pronounce": "hu:",
    "vietnamese": "ai, người nào, kẻ nào, người như thế nào"
  },
  {
    "english": "whoever",
    "type": "n, pro",
    "pronounce": "hu:'ev",
    "vietnamese": "ai, người nào, bất cứ ai, bất cứ người nào, dù ai"
  },
  {
    "english": "whole",
    "type": "adj, n",
    "pronounce": "həʊl",
    "vietnamese": "bình an vô sự, không suy suyển, không hư hỏng; toàn bộ, tất cả, toàn thể"
  },
  {
    "english": "whom",
    "type": "n, pro",
    "pronounce": "hu:m",
    "vietnamese": "ai, người nào; người mà"
  },
  {
    "english": "whose",
    "type": "n, det, pro",
    "pronounce": "hu:z",
    "vietnamese": "của ai"
  },
  {
    "english": "why",
    "type": "adv",
    "pronounce": "wai",
    "vietnamese": "tại sao, vì sao"
  },
  {
    "english": "wide",
    "type": "adj",
    "pronounce": "waid",
    "vietnamese": "rộng, rộng lớn"
  },
  {
    "english": "widely",
    "type": "adv",
    "pronounce": "́waidli",
    "vietnamese": "nhiều, xa; rộng rãi"
  },
  {
    "english": "width",
    "type": "n",
    "pronounce": "wɪdθ; wɪtθ",
    "vietnamese": "tính chất rộng, bề rộng"
  },
  {
    "english": "wife",
    "type": "n",
    "pronounce": "waif",
    "vietnamese": "vợ"
  },
  {
    "english": "wild",
    "type": "adj",
    "pronounce": "waɪld",
    "vietnamese": "dại, hoang"
  },
  {
    "english": "wildly",
    "type": "adv",
    "pronounce": "waɪldli",
    "vietnamese": "dại, hoang"
  },
  {
    "english": "will",
    "type": "v, n, modal",
    "pronounce": "wil",
    "vietnamese": "sẽ; ý chí, ý định"
  },
  {
    "english": "willing",
    "type": "adj",
    "pronounce": "́wiliη",
    "vietnamese": "bằng lòng, vui lòng, muốn"
  },
  {
    "english": "willingly",
    "type": "adv",
    "pronounce": "wiliηli",
    "vietnamese": "sẵn lòng, tự nguyện"
  },
  {
    "english": "willingness",
    "type": "n",
    "pronounce": "́wiliηnis",
    "vietnamese": "sự bằng lòng, sự vui lòng"
  },
  {
    "english": "win",
    "type": "v",
    "pronounce": "win",
    "vietnamese": "chiếm, đọat, thu được"
  },
  {
    "english": "wind",
    "type": "v",
    "pronounce": "wind",
    "vietnamese": "quấn lại, cuộn lại. wind sth up: lên dây, quấn, giải quyết"
  },
  {
    "english": "window",
    "type": "n",
    "pronounce": "windəʊ",
    "vietnamese": "cửa sổ"
  },
  {
    "english": "wine",
    "type": "n",
    "pronounce": "wain",
    "vietnamese": "rượu, đồ uống"
  },
  {
    "english": "wing",
    "type": "n",
    "pronounce": "wiη",
    "vietnamese": "cánh, sự bay, sự cất cánh"
  },
  {
    "english": "winner",
    "type": "n",
    "pronounce": "winər",
    "vietnamese": "người thắng cuộc"
  },
  {
    "english": "winning",
    "type": "adj",
    "pronounce": "́winiη",
    "vietnamese": "đang dành thắng lợi, thắng cuộc"
  },
  {
    "english": "winter",
    "type": "n",
    "pronounce": "ˈwɪntər",
    "vietnamese": "mùa đông"
  },
  {
    "english": "wire",
    "type": "n",
    "pronounce": "waiə",
    "vietnamese": "dây (kim loại)"
  },
  {
    "english": "wise",
    "type": "adj",
    "pronounce": "waiz",
    "vietnamese": "khôn ngoan, sáng suốt, thông thái"
  },
  {
    "english": "wish",
    "type": "v, n",
    "pronounce": "wi∫",
    "vietnamese": "ước, mong muốn; sự mong ước, lòng mong muốn"
  },
  {
    "english": "with",
    "type": "prep",
    "pronounce": "wið",
    "vietnamese": "với, cùng"
  },
  {
    "english": "withdraw",
    "type": "v",
    "pronounce": "wɪðˈdrɔ , wɪθˈdrɔ",
    "vietnamese": "rút, rút khỏi, rút lui"
  },
  {
    "english": "within",
    "type": "prep",
    "pronounce": "wið ́in",
    "vietnamese": "trong vong thời gian, trong khoảng thời gian"
  },
  {
    "english": "without",
    "type": "prep",
    "pronounce": "wɪðˈaʊt , wɪθaʊt",
    "vietnamese": "không, không có"
  },
  {
    "english": "witness",
    "type": "n, v",
    "pronounce": "witnis",
    "vietnamese": "sự làm chứng, bằng chứng; chứng kiến, làm chứng"
  },
  {
    "english": "woman",
    "type": "n",
    "pronounce": "wʊmən",
    "vietnamese": "đàn bà, phụ nữ"
  },
  {
    "english": "wonder",
    "type": "v",
    "pronounce": "wʌndə",
    "vietnamese": "ngạc nhiên, lấy làm lạ, kinh ngạc"
  },
  {
    "english": "wonderful",
    "type": "adj",
    "pronounce": "́wʌndəful",
    "vietnamese": "phi thường, khác thường, kỳ diệu, tuyệt vời"
  },
  {
    "english": "wood",
    "type": "n",
    "pronounce": "wud",
    "vietnamese": "gỗ"
  },
  {
    "english": "wooden",
    "type": "adj",
    "pronounce": "́wudən",
    "vietnamese": "làm bằng gỗ"
  },
  {
    "english": "wool",
    "type": "n",
    "pronounce": "wul",
    "vietnamese": "len"
  },
  {
    "english": "work",
    "type": "v, n",
    "pronounce": "wɜ:k",
    "vietnamese": "làm việc, sự làm việc"
  },
  {
    "english": "worker",
    "type": "n",
    "pronounce": "wə:kə",
    "vietnamese": "người lao động"
  },
  {
    "english": "working",
    "type": "adj",
    "pronounce": "́wə:kiη",
    "vietnamese": "sự làm, sự làm việc"
  },
  {
    "english": "world",
    "type": "n",
    "pronounce": "wɜ:ld",
    "vietnamese": "thế giới"
  },
  {
    "english": "worried",
    "type": "adj",
    "pronounce": "́wʌrid",
    "vietnamese": "bồn chồn, lo nghĩ, tỏ ra lo lắng."
  },
  {
    "english": "worry",
    "type": "v, n",
    "pronounce": "wʌri",
    "vietnamese": "lo lắng, suy nghĩ; sự lo lắng, suy nghĩ"
  },
  {
    "english": "worrying",
    "type": "adj",
    "pronounce": "́wʌriiη",
    "vietnamese": "gây lo lắng, gây lo nghĩ"
  },
  {
    "english": "worse, worst, bad",
    "type": "",
    "pronounce": "",
    "vietnamese": "xấu"
  },
  {
    "english": "worship",
    "type": "n, v",
    "pronounce": "ˈwɜrʃɪp",
    "vietnamese": "sự tôn thờ, sự tôn sùng; thờ, thờ phụng, tôn thờ"
  },
  {
    "english": "worth",
    "type": "adj",
    "pronounce": "wɜrθ",
    "vietnamese": "đáng giá, có giá trị"
  },
  {
    "english": "would",
    "type": "v, modal",
    "pronounce": "wud",
    "vietnamese": "sẽ"
  },
  {
    "english": "wound",
    "type": "n, v",
    "pronounce": "waund",
    "vietnamese": "vết thương, thương tích; làm bị thường, gây thương tích"
  },
  {
    "english": "wounded",
    "type": "adj",
    "pronounce": "wu:ndid",
    "vietnamese": "bị thương"
  },
  {
    "english": "wrap",
    "type": "v",
    "pronounce": "ræp",
    "vietnamese": "gói, bọc, quấn"
  },
  {
    "english": "wrapping",
    "type": "n",
    "pronounce": "ræpiɳ",
    "vietnamese": "vật bao bọc, vật quấn quanh"
  },
  {
    "english": "wrist",
    "type": "n",
    "pronounce": "rist",
    "vietnamese": "cổ tay"
  },
  {
    "english": "write",
    "type": "v",
    "pronounce": "rait",
    "vietnamese": "viết"
  },
  {
    "english": "writer",
    "type": "n",
    "pronounce": "raitə",
    "vietnamese": "người viết"
  },
  {
    "english": "writing",
    "type": "n",
    "pronounce": "́raitiη",
    "vietnamese": "sự viết"
  },
  {
    "english": "written",
    "type": "adj",
    "pronounce": "ritn",
    "vietnamese": "viết ra, được thảo ra"
  },
  {
    "english": "wrong",
    "type": "adj, adv",
    "pronounce": "rɔɳ",
    "vietnamese": "sai. go wrong mắc lỗi, sai lầm"
  },
  {
    "english": "wrongly",
    "type": "adv",
    "pronounce": "́rɔηgli",
    "vietnamese": "một cách bất công, không đúng"
  },
  {
    "english": "yard",
    "type": "n",
    "pronounce": "ja:d",
    "vietnamese": "sân, thước Anh (bằng 0, 914 mét)"
  },
  {
    "english": "yawn",
    "type": "v, n",
    "pronounce": "jɔ:n",
    "vietnamese": "há miệng; cử chỉ ngáp"
  },
  {
    "english": "yeah",
    "type": "exclamation",
    "pronounce": "jeə",
    "vietnamese": "vâng, ư"
  },
  {
    "english": "year",
    "type": "n",
    "pronounce": "jə:",
    "vietnamese": "năm"
  },
  {
    "english": "yellow",
    "type": "adj, n",
    "pronounce": "jelou",
    "vietnamese": "vàng; màu vàng"
  },
  {
    "english": "yes",
    "type": "n, exclamation",
    "pronounce": "jes",
    "vietnamese": "vâng, phải, có chứ"
  },
  {
    "english": "yesterday",
    "type": "adv, n",
    "pronounce": "jestədei",
    "vietnamese": "hôm qua"
  },
  {
    "english": "yet",
    "type": "adv, conj",
    "pronounce": "yet",
    "vietnamese": "còn, hãy cỏn, còn nữa; như mà, xong, tuy thế, tuy nhiên"
  },
  {
    "english": "you",
    "type": "pron",
    "pronounce": "ju:",
    "vietnamese": "anh, chị, ông, bà, ngài, ngươi, mày; các anh, các chị, các ông, các bà, các ngài, các người, chúng mày"
  },
  {
    "english": "young",
    "type": "adj",
    "pronounce": "jʌɳ",
    "vietnamese": "trẻ, trẻ tuổi, thanh niên"
  },
  {
    "english": "your",
    "type": "det",
    "pronounce": "jo:",
    "vietnamese": "của anh, của chị, của ngài, của mày; của các anh, của các chị, của các ngài, của chúng mày"
  },
  {
    "english": "yours",
    "type": "n, pro",
    "pronounce": "jo:z",
    "vietnamese": "cái của anh, cái của chị, cái của ngài, cái của mày; cái của các anh, cái của các chị, cái của các ngài, cái của chúng mày"
  },
  {
    "english": "Yours sincerely",
    "type": "",
    "pronounce": "",
    "vietnamese": "bạn chân thành của anh, chị.. (viết ở cuối thư)"
  },
  {
    "english": "Yours Truly",
    "type": "",
    "pronounce": "",
    "vietnamese": "lời kết thúc thư (bạn chân thành của...)"
  },
  {
    "english": "yourself",
    "type": "pron",
    "pronounce": "jɔ:'self",
    "vietnamese": "tự anh, tự chị, chính anh, chính mày, tự mày, tự mình"
  },
  {
    "english": "youth",
    "type": "n",
    "pronounce": "ju:θ",
    "vietnamese": "tuổi trẻ, tuổi xuân, tuổi thanh niên, tuổi niên thiếu"
  },
  {
    "english": "zero number",
    "type": "",
    "pronounce": "ziərou",
    "vietnamese": "số không"
  },
  {
    "english": "zone",
    "type": "n",
    "pronounce": "zoun",
    "vietnamese": "khu vực, miền, vùng"
  }
];

var dataE3 = [
{
        "english": "miserable",
        "type": "adj",
        "vietnamese": "khốn khổ, nghèo khó"
    },
    {
        "english": "incapable",
        "type": "adj",
        "vietnamese": "không có khả năng"
    },
    {
        "english": "permanent",
        "type": "adj",
        "vietnamese": "vĩnh viễn"
    },
    {
        "english": "faithful",
        "type": "adj",
        "vietnamese": "trung thành"
    },
    {
        "english": "retrospect",
        "type": "n",
        "vietnamese": "sự nhìn lại, xem xét lại"
    },
    {
        "english": "lottery winners",
        "type": "n",
        "vietnamese": "người chúng xổ số"
    },
    {
        "english": "fortunes",
        "type": "n",
        "vietnamese": "vận may"
    },
    {
        "english": "equip",
        "type": "v",
        "vietnamese": "trang bị"
    },
    {
        "english": "countryside",
        "type": "n",
        "vietnamese": "đồng quê, nông thôn"
    },
    {
        "english": "superficial",
        "type": "adj",
        "vietnamese": "hời hợt, nông cạn"
    },
    {
        "english": "perseverance",
        "type": "n",
        "vietnamese": "sự kiên trì"
    },
    {
        "english": "inaccurate",
        "type": "adj",
        "vietnamese": "không chính xác"
    },
    {
        "english": "prisoner",
        "type": "n",
        "vietnamese": "tù nhân"
    },
    {
        "english": "trapped",
        "type": "v",
        "vietnamese": "mắc kẹt"
    },
    {
        "english": "haunt",
        "type": "v",
        "vietnamese": "ám ảnh"
    },
  {
    "english": "unpleasant",
    "type": "adj",
    "pronounce": "ʌn'plezənt",
    "vietnamese": "không dễ chịu, khó chịu, khó ưa"
  },
    {
        "english": "belittle",
        "type": "v",
        "vietnamese": "chê bai"
    },
    {
        "english": "nurture",
        "type": "v",
        "vietnamese": "nuôi dưỡng, dưỡng dục"
    },
    {
        "english": "desire",
        "type": "n",
        "vietnamese": "sự mong muốn"
    },
    {
        "english": "potential",
        "type": "adj",
        "vietnamese": "tiềm năng"
    },
    {
        "english": "afterwards",
        "type": "adv",
        "vietnamese": "sau đó"
    },
    {
        "english": "influence",
        "type": "v",
        "vietnamese": "ảnh hưởng"
    },
    {
        "english": "vibe",
        "type": "adj",
        "vietnamese": "rung cảm"
    },
    {
        "english": "reinforce",
        "type": "v",
        "vietnamese": "củng cố"
    },
    {
        "english": "rarely",
        "type": "adv",
        "vietnamese": "hiếm khi"
    },
    {
        "english": "resentment",
        "type": "adj",
        "vietnamese": "phẫn nộ"
    },
    {
        "english": "bitterness",
        "type": "v",
        "vietnamese": "sự cay đắng"
    },
    {
        "english": "jealous",
        "type": "adj",
        "vietnamese": "gen tị"
    },
    {
        "english": "disrupt",
        "type": "v",
        "vietnamese": "làm gián đoạn"
    },
    {
        "english": "explicitly",
        "type": "adj",
        "vietnamese": "rõ ràng"
    },
    {
        "english": "pure",
        "type": "adj",
        "vietnamese": "nguyên chất"
    },
    {
        "english": "seek",
        "type": "v",
        "vietnamese": "tìm kiếm"
    },
    {
        "english": "betray",
        "type": "v",
        "vietnamese": "phản bội"
    },
    {
        "english": "devoted",
        "type": "adj",
        "vietnamese": "tận tâm"
    },
    {
        "english": "enrich",
        "type": "v",
        "vietnamese": "làm giàu"
    },
    {
        "english": "willful",
        "type": "v",
        "vietnamese": "cố ý"
    },
  {
    "english": "pleasure",
    "type": "n",
    "pronounce": "ˈplɛʒuə(r)",
    "vietnamese": "niềm vui thích, điều thích thú, điều thú vị; ý muốn, ý thích"
  },
    {
        "english": "harm",
        "type": "v",
        "vietnamese": "làm hại"
    },
    {
        "english": "interfere",
        "type": "v",
        "type": "v",
        "vietnamese": "can thiệp vào"
    },
    {
        "english": "endeavor",
        "type": "v",
        "vietnamese": "nỗ lực"
    },
    {
        "english": "delusional",
        "type": "adj",
        "vietnamese": "ảo tưởng"
    },
  {
    "english": "tendency",
    "type": "n",
    "pronounce": "ˈtɛndənsi",
    "vietnamese": "xu hướng, chiều hướng, khuynh hướng"
  },
    {
        "english": "ego",
        "type": "n",
        "vietnamese": "cái tôi"
    },
    {
        "english": "exhausted",
        "type": "adj",
        "vietnamese": "kiệt sức"
    },
    {
        "english": "anxious",
        "type": "adj",
        "vietnamese": "lo lắng"
    },
    {
        "english": "rectify",
        "type": "v",
        "vietnamese": "sửa chữa"
    },
    {
        "english": "tribulations",
        "type": "v",
        "vietnamese": "hoạn nạn"
    },
    {
        "english": "surrender",
        "type": "v",
        "vietnamese": "đầu hàng"
    },
    {
        "english": "guide",
        "type": "n",
        "vietnamese": "hướng dẫn"
    },
    {
        "english": "attentive",
        "type": "adj",
        "vietnamese": "chú ý"
    },
    {
        "english": "intuition",
        "type": "n",
        "vietnamese": "trực giác"
    },
    {
        "english": "delicate",
        "type": "adj",
        "vietnamese": "tinh xảo, thanh tú"
    },
    {
        "english": "variety",
        "type": "n",
        "vietnamese": "sự đa dạng"
    },
    {
        "english": "surrounded",
        "type": "v",
        "vietnamese": "được bao quanh"
    },
    {
        "english": "captivate",
        "type": "v",
        "vietnamese": "mê hoặc"
    },
    {
        "english": "distraction",
        "type": "n",
        "vietnamese": "xao lãng"
    },
    {
        "english": "passing out",
        "type": "",
        "vietnamese": "bất tỉnh"
    },
    {
        "english": "confront",
        "type": "v",
        "vietnamese": "đối đầu, đối chất, so sánh"
    },
    {
        "english": "frantically",
        "type": "adv",
        "vietnamese": "một cách điên cuồng"
    },
    {
        "english": "contradict",
        "type": "v",
        "vietnamese": "mâu thuẫn"
    },
    {
        "english": "sympathize",
        "type": "v",
        "vietnamese": "thông cảm"
    },
    {
        "english": "regain",
        "type": "v",
        "vietnamese": "lấy lại, chiếm lại, chuộc lại"
    },
    {
        "english": "praise",
        "type": "v",
        "vietnamese": "khen"
    },
    {
        "english": "mental",
        "type": "adj",
        "vietnamese": "tâm thần"
    },
    {
        "english": "productive",
        "type": "adj",
        "vietnamese": "năng suất"
    },
    {
        "english": "stigmatize",
        "type": "adj",
        "vietnamese": "kì thị"
    },
    {
        "english": "guilty",
        "type": "adj",
        "vietnamese": "tội lỗi"
    },
    {
        "english": "urgent",
        "type": "adj",
        "vietnamese": "cấp bách"
    },
    {
        "english": "sluggish",
        "type": "adj",
        "vietnamese": "trì trệ, chậm chạp"
    },
    {
        "english": "strict",
        "type": "adj",
        "vietnamese": "nghiêm khắc"
    },
    {
        "english": "against",
        "type": "v",
        "vietnamese": "chống lại"
    },{
        "english": "typical",
        "type": "adj",
        "pronounce": "́tipikəl",
        "vietnamese": "tiêu biểu, điển hình, đặc trưng"
    },
    {
        "english": "revise",
        "type": "v",
        "pronounce": "ri'vaiz",
        "vietnamese": "đọc lại, xem lại, sửa lại, ôn lại"
    },
    {
        "english": "pay attention to",
        "type": "",
        "pronounce": "",
        "vietnamese": "chú ý tới"
    },
    {
        "english": "gradually",
        "type": "adv",
        "pronounce": "grædzuəli",
        "vietnamese": "dần dần, tư tư"
    },
    {
        "english": "permission",
        "type": "n",
        "pronounce": "pə'miʃn",
        "vietnamese": "sự cho phép, giấy phép"
    },
    {
        "english": "capable",
        "type": "of, adj",
        "pronounce": "keipəb(ə)l",
        "vietnamese": "có tài, có năng lực; có khả năng, cả gan"
    },
    {
        "english": "possibly",
        "type": "adv",
        "pronounce": "́pɔsibli",
        "vietnamese": "có lẽ, có thể, có thể chấp nhận được"
    },
    {
        "english": "suitable",
        "type": "adj",
        "pronounce": "́su:təbl",
        "vietnamese": "hợp, phù hợp, thích hợp với"
    },
    {
        "english": "recording",
        "type": "n",
        "pronounce": "ri ́kɔ:diη",
        "vietnamese": "sự ghi, sự thu âm"
    },
    {
        "english": "deliver",
        "type": "v",
        "pronounce": "di'livə",
        "vietnamese": "cứu khỏi, thoát khỏi, bày tỏ, giãi bày"
    },
    {
        "english": "material",
        "type": "n, adj",
        "pronounce": "mə ́tiəriəl",
        "vietnamese": "nguyên vật liệu; vật chất, hữu hình"
    },
    {
        "english": "tribulations",
        "type": "v",
        "vietnamese": "hoạn nạn"
    },
    {
        "english": "sensitive",
        "type": "adj",
        "pronounce": "sensitiv",
        "vietnamese": "dễ bị thương, dễ bị hỏng; dễ bị xúc phạm"
    },
    {
        "english": "entire",
        "type": "adj",
        "pronounce": "in'taiə",
        "vietnamese": "toàn thể, toàn bộ"
    },
    {
        "english": "wonder",
        "type": "v",
        "pronounce": "wʌndə",
        "vietnamese": "ngạc nhiên, lấy làm lạ, kinh ngạc"
    },
    {
        "english": "struggled",
        "type": "v",
        "vietnamese": "đấu tranh"
    },
    {
        "english": "worth",
        "type": "adj",
        "pronounce": "wɜrθ",
        "vietnamese": "đáng giá, có giá trị"
    },
    {
        "english": "criticize",
        "type": "v",
        "pronounce": "ˈkrɪtəˌsaɪz",
        "vietnamese": "phê bình, phê phán, chỉ trích"
    }
]

var dataE3 = [
{
    "english": "anticipate",
    "type": "v",
    "pronounce": "æn'tisipeit",
    "vietnamese": "thấy trước, chặn trước, lường trước"
  },{
    "english": "predict",
    "type": "v",
    "pronounce": "pri'dikt",
    "vietnamese": "báo trước, tiên đoán, dự báo"
  },{
    "english": "belief",
    "type": "n",
    "pronounce": "bi'li:f",
    "vietnamese": "lòng tin, đức tin, sự tin tưởng"
  },{
    "english": "consider",
    "type": "v",
    "pronounce": "kən ́sidə",
    "vietnamese": "cân nhắc, xem xét; để ý, quan tâm, lưu ý đến"
  },{
        "english": "frustrate",
        "type": "adj",
        "vietnamese": "chống lại, làm hỏng, sự thất bại, thất bại"
    },{
        "english": "resentment",
        "type": "adj",
        "vietnamese": "phẫn nộ"
    },{
    "english": "predict",
    "type": "v",
    "pronounce": "pri'dikt",
    "vietnamese": "báo trước, tiên đoán, dự báo"
  },{
    "english": "majority",
    "type": "n",
    "pronounce": "mə'dʒɔriti",
    "vietnamese": "phần lớn, đa số, ưu thế"
  },{
    "english": "partly",
    "type": "adv",
    "pronounce": "́pa:tli",
    "vietnamese": "đến chừng mực nào đó, phần nào đó"
  },{
        "english": "reframe",
        "type": "v",
        "vietnamese": "điều chỉnh lại"
    },{
    "english": "artificial",
    "type": "adj",
    "pronounce": ",ɑ:ti'fiʃəl",
    "vietnamese": "nhân tạo"
  },{
    "english": "distance",
    "type": "n",
    "pronounce": "distəns",
    "vietnamese": "khoảng cách, tầm xa"
  },{
    "english": "observe",
    "type": "v",
    "pronounce": "əbˈzə:v",
    "vietnamese": "quan sát, theo dõi"
  },{
        "english": "frustrate",
        "type": "adj",
        "vietnamese": "chống lại, làm hỏng, sự thất bại, thất bại"
    },{
    "english": "exercise",
    "type": "n, v",
    "pronounce": "eksəsaiz",
    "vietnamese": "bài tập, sự thi hành, sự thực hiện; làm, thi hành, thực hiện"
  },{
    "english": "reduce",
    "type": "v",
    "pronounce": "ri'dju:s",
    "vietnamese": "giảm, giảm bớt"
  },{
    "english": "approach",
    "type": "v, n",
    "pronounce": "ə'proutʃ",
    "vietnamese": "đến gần, lại gần; sự đến gần, sự lại gần"
  },{
    "english": "regarding",
    "type": "prep",
    "pronounce": "ri ́ga:diη",
    "vietnamese": "về, về việc, đối với (vấn đề...)"
  },{
    "english": "analyse, analyze",
    "type": "v",
    "pronounce": "ænəlaiz",
    "vietnamese": "phân tích"
  },{
    "english": "previous",
    "type": "adj",
    "pronounce": "ˈpriviəs",
    "vietnamese": "vội vàng, hấp tấp; trước (vd. ngày hôm trước), ưu tiên"
  },{
    "english": "behind",
    "type": "prep, adv",
    "pronounce": "bi'haind",
    "vietnamese": "sau, ở đằng sau"
  },{
    "english": "entire",
    "type": "adj",
    "pronounce": "in'taiə",
    "vietnamese": "toàn thể, toàn bộ"
  },
{
    "english": "anticipate",
    "type": "v",
    "pronounce": "æn'tisipeit",
    "vietnamese": "thấy trước, chặn trước, lường trước"
  },{
    "english": "predict",
    "type": "v",
    "pronounce": "pri'dikt",
    "vietnamese": "báo trước, tiên đoán, dự báo"
  },{
    "english": "belief",
    "type": "n",
    "pronounce": "bi'li:f",
    "vietnamese": "lòng tin, đức tin, sự tin tưởng"
  },{
    "english": "consider",
    "type": "v",
    "pronounce": "kən ́sidə",
    "vietnamese": "cân nhắc, xem xét; để ý, quan tâm, lưu ý đến"
  },{
        "english": "frustrate",
        "type": "adj",
        "vietnamese": "chống lại, làm hỏng, sự thất bại, thất bại"
    },{
        "english": "resentment",
        "type": "adj",
        "vietnamese": "phẫn nộ"
    },{
    "english": "predict",
    "type": "v",
    "pronounce": "pri'dikt",
    "vietnamese": "báo trước, tiên đoán, dự báo"
  },{
    "english": "majority",
    "type": "n",
    "pronounce": "mə'dʒɔriti",
    "vietnamese": "phần lớn, đa số, ưu thế"
  },{
    "english": "partly",
    "type": "adv",
    "pronounce": "́pa:tli",
    "vietnamese": "đến chừng mực nào đó, phần nào đó"
  },{
        "english": "reframe",
        "type": "v",
        "vietnamese": "điều chỉnh lại"
    },{
    "english": "artificial",
    "type": "adj",
    "pronounce": ",ɑ:ti'fiʃəl",
    "vietnamese": "nhân tạo"
  },{
    "english": "distance",
    "type": "n",
    "pronounce": "distəns",
    "vietnamese": "khoảng cách, tầm xa"
  },{
    "english": "observe",
    "type": "v",
    "pronounce": "əbˈzə:v",
    "vietnamese": "quan sát, theo dõi"
  },{
        "english": "frustrate",
        "type": "adj",
        "vietnamese": "chống lại, làm hỏng, sự thất bại, thất bại"
    },{
    "english": "exercise",
    "type": "n, v",
    "pronounce": "eksəsaiz",
    "vietnamese": "bài tập, sự thi hành, sự thực hiện; làm, thi hành, thực hiện"
  },{
    "english": "reduce",
    "type": "v",
    "pronounce": "ri'dju:s",
    "vietnamese": "giảm, giảm bớt"
  },{
    "english": "approach",
    "type": "v, n",
    "pronounce": "ə'proutʃ",
    "vietnamese": "đến gần, lại gần; sự đến gần, sự lại gần"
  },{
    "english": "regarding",
    "type": "prep",
    "pronounce": "ri ́ga:diη",
    "vietnamese": "về, về việc, đối với (vấn đề...)"
  },{
    "english": "analyse, analyze",
    "type": "v",
    "pronounce": "ænəlaiz",
    "vietnamese": "phân tích"
  },{
    "english": "previous",
    "type": "adj",
    "pronounce": "ˈpriviəs",
    "vietnamese": "vội vàng, hấp tấp; trước (vd. ngày hôm trước), ưu tiên"
  },{
    "english": "behind",
    "type": "prep, adv",
    "pronounce": "bi'haind",
    "vietnamese": "sau, ở đằng sau"
  },{
    "english": "entire",
    "type": "adj",
    "pronounce": "in'taiə",
    "vietnamese": "toàn thể, toàn bộ"
  }
]