



class QuestionPool {
    constructor(questions, reductionRate = 0.1) {
		this.check = false;
        this.questions = questions; // Danh sách các đối tượng câu hỏi
        this.n = questions.length; // Số lượng câu hỏi
        this.reductionRate = reductionRate; // Tỷ lệ giảm xác suất khi trả lời đúng
        this.probabilities = Array(this.n).fill(1 / this.n); // Mảng xác suất khởi tạo cho mỗi câu hỏi
    }

    getQuestion() {
        const sum = this.probabilities.reduce((acc, val) => acc + val, 0); // Tổng xác suất hiện tại
        const rand = Math.random() * sum; // Tạo số ngẫu nhiên từ 0 đến tổng xác suất
        let cumulative = 0; // Biến tổng tích lũy

        // Duyệt qua danh sách câu hỏi
        for (let i = 0; i < this.questions.length; i++) {
            cumulative += this.probabilities[i]; // Cộng dồn xác suất
            if (rand < cumulative) { // Nếu tổng tích lũy vượt qua số ngẫu nhiên
                return this.questions[i]; // Trả về câu hỏi hiện tại
            }
        }
    }

    answerQuestion(question, correct) {
        const index = this.questions.findIndex(q => q.english === question.english); // Tìm vị trí của câu hỏi trong mảng
        if (index === -1) return; // Nếu câu hỏi không tồn tại trong danh sách, thoát ra

        if (correct) {
            const originalProbability = this.probabilities[index]; // Lưu xác suất ban đầu của câu hỏi
            const decreaseAmount = originalProbability * this.reductionRate; // Tính toán lượng xác suất giảm
            this.probabilities[index] -= decreaseAmount; // Giảm xác suất của câu hỏi được trả lời đúng
            const redistributeAmount = decreaseAmount / (this.n - 1); // Phân phối lại lượng xác suất giảm cho các câu hỏi còn lại

            // Phân phối lại xác suất cho các câu hỏi khác
            for (let i = 0; i < this.probabilities.length; i++) {
                if (i !== index) { // Không phân phối lại cho câu hỏi vừa được trả lời đúng
                    this.probabilities[i] += redistributeAmount; // Tăng xác suất cho các câu hỏi khác
                }
            }
        }
    }
	showProbabilities() {
        this.questions.forEach((question, index) => {
			console.log("--")
            console.log(`Question: ${question.english}, Probability: ${this.probabilities[index].toFixed(4)}`);
        });
    }
}
const questionPool = new QuestionPool(dataE3);


function getRandomItem(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}

let randomWord = "";
randomQuestion();
function randomQuestion(){
	questionPool.check = false
	randomWord = questionPool.getQuestion();
	document.getElementById('answer').value = '';
	document.getElementById('title').innerText = randomWord.vietnamese +"-"+ randomWord.type;
	document.getElementById('description').innerText = '';
//	document.getElementById('description').innerText = randomWord.english;
	var audio = document.getElementById("myAudio");
	return randomWord;
}
function takeAnswer(){
	let answer = document.getElementById('answer').value;
	if(answer.toLowerCase() == randomWord.english.toLowerCase()){
		questionPool.answerQuestion(randomWord, questionPool.check == false ? true : false);
		questionPool.showProbabilities();
		randomQuestion();
	}else{
		document.getElementById('description').innerText = 'answer is wrong!';
		document.getElementById('description').style.color = 'red';
		document.getElementById('answer').select();
	}
}

const answerInput = document.getElementById('answer');

function handleKeyPress(event) {
      if (event.key === 'Enter') {
        event.preventDefault(); // Ngăn form gửi đi (nếu có thẻ <form> bao bọc)
		let answer = document.getElementById('answer').value;
	  if(answer.search('next') !== -1){
//	  if(true){
		randomQuestion();
		return;
	  }
	  if(answer.search('show me answer') !== -1){
		document.getElementById('description').innerText = randomWord.english;
		document.getElementById('answer').value = '';
		questionPool.check = true;
		return;
	  }
        takeAnswer();
		return true;
      }
	  
    }
	
//answerInput.addEventListener('keypress', handleKeyPress);

var audio = document.getElementById("myAudio");
var playButton = document.getElementById("playButton");

// Bắt sự kiện click từ người dùng
var audio = document.getElementById("myAudio");
var enterKey = 13;
document.addEventListener("keydown", function(event) {
            // Kiểm tra xem phím nhấn có phải là phím Enter không
	if (event.keyCode === enterKey) {
		handleKeyPress(event);
		let audioFileName = encodeURIComponent(randomWord.english.toLowerCase());
		audio.src = './audio/' + audioFileName + '.mp3';
		// Chạy audio
		//audio.play();
	}
});
